package infra.user

import org.koin.dsl.module
import domain.user.UserSettingsRepository
import infra.user.providers.FileSystemUserSettingsRepository
import infra.user.providers.InMemoryUserSettingsRepository
import java.nio.file.Path
import org.koin.core.module.Module
import domain.user.HandleUserProfile
import domain.user.UserProfileHandler

fun userModule(path: Path) = module {
    single<UserSettingsRepository> {
        FileSystemUserSettingsRepository(path)
    }

    domain()
    infra()
}

fun userModule() = module {
    single<UserSettingsRepository> {
        InMemoryUserSettingsRepository()
    }

    domain()
    infra()
}


private fun Module.domain() {
    single<HandleUserProfile> {
        UserProfileHandler(get())
    }
}


private fun Module.infra() {
}