package infra.user.providers

import domain.sharedkernel.model.ID
import domain.user.UserSettings
import domain.user.User
import domain.user.UserSettingsRepository

class InMemoryUserSettingsRepository: UserSettingsRepository {
    val items = mutableListOf<UserSettingsDB>()

    override fun save(userSettings: UserSettings) {
        val dbo = userSettings.toDB()
        items.removeIf { it.user == userSettings.user.value }
        items.add(dbo)
    }

    override fun find(user: ID<User>): UserSettings? {
        return items.firstOrNull { it.user == user.value }?.toDomain()
    }

    fun clear() = items.clear()
}