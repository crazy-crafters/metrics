package infra.user.providers

import infra.platform.FileSystemRepository
import domain.sharedkernel.model.ID
import domain.user.UserSettings
import domain.user.User
import domain.user.UserSettingsRepository
import java.nio.file.Path

class FileSystemUserSettingsRepository(path: Path): FileSystemRepository(path), UserSettingsRepository {
    override fun save(userSettings: UserSettings) {
        val dbo = userSettings.toDB()
        val all = (readAll<UserSettingsDB>().filter { it.user != userSettings.user.value } + dbo)
        writeAll(all)
    }

    override fun find(user: ID<User>): UserSettings? {
        return readAll<UserSettingsDB>().firstOrNull { it.user == user.value }?.toDomain()
    }
}

