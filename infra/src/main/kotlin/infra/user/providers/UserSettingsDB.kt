package infra.user.providers

import kotlinx.serialization.Serializable
import domain.sharedkernel.model.ID
import domain.user.UserSettings

@Serializable
data class UserSettingsDB(
    val user: String,
    val project: String
)

fun UserSettings.toDB() = UserSettingsDB(
    user = user.value,
    project = project.value
)

fun UserSettingsDB.toDomain() = UserSettings(
    user = ID.from(user),
    project = ID.from(project)
)