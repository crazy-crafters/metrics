package infra.user.controllers

data class UserSettingsDTO(val user: String, val project: String, val squad: String = "")