package infra.user.controllers


class UserNotFoundException(user: String): Exception("The user $user does not exist")

/*
class UserController(
    val handleSettings: HandleSettings
) {
    fun save(settings: UserSettingsDTO) {
        handleSettings.save(settings.toDomain())
    }

    fun get(user: String): UserSettingsDTO {
        return handleSettings.get(ID.from(user)).fold(
            onSuccess = { it.toDTO() },
            onFailure = { throw UserNotFoundException(it.id.value) }
        )
    }
}

fun UserSettingsDTO.toDomain() = UserSettings(
    user = ID.from(user),
    project = ID.from(project)
)

fun UserSettings.toDTO() = UserSettingsDTO(
    user = user.value,
    project = project.value
)*/