package infra.project.providers


import domain.project.model.Project
import domain.project.model.ProjectAssigned
import domain.project.model.ProjectEnded
import domain.project.model.ProjectEvent
import domain.project.model.ProjectStarted
import domain.project.spi.ProjectEventRepository
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import infra.platform.FileSystemRepository
import infra.project.providers.dbo.ProjectAssignedDB
import infra.project.providers.dbo.ProjectEndedDB
import infra.project.providers.dbo.ProjectEventDB
import infra.project.providers.dbo.ProjectStartedDB
import java.nio.file.Path
import domain.sharedkernel.model.DateTime


class InMemoryProjectEventRepository: ProjectEventRepository {
    val items = mutableListOf<ProjectEventDB>()

    override fun all(): List<ProjectEvent> {
        return items.map { it.toDomain() }
    }

    override fun save(project: ProjectEvent) {
        items.add(project.toDB())
    }

    override fun find(from: ID<Project>): List<ProjectEvent> {
        return all().filter { it.id == from }
    }

    fun clear() {
        items.clear()
    }
}

class FileSystemProjectEventRepository(path: Path): ProjectEventRepository, FileSystemRepository(path) {
    override fun all(): List<ProjectEvent> {
        return readAll<ProjectEventDB>().map { it.toDomain() }
    }

    override fun save(project: ProjectEvent) {
        writeAll<ProjectEventDB>(readAll<ProjectEventDB>() + project.toDB())
    }

    override fun find(from: ID<Project>): List<ProjectEvent> {
        return all().filter { it.id == from }
    }
}



fun ProjectEvent.toDB(): ProjectEventDB =
    when(this) {
        is ProjectStarted -> ProjectStartedDB(
            entityId = id.value,
            name = name.value,
            description = description,
            on = date.toString(),

        )

        is ProjectAssigned -> ProjectAssignedDB(
            entityId = id.value,
            squad = squad.value,
            on = date.toString(),
        )

        is ProjectEnded -> ProjectEndedDB(
            entityId = id.value,
            on = date.toString()
            )
    }


fun ProjectEventDB.toDomain(): ProjectEvent =
    when(this) {
        is ProjectStartedDB -> ProjectStarted(
            id = ID.from(entityId),
            name = Name.from(name),
            description = description,
            date = DateTime.parse(on),
        )

        is ProjectAssignedDB -> ProjectAssigned(
            id = ID.from(entityId),
            squad = ID.from(squad),
            date = DateTime.parse(on),
        )

        is ProjectEndedDB -> ProjectEnded(
            id = ID.from(entityId),
            date = DateTime.parse(on),
        )
    }