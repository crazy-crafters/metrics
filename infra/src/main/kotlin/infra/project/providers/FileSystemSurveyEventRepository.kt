package infra.project.providers

import domain.project.model.Project
import domain.survey.model.CloseSurvey
import domain.survey.model.OpenSurvey
import domain.survey.model.SurveyEvent
import domain.survey.model.Vote
import infra.platform.FileSystemRepository
import domain.survey.spi.SurveyEventRepository
import infra.project.providers.dbo.CloseSurveyDB
import infra.project.providers.dbo.OpenSurveyDB
import infra.project.providers.dbo.SurveyEventDB
import infra.project.providers.dbo.VoteSurveyDB
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import java.nio.file.Path

class FileSystemSurveyEventRepository(path: Path): FileSystemRepository(path), SurveyEventRepository {
    override fun findAll(project: ID<Project>): List<SurveyEvent> {
        return readAll<SurveyEventDB>().filter{ it.project == project.value }.map { it.toDomain() }
    }

    override fun save(event: SurveyEvent) {
        val dbo = event.toDB()
        val all = (readAll<SurveyEventDB>() + dbo)
        writeAll(all)
    }
}

fun SurveyEvent.toDB() = when(this) {
    is OpenSurvey -> OpenSurveyDB(
        date = date.toString(),
        project = id.value,
    )
    is CloseSurvey -> CloseSurveyDB(
        date = date.toString(),
        project = id.value,
    )
    is Vote -> VoteSurveyDB(
        date = date.toString(),
        project = id.value,
        score = score
    )
}

fun SurveyEventDB.toDomain() = when(this) {
    is OpenSurveyDB -> OpenSurvey(
        date = DateTime.parse(date),
        id = ID.from(project),
    )
    is CloseSurveyDB -> CloseSurvey(
        date = DateTime.parse(date),
        id = ID.from(project),
    )
    is VoteSurveyDB -> Vote(
        date = DateTime.parse(date),
        id = ID.from(project),
        score = score
    )
}
