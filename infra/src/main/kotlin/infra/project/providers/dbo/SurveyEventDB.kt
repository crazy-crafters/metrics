package infra.project.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed interface SurveyEventDB {
    val date: String
    val project: String
}

@Serializable
@SerialName("OpenSurvey")
data class OpenSurveyDB(
    override val date: String,
    override val project: String
) : SurveyEventDB

@Serializable
@SerialName("CloseSurvey")
data class CloseSurveyDB(
    override val date: String,
    override val project: String
) : SurveyEventDB

@Serializable
@SerialName("VoteSurvey")
data class VoteSurveyDB(
    val score: Int,
    override val date: String,
    override val project: String
) : SurveyEventDB