package infra.project.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed interface ProjectEventDB {
    val entityId: String
    val on: String
}

@Serializable
@SerialName("ProjectStarted")
data class ProjectStartedDB(
    override val entityId: String,
    override val on: String,
    val name: String,
    val description: String,
) : ProjectEventDB

@Serializable
@SerialName("ProjectAssigned")
data class ProjectAssignedDB(override val entityId: String,
                             val squad: String,
                             override val on: String) :
    ProjectEventDB

@Serializable
@SerialName("ProjectEnded")
data class ProjectEndedDB(
    override val entityId: String,
    override val on: String,
) : ProjectEventDB