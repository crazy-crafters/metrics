package infra.project.providers

import domain.project.model.Project
import domain.survey.model.SurveyEvent
import domain.survey.spi.SurveyEventRepository
import infra.project.providers.dbo.SurveyEventDB
import domain.sharedkernel.model.ID

class InMemorySurveyEventRepository: SurveyEventRepository {
    val events = mutableListOf<SurveyEventDB>()

    override fun findAll(project: ID<Project>): List<SurveyEvent> {
        return events.filter { it.project == project.value }.map { it.toDomain() }
    }

    override fun save(event: SurveyEvent) {
        events.add(event.toDB())
    }
}