package infra.project.providers

import domain.project.model.Project
import domain.sharedkernel.model.ID

interface User

data class UserProjectSettings(val project: ID<Project>)

interface UserSettingsRepository {
    fun find(user: ID<User>): UserProjectSettings?
}