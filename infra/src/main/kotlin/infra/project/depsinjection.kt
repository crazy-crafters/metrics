package infra.project

import org.koin.core.module.Module
import org.koin.dsl.module
import domain.project.api.GetProject
import domain.project.api.HandleProject
import domain.project.spi.ClockService
import domain.project.spi.ProjectEventRepository
import domain.survey.SurveyMeasurement
import domain.survey.api.GetSurvey
import domain.survey.api.HandleSurvey
import domain.survey.spi.SurveyEventRepository
import infra.project.providers.*
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import java.nio.file.Path
import domain.project.GetProject as GetProjectService
import domain.project.HandleProject as HandleProjectService


class UserSettingsRepositoryFake(val projects: ProjectEventRepository): UserSettingsRepository {
    override fun find(user: ID<User>): UserProjectSettings {
        return UserProjectSettings(projects.all().first().id)
    }

}

fun projectModule(path: Path) = module {
    single<ProjectEventRepository> {
        FileSystemProjectEventRepository(path)
    }

    single<SurveyEventRepository> {
        FileSystemSurveyEventRepository(path)
    }

    domain()
    infra()
}

fun projectModule() = module {
    single<ProjectEventRepository> {
        InMemoryProjectEventRepository()
    }

    single<SurveyEventRepository> {
        InMemorySurveyEventRepository()
    }

    single<UserSettingsRepository> {
        UserSettingsRepositoryFake(get())
    }

    domain()
    infra()
}


private fun Module.domain() {

    single<GetProject> {
        GetProjectService(get())
    }

    single<HandleProject> {
        HandleProjectService(get())
    }

    single<SurveyMeasurement> {
        SurveyMeasurement(get(), get())
    }

    single<GetSurvey> {
        get<SurveyMeasurement>()
    }

    single<HandleSurvey> {
        get<SurveyMeasurement>()
    }
}


private fun Module.infra() {

    /*single<ProjectController> {
        ProjectController(get(), get(), get(), get())
    }

    single<SurveyController> {
        SurveyController(get(), get(), get())
    }*/

    single<ClockService> {
        object : ClockService {
            override fun now() = DateTime.now()
        }
    }
}