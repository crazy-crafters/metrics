package infra.teamwork

import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import org.koin.core.module.Module
import org.koin.dsl.module
import domain.teamwork.configuration.ConfigurationHandler
import domain.teamwork.configuration.Updater
import domain.teamwork.configuration.api.AnalyzeRemote
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.spi.ConfigurationRepository
import domain.teamwork.issue.IssueHandler
import domain.teamwork.issue.api.GetIssue
import domain.teamwork.shared.spi.EventRepository
import domain.teamwork.status.StatusHandler
import domain.teamwork.status.api.ManageStatus
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.connectors.azure.HarvestDashboardFromAzure
import infra.teamwork.connectors.github.GithubRepository
import infra.teamwork.connectors.github.HarvestDashboardFromGithub
import infra.teamwork.connectors.jira.HarvestDashboardFromJira
import infra.teamwork.connectors.jira.JiraRepository
import infra.teamwork.connectors.trello.HarvestDashboardFromTrello
import infra.teamwork.connectors.trello.TrelloRepository
import infra.teamwork.providers.*
import java.nio.file.Path
import java.security.cert.X509Certificate
import javax.net.ssl.X509TrustManager


interface EventSink {
    fun publish(event: String)
}

fun teamworkModule(path: Path) = module {
    single<ConfigurationRepository> {
        FileSystemConfigurationRepository(path)
    }

    single<FileSystemTimelineEventRepository> {
        FileSystemTimelineEventRepository(path, get())
    }
    single<EventRepository> {
        get<FileSystemTimelineEventRepository>()
    }

    single<EventDBRepository> {
        get<FileSystemTimelineEventRepository>()
    }

    single<UpdateStatusRepository> {
        InMemoryUpdateStatusRepository()
    }

    single<StatusRepository> {
        FileSystemStatusRepository(path)
    }

    teamworkDeps()
}

fun teamworkModule() = module {
    single<ConfigurationRepository> {
        InMemoryConfigurationRepository()
    }

    single<InMemoryTimelineEventRepository> {
        InMemoryTimelineEventRepository(get())
    }

    single<EventRepository> {
        get<InMemoryTimelineEventRepository>()
    }

    single<EventDBRepository> {
        get<InMemoryTimelineEventRepository>()
    }

    single<UpdateStatusRepository> {
        InMemoryUpdateStatusRepository()
    }

    single<StatusRepository> {
        InMemoryStatusRepository()
    }

    teamworkDeps()
}

private fun Module.teamworkDeps() {

    single<JiraRepository> {
        JiraRepository(get(), get())
    }

    single<TrelloRepository> {
        TrelloRepository(get(), get())
    }

    single<GithubRepository> {
        GithubRepository(get(), get())
    }

    single<AnalyzeRemote> {
        Updater()
            .register(BoardType.Jira, HarvestDashboardFromJira(get(), get()))
            .register(BoardType.Azuredevops, HarvestDashboardFromAzure(get(), get(), get()))
            .register(BoardType.Trello, HarvestDashboardFromTrello(get(), get()))
            .register(BoardType.Github, HarvestDashboardFromGithub(get(), get()))
    }

    single<domain.teamwork.report.api.GetReport> {
        domain.teamwork.report.GetReport(get())
    }

    single<HandleConfiguration> {
        ConfigurationHandler(get())
    }

    single<GetIssue> {
        IssueHandler(get())
    }

    single<ScanBoard> {
        BoardWorker(get(), get(), get())
    }

    single<ManageStatus> {
        StatusHandler(get())
    }

    single<HttpClientEngine> {
        CIO.create {
            https {
                trustManager = object : X509TrustManager {
                    override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
                    override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
                    override fun getAcceptedIssuers(): Array<X509Certificate>? = null
                }
            }
        }
    }

    single<EventSink> {
        object : EventSink {
            override fun publish(event: String) {
                println(event)
            }

        }
    }
}



