package infra.teamwork.providers

import domain.sharedkernel.model.ID
import domain.project.model.Project
import infra.teamwork.UpdateStatus

class InMemoryUpdateStatusRepository: UpdateStatusRepository {
    val items = mutableListOf<UpdateStatus>()
    override fun get(project: ID<Project>): UpdateStatus? {
        return items.firstOrNull { it.project == project }
    }

    override fun save(status: UpdateStatus) {
        items.removeIf { it.project == status.project }
        items.add(status)
    }

    override fun clear() {
        items.clear()
    }
}