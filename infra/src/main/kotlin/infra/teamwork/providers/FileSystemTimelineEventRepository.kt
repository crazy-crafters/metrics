package infra.teamwork.providers


import infra.platform.FileSystemRepository
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.sharedkernel.model.Title
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.project.model.Project
import domain.teamwork.issue.model.Tag
import domain.teamwork.issue.model.Tags
import domain.teamwork.status.model.Status
import domain.teamwork.shared.events.*
import domain.teamwork.shared.spi.EventRepository
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.providers.dbo.*
import java.nio.file.Path

class FileSystemTimelineEventRepository(path: Path, private val statusRepository: StatusRepository): FileSystemRepository(path),
    EventRepository, EventDBRepository {

        override fun all(of: ID<Project>): List<Event> {
        return readAll<TimelineEventDB>()
            .filter { it.project == of.value }
            .map { it.toDomain(statusRepository.findAll(of)) }
    }

    override fun findAll(of: ID<Project>, issue: ID<Issue>): List<Event> {
        return all(of).filter { it.entityId == issue }
    }

    override fun clear(project: ID<Project>) {
        val allWithoutFromProject = readAll<TimelineEventDB>().filter { it.project != project.value }
        writeAll<TimelineEventDB>(allWithoutFromProject)
    }

    override fun save(event: TimelineEventDB) {
        val all = (readAll<TimelineEventDB>() + event)
        writeAll(all)
    }

    override fun saveAll(events: List<TimelineEventDB>) {
        val all = (readAll<TimelineEventDB>() + events)
        writeAll(all)
    }
}


fun Event.toDB(): TimelineEventDB =
    when(this) {
        is IssueAdded -> IssueAddedDB(
            entityId = this.entityId.toString(),
            project = this.project.toString(),
            title = this.title.toString(),
            complexity = this.estimation.value,
            status = this.status.id.value,
            date = this.date.toString()
        )
        is IssueMoved -> IssueMovedDB(
            entityId = this.entityId.value,
            project = this.project.value,
            newStatus = this.status.id.value,
            date = this.date.toString()
        )
        is IssueClosed -> IssueRemovedDB(
            entityId = this.entityId.value,
            project = this.project.value,
            date = this.date.toString(),
        )
        is IssueRenamed -> IssueRenamedDB(
            entityId = this.entityId.toString(),
            project = this.project.toString(),
            title = this.title.toString(),
            date = this.date.toString()
        )
        is IssueEstimated -> IssueEstimatedDB(
            entityId = entityId.toString(),
            project = project.toString(),
            estimation = estimation.value,
            date = date.toString())
        is IssueTagAdded -> IssueTagAddedDB(
            entityId = entityId.toString(),
            project = project.toString(),
            value = this.tag.value,
            date = date.toString())
        is IssueTagRemoved -> IssueTagRemovedDB(
            entityId = entityId.toString(),
            project = project.toString(),
            value = this.tag.value,
            date = date.toString())


        is BoardCreated -> BoardCreatedDB(
            entityId = this.entityId.toString(),
            project = this.project.toString(),
            title = this.title.toString(),
            date = this.date.toString()
        )

        is BoardRenamed -> BoardRenamedDB(
            entityId = this.entityId.toString(),
            project = this.project.toString(),
            title = this.title.toString(),
            date = this.date.toString()
        )

        is ColumnAdded -> ColumnAddedDB(
            date = this.date.toString(),
            entityId = this.entityId.value,
            project = this.project.value,
            title = this.title.value
        )
        is ColumnRemoved -> ColumnRemovedDB(
            date = this.date.toString(),
            entityId = this.entityId.value,
            project = this.project.value
        )
        is ColumnRenamed -> ColumnRenamedDB(
            date = this.date.toString(),
            entityId = this.entityId.value,
            project = this.project.value,
            title = this.project.value,
        )

        is IterationFinished -> IterationFinishedDB(
            entityId = entityId.toString(),
            project = project.toString(),
            date = date.toString())
        is IterationStarted -> IterationStartedDB(
            entityId = entityId.toString(),
            project = project.toString(),
            name = name.toString(),
            date = date.toString())
    }

fun TimelineEventDB.toDomain(statuses: List<Status>): Event =
    when(this) {
        is IssueAddedDB -> IssueAdded(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            title = Title.from(this.title),
            estimation = PointsOfComplexity.of(this.complexity),
            status = statuses.firstOrNull { it.id == ID.from<Status>(this.status) } ?: Status.OutOfScope,
            date = DateTime.parse(this.date),
            tags = Tags.empty()
        )
        is IssueMovedDB -> IssueMoved(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            status = statuses.firstOrNull { it.id.value.equals(this.newStatus.replace(" ", "").replace("_", ""), ignoreCase = true) } ?: Status.OutOfScope,
            date = DateTime.parse(this.date)
        )
        is IssueRemovedDB -> IssueClosed(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            date = DateTime.parse(this.date)
        )
        is IssueRenamedDB -> IssueRenamed(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            title = Title.from(this.title),
            date = DateTime.parse(this.date)
        )
        is IssueEstimatedDB -> IssueEstimated(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            estimation = PointsOfComplexity.of(this.estimation),
            date = DateTime.parse(this.date)
        )
        is IssueTagAddedDB -> IssueTagAdded(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            tag = Tag.on(this.value),
            date = DateTime.parse(this.date)
        )
        is IssueTagRemovedDB -> IssueTagRemoved(
            entityId = ID.from(this.entityId),
            project = ID.from(this.project),
            tag = Tag.on(this.value),
            date = DateTime.parse(this.date)
        )

        is BoardCreatedDB -> BoardCreated(
            date = DateTime.parse(date),
            entityId = ID.from(entityId),
            project = ID.from(project),
            title = Title.from(title),
        )
        is BoardRenamedDB -> BoardRenamed(
            date = DateTime.parse(date),
            entityId = ID.from(entityId),
            project = ID.from(project),
            title = Title.from(title),
        )
        is ColumnAddedDB -> ColumnAdded(
            date = DateTime.parse(date),
            entityId = ID.from(entityId),
            project = ID.from(project),
            title = Title.from(title),
        )
        is ColumnRemovedDB -> ColumnRemoved(
            date = DateTime.parse(date),
            entityId = ID.from(entityId),
            project = ID.from(project),
        )
        is ColumnRenamedDB -> ColumnRenamed(
            date = DateTime.parse(date),
            entityId = ID.from(entityId),
            project = ID.from(project),
            title = Title.from(title),
        )

        is IterationFinishedDB -> IterationFinished(
            entityId = ID.from(entityId),
            project = ID.from(project),
            date = DateTime.parse(date),
        )

        is IterationStartedDB -> IterationStarted(
            entityId = ID.from(entityId),
            project = ID.from(project),
            name = Name.from(name),
            date = DateTime.parse(date),
        )
        else -> TODO()
    }