package infra.teamwork.providers

import domain.sharedkernel.model.ID
import infra.platform.FileSystemRepository
import domain.project.model.Project
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.providers.dbo.StatusDB
import infra.teamwork.providers.dbo.toDB
import infra.teamwork.providers.dbo.toDomain
import java.nio.file.Path

class FileSystemStatusRepository(path: Path): StatusRepository, FileSystemRepository(path) {
    override fun save(project: ID<Project>, status: Status) {
        val all = readAll<StatusDB>().filter { !(it.project == project.value && it.id == status.id.value) } + status.toDB(project)
        writeAll(all)
    }

    override fun find(project: ID<Project>, id: ID<Status>): Status? {
        return readAll<StatusDB>().firstOrNull { it.project == project.value && it.id == id.value }?.toDomain()
    }

    override fun findAll(of: ID<Project>): List<Status> {
        return  readAll<StatusDB>().filter { it.project == of.value }.map { it.toDomain() }
    }
}

