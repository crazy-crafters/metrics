package infra.teamwork.providers

import domain.sharedkernel.model.ID
import domain.project.model.Project
import infra.teamwork.UpdateStatus


interface UpdateStatusRepository {
    fun get(project: ID<Project>): UpdateStatus?
    fun save(status: UpdateStatus)
    fun clear()
}


