package infra.teamwork.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import domain.sharedkernel.model.ID
import domain.project.model.Project
import infra.teamwork.UpdateStatus

@Serializable
sealed interface UpdateStatusDB {
    val project: String
}

@Serializable
@SerialName("OKUpdateStatus")
data class OKUpdateStatusDB(override val project: String): UpdateStatusDB

@Serializable
@SerialName("PendingUpdateStatus")
data class PendingUpdateStatusDB(override val project: String): UpdateStatusDB

@Serializable
@SerialName("FailedUpdateStatus")
data class FailedUpdateStatusDB(override val project: String, val reason: String): UpdateStatusDB

fun UpdateStatusDB.toDomain(): UpdateStatus {
    val id = ID.from<Project>(project)
    return when(this) {
        is FailedUpdateStatusDB -> UpdateStatus.Failed(id, reason)
        is OKUpdateStatusDB -> UpdateStatus.OK(id)
        is PendingUpdateStatusDB -> UpdateStatus.Pending(id)
    }
}