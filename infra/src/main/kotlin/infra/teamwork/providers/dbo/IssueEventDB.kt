package infra.teamwork.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("IssueAdded")
data class IssueAddedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val title: String,
    val complexity: Int,
    val status: String,
) : TimelineEventDB

@Serializable
@SerialName("IssueMoved")
data class IssueMovedDB(
    override val entityId: String,
    override val date: String,
    override val project: String,
    val newStatus: String
) : TimelineEventDB

@Serializable
@SerialName("IssueRemoved")
data class IssueRemovedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
) : TimelineEventDB

@Serializable
@SerialName("IssueRenamed")
data class IssueRenamedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val title: String,
) : TimelineEventDB

@Serializable
@SerialName("IssueEstimated")
data class IssueEstimatedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val estimation: Int
) : TimelineEventDB

@Serializable
@SerialName("IssueTagAdded")
data class IssueTagAddedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val value: String
) : TimelineEventDB

@Serializable
@SerialName("IssueTagRemoved")
data class IssueTagRemovedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val value: String
) : TimelineEventDB