package infra.teamwork.providers.dbo
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("ColumnRemoved")
data class ColumnRemovedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
) : TimelineEventDB