package infra.teamwork.providers.dbo

import kotlinx.serialization.Serializable

@Serializable
sealed interface TimelineEventDB {
    val date: String
    val project: String
    val entityId: String
}