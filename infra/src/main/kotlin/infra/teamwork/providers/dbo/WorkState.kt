package infra.teamwork.providers.dbo

enum class WorkState {
    Undefined,
    Planned,
    Waiting,
    Working,
    Done,
    Closed,
}