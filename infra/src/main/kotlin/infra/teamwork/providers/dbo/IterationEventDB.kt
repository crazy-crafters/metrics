package infra.teamwork.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("IterationStarted")
data class IterationStartedDB(
    override val entityId: String,
    override val project: String,
    val name: String,
    override val date: String
) : TimelineEventDB

@Serializable
@SerialName("IterationFinished")
data class IterationFinishedDB(
    override val entityId: String,
    override val project: String,
    override val date: String
) : TimelineEventDB