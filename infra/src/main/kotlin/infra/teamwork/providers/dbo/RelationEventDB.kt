package infra.teamwork.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("RelationAdded")
data class RelationAddedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val parent: String
) : TimelineEventDB

@Serializable
@SerialName("RelationRemoved")
data class RelationRemovedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val parent: String
) : TimelineEventDB