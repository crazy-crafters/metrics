package infra.teamwork.providers.dbo

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.project.model.Project
import domain.teamwork.status.model.Status
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("Status")
data class StatusDB(val id: String, val value: String, val state: WorkState, val project: String)

fun StatusDB.toDomain() =
    when(state) {
        WorkState.Undefined -> Status.Undefined(ID.from(this.id), Label.from(this.value))
        WorkState.Planned -> Status.Planned(ID.from(this.id), Label.from(this.value))
        WorkState.Waiting -> Status.Waiting(ID.from(this.id), Label.from(this.value))
        WorkState.Working -> Status.Working(ID.from(this.id), Label.from(this.value))
        WorkState.Done -> Status.Done(ID.from(this.id), Label.from(this.value))
        WorkState.Closed -> Status.Closed(ID.from(this.id), Label.from(this.value))
    }
    //Status(id = ID.from(this.id), value = Label.from(value), state = state)

//fun Status.toDB(project: ID<Project>) = StatusDB(id = id.value, value = value.value, project = project.value, state = state)

fun Status.toDB(project: ID<Project>) = StatusDB(id = id.value, value = label.value, project = project.value, state = when(this){
    Status.OutOfScope -> TODO()
    is Status.Undefined -> WorkState.Undefined
    is Status.Planned -> WorkState.Planned
    is Status.Waiting -> WorkState.Waiting
    is Status.Working -> WorkState.Working
    is Status.Done -> WorkState.Done
    is Status.Closed -> WorkState.Closed
})


