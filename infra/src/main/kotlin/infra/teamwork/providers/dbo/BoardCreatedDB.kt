package infra.teamwork.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("BoardCreated")
data class BoardCreatedDB(
    override val date: String,
    override val project: String,
    override val entityId: String,
    val title: String,
) : TimelineEventDB

