package infra.teamwork.providers.dbo

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
@SerialName("ColumnAdded")
data class ColumnAddedDB(
    override val date: String,
    override val entityId: String,
    override val project: String,
    val title: String,
) : TimelineEventDB