package infra.teamwork.providers

import domain.sharedkernel.model.ID
import domain.project.model.Project
import infra.teamwork.providers.dbo.TimelineEventDB

interface EventDBRepository {
    fun clear(project: ID<Project>)
    fun save(event: TimelineEventDB)
    fun saveAll(events: List<TimelineEventDB>)
}