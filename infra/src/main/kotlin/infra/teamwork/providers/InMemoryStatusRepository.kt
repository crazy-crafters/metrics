package infra.teamwork.providers

import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository

class InMemoryStatusRepository(initialValues: Map<ID<Project>, List<Status>> = emptyMap()): StatusRepository {
    val items: MutableMap<ID<Project>, List<Status>> = initialValues.toMutableMap()

    override fun save(project: ID<Project>, status: Status) {
        items[project] = items.getOrDefault(project, emptyList()).filter { it.id != status.id } + status
    }

    override fun find(project: ID<Project>, id: ID<Status>): Status? {
        return items.getOrDefault(project, emptyList()).firstOrNull { it.id == id }
    }

    override fun findAll(of: ID<Project>): List<Status> {
        return items.getOrDefault(of, emptyList())
    }
}


