package infra.teamwork.providers

import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import infra.teamwork.ScanBoard
import infra.teamwork.UpdateStatus

class UpdateBoard(private val scanner: ScanBoard, private val repository: UpdateStatusRepository, private val today : () -> DateTime) {
    fun get(project: String): UpdateStatus {
        return repository.get(ID.from(project)) ?: UpdateStatus.Nothing(ID.from(project))
    }

    @OptIn(DelicateCoroutinesApi::class)
    suspend fun run(project: String) {
        if (get(project) is UpdateStatus.Pending)
            return
        GlobalScope.launch {
            scanner.start(ID.from(project))
        }
        /*GlobalScope.launch {
            repository.save(UpdateStatus.Pending(ID.from(project)))
            val result = scanner.start(ID.from(project))
            repository.save(result)
        }*/
    }
}