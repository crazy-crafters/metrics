package infra.teamwork.providers

import domain.teamwork.configuration.model.BoardConfiguration
import domain.project.model.Project
import domain.teamwork.configuration.spi.ConfigurationRepository
import domain.sharedkernel.model.ID

class InMemoryConfigurationRepository: ConfigurationRepository {
    val data = mutableListOf<BoardConfiguration.Valid>()

    override fun save(project: ID<Project>, configuration: BoardConfiguration) {
        if (configuration !is BoardConfiguration.Valid)
            return
        data.removeIf { it.project == project }
        data.add(configuration)
    }

    override fun find(project: ID<Project>): BoardConfiguration? {
        return data.firstOrNull { it.project == project }
    }

    override fun all(): List<BoardConfiguration> {
        return data
    }

    fun clear() = data.clear()
}

