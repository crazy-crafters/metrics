package infra.teamwork.providers

import domain.project.model.Project
import domain.teamwork.shared.events.Event
import domain.teamwork.shared.spi.EventRepository
import domain.sharedkernel.model.ID
import domain.teamwork.issue.model.Issue
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.providers.dbo.TimelineEventDB

class InMemoryTimelineEventRepository(private val statuses: StatusRepository): EventRepository, EventDBRepository {
    val events = mutableListOf<TimelineEventDB>()
    override fun all(of: ID<Project>): List<Event> {
        val status = statuses.findAll(of)
        return events.filter { it.project == of.value }.map { it.toDomain(status) }
    }

    override fun findAll(of: ID<Project>, issue: ID<Issue>): List<Event> {
        return all(of).filter { it.entityId == issue }
    }

    override fun clear(project: ID<Project>) {
        events.removeIf { it.project == project.value }
    }

    override fun save(event: TimelineEventDB) {
        events.add(event)
    }

    override fun saveAll(events: List<TimelineEventDB>) {
        this.events.addAll(events)
    }
}