package infra.teamwork.providers

import kotlinx.serialization.Serializable
import infra.platform.FileSystemRepository
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login
import domain.project.model.Project
import domain.teamwork.configuration.spi.ConfigurationRepository
import domain.sharedkernel.model.ID
import java.nio.file.Path

@Serializable
data class BoardConfigurationDB(val project: String, val type: BoardType, val fields: Map<String, String>)


class FileSystemConfigurationRepository(path: Path): ConfigurationRepository, FileSystemRepository(path) {
    override fun save(project: ID<Project>, configuration: BoardConfiguration) {
        /*val dbo = configuration.toDB()
        val all = (readAll<BoardConfigurationDB>() + dbo).distinctBy { it.id }.sortedBy { it.id }
        writeAll(all)*/
        val dbo = when(configuration) {
            is BoardConfiguration.None -> TODO()
            is BoardConfiguration.Valid ->
                BoardConfigurationDB(
                    project.toString(),
                    configuration.type,
                    mapOf(
                        "username" to configuration.login.user,
                        "token" to configuration.login.pass,
                        "root" to configuration.url,
                        "board" to configuration.board,
                    ))
        }

        val all = (readAll<BoardConfigurationDB>() + dbo).distinctBy { it.project }.sortedBy { it.project }
        writeAll(all)

    }

    override fun find(project: ID<Project>): BoardConfiguration? {
        val conf = readAll<BoardConfigurationDB>().firstOrNull { it.project == project.toString() } ?: return null
        return BoardConfiguration.Valid(type=conf.type, project = ID.from(conf.project), url = conf.fields["root"] ?: "", board = conf.fields["board"] ?: "", login = Login(conf.fields["username"] ?: "", conf.fields["token"] ?: ""))
    }


    override fun all(): List<BoardConfiguration> {
        return readAll<BoardConfigurationDB>().map {
            conf ->  BoardConfiguration.Valid(type=conf.type, project = ID.from(conf.project), url = conf.fields["root"] ?: "", board = conf.fields["board"] ?: "", login = Login(conf.fields["username"] ?: "", conf.fields["token"] ?: ""))
        }

    }
}