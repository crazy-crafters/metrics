package infra.teamwork

import domain.sharedkernel.model.ID
import domain.project.model.Project

sealed interface UpdateStatus {
    val project: ID<Project>

    data class OK(override val project: ID<Project>): UpdateStatus
    data class Pending(override val project: ID<Project>): UpdateStatus
    data class Nothing(override val project: ID<Project>): UpdateStatus
    data class Failed(override val project: ID<Project>, val reason: String): UpdateStatus
}