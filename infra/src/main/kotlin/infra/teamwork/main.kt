package infra.teamwork

import domain.teamwork.issue.model.Route


suspend fun main() {
    /*val path = Path.of(System.getProperty("user.home")).resolve(".blackbox")
    val configurations = FileSystemConfigurationRepository(path)
    val statusRepository = InMemoryStatusRepository()
    val eventRepository = InMemoryTimelineEventRepository(statusRepository)
    val fsEventRepository = FileSystemTimelineEventRepository(path, statusRepository)

    val configuration = configurations.find(ID.from("03b8b76d-0069-402a-9a75-847341ee8983"))

    if (configuration !is BoardConfiguration.Valid) {
        println("Wrong configuration")
        return
    }

    val engine = CIO.create {
        https {
            trustManager = object : X509TrustManager {
                override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
                override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {}
                override fun getAcceptedIssuers(): Array<X509Certificate>? = null
            }
        }
    }

    val terminal = Terminal()
    val progress = progressBarLayout {
        marquee(terminal.theme.warning("Issue Events"), width = 15)
        percentage()
        progressBar()
        completed(style = terminal.theme.success)
    }.animateInCoroutine(terminal)


    val job1 = GlobalScope.async {
        println("Job 1 started")
        progress.execute()
    }

    val job2 = GlobalScope.async {
        println("Job 2 started")
        val azureRestApi = AzureRestApi(engine, configuration, statusRepository, eventRepository,
            progression = { progression ->
                progress.update { total = progression.total.toLong() }
                progress.advance(progression.current.toLong())
            },
         eventSink = {})

        azureRestApi.fetchStatuses()
        azureRestApi.fetchWorkitems()
    }

    awaitAll(job1, job2)*/

/*    val status = statusRepository.findAll(configuration.project)
    Issue.allFrom(fsEventRepository.readAll<TimelineEventDB>().filter { it.project == configuration.project.value }.map { it.toDomain(status) }).forEach { issue ->
        println("ID: ${issue.id.toString()}")
        println("Title: ${issue.title.toString()}")
        println("Lead Time: ${issue.leadTime.toString()}")
        println("Cycle Time: ${issue.cycleTime.toString()}")
        println("Timeline: ${issue.route().toRepr()}")
        println("Tags: ${issue.tags.tags.joinToString(", ") { it.value }}")
        println("")
    }*/
}

fun Route.toRepr() = this.steps.joinToString {
    "${it.date.toDateString()} - ${it.status.label}"
}