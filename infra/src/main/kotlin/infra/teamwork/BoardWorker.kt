package infra.teamwork

import domain.sharedkernel.model.ID
import domain.teamwork.configuration.api.AnalyzeRemote
import domain.project.model.Project
import domain.teamwork.configuration.spi.ConfigurationRepository
import domain.teamwork.status.model.Progression
import infra.teamwork.providers.UpdateStatusRepository


interface ScanBoard {
    suspend fun start(id: ID<Project>, progression: (Progression) -> Unit = {}, eventSink: (UpdateStatus) -> Unit = {})
}

class BoardWorker(private val configurations: ConfigurationRepository,
                  val analyzeRemoteBoard: AnalyzeRemote,
                  private val repository: UpdateStatusRepository
): ScanBoard {

    override suspend fun start(id: ID<Project>,
                               progression: (Progression) -> Unit,
                               eventSink: (UpdateStatus) -> Unit) {
        val result = configurations.find(id)?.let { project ->
                repository.save(UpdateStatus.Pending(id))
                analyzeRemoteBoard(project, progression)
                UpdateStatus.OK(id)
            } ?: UpdateStatus.Failed(id, "There is no board configured")
        repository.save(result)
    }
}