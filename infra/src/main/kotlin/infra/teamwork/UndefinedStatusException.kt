package infra.teamwork


class UndefinedStatusException(val status: List<String>) :
    RuntimeException("The status ${status.joinToString(", ", postfix = "'", prefix = "'")} have an undefined state")

