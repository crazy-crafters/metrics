package infra.teamwork.connectors.github

import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.spi.HarvestRemoteDashboard
import domain.teamwork.status.model.Progression
import infra.teamwork.providers.EventDBRepository

class HarvestDashboardFromGithub(
    val github: GithubRepository,
    private val eventRepository: EventDBRepository
): HarvestRemoteDashboard {
    override suspend operator fun invoke(configuration: BoardConfiguration.Valid, progression: (Progression) -> Unit) {
        eventRepository.clear(configuration.project)
        val events = github.findIssuesEvents(of=configuration)
        events.forEach {
            eventRepository.save(it)
        }
    }
}