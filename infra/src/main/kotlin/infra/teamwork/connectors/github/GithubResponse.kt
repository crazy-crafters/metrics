package infra.teamwork.connectors.github

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
sealed interface GithubRestResponse

@Serializable
data class GithubIssue(
    //@SerialName("url") val url: String,
    //@SerialName("repository_url") val repositoryUrl: String,
    //@SerialName("labels_url") val labelsUrl: String,
    //@SerialName("comments_url") val commentsUrl: String,
    //@SerialName("events_url") val eventsUrl: String,
    //@SerialName("html_url") val htmlUrl: String,
    //@SerialName("node_id") val nodeId: String,
    //@SerialName("number") val number: Int,
    //@SerialName("locked") val locked: Boolean,
    //@SerialName("milestone") val milestone: Milestone?,
    //@SerialName("active_lock_reason") val activeLockReason: String?,
    //@SerialName("draft") val draft: Boolean,
    //@SerialName("performed_via_github_app") val performedViaGithubApp: String?,
    @SerialName("id") val id: Long,
    @SerialName("title") val title: String,
    @SerialName("labels") val labels: List<Label>,
    @SerialName("state") val state: String,
    @SerialName("created_at") val createdAt: String,
    @SerialName("updated_at") val updatedAt: String,
    @SerialName("closed_at") val closedAt: String?,
    @SerialName("pull_request") val pullRequest: PullRequest?,
    @SerialName("body") val body: String?,
    @SerialName("timeline_url") val timelineUrl: String,
    @SerialName("state_reason") val stateReason: String?
): GithubRestResponse

@Serializable
data class Label(
    @SerialName("id") val id: Long?,
    @SerialName("node_id") val nodeId: String?,
    @SerialName("url") val url: String?,
    @SerialName("name") val name: String?,
    @SerialName("color") val color: String?,
    @SerialName("default") val default: Boolean?
)

@Serializable
data class Milestone(
    @SerialName("url") val url: String?,
    @SerialName("html_url") val htmlUrl: String?,
    @SerialName("labels_url") val labelsUrl: String?,
    @SerialName("id") val id: Long?,
    @SerialName("node_id") val nodeId: String?,
    @SerialName("number") val number: Int?,
    @SerialName("state") val state: String?,
    @SerialName("title") val title: String?,
    @SerialName("description") val description: String?,
    @SerialName("open_issues") val openIssues: Int?,
    @SerialName("closed_issues") val closedIssues: Int?,
    @SerialName("created_at") val createdAt: String?,
    @SerialName("updated_at") val updatedAt: String?,
    @SerialName("closed_at") val closedAt: String?,
    @SerialName("due_on") val dueOn: String?
)

@Serializable
data class PullRequest(
    @SerialName("url") val url: String,
    @SerialName("html_url") val htmlUrl: String,
    @SerialName("diff_url") val diffUrl: String,
    @SerialName("patch_url") val patchUrl: String,
    @SerialName("merged_at") val mergedAt: String?
)