package infra.teamwork.connectors.github

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.util.*
import kotlinx.coroutines.runBlocking
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.connectors.RemoteBoardConnector
import infra.teamwork.providers.dbo.IssueAddedDB
import infra.teamwork.providers.dbo.IssueMovedDB
import infra.teamwork.providers.dbo.IssueRemovedDB
import infra.teamwork.providers.dbo.TimelineEventDB

class GithubRepository(
    engine: HttpClientEngine,
    val statusRepository: StatusRepository
) : RemoteBoardConnector(engine) {

    fun findIssuesEvents(of: BoardConfiguration.Valid): List<TimelineEventDB> =
        getUrlAuth(configuration = of) { client ->
            println(client)
            val response = client.fetch<List<GithubIssue>>("${of.board}/issues?state=all&per_page=100")

            val open = Status.Working(ID.from("Open"), Label.from("Open"))
            val closed = Status.Working(ID.from("Closed"), Label.from("Closed"))

            statusRepository.save(of.project, open)
            statusRepository.save(of.project, closed)

            response.flatMap { issue ->
                val l = mutableListOf(
                    IssueAddedDB(
                        date = DateTime.parse(issue.createdAt).toString(),
                        entityId = issue.id.toString(),
                        project = of.project.value,
                        title = issue.title,
                        status = open.id.value,
                        complexity = 1
                    ),
                    IssueMovedDB(
                        date = DateTime.parse(issue.createdAt).toString(),
                        entityId = issue.id.toString(),
                        project = of.project.value,
                        newStatus = open.id.value
                    )
                )

                if (issue.closedAt != null) {
                    l.add(
                        IssueMovedDB(
                            date = DateTime.parse(issue.closedAt).toString(),
                            entityId = issue.id.toString(),
                            project = of.project.value,
                            newStatus = closed.id.value
                        )
                    )
                    l.add(
                        IssueRemovedDB(
                            date = DateTime.parse(issue.closedAt).toString(),
                            entityId = issue.id.toString(),
                            project = of.project.value
                        )
                    )
                }

                l

            }
        }

    override fun <T> getUrlAuth(configuration: BoardConfiguration.Valid, f: suspend (HttpClient) -> T): T {
        val client = HttpClient(engine) {
            defaultRequest {
                url(urlString = "${configuration.url}/${configuration.login.user}/")
                headers.appendIfNameAbsent(HttpHeaders.ContentType, ContentType.Application.Json.toString())
                    .appendIfNameAbsent(HttpHeaders.Accept, "application/vnd.github.raw+json")
                    .appendIfNameAbsent("X-GitHub-Api-Version", "2022-11-28")
                    .appendIfNameAbsent(HttpHeaders.Authorization, "Bearer " + configuration.login.pass)
            }

            install(ContentNegotiation) {
                json()
            }
        }
        return runBlocking {
            val result = f(client)
            client.close()
            result
        }
    }

}