package infra.teamwork.connectors.jira.dto

import kotlinx.serialization.Serializable

@Serializable
data class JiraIssuesResponse(
    val maxResults: Int,
    val startAt: Int,
    val total: Int,
    val issues: List<JiraIssue>
): JiraRestResponse

@Serializable
data class JiraIssue(
    val id: String,
    val self: String,
    val key: String,
    val fields: JiraIssueFields
)

@Serializable
data class JiraIssueFields(
    val issuetype: JiraIssueType,
    //val sprint: String?,
    val summary: String,
    val status: JiraIssueStatus,
    val created: String,
    val customfield_10020: List<JiraIssueCreationStatus>?,
    val customfield_10016: Int
)

@Serializable
data class JiraIssueStatus(
    val id: String = ""
)

@Serializable
data class JiraIssueType(
    val id: String,
    val name: String
)

@Serializable
data class JiraIssueCreationStatus(
    val name: String,
    val state: String,
    val startDate: String?,
    val endDate: String?,
    val completeDate: String?
)