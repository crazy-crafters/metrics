package infra.teamwork.connectors.jira

import infra.teamwork.connectors.RemoteBoardConnector
import infra.teamwork.connectors.jira.dto.*
import io.ktor.client.engine.*
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.status.spi.StatusRepository
import domain.teamwork.status.model.Status
import infra.teamwork.providers.dbo.*

class JiraRepository(
    engine: HttpClientEngine,
    val statusRepository: StatusRepository
) : RemoteBoardConnector(engine) {
    fun findIterationsEvents(of: BoardConfiguration.Valid): List<TimelineEventDB> = get(configuration = of) { client ->
        val result = client.fetch<JiraSprintsResponse>("/rest/agile/1.0/board/${of.board}/sprint")

        result.values.filter { it.startDate != null }.flatMap {

            listOf(
                IterationStartedDB(
                    entityId = it.id.toString(),
                    project = of.project.value,
                    date = DateTime.parse(it.startDate!!).toString(),
                    name = it.name
                ),
                if (it.completeDate != null) {
                    IterationFinishedDB(
                        entityId = it.id.toString(),
                        project = of.project.value,
                        date = DateTime.parse(it.completeDate).toString(),
                    )
                } else {
                    null
                }
            )
        }.filterNotNull()
    }

    fun findIssuesEvents(of: BoardConfiguration.Valid): List<TimelineEventDB> = get(configuration = of) { client ->
        val statuses = client.fetch<List<JiraStatus>>("/rest/api/3/status")
        statuses.forEach {
            statusRepository.save(of.project, Status.Undefined(id = ID.from(it.id), label = Label.from(it.name)))
        }

        val issues = client.fetch<JiraIssuesResponse>("/rest/agile/1.0/board/${of.board}/issue").issues
        issues.flatMap {
            val origin = IssueAddedDB(
                date = DateTime.parse(it.fields.created).toString(),
                entityId = it.id,
                project = of.project.value,
                title = it.fields.summary,
                status = Status.OutOfScope.id.value,
                complexity = it.fields.customfield_10016
            )

            listOf(origin) + client.fetch<JiraIssueChangelogResponse>("/rest/api/3/issue/${it.id}/changelog").values.flatMap {
                it.toEvent(
                    origin,
                    statuses
                )
            }
        }
    }

    private fun JiraIssueChangelog.toEvent(origin: IssueAddedDB, statuses: List<JiraStatus>): List<TimelineEventDB> {
        return this.items.mapNotNull { change ->
            if (change.field == "status") {
                if (change.toString.equals("Removed", ignoreCase = true)) {
                    IssueRemovedDB(
                        entityId = origin.entityId,
                        project = origin.project,
                        date = DateTime.parse(created).toString()
                    )
                } else {
                    IssueMovedDB(
                        entityId = origin.entityId,
                        project = origin.project,
                        newStatus = change.to!!,
                        date = DateTime.parse(created).toString()
                    )
                }
            } else if (change.field == "Sprint") {
                if (change.from == "") {
                    IssueMovedDB(
                        entityId = origin.entityId,
                        project = origin.project,
                        newStatus = TODO(), // statusRepository.findByState(origin.project, WorkState.Planned) ?: createDefaultToDoStatus(origin.project)
                        date = DateTime.parse(created).toString()
                    )
                } else if (change.to == "") {
                    IssueMovedDB(
                        entityId = origin.entityId,
                        project = origin.project,
                        newStatus = TODO(), //statusRepository.findByState(origin.project, WorkState.Planned) ?: createDefaultToDoStatus(origin.project),
                        date = DateTime.parse(created).toString()
                    )
                } else {
                    null
                }
            } else {
                null
            }
        }
        /*return this.items.filter { change -> change.field == "status" }.map {
            if (it.toString.equals("Removed", ignoreCase = true)) {
                IssueClosed(
                    entityId = origin.entityId,
                    project = origin.project,
                    date = created.parseToDate()
                )
            } else {
                IssueMoved(
                    entityId = origin.entityId,
                    project = origin.project,
                    status = statuses.resolve(it.to ?: "", it.toString ?: ""),
                    date = created.parseToDate()
                )
            }
        }*/
    }
}


