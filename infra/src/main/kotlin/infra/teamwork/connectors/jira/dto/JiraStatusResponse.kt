package infra.teamwork.connectors.jira.dto

import kotlinx.serialization.Serializable

@Serializable
data class JiraStatus(
    val id: String,
    val name: String,
    val statusCategory: JiraStatusCategory
): JiraRestResponse

@Serializable
data class JiraStatusCategory(
    val key: String
): JiraRestResponse