package infra.teamwork.connectors.jira.dto

import kotlinx.serialization.Serializable

@Serializable
data class JiraSprintsResponse(
    val maxResults: Int,
    val startAt: Int,
    val total: Int,
    val isLast: Boolean,
    val values: List<JiraSprint>
): JiraRestResponse

@Serializable
data class JiraSprint(
    val id: Int,
    val self: String,
    val state: String,
    val name: String,
    val startDate: String?,
    val endDate: String?,
    val completeDate: String?,
    val createdDate: String,
    val originBoardId: Int,
    val goal: String?
): JiraRestResponse