package infra.teamwork.connectors.jira

import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.spi.HarvestRemoteDashboard
import domain.teamwork.status.model.Progression
import infra.teamwork.providers.EventDBRepository

class HarvestDashboardFromJira(
    private val jira: JiraRepository,
    private val eventRepository: EventDBRepository
): HarvestRemoteDashboard {
    override suspend operator fun invoke(configuration: BoardConfiguration.Valid, progression: (Progression) -> Unit) {
        val events = jira.findIssuesEvents(configuration) + jira.findIterationsEvents(configuration)
        eventRepository.clear(project=configuration.project)

        events.forEach {
            eventRepository.save(it)
        }
    }

    /*override fun events(configuration: BoardConfiguration.Valid): List<Event> {
        return eventRepository.all(configuration.project).filter { it.project == configuration.project }
    }*/
}