package infra.teamwork.connectors.jira.dto

import kotlinx.serialization.Serializable

@Serializable
sealed interface JiraRestResponse

@Serializable
data class JiraIssueChangelogResponse(
    val maxResults: Int,
    val startAt: Int,
    val total: Int,
    val values: List<JiraIssueChangelog>
): JiraRestResponse

@Serializable
data class JiraIssueChangelog(
    val id: String,
    val created: String,
    val items: List<JiraIssueChangelogItem>
): JiraRestResponse

@Serializable
data class JiraIssueChangelogItem(
    val field: String,
    val fieldId: String,
    val from: String?,
    val to: String?,
    val fromString: String?,
    val toString: String?,
): JiraRestResponse