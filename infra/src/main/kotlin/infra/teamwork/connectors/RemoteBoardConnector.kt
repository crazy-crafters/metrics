package infra.teamwork.connectors

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import domain.teamwork.configuration.model.BoardConfiguration


class NotFoundException(val url: String): RuntimeException()

abstract class RemoteBoardConnector(
    val engine: HttpClientEngine
) {
    protected fun <T> get(configuration: BoardConfiguration.Valid, f: suspend (HttpClient) -> T): T {
        val client = HttpClient(engine) {
            install(DefaultRequest) {
                url(configuration.url)
                configuration.login.let {
                    basicAuth(it.user, it.pass)
                }
            }

            install(ContentNegotiation) {
                json()
            }
        }
        return runBlocking {
            val result = f(client)
            client.close()
            result
        }
    }

    protected open fun <T> getUrlAuth(configuration: BoardConfiguration.Valid, f: suspend (HttpClient) -> T): T {
        val client = HttpClient(engine) {
            install(DefaultRequest) {
                url(configuration.url)
                url {
                    parameters.append("key", configuration.login.user)
                    parameters.append("token", configuration.login.pass)
                }
            }

            install(ContentNegotiation) {
                json()
            }
        }
        return runBlocking {
            val result = f(client)
            client.close()
            result
        }
    }

    private val cache = mutableMapOf<String, String>()
    suspend fun HttpClient.fetchResponse(url: String): String {
        try {
            if (cache.containsKey(url)) {
                return cache[url]!!
            }
            val response = get(url)
            val status = response.status
            val content = response.bodyAsText()
            val check = "$url, ${status}"
            when (status) {
                HttpStatusCode.OK -> {
                    cache[url] = content
                    return content
                }
                HttpStatusCode.NotFound -> throw NotFoundException(url)
                else -> TODO(check)
            }
        } catch (e: NullPointerException) {
            throw NotFoundException(url)
        }
    }

    val decoder = Json { ignoreUnknownKeys = true }
    protected suspend inline fun <reified T> HttpClient.fetch(url: String): T {
        println(url)
        val content = fetchResponse(url)
        try {
            return decoder.decodeFromString(content)//mapper.readValue<T>(content)
        } catch (e: NullPointerException) {
            println("Error deserializing $url:")
            println(content)
            throw e
        }
    }
}