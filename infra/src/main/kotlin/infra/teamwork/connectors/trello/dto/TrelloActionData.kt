package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
data class TrelloActionData(
    val card: TrelloActionCard?,
    val list: TrelloList?,
    val board: TrelloBoardAction?,
    val old: TrelloOld?,
    //val listBefore: TrelloListDetails?,
    val listAfter: TrelloListDetails?,
)


