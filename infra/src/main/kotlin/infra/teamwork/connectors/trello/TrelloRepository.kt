package infra.teamwork.connectors.trello

import io.ktor.client.engine.*
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.sharedkernel.or
import domain.teamwork.configuration.model.BoardConfiguration
import domain.project.model.Project
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.connectors.RemoteBoardConnector
import infra.teamwork.connectors.trello.dto.TrelloAction
import infra.teamwork.providers.dbo.*

class TrelloRepository(
    engine: HttpClientEngine,
    private val statusRepository: StatusRepository
): RemoteBoardConnector(engine) {
    fun findIssuesEvents(of: BoardConfiguration.Valid): List<TimelineEventDB> = getUrlAuth(configuration = of) { client ->
        val response = client.fetch<List<TrelloAction>>("boards/${of.board}/actions?filter=all")

        response.mapNotNull { action ->
            action.toEvent(of.project)
        }
    }

    private fun TrelloAction.toEvent(project: ID<Project>): TimelineEventDB? {
        if (type in "createBoard" or "copyBoard") {
            return BoardCreatedDB(
                date = DateTime.parse(date).toString(),
                entityId = data.board!!.id,
                project = project.value,
                title = data.board.name
            )
        }

        if (type == "updateBoard") {
            if (data.old?.name != null) {
                return BoardRenamedDB(
                    date = DateTime.parse(date).toString(),
                    entityId = data.board!!.id,
                    project = project.value,
                    title = data.board.name
                )
            }

        }

        if (type == "createList") {
            statusRepository.save(project, Status.Undefined(ID.from(data.list!!.id), Label.from(data.list.name)))
            return ColumnAddedDB(
                date = DateTime.parse(date).toString(),
                entityId = data.list.id,
                project = project.value,
                title = data.list.name
            )
        }

        if (type == "updateList") {
            statusRepository.save(project, Status.Undefined(ID.from(data.list!!.id), Label.from(data.list.name)))
            if (data.old?.closed != null) {
                if (data.old.closed) {
                    return ColumnRemovedDB(
                        date = DateTime.parse(date).toString(),
                        entityId = data.list.id,
                        project = project.value
                    )
                }
            }
            if (data.old?.name != null) {
                return ColumnRenamedDB(
                    date = DateTime.parse(date).toString(),
                    entityId = data.list.id,
                    project = project.value,
                    title = data.list.name
                )
            }
            return ColumnAddedDB(
                date = DateTime.parse(date).toString(),
                entityId = data.list.id,
                project = project.value,
                title = data.list.name
            )
        }

        if (type in "createCard" or "copyCard") {
            return IssueAddedDB(
                date = DateTime.parse(date).toString(),
                entityId = data.card!!.id,
                project = project.value,
                title = data.card.name,
                status = Status.OutOfScope.id.value,
                complexity = 1
            )
        }

        if (type == "deleteCard") {
            return IssueRemovedDB(
                date = DateTime.parse(date).toString(),
                entityId = data.card!!.id,
                project = project.value
            )
        }

        if (type == "updateCard") {
            if (data.old?.name != null) {
                return IssueRenamedDB(
                    date = DateTime.parse(date).toString(),
                    entityId = data.card!!.id,
                    project = project.value,
                    title = data.card.name
                )
            }

            if (data.old?.idList != null) {
                return IssueMovedDB(
                    date = DateTime.parse(date).toString(),
                    entityId = data.card!!.id,
                    project = project.value,
                    newStatus = data.listAfter?.id!!,
                )
            }

            if (data.old?.closed != null) {
                if (data.old.closed) {
                    return IssueRemovedDB(
                        date = DateTime.parse(date).toString(),
                        entityId = data.card!!.id,
                        project = project.value
                    )
                }
            }
            return IssueAddedDB(
                date = DateTime.parse(date).toString(),
                entityId = id,
                project = project.value,
                title = data.card!!.name,
                status = Status.OutOfScope.id.value,
                complexity = 1
            )
        }

        println(this.toString())
        return null
    }
}


