package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
data class TrelloList(val name: String, val id: String): TrelloRestResponse

@Serializable
data class TrelloCard(
    val id: String,
    val closed: Boolean,
    val name: String,
    //val pos: Int,
    //val dueComplete: Boolean,
    //val dateLastActivity: String,
    //val due: String?,
    //val dueReminder: String?,
    //val idBoard: String,
    //val idChecklists: List<String>,
    //val idList: String,
    // val shortLink: String,
    // val shortUrl: String,
    // val start: String?,
    // val url: String,
    // val cardRole: String?
): TrelloRestResponse