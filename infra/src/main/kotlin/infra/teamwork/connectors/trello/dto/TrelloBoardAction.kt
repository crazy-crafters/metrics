package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
data class TrelloBoardAction(
    val id: String,
    val name: String
)