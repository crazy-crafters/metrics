package infra.teamwork.connectors.trello

import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.spi.HarvestRemoteDashboard
import domain.teamwork.status.model.Progression
import infra.teamwork.providers.EventDBRepository


class HarvestDashboardFromTrello(
    val trello: TrelloRepository,
    private val eventRepository: EventDBRepository
): HarvestRemoteDashboard {
    override suspend operator fun invoke(configuration: BoardConfiguration.Valid, progression: (Progression) -> Unit) {
        eventRepository.clear(configuration.project)
        val events = trello.findIssuesEvents(of=configuration)
        events.forEach {
            eventRepository.save(it)
        }
    }
}