package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
data class TrelloActionCard(
    val closed: Boolean?,
    val name: String,
    val id: String,
): TrelloRestResponse