package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
data class TrelloListDetails(
    val id: String,
    val name: String
)