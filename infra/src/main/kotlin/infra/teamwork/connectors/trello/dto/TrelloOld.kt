package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
data class TrelloOld(
    val idList: String?,
    val name: String?,
    val closed: Boolean?
)