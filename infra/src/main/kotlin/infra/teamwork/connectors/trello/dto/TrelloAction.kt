package infra.teamwork.connectors.trello.dto

import kotlinx.serialization.Serializable

@Serializable
sealed interface TrelloRestResponse

@Serializable
data class TrelloAction(
    val id: String,
    val type: String,
    val date: String,
    val data: TrelloActionData
): TrelloRestResponse