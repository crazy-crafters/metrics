package infra.teamwork.connectors.azure

import com.github.benmanes.caffeine.cache.Cache
import com.github.benmanes.caffeine.cache.Caffeine
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import domain.teamwork.configuration.model.BoardConfiguration
import infra.teamwork.connectors.NotFoundException
import java.util.concurrent.TimeUnit

class RestClient(
    engine: HttpClientEngine,
    val configuration: BoardConfiguration.Valid
) {

    val client = HttpClient(engine) {
        install(DefaultRequest) {
            url(configuration.url)
            configuration.login.let {
                basicAuth(it.user, it.pass)
            }
        }

        install(ContentNegotiation) {
            json()
        }
    }

    private val cache: Cache<String, String> = Caffeine.newBuilder()
        .expireAfterWrite(15, TimeUnit.MINUTES)
        .maximumSize(5000)
        .build()


    suspend fun HttpClient.fetchResponse(url: String): String {
        return executeRequest(url) { get(it) }
    }

    suspend fun HttpClient.postResponse(url: String, body: String): String {
        return executeRequest(url) {
            post(it) {
                contentType(ContentType.Application.Json)
                setBody(body)
            }
        }
    }


    private suspend fun HttpClient.executeRequest(
        url: String,
        requestBlock: suspend HttpClient.(String) -> HttpResponse
    ): String {
        return cache.get(url) {
            try {
                runBlocking {
                    val response = requestBlock(url)
                    val status = response.status
                    val content = response.bodyAsText()
                    val check = "$url, $status"
                    when (status) {
                        HttpStatusCode.OK -> content
                        HttpStatusCode.NotFound -> throw NotFoundException(url)
                        else -> TODO(check)
                    }
                }
            } catch (e: NullPointerException) {
                throw NotFoundException(url)
            }
        }
    }

    val decoder = Json { ignoreUnknownKeys = true }

    suspend inline fun <reified T> HttpClient.fetch(url: String, body: String? = null): T {
        val content = when (body) {
            null -> fetchResponse(url)
            else -> postResponse(url, body)
        }
        try {
            return decoder.decodeFromString(content)
        } catch (e: NullPointerException) {
            println("Error deserializing $url:")
            println(content)
            throw e
        }
    }

    suspend inline fun <reified T> fetch(url: String, body: String? = null): T {
        return client.fetch<T>(url, body)
    }
}