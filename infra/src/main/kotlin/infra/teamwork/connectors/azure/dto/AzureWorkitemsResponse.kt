package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
data class AzureWorkitemsResponse(
    val workItemRelations: List<AzureWorkitemsWorkItemRelation>,
    val url: String,
    //val _links: AzureWorkitemsLinks
): AzureRestResponse

@Serializable
data class AzureWorkitemsWorkItemRelation(
    val rel: String? = null,
    val source: AzureWorkitemsWorkItem? = null,
    val target: AzureWorkitemsWorkItem
)

@Serializable
data class AzureWorkitemsWorkItem(
    val id: Int,
    val url: String
)

