package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
data class AzureWorkItemType(
    val name: String,
    val states: List<AzureWorkItemState>,
)