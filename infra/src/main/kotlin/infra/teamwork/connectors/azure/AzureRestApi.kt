package infra.teamwork.connectors.azure


import io.ktor.client.engine.*
import kotlinx.coroutines.runBlocking
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.issue.model.Issue
import domain.teamwork.status.model.Progression
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.connectors.NotFoundException
import infra.teamwork.connectors.azure.dto.AzureAllWorkItemsResponse
import infra.teamwork.connectors.azure.dto.AzureWorkItemTypeResponse
import infra.teamwork.connectors.azure.dto.AzureWorkitemResponse
import infra.teamwork.connectors.azure.dto.AzureWorkitemUpdatesResponse
import infra.teamwork.providers.EventDBRepository
import infra.teamwork.providers.dbo.*


class AzureRestApi(
    engine: HttpClientEngine,
    private val configuration: BoardConfiguration.Valid,
    private val statusRepository: StatusRepository,
    private val eventRepository: EventDBRepository,
    private val eventSink: (String) -> Unit,
    private val progression: (Progression) -> Unit
) {

    private fun add(event: TimelineEventDB) = eventRepository.save(event)
    private fun conditionalAdd(condition: String?, item: TimelineEventDB) {
        if (!condition.isNullOrBlank()) {
            eventRepository.save(item)
        }
    }

    private val project = configuration.project.value

    private val client = RestClient(engine, configuration)

    suspend fun findEvents() {
        fetchStatuses()
        fetchWorkitems()
    }

    private suspend fun fetchStatuses() {
        val u = client.fetch<AzureWorkItemTypeResponse>("${configuration.url}/_apis/wit/workItemTypes")
        u.value.forEach { type ->
            type.states.forEach {
                val existing = statusRepository.find(configuration.project, ID.from(it.name))
                if (existing == null || existing is Status.Undefined) {
                    statusRepository.save(
                        configuration.project,
                        when (it.category) {
                            "Proposed" -> Status.Waiting(ID.from(it.name), Label.from(it.name))
                            "InProgress" -> Status.Working(ID.from(it.name), Label.from(it.name))
                            "Resolved" -> Status.Done(ID.from(it.name), Label.from(it.name))
                            "Completed" -> Status.Done(ID.from(it.name), Label.from(it.name))
                            "Rejected" -> Status.Done(ID.from(it.name), Label.from(it.name))
                            else -> {
                                eventSink("Cannot define the state of the status '${it.name}'")
                                Status.Undefined(ID.from(it.name), Label.from(it.name))
                            }
                        }
                    )
                }
            }
        }
        eventSink("${u.value.count()} status has been saved")
    }

    private suspend fun fetchWorkitems() {
        val r = client.fetch<AzureAllWorkItemsResponse>(
            "${configuration.url}/_apis/wit/wiql?api-version=5.1",
            body = """{ "query": "Select [System.Id] From WorkItems " }"""
        )

        eventSink("${r.workItems.count()} workitems found")

        r.workItems.forEachIndexed { index, workItem ->
            fetchWorkItem(workItem.url)
            fetchWorkitemUpdates(workItem.url)
            fetchTags(workItem.url)
            fetchFeatureTag(workItem.url)
            progression(Progression(1, r.workItems.size))
        }
    }

    private suspend fun fetchWorkItem(url: String) {
        val workitemResponse = client.fetch<AzureWorkitemResponse>(url)
        if (workitemResponse.fields.systemWorkItemType != "User Story") {
            return
        }

        val id = ID.from<Issue>(workitemResponse.id.toString())
        val date = DateTime.parse(workitemResponse.fields.systemCreatedDate).toString()
        add(
            IssueAddedDB(
                entityId = id.value,
                project = configuration.project.value,
                title = workitemResponse.fields.systemTitle,
                date = date,
                status = Status.OutOfScope.id.value,
                complexity = workitemResponse.fields.microsoftVstsSchedulingStoryPoints.toInt(),
            )
        )
    }

    private suspend fun fetchWorkitemUpdates(url: String) {
        val workitemResponse = client.fetch<AzureWorkitemResponse>(url)
        val updatesResponse = client.fetch<AzureWorkitemUpdatesResponse>(workitemResponse._links.workItemUpdates.href)
        updatesResponse.value.mapNotNull {
            if (it.fields != null) {
                DateTime.parse(it.date()).toString() to it.fields
            } else {
                null
            }
        }.forEach { (date, fields) ->
            val state = fields.systemState
            if (state != null) {
                val old = state.oldValue
                val new = state.newValue
                if (old != new && (new != null)) {
                    add(
                        IssueMovedDB(
                            date = date,
                            entityId = workitemResponse.id.toString(),
                            project = configuration.project.value,
                            newStatus = new
                        )
                    )

                    if (new in listOf("Removed", "Resolved", "Closed")) {
                        add(
                            IssueRemovedDB(
                                date = date,
                                entityId = workitemResponse.id.toString(),
                                project = configuration.project.value
                            )
                        )
                    }
                }
            }
        }
    }

    private suspend fun fetchTags(url: String) {
        val workitem = client.fetch<AzureWorkitemResponse>(url)
        val updates = client.fetch<AzureWorkitemUpdatesResponse>(workitem._links.workItemUpdates.href)

        val id = ID.from<Issue>(workitem.id.toString())
        val date = DateTime.parse(workitem.fields.systemCreatedDate).toString()

        conditionalAdd(
            workitem.fields.systemWorkItemType,
            IssueTagAddedDB(date, id.value, project, "type:${workitem.fields.systemWorkItemType}")
        )
        conditionalAdd(
            workitem.fields.microsoftVstsCommonValueArea,
            IssueTagAddedDB(date, id.value, project, "area:${workitem.fields.microsoftVstsCommonValueArea}")
        )
        workitem.fields.systemTags.split(";").filter { it.isNotBlank() }.forEach {
            add(IssueTagAddedDB(date, id.value, project, value = it.trim()))
        }

        updates.value.mapNotNull {
            if (it.fields != null) {
                DateTime.parse(it.date()).toString() to it.fields
            } else {
                null
            }
        }.forEach { (date, fields) ->
            fields.systemTags?.let {
                if (!(fields.systemTags.oldValue.isNullOrBlank())) {
                    fields.systemTags.oldValue.split(";").filter { it.isNotBlank() }.forEach {
                        add(IssueTagRemovedDB(date, id.value, project, value = it.trim()))
                    }
                }

                if (!(fields.systemTags.newValue.isNullOrBlank())) {
                    fields.systemTags.newValue.split(";").filter { it.isNotBlank() }.forEach {
                        add(IssueTagAddedDB(date, id.value, project, value = it.trim()))
                    }
                }
            }

            fields.microsoftValueArea?.let {
                if (!fields.microsoftValueArea.oldValue.isNullOrBlank()) {
                    add(
                        IssueTagRemovedDB(
                            date,
                            id.value,
                            project,
                            "area:${fields.microsoftValueArea.oldValue}"
                        )
                    )
                }
                if (!fields.microsoftValueArea.newValue.isNullOrBlank()) {
                    add(
                        IssueTagAddedDB(
                            date,
                            id.value,
                            project,
                            "area:${fields.microsoftValueArea.newValue}"
                        )
                    )
                }
            }
        }
    }

    private fun fetchWorkitemType(url: String): String {
        try {
            val r = runBlocking { client.fetch<AzureWorkitemResponse>(url) }
            return r.fields.systemWorkItemType
        } catch (e: NotFoundException) {
            return ""
        }
    }

    private suspend fun fetchFeatureTag(
        url: String
    ) {
        val workitem: AzureWorkitemResponse = client.fetch(url)
        val updates: AzureWorkitemUpdatesResponse = client.fetch(workitem._links.workItemUpdates.href)

        val issue = workitem.id.toString()
        updates.value.filter { it.relations != null }.forEach { update ->
            val relations = update.relations!!
            val date = update.date()
            relations.added?.let { relation ->
                relation.forEach {
                    if (it.rel == "System.LinkTypes.Hierarchy-Reverse" && !it.url.isNullOrBlank()) {
                        val parentId = it.url.split("/").last()
                        val type = fetchWorkitemType(it.url)
                        add(
                            IssueTagAddedDB(
                                date = date,
                                project = project,
                                entityId = issue,
                                value = "$type:$parentId"
                            )
                        )
                        fetchEpicTags(issue, date, parentId)
                    }
                }
            }
            relations.removed?.let { relation ->
                relation.forEach { it ->
                    if (it.rel == "System.LinkTypes.Hierarchy-Reverse" && !it.url.isNullOrBlank()) {
                        val parentId = it.url.split("/").last()
                        val type: String = fetchWorkitemType(it.url).let { t ->
                            when {
                                t.isNotBlank() -> t
                                else -> "Parent"
                            }
                        }
                        add(
                            IssueTagRemovedDB(
                                date = date,
                                project = project,
                                entityId = issue,
                                value = "$type:$parentId"
                            )
                        )
                        fetchEpicTags(issue, date, parentId)
                    }
                }
            }
        }
    }

    private fun fetchEpicTags(issue: String, date: String, parent: String) {
        val r =
            try {
                runBlocking { client.fetch<AzureWorkitemUpdatesResponse>("${configuration.url}/_apis/wit/workitems/$parent/updates") }
            } catch (e: NotFoundException) {
                return
            }
        r.value.filter { it.relations != null }.forEach { update ->
            val relations = update.relations!!
            relations.added?.let { relation ->
                relation.forEach {
                    try {
                        if (it.rel == "System.LinkTypes.Hierarchy-Reverse" && !it.url.isNullOrBlank()) {
                            val parentId = it.url.split("/").last()
                            val type = fetchWorkitemType(it.url)
                            add(
                                IssueTagAddedDB(
                                    date = max(date, update.date()),
                                    project = project,
                                    entityId = issue,
                                    value = "$type:$parentId"
                                )
                            )
                        }
                    } catch (_: NotFoundException) {
                        eventSink("Cannot fetch :${it.url}")
                    }
                }
            }
            relations.removed?.let { relation ->
                relation.forEach {
                    try {
                        if (it.rel == "System.LinkTypes.Hierarchy-Reverse" && !it.url.isNullOrBlank()) {
                            val parentId = it.url.split("/").last()
                            val type = fetchWorkitemType(it.url)
                            add(
                                IssueTagRemovedDB(
                                    date = max(date, update.date()),
                                    project = project,
                                    entityId = issue,
                                    value = "$type:$parentId"
                                )
                            )
                        }
                    } catch (_: NotFoundException) {
                        eventSink("Cannot fetch ${it.url}")
                    }
                }
            }
        }
    }
}

fun max(s1: String, s2: String) = listOf(s1, s2).max()