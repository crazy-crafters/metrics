package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
data class AzureWorkItemTypeResponse(
    val value: List<AzureWorkItemType>
): AzureRestResponse