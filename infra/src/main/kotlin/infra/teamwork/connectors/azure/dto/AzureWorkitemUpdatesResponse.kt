package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


class NoRevisionDate(val id: Int, val rev: Int) : RuntimeException("Cannot determine revision date for $id (rev $rev)")

@Serializable
data class AzureWorkitemUpdatesResponse(
    val count: Int,
    val value: List<AzureWorkitemUpdatesValue>
): AzureRestResponse

@Serializable
data class AzureWorkitemUpdatesValue(
    val id: Int,
    val workItemId: Int,
    val rev: Int,
    val revisedDate: String? = null,
    val fields: AzureWorkitemUpdatesFields? = null,
    val relations: AzureWorkitemUpdatesRelation? = null,
    val url: String
) {
    fun date(): String {
        return fields?.systemChangedDate?.newValue ?: revisedDate ?: throw NoRevisionDate(id, rev)
    }
}

@Serializable
data class AzureWorkitemUpdatesFields(
    // val systemId: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemAreaId: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemNodeName: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemAreaLevel1: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemRev: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemAuthorizedDate: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemRevisedDate: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemIterationId: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemIterationLevel1: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemWorkItemType: AzureWorkitemUpdatesFieldValue<String>?,
    @SerialName("System.State")
    val systemState: AzureWorkitemUpdatesFieldValue<String>? = null,
    // val systemReason: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemCreatedDate: AzureWorkitemUpdatesFieldValue<String>?,
    @SerialName("System.ChangedDate")
    val systemChangedDate: AzureWorkitemUpdatesFieldValue<String>? = null,
    // val systemPersonId: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemWatermark: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemIsDeleted: AzureWorkitemUpdatesFieldValue<Boolean>?,
    // val systemCommentCount: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemTeamProject: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemAreaPath: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemIterationPath: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemBoardColumnDone: AzureWorkitemUpdatesFieldValue<Boolean>?,
    @SerialName("Microsoft.VSTS.Common.ValueArea")
    val microsoftValueArea: AzureWorkitemUpdatesFieldValue<String>? = null,
    // val microsoftPriority: AzureWorkitemUpdatesFieldValue<Int>?,
    // val systemBoardColumn: AzureWorkitemUpdatesFieldValue<String>?,
    // val microsoftStateChangeDate: AzureWorkitemUpdatesFieldValue<String>?,
    // val microsoftStoryPoints: AzureWorkitemUpdatesFieldValue<Double>?,
    // val systemDescription: AzureWorkitemUpdatesFieldValue<String>?,
    // val systemTitle: AzureWorkitemUpdatesFieldValue<String>?,
    @SerialName("System.Tags")
    val systemTags: AzureWorkitemUpdatesFieldValue<String>? = null,
    // val systemParent: AzureWorkitemUpdatesFieldValue<Int>?
)

@Serializable
data class AzureWorkitemUpdatesRelation(
    val added: List<AzureWorkitemUpdatesRelationAttributes>? = null,
    val removed: List<AzureWorkitemUpdatesRelationAttributes>? = null
)

@Serializable
data class AzureWorkitemUpdatesRelationAttributes(
    val rel: String? = null,
    val url: String? = null,
)

@Serializable
data class AzureWorkitemUpdatesAttributes(
    val isLocked: Boolean? = null,
    val name: String? = null
)

@Serializable
data class AzureWorkitemUpdatesFieldValue<T>(
    val newValue: T? = null,
    val oldValue: T? = null
)