package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AzureWorkitemResponse(
    val id: Int,
    val fields: AzureWorkitemFields,
    val relations: List<AzureWorkitemRelations> = emptyList(),
    val _links: WorkItemLinks = WorkItemLinks(),
    val url: String
): AzureRestResponse

@Serializable
data class AzureWorkitemFields(
    //val systemAreaPath: String,
    //val systemTeamProject: String,
    //val systemIterationPath: String,
    @SerialName("System.WorkItemType")
    val systemWorkItemType: String,
    //@SerialName("System.State")
    //val systemState: String?,
    //val systemReason: String,
    @SerialName("System.CreatedDate")
    val systemCreatedDate: String = "",
    //val systemChangedDate: String,
    //val systemCommentCount: Int,
    @SerialName("System.Title")
    val systemTitle: String = "",
    //val systemBoardColumn: String,
    //val systemBoardColumnDone: Boolean,
    val microsoftVstsSchedulingStoryPoints: Double = 0.0,
    //val microsoftVstsCommonStateChangeDate: String,
    //val microsoftVstsCommonPriority: Int,
    //val microsoftVstsCommonStackRank: Double,
    @SerialName("Microsoft.VSTS.Common.ValueArea")
    val microsoftVstsCommonValueArea: String = "",
    //val systemDescription: String,
    @SerialName("System.Tags")
    val systemTags: String = "",
)

@Serializable
data class WorkItemLinks(
    val self: AzureLink = AzureLink(),
    val workItemUpdates: AzureLink = AzureLink(),
    val workItemRevisions: AzureLink = AzureLink(),
    val workItemComments: AzureLink = AzureLink(),
    val html: AzureLink = AzureLink(),
    val workItemType: AzureLink = AzureLink(),
    val fields: AzureLink = AzureLink()
)

@Serializable
data class AzureWorkitemRelations(
    val rel: String,
    val url: String,
    val attributes: AzureWorkitemRelationAttributes? = null
)

@Serializable
data class AzureWorkitemRelationAttributes(
    val isLocked: Boolean,
    val comment: String? = null
)