package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
data class AzureWorkItemState(
    val name: String,
    val category: String
)