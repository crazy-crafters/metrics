package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
sealed interface AzureRestResponse