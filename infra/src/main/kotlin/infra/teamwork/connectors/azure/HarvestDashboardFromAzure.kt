package infra.teamwork.connectors.azure

import io.ktor.client.engine.*
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.spi.HarvestRemoteDashboard
import domain.teamwork.status.model.Progression
import domain.teamwork.status.spi.StatusRepository
import infra.teamwork.providers.EventDBRepository

class HarvestDashboardFromAzure(
    private val eventRepository: EventDBRepository,
    private val statusRepository: StatusRepository,
    private val engine: HttpClientEngine
): HarvestRemoteDashboard {
    override suspend operator fun invoke(
        configuration: BoardConfiguration.Valid,
        progression: (Progression) -> Unit,
    ) {
        eventRepository.clear(project=configuration.project)
        val azureRestApi = AzureRestApi(
            engine = engine,
            configuration = configuration,
            statusRepository = statusRepository,
            eventRepository = eventRepository,
            eventSink = {},
            progression = progression
        )
        azureRestApi.findEvents()
    }
}