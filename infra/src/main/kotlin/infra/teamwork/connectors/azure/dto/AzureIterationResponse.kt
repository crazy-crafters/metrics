package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
class AzureIterationResponse(
    //val id: String,
    //val name: String,
    //val path: String,
    //val attributes: AzureIterationAttributes,
    //val url: String,
    val _links: AzureIterationLinks
): AzureRestResponse

@Serializable
data class AzureIterationLinks(
    //val self: AzureLink,
    //val project: AzureLink,
    //val team: AzureLink,
    //val teamSettings: AzureLink,
    //val teamIterations: AzureLink,
    //val capacity: AzureLink,
    val workitems: AzureLink,
    //val classificationNode: AzureLink,
    //val teamDaysOff: AzureLink
): AzureRestResponse

@Serializable
data class AzureLink(
    val href: String = ""
)