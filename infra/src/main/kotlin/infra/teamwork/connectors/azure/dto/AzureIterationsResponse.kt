package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
data class AzureIterationsResponse(
    val count: Int,
    val value: List<AzureIteration>
): AzureRestResponse

@Serializable
data class AzureIteration(
    val id: String,
    val name: String,
    val path: String,
    val attributes: AzureIterationAttributes,
    val url: String
)

@Serializable
data class AzureIterationAttributes(
    val startDate: String?,
    val finishDate: String?,
    val timeFrame: String
)