package infra.teamwork.connectors.azure.dto

import kotlinx.serialization.Serializable

@Serializable
data class AzureAllWorkItemsResponse(
    val workItems: List<WorkItem> = emptyList(),
): AzureRestResponse {
    @Serializable
    data class WorkItem(val id: Int, val url: String)
}