package infra.platform

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Path

abstract class FileSystemRepository(val path: Path) {
    inline fun < reified T: Any> readAll(): List<T> {
        val file = path.resolve("${T::class.simpleName!!.lowercase()}.json").toFile()
        val content = if (!file.exists()) {
            ""
        } else {
            file.readText()
        }
        if (content.isBlank())
            return emptyList()

        return Json.decodeFromString<List<T>>(content)
    }

    val jsonPrinter = Json {
        prettyPrint = true
    }


    inline fun <reified T : Any> writeAll(values: List<T>) {
        val file = path.resolve("${T::class.simpleName!!.lowercase()}.json").toFile()
        file.writeText(jsonPrinter.encodeToString(values))
    }
}