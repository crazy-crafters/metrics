package infra.platform

import domain.sharedkernel.EventBus
import domain.sharedkernel.Listener
import domain.sharedkernel.SystemEventsRepository
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID

class InMemoryBus: EventBus, SystemEventsRepository {

    val events = mutableListOf<DomainEvent>()
    override fun publish(event: DomainEvent) {
        events.add(event)
    }

    override fun subscribe(subscriber: Listener) {}
    override fun findAll(id: ID<*>): List<DomainEvent> = events.filter { it.id == id }

}