package infra.platform

import domain.sharedkernel.EventBus
import domain.sharedkernel.Listener
import domain.sharedkernel.model.DomainEvent


class SimpleEventBus: EventBus {

    private val subscribers = mutableListOf<Listener>()
    override fun subscribe(subscriber: Listener) {
        subscribers.add(subscriber)
    }

    override fun publish(event: DomainEvent) {
        subscribers.forEach { it(event) }
    }
}