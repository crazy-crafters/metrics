package infra.squad

import org.koin.core.module.Module
import org.koin.dsl.module
import java.nio.file.Path
import domain.sharedkernel.model.DateTime
import domain.squad.*
import domain.squad.api.*
import domain.squad.spi.*
import infra.squad.providers.*

fun squadModule(path: Path) = module {
    domain()
    infra()

    single<SquadRepository> {
        FileSystemSquadRepository(path)
    }

    single<MemberRepository> {
        FileSystemMemberRepository(path)
    }

    single<MilestoneRepository> {
        FileSystemMilestoneRepository(path)
    }

    single<AssessmentRepository> {
        FileSystemAssessmentRepository(path)
    }

    single<AttachedFileRepository> {
        FileSystemAttachedFileRepository(path)
    }
}

fun squadModule() = module {
    domain()
    infra()

    single<SquadRepository> {
        InMemorySquadRepository()
    }

    single<MemberRepository> {
        InMemoryMemberRepository()
    }

    single<MilestoneRepository> {
        InMemoryMilestoneRepository()
    }

    single<AssessmentRepository> {
        InMemoryAssessmentRepository()
    }

    single<AttachedFileRepository> {
        InMemoryAttachedFileRepository()
    }
}

private fun Module.domain() {
    single<SquadComposition> {
        SquadComposition(get(), get(), get())
    }

    single<GetSquad> {
        get<SquadComposition>()
    }

    single<ComposeSquad> {
        get<SquadComposition>()
    }


    single<TimelineBuilder> {
        TimelineBuilder(get(), get())
    }

    single<BuildTimeline> {
        get<TimelineBuilder>()
    }

    single<HandleMilestone> {
        get<TimelineBuilder>()
    }

    single<StaffingManagement> {
        StaffingManagement(get(), get(), get())
    }

    single<GetMember> {
        get<StaffingManagement>()
    }

    single<OnboardMember> {
        get<StaffingManagement>()
    }

    single<domain.squad.AssessmentMeasurement> {
        domain.squad.AssessmentMeasurement(get(), get(), get())
    }

    single<GetAssessment> {
        get<domain.squad.AssessmentMeasurement>()
    }

    single<EvaluatePillar> {
        get<domain.squad.AssessmentMeasurement>()
    }

    single<UploadFile> {
        FileUploader(get())
    }
}

private fun Module.infra() {
    single<ClockService> {
        object : ClockService {
            override fun now() = DateTime.now()
        }
    }
}