package infra.squad.providers

import domain.squad.model.Pillar
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import infra.platform.FileSystemRepository
import domain.squad.spi.AssessmentRepository
import java.nio.file.Path

class InMemoryAssessmentRepository: AssessmentRepository {
    val items = mutableListOf<PillarDB>()

    override fun save(pillar: Pillar) {
        items.removeIf { it.id == pillar.id.value }
        items.add(pillar.toDB())
    }

    override fun find(`for squad`: ID<Squad>, named: Title<Pillar>): Pillar? {
        return items.map{ it.toDomain() }.firstOrNull { it.title == named && it.squad == `for squad` }
    }

    override fun find(id: ID<Pillar>): Pillar? {
        return items.map{ it.toDomain() }.firstOrNull { it.id == id }
    }

    override fun findCurrentAssessment(`for squad`: ID<Squad>): List<Pillar> {
        return items.map{ it.toDomain() }.filter { it.squad == `for squad` }
    }

}

class FileSystemAssessmentRepository(path: Path): AssessmentRepository, FileSystemRepository(path) {
    override fun save(pillar: Pillar) {
        TODO("Not yet implemented")
    }

    override fun find(`for squad`: ID<Squad>, named: Title<Pillar>): Pillar? {
        TODO("Not yet implemented")
    }

    override fun find(id: ID<Pillar>): Pillar? {
        TODO("Not yet implemented")
    }

    override fun findCurrentAssessment(`for squad`: ID<Squad>): List<Pillar> {
        TODO("Not yet implemented")
    }

}