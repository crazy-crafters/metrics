package infra.squad.providers

import domain.sharedkernel.model.ID
import infra.platform.FileSystemRepository
import domain.squad.model.AttachedFile
import domain.squad.spi.AttachedFileRepository
import java.nio.file.Path

class InMemoryAttachedFileRepository: AttachedFileRepository {

    val items = mutableListOf<AttachedFileDB>()

    override fun save(file: AttachedFile) {
        items.removeIf { it.descriptor.id == file.descriptor.id.value }
        items.add(file.toAttachedFileDB())
    }

    override fun find(id: ID<AttachedFile>): AttachedFile? {
        return items.map { it.toDomain() }.firstOrNull { it.descriptor.id == id }
    }
}


class FileSystemAttachedFileRepository(path: Path): AttachedFileRepository, FileSystemRepository(path) {
    override fun save(file: AttachedFile) {
        writeAll(
            readAll<AttachedFileDB>().filter { it.descriptor.id == file.descriptor.id.value }
            + file.toAttachedFileDB()
        )
    }

    override fun find(id: ID<AttachedFile>): AttachedFile? {
        return readAll<AttachedFileDB>().firstOrNull { it.descriptor.id == id.value }?.toDomain()
    }

}