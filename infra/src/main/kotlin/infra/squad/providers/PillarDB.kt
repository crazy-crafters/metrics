package infra.squad.providers

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.squad.model.Pillar
import domain.squad.model.Score


data class PillarDB(val id: String,
                    val squad: String,
                    val title: String,
                    val description: String,
                    val value: Int,
                    val maximum: Int,
                    val notes: List<PillarNoteDB> = emptyList()) {
    fun toDomain() = Pillar(
        ID.from(id),
        ID.from(squad),
        Title.from(title),
        description,
        Score(value, maximum),
        notes.map { it.toDomain() }
    )
}


fun Pillar.toDB() = PillarDB(
    id.toString(),
    squad.toString(),
    title.toString(),
    description,
    score.value,
    score.maximum,
    notes.map { it.toDB() }
)