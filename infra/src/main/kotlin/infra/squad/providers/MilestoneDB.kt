package infra.squad.providers

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.squad.MilestoneType
import domain.squad.model.Milestone
import domain.sharedkernel.model.DateTime


data class MilestoneDB(
    val squad: String,
    val title: String,
    val type: MilestoneType,
    val description: String,
    val date: String
) {
    fun toDomain() = Milestone(
        ID.from(squad),
        Title.from(title),
        type,
        description,
        DateTime.parse(date)
    )
}

fun Milestone.toDB() = MilestoneDB(
    squad.toString(),
    title.toString(),
    type,
    description,
    date.toString()
)