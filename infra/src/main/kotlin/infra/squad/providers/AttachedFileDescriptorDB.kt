package infra.squad.providers

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.model.AttachedFileDescriptor


data class AttachedFileDescriptorDB(
    val id: String,
    val name: String
) {
    fun toDomain() = AttachedFileDescriptor(
        ID.from(id),
        Name.from(name)
    )
}

fun AttachedFileDescriptor.toAttachedFileDescriptorDB() =
    AttachedFileDescriptorDB(
        id.toString(),
        name.toString()
    )