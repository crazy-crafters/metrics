package infra.squad.providers

import infra.platform.FileSystemRepository
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID
import domain.squad.model.Member
import domain.squad.spi.MemberRepository
import java.nio.file.Path

class FileSystemMemberRepository(path: Path): MemberRepository, FileSystemRepository(path) {

    override fun findBy(id: ID<Member>): Member? {
        return findAll().firstOrNull { it.id == id }
    }

    override fun findByEmail(email: Email): Member? {
        return findAll().firstOrNull { it.email == email }
    }

    override fun save(newMember: Member) {
        writeAll<MemberDB>(readAll<MemberDB>().filter { it.id == newMember.id.value } + newMember.toDB())
    }

    override fun findAll(): List<Member> = readAll<MemberDB>().map { it.toDomain() }
}