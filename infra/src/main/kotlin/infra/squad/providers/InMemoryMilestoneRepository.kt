package infra.squad.providers

import domain.squad.model.Milestone
import domain.sharedkernel.model.ID
import infra.platform.FileSystemRepository
import domain.squad.spi.MilestoneRepository
import java.nio.file.Path

class InMemoryMilestoneRepository: MilestoneRepository {
    val items = mutableListOf<MilestoneDB>()

    fun add(e: List<Milestone>) {
        e.forEach { add(it) }
    }
    fun add(e: Milestone) {
        items.add(e.toDB())
    }

    override fun findAll(id: ID<*>) = items.map { it.toDomain() }.filter { it.squad == id }
    override fun save(milestone: Milestone) {
        add(milestone)
    }
}


class FileSystemMilestoneRepository(path: Path): MilestoneRepository, FileSystemRepository(path) {
    fun add(e: List<Milestone>) {
        e.forEach { add(it) }
    }

    fun add(e: Milestone) {
        writeAll(readAll<MilestoneDB>() + e.toDB())
    }

    override fun findAll(id: ID<*>) = readAll<MilestoneDB>().map { it.toDomain() }.filter { it.squad == id }
    override fun save(milestone: Milestone) {
        add(milestone)
    }
}
