package infra.squad.providers

import infra.platform.FileSystemRepository
import domain.sharedkernel.model.ID
import domain.squad.model.Squad
import domain.squad.spi.SquadRepository
import java.nio.file.Path

class InMemorySquadRepository: SquadRepository {
    val items = mutableListOf<SquadDB>()
    override fun find(squad: ID<Squad>): Squad? {
        return items.firstOrNull { it.id == squad.value }?.toDomain()
    }

    override fun save(squad: Squad) {
        items.removeIf { it.id == squad.id.value }
        items.add(squad.toDB())
    }

    override fun findAll() = items.map{it.toDomain() }
}

class FileSystemSquadRepository(path: Path): SquadRepository, FileSystemRepository(path) {
    override fun find(squad: ID<Squad>): Squad? {
        return findAll().firstOrNull { it.id == squad }
    }

    override fun save(squad: Squad) {
        val dbo = squad.toDB()
        val all = (readAll<SquadDB>().filter { it.id != dbo.id } + dbo)
        writeAll(all)
    }

    override fun findAll() = readAll<SquadDB>().map{it.toDomain() }
}