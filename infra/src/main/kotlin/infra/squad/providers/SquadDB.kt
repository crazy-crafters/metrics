package infra.squad.providers

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.model.Squad

data class SquadDB(
    val id: String,
    val name: String,
    val members: List<String>
) {
    fun toDomain() = Squad(
        ID(id),
        Name.from(name),
        members.map { ID.from(it) }
    )
}

fun Squad.toDB() = SquadDB(
    id.toString(),
    name.toString(),
    members.map { it.toString() }
)
