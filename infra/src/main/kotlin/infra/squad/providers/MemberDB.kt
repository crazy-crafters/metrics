package infra.squad.providers

import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.model.Member


data class MemberDB(val id: String, val name: String, val email: String) {
    fun toDomain() = Member(
        ID(id),
        Name.from(name),
        Email.from(email)
    )
}

fun Member.toDB() = MemberDB(
    id.toString(),
    name.value,
    email.value
)