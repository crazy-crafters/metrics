package infra.squad.providers

import domain.squad.model.AttachedFile

data class AttachedFileDB(
    val descriptor: AttachedFileDescriptorDB,
    val content: ByteArray
) {
    fun toDomain() = AttachedFile(
        descriptor.toDomain(),
        content
    )
}


fun AttachedFile.toAttachedFileDB() =
    AttachedFileDB(
        descriptor.toAttachedFileDescriptorDB(),
        content
    )