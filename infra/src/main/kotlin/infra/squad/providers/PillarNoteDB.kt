package infra.squad.providers

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.squad.model.PillarNote


data class PillarNoteDB(
    val id: String,
    val title: String,
    val description: String,
    val attachedFiles: List<AttachedFileDescriptorDB>
) {
    fun toDomain() = PillarNote(
        ID.from(id),
        Title.from(title),
        description,
        attachedFiles.map { it.toDomain() }
    )
}

fun PillarNote.toDB() =
    PillarNoteDB(
        id.toString(),
        title.toString(),
        description,
        attachedFiles.map { it.toAttachedFileDescriptorDB() }
    )
