package infra.squad.providers

import domain.squad.model.Member
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID
import domain.squad.spi.MemberRepository


class InMemoryMemberRepository: MemberRepository {
    val items = mutableListOf<MemberDB>()
    override fun findBy(id: ID<Member>): Member? {
        return findAll().firstOrNull { it.id == id }
    }

    override fun findByEmail(email: Email): Member? {
        return items.map { it.toDomain() }.firstOrNull { it.email == email }
    }

    override fun save(newMember: Member) {
        items.removeIf { it.id == newMember.id.value }
        items.add(newMember.toDB())
    }

    override fun findAll(): List<Member> = items.map { it.toDomain() }
}

