import io.ktor.client.engine.mock.*
import io.ktor.http.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.ClassDiscriminatorMode
import kotlinx.serialization.json.Json
import infra.teamwork.connectors.azure.dto.AzureRestResponse
import infra.teamwork.connectors.github.GithubRestResponse
import infra.teamwork.connectors.jira.dto.JiraRestResponse
import infra.teamwork.connectors.trello.dto.TrelloRestResponse

class MockedEngineWrapper {
    private val responses = mutableMapOf<String, String>()

    val engine = MockEngine { request ->
        val content = getResponse(request.url)
        respond(
            content = content.toByteArray(),
            status = HttpStatusCode.OK,
            headers = headersOf(HttpHeaders.ContentType, "application/json")
        )
    }

    private fun getResponse(url: Url): String {
        return responses[url.toString()] ?: TODO(url.toString())
    }

    @OptIn(ExperimentalSerializationApi::class)
    private val jsonPrinter = Json {
        prettyPrint = true
        classDiscriminatorMode = ClassDiscriminatorMode.NONE
    }

    fun add(url: String, response: Any) {
        val content = when (response) {
            is AzureRestResponse -> jsonPrinter.encodeToString(response)
            is JiraRestResponse -> jsonPrinter.encodeToString(response)
            is TrelloRestResponse -> jsonPrinter.encodeToString(response)
            is GithubRestResponse -> jsonPrinter.encodeToString(response)
            else -> TODO()
        }
        responses[url] = content
    }

    @Suppress("UNCHECKED_CAST")
    fun add(url: String, response: List<Any>) {
        val content = when {
            response.isEmpty() -> "[]"
            else -> when (response.first()) {
                is AzureRestResponse -> jsonPrinter.encodeToString(response as List<AzureRestResponse>)
                is JiraRestResponse -> jsonPrinter.encodeToString(response as List<JiraRestResponse>)
                is TrelloRestResponse -> jsonPrinter.encodeToString(response as List<TrelloRestResponse>)
                is GithubRestResponse -> jsonPrinter.encodeToString(response as List<GithubRestResponse>)
                else -> TODO()
            }
        }
        responses[url] = content
    }

    /*fun add(url: String, response: AzureRestResponse) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: List<AzureRestResponse>) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: TrelloRestResponse) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: List<TrelloRestResponse>) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: JiraRestResponse) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: List<JiraRestResponse>) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: GithubRestResponse) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }

    fun add(url: String, response: List<GithubRestResponse>) {
        val content = jsonPrinter.encodeToString(response)
        responses[url] = content
    }*/
}