import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.issue.model.Issue
import infra.teamwork.connectors.jira.dto.*

import domain.sharedkernel.model.ID


private fun String.toJiraIssue(): Pair<JiraIssue, List<JiraIssueChangelog>> {
    val rx = "Issue (\\d+): (.+) for (\\d+) points".toRegex()
    val result = rx.find(this)!!.groups.mapNotNull { it?.value }.drop(1)

    val id = result.first()
    val points = result.last()

    val rxChanges = "(.+) (\\d{2}-\\d{2})".toRegex()
    val changes = result[1].split(",").map { it.trim() }.map {
        val g = rxChanges.find(it)!!.groups.mapNotNull { it?.value }
        g[1] to "2024-${g[2]}T12:00:00"
    }

    val start = changes.first().second

    return JiraIssue(
        id = ID.from<Issue>(id).toString(),
        self = "foo/$id",
        key = "Issue $id",
        fields = JiraIssueFields(
            issuetype = JiraIssueType(
                id = "Story",
                name = "Story"
            ),
            summary = "Issue $id",
            status = JiraIssueStatus(
                id = "closed"
            ),
            created = start,
            customfield_10020 = listOf(
                JiraIssueCreationStatus(
                    name = "Issue $id",
                    state = "closed",
                    startDate = start,
                    endDate = "",
                    completeDate = "",
                )
            ),
            customfield_10016 = points.toInt(),
        )
    ) to
            changes.let {
                var previousStatus = ""
                it.map {
                    val o = JiraIssueChangelog(
                        id = "",
                        created = it.second,
                        items = listOf(
                            JiraIssueChangelogItem(
                                field = "status",
                                fieldId = "status",
                                from = previousStatus,
                                fromString = previousStatus,
                                to = it.first,
                                toString = it.first,
                            )
                        )
                    )
                    previousStatus = it.first
                    o
                }
            }
}

fun MockedEngineWrapper.buildJiraRestResponses(of: BoardConfiguration.Valid, values: String) {
    val engineWrapper = this
    val lines = values.trimIndent()
        .split("\n").filter { it.isNotBlank() }.toList()

    val sprints = mutableListOf<JiraSprint>()
    val issues = mutableListOf<JiraIssue>()

    lines.forEach {
        if (it.startsWith(" ")) {
            val (issue, changelog) = it.toJiraIssue()
            issues.add(issue)
            engineWrapper.add(
                "http://localhost/rest/api/3/issue/${issue.id}/changelog",
                JiraIssueChangelogResponse(
                    maxResults = changelog.count(),
                    startAt = 0,
                    total = changelog.count(),
                    values = changelog,
                )
            )
        } else {
            sprints.add(it.toJiraSprint(sprints.count()))
        }
    }

    engineWrapper.add("http://localhost/rest/api/3/status", emptyList<JiraStatus>())

    engineWrapper.add(
        "http://localhost/rest/agile/1.0/board/${of.board}/sprint", JiraSprintsResponse(
            maxResults = sprints.count(),
            startAt = 0,
            total = sprints.count(),
            isLast = true,
            values = sprints,
        )
    )

    engineWrapper.add(
        "http://localhost/rest/agile/1.0/board/${of.board}/issue",
        JiraIssuesResponse(
            maxResults = issues.count(),
            startAt = 0,
            total = issues.count(),
            issues = issues,
        )
    )
}


fun String.toJiraSprint(index: Int): JiraSprint {
    val (start, stop, state) = split(" - ", " ")
    return JiraSprint(
        id = index,
        self = "",
        state = state,
        name = "Sprint $index",
        startDate = "2024-${start}T00:00:00.000Z",
        endDate = "2024-${stop}T23:59:59.000Z",
        completeDate = if (state == "closed") "2024-${stop}T23:59:59.000Z" else null,
        createdDate = "2024-${start}T00:00:00.000Z",
        originBoardId = 0,
        goal = ""
    )
}