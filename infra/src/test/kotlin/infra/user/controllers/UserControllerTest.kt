package infra.user.controllers

import domain.project.model.Project
import io.kotest.assertions.fail
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.model.ID
import domain.user.HandleUserProfile
import domain.user.UserSettings
import domain.user.UserProfileHandler
import domain.user.doubles.UserSettingsRepositoryDouble

class UserControllerTest: ContextualizedTest<UserControllerTestContext>(UserControllerTestContext()) {

    @Test
    fun `save settings`() {
        given {
            `I am`("Grace")
        } `when` {
            `I save this default settings`(ID.from("apollo"))
        } then {
            `based on side effects` {
                `my settings should be`(
                    project = "apollo"
                )
            }
        }
    }

    @Test
    fun `get settings for the given user`() {
        given {
            `I am`("Grace", `my default project is` = "apollo")
        } `when` {
            `I get default settings`()
        } then {

        }
    }
}



class UserControllerTestContext: TestContext {
    var user: String? = null
    var userSettings: UserSettings? = null

    val repository: UserSettingsRepositoryDouble = UserSettingsRepositoryDouble()
    val controller: HandleUserProfile = UserProfileHandler(repository)

    fun `I am`(name: String, `my default project is`: String? = null) {
        user = name

        if (`my default project is` != null) {
            userSettings = UserSettings(ID.from(user!!), ID.from(`my default project is`))
        }

        if (userSettings != null)
            repository.save(userSettings!!)
    }

    fun `I save this default settings`(project: ID<Project>) {
        val userSettings = UserSettings(ID.from(user!!), project = project)
        controller.save(userSettings)
    }

    fun `I get default settings`() = controller.get(ID.from(user!!))

    fun `my settings should be`(project: String) {
        repository.items.firstOrNull { it.user.value == user }?.let {
            it.project.shouldBe(ID.from(project))
        }?: fail("expecting settings for $user. But no settings has been found")
    }
}