package infra.platform

import io.kotest.matchers.shouldBe
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID
import org.junit.jupiter.api.Test


private val now = DateTime.now()

class Foobar
class FoobarEvent(override val date: DateTime, override val id: ID<Foobar>): DomainEvent
class DomainEventBusTest {
    @Test
    fun `should register a listener`() {
        val eventBus = SimpleEventBus()
        lateinit var received: DomainEvent
        eventBus.subscribe { event -> received = event }

        val e = FoobarEvent(now, ID())
        eventBus.publish(e)

        received shouldBe e
    }
}