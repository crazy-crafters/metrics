package infra.teamwork.providers

import MockedEngineWrapper
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.doubles.utils.toDatetime
import domain.teamwork.issue.model.Issue
import domain.teamwork.status.model.Status
import infra.teamwork.providers.dbo.*

abstract class UpdateIterationsTestContext : TestContext {
    abstract val apollo: BoardConfiguration.Valid

    abstract fun `the iterations on board`(of: BoardConfiguration.Valid, values: String)

    protected val engineWrapper = MockedEngineWrapper()

    open fun issueId(from: String) = ID.from<Issue>(from)
    open fun initialStatus(): Status = Status.OutOfScope

    private fun parse(expected: String): List<TimelineEventDB> {
        val lines = expected.trimIndent().split("\n")

        val rx = "(\\d+-\\d+) (.+) (Added|Removed|Moved|Tagged)(.*)".toRegex()

        var previous: Status? = null
        val events: List<TimelineEventDB> = lines.map { line ->
            val (date, name, event, status) = rx.find(line)!!.groups.drop(1).map { (it?.value ?: "").trim() }
            previous = if (status.isNotBlank()) Status.Working(ID.from(status), Label.from(status)) else previous
            when (event) {
                "Added" -> IssueAddedDB(
                    date = "2024-${date}T12:00:00".toDatetime().toString(),
                    entityId = name.split(" ").last(),
                    project = "apollo",
                    status = initialStatus().id.value,
                    title = name,
                    complexity = 1
                )

                "Moved" -> IssueMovedDB(
                    date = "2024-${date}T12:00:00".toDatetime().toString(),
                    entityId = name.split(" ").last(),
                    project = "apollo",
                    newStatus = status
                )

                "Removed" -> IssueRemovedDB(
                    date = "2024-${date}T12:00:00".toDatetime().toString(),
                    entityId = name.split(" ").last(),
                    project = "apollo"
                )

                "Tagged" -> IssueTagAddedDB(
                    date = "2024-${date}T12:00:00".toDatetime().toString(),
                    entityId = name.split(" ").last(),
                    project = "apollo",
                    value = status
                )
                else -> TODO(event)
            }
        }
        return events
    }
}