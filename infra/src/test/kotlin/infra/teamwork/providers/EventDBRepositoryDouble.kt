package infra.teamwork.providers

import domain.teamwork.shared.events.Event
import domain.teamwork.shared.events.IssueAdded
import domain.teamwork.shared.events.IssueMoved
import domain.teamwork.shared.spi.EventRepository
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.teamwork.doubles.utils.toDatetime
import domain.teamwork.shared.events.IssueClosed
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Label
import domain.project.model.Project
import domain.teamwork.doubles.*
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Tags
import domain.teamwork.status.model.Status
import infra.teamwork.providers.dbo.TimelineEventDB
import java.util.*

class EventDBRepositoryDouble: EventRepository, EventDBRepository {
    val events = mutableListOf<TimelineEventDB>()

    fun events(of: ID<Project>): List<TimelineEventDB> {
        return events.filter { it.project == of.value }
    }

    override fun findAll(of: ID<Project>, issue: ID<Issue>): List<Event> {
        return all(of).filter { it.entityId == issue }
    }

    override fun all(of: ID<Project>): List<Event> {
        return events(of).map { it.toDomain(emptyList()) }
    }

    override fun save(event: TimelineEventDB) {
        events.add(event)
    }

    override fun clear(project: ID<Project>) {
        events.removeIf { it.project == project.value }
    }

    override fun saveAll(events: List<TimelineEventDB>) {
       this.events.addAll(events)
    }

    private fun String.toChangelog(): List<Pair<Status, DateTime>> {
        if (this.isBlank())
            return emptyList()
        return this.split(",").map {
            val (state, date) = it.trim().split( " ")
            val id = state.lowercase(Locale.getDefault()).replace("_", "").replace(" ", "")
            Status.of(id = ID.from(id), value = Label.from(state)) to "2024-${date}T12:00:00".toDatetime()
        }
    }

    fun contains(project: ID<Project>, s: String) {
        s.trimIndent().trim().split("\n").filter { it.isNotBlank() }
            .flatMap { line ->
                val (name, points, states) = line.split(":", "->").map { it.trim() }
                val changelog = states.toChangelog()
                listOf(
                    IssueAdded(
                        entityId = ID.from(name),
                        project = project,
                        title = Title.from(name),
                        date = changelog.firstOrNull()?.second ?: DateTime.MIN,
                        status = Status.Backlog,
                        estimation = PointsOfComplexity.of(points.replace(" points", "").toInt()),
                        tags = Tags.empty()
                    )
                ) +
                changelog.map {
                    if (it.first == Status.of(id = ID.from("cancelled"), Label.from("Cancelled"))) {
                        IssueClosed(
                            entityId = ID.from(name),
                            project = project,
                            date = it.second,
                        )
                    } else {
                        IssueMoved(
                            entityId = ID.from(name),
                            project = project,
                            date = it.second,
                            status = it.first
                        )
                    }
                }
            }.forEach {
                events.add(it.toDB())
            }
    }

    private fun String.toChangelogFromLogs(): List<Pair<Status, DateTime>> {
        if (this.isBlank())
            return emptyList()
        return this.split(",").map { it ->
            val (state, date) = it.trim().split( " ")
            val id = state.lowercase(Locale.getDefault()).replace("_", "").replace(" ", "")
            Status.of(id = ID.from(id), Label.from(state))to date.toDatetime()
        }
    }

    fun containsFromLogs(project: ID<Project>, s: String) {
        s.trimIndent().trim().split("\n").filter { it.isNotBlank() }
            .flatMap { line ->
                // Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03
                val (name, points, states) = line.split("=", "->").map { it.trim() }
                val changes = states.toChangelogFromLogs()
                listOf(
                    IssueAdded(
                        entityId = ID.from(name),
                        project = project,
                        title = Title.from(name),
                        date = changes.firstOrNull()?.second ?: DateTime.MIN,
                        status = Status.Backlog,
                        estimation = PointsOfComplexity.of(points.replace(" points", "").toInt()),
                        tags = Tags.empty()
                    )
                ) +
                        changes.map {
                            IssueMoved(
                                entityId = ID.from(name),
                                project = project,
                                date = it.second,
                                status = it.first
                            )
                        }
            }.forEach {
                events.add(it.toDB())
            }
    }
}

fun Status.Companion.of(id: ID<Status>, value: Label<Status>): Status {
    return when(id) {
        Status.OutOfScope.id -> Status.OutOfScope
        Backlog.id -> Backlog
        Planned.id -> Planned
        Ready.id -> Ready
        InProgress.id -> InProgress
        isDone.id -> isDone
        Closed.id -> Closed
        else -> Status.Undefined(label = value, id = id)
    }
}


/*val Status.Companion.Backlog: Status
    get() = Status.Waiting(label = Label.from("backlog"), id = ID.from("backlog"))
val Status.Companion.Created: Status
    get() = Status.Waiting(label = Label.from("Created"), id = ID.from("created"))
val Status.Companion.Ready: Status
    get() = Status.Waiting(label = Label.from("Ready"), id = ID.from("ready"))
val Status.Companion.InProgress: Status
    get() = Status.Working(label = Label.from("In Progress"), id = ID.from("inprogress"))
val Status.Companion.Done: Status
    get() = Status.Done(label = Label.from("Done"), id = ID.from("done"))
val Status.Companion.Closed: Status
    get() = Status.Done(label = Label.from("Closed"), id = ID.from("closed"))*/