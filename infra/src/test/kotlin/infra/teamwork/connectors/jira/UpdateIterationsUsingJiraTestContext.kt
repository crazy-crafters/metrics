package infra.teamwork.connectors.jira

import buildJiraRestResponses
import infra.teamwork.providers.UpdateIterationsTestContext
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login

class UpdateIterationsUsingJiraTestContext : UpdateIterationsTestContext() {

    override val apollo: BoardConfiguration.Valid
        get() = BoardConfiguration.Valid(
            project = ID.from("apollo"),
            type = BoardType.Jira,
            url = "http://localhost/rest",
            board = "apollo",
            login = Login("", ""),
        )


    override fun `the iterations on board`(of: BoardConfiguration.Valid, values: String) {
        engineWrapper.buildJiraRestResponses(of, values)
    }
}
/*fun buildRestResponses(of: BoardConfiguration.Valid, values: String) {
    val lines = values.trimIndent()
        .split("\n").filter { it.isNotBlank() }.toList()


    val sprints = mutableListOf<JiraSprint>()
    val issues = mutableListOf<JiraIssue>()

    lines.forEach {
        if (it.startsWith(" ")) {
            val (issue, changelog) = it.toJiraIssue()
            issues.add(issue)
            engineWrapper.add(
                "http://localhost/rest/api/3/issue/${issue.id}/changelog",
                        JiraIssueChangelogResponse(
                            maxResults = changelog.count(),
                            startAt = 0,
                            total = changelog.count(),
                            values = changelog,
                        )
            )
        } else {
            sprints.add(it.toJiraSprint(sprints.count()))
        }
    }

    engineWrapper.add("http://localhost/rest/api/3/status", emptyList<JiraStatus>())

    engineWrapper.add(
        "http://localhost/rest/agile/1.0/board/${of.board}/sprint", JiraSprintsResponse(
            maxResults = sprints.count(),
            startAt = 0,
            total = sprints.count(),
            isLast = true,
            values = sprints,
        )
    )

    engineWrapper.add(
        "http://localhost/rest/agile/1.0/board/${of.board}/issue",
                JiraIssuesResponse(
                    maxResults = issues.count(),
                    startAt = 0,
                    total = issues.count(),
                    issues = issues,
                )
    )
}*/
