package infra.teamwork.connectors.github

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login
import domain.teamwork.status.model.Status
import infra.teamwork.providers.UpdateIterationsTestContext

class UpdateIterationsUsingGithubTestContext : UpdateIterationsTestContext() {
    override val apollo: BoardConfiguration.Valid
        get() = BoardConfiguration.Valid(
            project = ID.from("apollo"),
            type = BoardType.Github,
            url = "http://localhost",
            board = "apollo",
            login = Login(user = "key", pass = "token"),
        )

    override fun initialStatus(): Status = Status.Working(ID.from("Open"), Label.from("Open"))

    override fun `the iterations on board`(of: BoardConfiguration.Valid, values: String) {
        val lines = values.trimIndent()
            .split("\n").filter { it.isNotBlank() }.toList()

        val issues: List<GithubIssue> = lines.mapNotNull {
            if (it.startsWith(" ")) {
                // Issue 1: Ready 01-01, InProgress 01-02, Done 01-03, Removed 01-04 for 1 points
                val (id, changes) = parse(it)

                val start = changes.first().second
                val end = changes.findLast { it.first == "Done" }?.second

                GithubIssue(
                    id = id.toLong(),
                    title = "Issue $id",
                    labels = emptyList(),
                    state = if (end == null) "closed" else "open",
                    createdAt = start,
                    updatedAt = start,
                    closedAt = end,
                    pullRequest = null,
                    body = "",
                    timelineUrl = "",
                    stateReason = if (end == "") null else "completed"
                )
            } else {
                null
            }
        }
        engineWrapper.add("http://localhost/key/apollo/issues?state=all&per_page=100", issues)
    }

    fun parse(line: String): Pair<String, List<Pair<String, String>>> {
        val rx = "Issue (\\d+): (.+) for (\\d+) points".toRegex()
        val result = rx.find(line)!!.groups.mapNotNull { it?.value }.drop(1)

        val id = result.first()

        val rxChanges = "(.+) (\\d{2}-\\d{2})".toRegex()
        val changes = result[1].split(",").map { it.trim() }.map {
            val g = rxChanges.find(it)!!.groups.mapNotNull { it?.value }
            g[1] to "2024-${g[2]}T12:00:00"
        }

        return id to changes
    }


    /*fun `the issues`(s: String) {
        val issues = s.trimIndent().split("\n").map {line ->
            val (name, e) = line.split(" : ")
            val events = e.split(", ").map { it.split(" on ").last() }
            val (start, closed) = if (events.count() == 2) events else events + null

            GithubIssue(
                id = name.split(" ").last().toLong(),
                title = name,
                labels = emptyList(),
                state = if (closed == null) "closed" else "open",
                createdAt = start!!,
                updatedAt = start,
                closedAt = closed,
                pullRequest = null,
                body = "",
                timelineUrl = "",
                stateReason = "completed"
            )
        }

        engineWrapper.add("http://localhost/rest/issues?state=all&per_page=100", issues)

    }*/
}

/*class UpdatFromGithubTest: ContextualizedTest<UpdateIterationsUsingGithubTestContext>(UpdateIterationsUsingGithubTestContext()) {
    @Test
    fun `should get all issues`() {
        given {
            `the issues`(
                """
                Issue 1 : created on 2024-01-01T12:00:00
                Issue 2 : created on 2024-01-02T13:00:00, done on 2024-01-03T18:00:00
                Issue 3 : created on 2024-01-02T13:00:00, done on 2024-01-03T18:00:00
                Issue 4 : created on 2024-01-02T13:00:00, done on 2024-01-03T18:00:00
                """
            )
        } `when` {
            `we update the iterations of`(apollo)
        } then {
            `the following events should be saved`(`for the project` = apollo,

            )
        }
    }
}*/