package infra.teamwork.connectors.azure

import io.kotest.matchers.collections.shouldContainAll
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Test
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.Updater
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login
import domain.teamwork.doubles.StatusRepositoryDouble
import MockedEngineWrapper
import infra.teamwork.connectors.azure.dto.*
import infra.teamwork.providers.EventDBRepositoryDouble
import infra.teamwork.providers.dbo.IssueTagAddedDB

class AzureConnectorTest {
    val conf: BoardConfiguration.Valid
        get() = BoardConfiguration.Valid(
            project = ID.from("apollo"),
            type = BoardType.Azuredevops,
            url = "http://localhost/rest",
            board = "apollo",
            login = Login("", ""),
        )

    @Test
    fun `get feature tags`() = runTest {
        val engineWrapper = MockedEngineWrapper()
        engineWrapper.add(
            "http://localhost/rest/_apis/wit/workItemTypes",
            AzureWorkItemTypeResponse(
                value = listOf(
                    AzureWorkItemType(
                        "foo",
                        states = listOf()
                    )
                )
            )
        )
        engineWrapper.add(
            "http://localhost/rest/_apis/wit/wiql?api-version=5.1",
            AzureAllWorkItemsResponse(
                workItems = listOf(
                    AzureAllWorkItemsResponse.WorkItem(
                        id = 1,
                        url = "http://localhost/rest/_apis/work/workitems/1"
                    )
                )
            )
        )
        engineWrapper.add(
            "http://localhost/rest/_apis/work/workitems/1",
            AzureWorkitemResponse(
                id = 1,
                fields = AzureWorkitemFields(
                    systemWorkItemType = "User Story",
                    systemCreatedDate = "2025-01-15T12:00:00Z",
                    systemTitle = "An US",
                    microsoftVstsSchedulingStoryPoints = 12.0,
                    microsoftVstsCommonValueArea = "value area",
                    systemTags = "tag"
                ),
                _links = WorkItemLinks(
                    workItemUpdates = AzureLink(href = "http://localhost/rest/_apis/work/workitems/1/updates"),
                ),
                url = ""
            )
        )

        engineWrapper.add(
            "http://localhost/rest/_apis/work/workitems/1/updates",
            AzureWorkitemUpdatesResponse(
                count = 1,
                value = listOf(
                    AzureWorkitemUpdatesValue(
                        id = 1,
                        workItemId = 1,
                        rev = 1,
                        revisedDate = "2025-01-16T12:00:00Z",
                        fields = null,
                        relations = AzureWorkitemUpdatesRelation(
                            added = listOf(
                                AzureWorkitemUpdatesRelationAttributes(
                                    rel = "System.LinkTypes.Hierarchy-Reverse",
                                    url = "http://localhost/rest/_apis/work/workitems/11",
                                )
                            )
                        ),
                        url = ""
                    )
                )
            )
        )

        engineWrapper.add(
            "http://localhost/rest/_apis/work/workitems/11",
            AzureWorkitemResponse(
                id =11,
                fields = AzureWorkitemFields(
                    systemWorkItemType = "Feature"
                ),
                url = "",
                _links = WorkItemLinks(
                    workItemUpdates = AzureLink(href = "http://localhost/rest/_apis/work/workitems/11/updates")
                )
            )
        )

        engineWrapper.add(
            "http://localhost/rest/_apis/wit/workitems/11/updates",
            AzureWorkitemUpdatesResponse(
                count = 1,
                value = listOf(
                    AzureWorkitemUpdatesValue(
                        id = 1,
                        workItemId = 1,
                        rev = 1,
                        revisedDate = "2025-01-16T12:00:00Z",
                        fields = null,
                        relations = AzureWorkitemUpdatesRelation(
                            added = listOf(
                                AzureWorkitemUpdatesRelationAttributes(
                                    rel = "System.LinkTypes.Hierarchy-Reverse",
                                    url = "http://localhost/rest/_apis/work/workitems/111",
                                )
                            )
                        ),
                        url = ""
                    )
                )
            )
        )

        engineWrapper.add(
            "http://localhost/rest/_apis/work/workitems/111",
            AzureWorkitemResponse(
                id =11,
                fields = AzureWorkitemFields(
                    systemWorkItemType = "Epic"
                ),
                url = "",
            )
        )



        val issueEventRepository = EventDBRepositoryDouble()
        val statusRepository = StatusRepositoryDouble()
        val updateDashboardFromAzure = HarvestDashboardFromAzure(
                engine = engineWrapper.engine,
                statusRepository = statusRepository,
                eventRepository = issueEventRepository
        )
        val updateDashboard = Updater()
            .register(`for board from` = BoardType.Azuredevops, use = updateDashboardFromAzure)

        updateDashboard(conf, {})

        issueEventRepository.events.shouldContainAll(
            IssueTagAddedDB("2025-01-16T12:00:00Z", entityId = "1", project = "apollo", "Feature:11"),
            IssueTagAddedDB("2025-01-16T12:00:00Z", entityId = "1", project = "apollo", "Epic:111")
        )
    }
}