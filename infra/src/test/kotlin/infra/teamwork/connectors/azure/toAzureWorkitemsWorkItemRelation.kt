package infra.teamwork.connectors.azure

import infra.teamwork.connectors.azure.dto.*


fun AzureWorkitemResponse.toAzureWorkitemsWorkItemRelation() =
    AzureWorkitemsWorkItemRelation(
        rel = "",
        source = null,
        target = AzureWorkitemsWorkItem(
            id = this.id,
            url = this.url
        ),
    )