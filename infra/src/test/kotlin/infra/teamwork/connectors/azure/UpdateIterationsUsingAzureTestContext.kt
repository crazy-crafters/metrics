package infra.teamwork.connectors.azure

import infra.teamwork.providers.UpdateIterationsTestContext
import infra.teamwork.connectors.azure.dto.*
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login

class UpdateIterationsUsingAzureTestContext : UpdateIterationsTestContext() {

    override val apollo: BoardConfiguration.Valid
        get() = BoardConfiguration.Valid(
            project = ID.from("apollo"),
            type = BoardType.Azuredevops,
            url = "http://localhost/rest",
            board = "apollo",
            login = Login("", ""),
        )

    override fun `the iterations on board`(of: BoardConfiguration.Valid, values: String) {
        val lines = values.trimIndent()
            .split("\n").filter { it.isNotBlank() }.toList()


        val sprints = mutableListOf<AzureIteration>()

        val workItemsBySprint = mutableMapOf<Int, MutableList<AzureWorkitemsWorkItemRelation>>()

        lines.forEach {
            if (it.startsWith(" ")) {
                val (issue, changelog) = it.toAzureWorkitem()
                workItemsBySprint.getOrDefault(sprints.count() - 1, mutableListOf()).add(
                    issue.toAzureWorkitemsWorkItemRelation()
                )
                engineWrapper.add(
                    "http://localhost/_apis/work/teamsettings/workitem/${issue.id}",
                    issue
                )
                engineWrapper.add(
                    "http://localhost/_apis/work/teamsettings/workitem/${issue.id}/updates",
                    changelog
                )
            } else {
                val (start, stop, state) = it.split(" - ", " ")

                val startDate = if (start == "null") {
                    null
                } else {
                    "2024-${start}T00:00:00"
                }

                val finishDate = if (stop == "null") {
                    null
                } else {
                    "2024-${stop}T23:59:59"
                }
                val iteration = AzureIteration(
                    id = sprints.count().toString(),
                    name = "Sprint ${sprints.count()}",
                    path = "",
                    url = "http://localhost/_apis/work/teamsettings/iterations/${sprints.count()}",
                    attributes = AzureIterationAttributes(
                        startDate = startDate,
                        finishDate = finishDate,
                        timeFrame = if (state == "closed") "past" else "current"
                    )
                ).apply {
                    engineWrapper.add(
                        "http://localhost/_apis/work/teamsettings/iterations/$id",
                        AzureIterationResponse(
                            _links = AzureIterationLinks(
                                workitems = AzureLink("http://localhost/_apis/work/teamsettings/iterations/$id/workitems")
                            )
                        )
                    )
                }
                workItemsBySprint[sprints.count()] = mutableListOf()
                sprints.add(iteration)
            }
        }

        workItemsBySprint.forEach { sprint, workitems ->
            engineWrapper.add(
                "http://localhost/_apis/work/teamsettings/iterations/$sprint/workitems",
                AzureWorkitemsResponse(
                    workItemRelations = workitems,
                    url = "http://localhost/_apis/work/teamsettings/iterations/$sprint/workitems",
                )
            )
        }

        engineWrapper.add(
            "http://localhost/rest/_apis/work/teamsettings/iterations",
            AzureIterationsResponse(
                value = sprints,
                count = sprints.count()
            )
        )

        engineWrapper.add(
            "http://localhost/rest/_apis/wit/workItemTypes",
            AzureWorkItemTypeResponse(
                value = listOf(
                    AzureWorkItemType(
                        "foo",
                            states = listOf()
                    )
                )
            )
        )
    }



    private fun String.toAzureWorkitem(): Pair<AzureWorkitemResponse, AzureWorkitemUpdatesResponse> {
        val rx = "Issue (\\d+): (.+) for (\\d+) points".toRegex()
        val result = rx.find(this)!!.groups.mapNotNull { it?.value }.drop(1)

        val id = issueId(from=result.first()).toString()
        val points = result.last()

        val rxChanges = "(.+) (\\d{2}-\\d{2})".toRegex()
        val changes = result[1].split(",").map { it.trim() }.map {
            val g = rxChanges.find(it)!!.groups.mapNotNull { it?.value }
            g[1] to "2024-${g[2]}T12:00:00"
        }

        val start = changes.first().second

        return AzureWorkitemResponse(
            id = id.toInt(),
            fields = AzureWorkitemFields(
                systemCreatedDate = start,
                systemTitle = "Issue $id",
                microsoftVstsSchedulingStoryPoints = points.toDouble(),
                systemWorkItemType = "User Story",
                microsoftVstsCommonValueArea = "Business",
                systemTags = "Urgent",
            ),
            _links = WorkItemLinks(
                self = AzureLink(href = ""),
                workItemUpdates = AzureLink(href = "http://localhost/_apis/work/teamsettings/workitem/$id/updates"),
                workItemRevisions = AzureLink(href = ""),
                workItemComments = AzureLink(href = ""),
                html = AzureLink(href = ""),
                workItemType = AzureLink(href = ""),
                fields = AzureLink(href = ""),
            ),
            url = "http://localhost/_apis/work/teamsettings/workitem/$id",
        ) to

                AzureWorkitemUpdatesResponse(
                    count = 0,
                    value = changes.mapIndexed{ index, (state, date) ->
                        AzureWorkitemUpdatesValue(
                            id = 0,
                            workItemId = 0,
                            rev = 0,
                            revisedDate = date,
                            fields = AzureWorkitemUpdatesFields(
                                systemState = AzureWorkitemUpdatesFieldValue(
                                    newValue = state,
                                    oldValue = if (index != 0) changes[index - 1].first else ""
                                ),
                                systemChangedDate = AzureWorkitemUpdatesFieldValue(
                                    oldValue = "",
                                    newValue = date
                                )
                            ),
                            url = ""
                        )
                    }
                )
    }
}