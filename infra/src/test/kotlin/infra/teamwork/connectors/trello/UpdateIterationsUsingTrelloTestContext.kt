package infra.teamwork.connectors.trello

import infra.teamwork.providers.UpdateIterationsTestContext
import infra.teamwork.connectors.trello.dto.*
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login
import domain.teamwork.issue.model.Issue
import java.util.*

class UpdateIterationsUsingTrelloTestContext : UpdateIterationsTestContext() {

    override fun issueId(from: String): ID<Issue> {
        return ID.from(UUID.nameUUIDFromBytes(from.toByteArray()))
    }

    override val apollo: BoardConfiguration.Valid
        get() = BoardConfiguration.Valid(
            project = ID.from("apollo"),
            type = BoardType.Trello,
            url = "http://localhost/rest",
            board = "apollo",
            login = Login("key", "token"),
        )

    override fun `the iterations on board`(of: BoardConfiguration.Valid, values: String) {
        buildTrelloRestResponses(of, values)
    }

    private fun buildTrelloRestResponses(of: BoardConfiguration.Valid, values: String) {
        val lines = values.trimIndent()
            .split("\n").filter { it.isNotBlank() }.toList()
        val cards = mutableListOf<TrelloCard>()

        val allActions = mutableListOf<TrelloAction>()
        lines.forEach {
            if (it.startsWith(" ")) {
                val (card, changelog) = it.toTrelloCard()
                cards.add(card)
                allActions.addAll(changelog)
                engineWrapper.add(
                    "http://localhost/cards/${card.id}/actions?filter=all&key=${of.login.user}&token=${of.login.pass}",
                    changelog
                )
            }
        }

        engineWrapper.add(
            "http://localhost/boards/${of.board}/cards?filter=all&key=${of.login.user}&token=${of.login.pass}",
            cards
        )


        engineWrapper.add(
            "http://localhost/boards/${of.board}/actions?filter=all&key=${of.login.user}&token=${of.login.pass}",
            allActions
        )
    }

    private fun String.toTrelloCard(): Pair<TrelloCard, List<TrelloAction>> {
        val rx = "Issue (\\d+): (.+) for (\\d+) points".toRegex()
        val result = rx.find(this)!!.groups.mapNotNull { it?.value }.drop(1)

        val id = result.first()
        val points = result.last()

        val rxChanges = "(.+) (\\d{2}-\\d{2})".toRegex()
        val changes = result[1].split(",").map { it.trim() }.map {
            val g = rxChanges.find(it)!!.groups.mapNotNull { it?.value }
            g[1] to "2024-${g[2]}T12:00:00"
        }

        return TrelloCard(
            id = issueId(from = id).toString(),
            closed = false,
            name = "Issue $id",
        ) to listOf(
            TrelloAction(
                id = id,
                type = "createCard",
                date = changes.first().second,
                data = TrelloActionData(
                    card = TrelloActionCard(
                        closed = null,
                        name = "Issue $id",
                        id = issueId(from = id).toString(),
                    ),
                    list = TrelloList(name = changes.first().first, id = changes.first().first),
                    listAfter = null,
                    board = null,
                    old = null
                )
            )
        ) + changes.map { (state, date) ->
            if (state == "Removed") {
                TrelloAction(
                    id = "",
                    type = "updateCard",
                    date = date,
                    data = TrelloActionData(
                        card = TrelloActionCard(
                            closed = true,
                            name = "Issue $id",
                            id = issueId(from = id).toString(),
                        ),
                        list = TrelloList(name = "Done", id = "Done"),
                        listAfter = null,
                        old = TrelloOld(
                            closed = true,
                            idList = null,
                            name = null
                        ),
                        board = null
                    )
                )
            } else {
                TrelloAction(
                    id = "",
                    type = "updateCard",
                    date = date,
                    data = TrelloActionData(
                        card = TrelloActionCard(
                            closed = null,
                            name = "Issue $id",
                            id = issueId(from = id).toString(),
                        ),
                        list = null,
                        listAfter = TrelloListDetails(
                            id = state,
                            name = state
                        ),
                        old = TrelloOld(
                            idList = "...",
                            name = null,
                            closed = null
                        ),
                        board = null
                    )
                )
            }
        }
    }
}