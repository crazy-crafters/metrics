

# Remontée des information azure sous forme de tag

- feature
- epic
- area
- type (bug, us, ...)
- tags

US crééer seule puis ratachée à une feature
US crée directement dans une feature

pareil pour feature et epique

```
Epic:1865 -> Feature:1870 -> US:1928
```

# Ajout d'une métric flow sur la proportion du tag <type>

ratio : type:bug, type:us, tag:dette

type:bug = 3/j
type:us = 1/j
tag:dette = 0.3/j


# Ajout de filtre de selection pour la commande report / flow

- date
- tag

```
report 'date >= 2024-10-01'
report 'date >= 2024-10-01 and tag == epic:432'
```

# Forecast avec deux filtres de selections

```
forecast --history 'date >= 2024-10-01' --selection 'date >= 2024-10-01 and tag == epic:432'
```

Cette commande calcule le forecast de la selection en utilisant l'historique du premmier filtre.


```
forecast --history 'date >= 2024-10-01'
forecast --history 'date >= 2024-10-01' --selection 'all'
```

Ces deux commandes sont identiques. Par defaut un filtre prend toutes les issues.

```
forecast --selection 'date >= 2024-10-01 and tag == epic:432'
forecast --history 'all' --selection 'date >= 2024-10-01 and tag == epic:432'
```

Ces deux commandes sont identiques. Par defaut un filtre prend toutes les issues.


# Reves

Avoir un DSL complet de selection et projections
```
report where date >= 2024-10-01 groupby epic:*
```
