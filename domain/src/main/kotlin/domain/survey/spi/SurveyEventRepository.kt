package domain.survey.spi

import domain.project.model.Project
import domain.survey.model.SurveyEvent
import domain.sharedkernel.model.ID

interface SurveyEventRepository {
    fun findAll(project: ID<Project>): List<SurveyEvent>
    fun save(event: SurveyEvent)
}