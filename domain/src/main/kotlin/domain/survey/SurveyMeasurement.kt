package domain.survey

import domain.project.model.Project
import domain.project.spi.ClockService
import domain.survey.api.GetSurvey
import domain.survey.api.HandleSurvey
import domain.survey.spi.SurveyEventRepository
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.survey.model.*
import java.time.temporal.ChronoUnit


class SurveyMeasurement(
    private val surveyEventProvider: SurveyEventRepository,
    val clock: ClockService
) : GetSurvey, HandleSurvey {

    override fun measured(of: ID<Project>, on: DateTime): Survey {
        val all = Survey.NPSSurvey.rebuildAll(surveyEventProvider.findAll(project = of))
        return all.minByOrNull {
            val u = it.from.until(on, ChronoUnit.SECONDS)
            when {
                u <= 0 -> Long.MAX_VALUE
                else -> u
            }
        } ?: Survey.Error.NoSurvey
    }

        override fun openSurvey(command: NewNPSSurvey): Survey {
            val lastEvent = surveyEventProvider.findAll(project = command.project).lastOrNull()
            if (lastEvent != null && lastEvent !is CloseSurvey) {
                return Survey.Error.OpeningOverlapping
            }

            val event = OpenSurvey(date = command.from, id = command.project)

            surveyEventProvider.save(event)

            return Survey.NPSSurvey.rebuild(event)
        }

        override fun closeSurvey(project: ID<Project>, on: DateTime): Survey {
            val lastSurvey = Survey.NPSSurvey.rebuildAll(surveyEventProvider.findAll(project = project)).lastOrNull()

            if (lastSurvey == null || !lastSurvey.isOpen) {
                return Survey.Error.Closed
            }

            val event = CloseSurvey(date = on, id = project)

            surveyEventProvider.save(event)

            return lastSurvey.with(event)
        }

        override fun registerVote(command: NPSVote): Survey {
            val lastSurvey =
                Survey.NPSSurvey.rebuildAll(surveyEventProvider.findAll(project = command.project)).lastOrNull()

            if (lastSurvey == null || !lastSurvey.isOpen) {
                return Survey.Error.Closed
            }

            val event =
                Vote(date = command.date, id = command.project, score = command.score)

            surveyEventProvider.save(event)

            return lastSurvey.with(event)
        }
    }