package domain.survey.api


import domain.project.model.Project
import domain.survey.model.Survey
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID

interface GetSurvey {
    fun measured(of: ID<Project>, on: DateTime): Survey
}