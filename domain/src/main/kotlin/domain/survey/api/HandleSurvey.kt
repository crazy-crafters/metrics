package domain.survey.api

import domain.project.model.Project
import domain.survey.model.NPSVote
import domain.survey.model.NewNPSSurvey
import domain.survey.model.Survey
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID

interface HandleSurvey {
    fun openSurvey(command: NewNPSSurvey): Survey
    fun closeSurvey(project: ID<Project>, on: DateTime): Survey
    fun registerVote(command: NPSVote): Survey
}