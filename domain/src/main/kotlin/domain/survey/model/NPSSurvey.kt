package domain.survey.model

import domain.project.model.Project
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID

sealed interface Survey {

    sealed interface Error: Survey {

        data object NoSurvey : Error
        data object Closed : Error
        data object OpeningOverlapping : Error
    }

    data class NPSSurvey(
        val project: ID<Project>,
        val from: DateTime,
        private val events: List<SurveyEvent>
    ) : Survey {

        data class Scores(
            val promoters: Int = 0,
            val passives: Int = 0,
            val detractors: Int = 0
        ) {
            operator fun plus(score: Int): Scores {
                return when {
                    score <= 6 -> copy(detractors = detractors + 1)
                    score <= 8 -> copy(passives = passives + 1)
                    else -> copy(promoters = promoters + 1)
                }
            }

            val total: Int = promoters + passives + detractors
            val value: Int = (((promoters.toDouble() / total) - (detractors.toDouble() / total)) * 100).toInt()
            val level: NPSLevel =
                when {
                    value < 0 -> NPSLevel.Poor
                    value < 30 -> NPSLevel.Average
                    value < 70 -> NPSLevel.Good
                    else -> NPSLevel.Excellent
                }
        }

        val to: DateTime
            get() {
                if (isOpen)
                    DateTime.INVALID
                return events.fold(DateTime.INVALID) { date, event ->
                    when (event) {
                        is OpenSurvey -> date
                        is Vote -> date
                        is CloseSurvey -> event.date
                    }
                }
            }

        val isOpen: Boolean
            get() = events.last() !is CloseSurvey

        val scores: Scores
            get() {
                return events.fold(Scores()) { scores, event ->
                    when (event) {
                        is OpenSurvey -> scores
                        is Vote -> scores + event.score
                        is CloseSurvey -> scores
                    }
                }
            }

        fun with(event: SurveyEvent) = this.copy(events = events + event)

        companion object {
            fun rebuild(vararg events: SurveyEvent) =
                rebuild(events.toList())
            fun rebuild(events: List<SurveyEvent>): Survey {
                if (events.isEmpty())
                    return Error.NoSurvey

                val sorted = events.sortedWith(
                    compareBy(
                        { it.date },
                        {
                            when (it) {
                                is OpenSurvey -> 1
                                is Vote -> 2
                                is CloseSurvey -> 3
                            }
                        })
                )
                val start = sorted.first()

                if (start !is OpenSurvey)
                    return Error.NoSurvey

                return NPSSurvey(start.id, start.date, events)
            }

            fun rebuildAll(vararg events: SurveyEvent) =
                rebuildAll(events.toList())
            fun rebuildAll(events: List<SurveyEvent>): List<NPSSurvey> {
                return events.fold(mutableListOf<MutableList<SurveyEvent>>()) { bySurvey, event ->
                    when (event) {
                        is CloseSurvey -> bySurvey.last().add(event)
                        is OpenSurvey -> bySurvey.add(mutableListOf(event))
                        is Vote -> bySurvey.last().add(event)
                    }
                    bySurvey
                }.mapNotNull { surveyEvents ->
                    when(val it =
                        rebuild(surveyEvents)) {
                        is NPSSurvey -> it
                        else -> null
                    }
                }
            }
        }
    }
}