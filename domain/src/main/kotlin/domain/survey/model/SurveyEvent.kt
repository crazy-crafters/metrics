package domain.survey.model

import domain.project.model.Project
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID

sealed interface SurveyEvent: DomainEvent {
    override val id: ID<Project>
}

data class OpenSurvey(override val date: DateTime,
                      override val id: ID<Project>
                     ): SurveyEvent
data class Vote(override val date: DateTime,
                override val id: ID<Project>,
                val score: Int
               ): SurveyEvent

data class CloseSurvey(override val date: DateTime,
                       override val id: ID<Project>
                      ): SurveyEvent