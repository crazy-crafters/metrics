package domain.survey.model

enum class NPSLevel {
    Poor,
    Average,
    Good,
    Excellent
}


