package domain.survey.model


import domain.project.model.Project
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.DateTime

data class NPSVote(
    val project: ID<Project>,
    val date: DateTime,
    val score: Int
)