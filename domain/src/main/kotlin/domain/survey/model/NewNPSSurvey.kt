package domain.survey.model

import domain.project.model.Project
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID


data class NewNPSSurvey(
    val project: ID<Project>,
    val from: DateTime,
)
