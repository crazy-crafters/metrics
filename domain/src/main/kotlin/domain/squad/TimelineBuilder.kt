package domain.squad

import domain.squad.api.BuildTimeline
import domain.squad.api.HandleMilestone
import domain.squad.model.commands.NewMilestoneCommand
import domain.squad.model.errors.SquadError
import domain.squad.model.events.MemberHasJoinedDomainEvent
import domain.squad.model.events.MemberHasLeftDomainEvent
import domain.squad.model.events.NewSquadCreatedDomainEvent
import domain.squad.model.events.SquadDomainEvent
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.squad.model.Milestone
import domain.squad.spi.MilestoneRepository
import domain.squad.model.Member
import domain.squad.model.Squad
import domain.sharedkernel.SystemEventsRepository
import domain.sharedkernel.model.DateTime


data class TimelineItem(
    val date: DateTime,
    val title: Title<Milestone>,
    val description: ItemDescription,
    val type: MilestoneType
    )



sealed interface ItemDescription
data class CompositionDescription(val joining: List<ID<Member>> = emptyList(),
                                  val leaving: List<ID<Member>> = emptyList()): ItemDescription {
    fun joining(member: ID<Member>) = this.copy(
        joining = this.joining + listOf(member)
    )

    fun leaving(member: ID<Member>) = this.copy(
        leaving = this.leaving + listOf(member)
    )
}

data class CustomDescription(val value: String): ItemDescription


class Timeline(of: ID<Squad>) {
    private val allItems = mutableListOf<TimelineItem>()
    private lateinit var dateStart: DateTime

    val squad = of
    val items: List<TimelineItem>
        get() = allItems.sortedBy { it.date }

    val startingOn: DateTime
        get() = dateStart

    fun add(event: SquadDomainEvent): Timeline {
        when(event) {
            is MemberHasJoinedDomainEvent -> join(event.date, event.member)
            is NewSquadCreatedDomainEvent -> dateStart =  event.date
            is MemberHasLeftDomainEvent -> leave(event.date, event.member)
        }

        return this
    }

    fun add(event: Milestone): Timeline {
        allItems.add(
            TimelineItem(
                date = event.date,
                title = event.title,
                description = CustomDescription(event.description),
                type = event.type
            )
        )
        return this
    }

    private fun join(date: DateTime, member: ID<Member>) {
        if(!allItems.update(MilestoneType.CompositionChange, date) { it.copy(description = (it.description as CompositionDescription).joining(member)) }) {
            allItems.add(squadCompositionChange(date, joining = listOf(member)))
        }
    }

    private fun leave(date: DateTime, member: ID<Member>) {
        if(!allItems.update(MilestoneType.CompositionChange, date) { it.copy(description = (it.description as CompositionDescription).leaving(member)) }) {
            allItems.add(squadCompositionChange(date, leaving = listOf(member)))
        }
    }

    private fun MutableList<TimelineItem>.update(type: MilestoneType, date: DateTime, f: (TimelineItem) -> TimelineItem): Boolean {
        val event = filter { it.type == type }.firstOrNull { it.date == date }

        if (event == null)
            return false

        replaceAll {
            if (it == event)
                f(event)
            else
                it
        }
        return true
    }
}

internal fun squadCompositionChange(date: DateTime, joining: List<ID<Member>> = emptyList(), leaving: List<ID<Member>> = emptyList()): TimelineItem {
    return TimelineItem(
        date = date,
        title = Title("Staffing"),
        description = CompositionDescription(joining = joining, leaving = leaving),
        type = MilestoneType.CompositionChange
    )
}


class TimelineBuilder(
    private val eventRepository: SystemEventsRepository,
    private val milestoneRepository: MilestoneRepository
): BuildTimeline, HandleMilestone {
    override fun getAll(of: ID<Squad>): Either<Timeline, SquadError> {
        var timeline = eventRepository.findAll(of).filterIsInstance<SquadDomainEvent>()
                    .fold(Timeline(of)) { timeline, event -> timeline.add(event) }
        timeline = milestoneRepository.findAll(of)
                    .fold(timeline) { timeline, milestone -> timeline.add(milestone) }
        return Either.success(timeline)
    }

    override fun declare(command: NewMilestoneCommand) {
        milestoneRepository.save(
            Milestone(
                squad = command.squad,
                title = command.title,
                description = command.description,
                date = command.date,
                type = command.type
            )
        )
    }
}