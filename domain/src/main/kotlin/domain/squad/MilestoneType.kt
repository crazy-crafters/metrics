package domain.squad

/*data class MilestoneType private constructor(val value: String) {
    val id: ID<MilestoneType> = ID(UUID.nameUUIDFromBytes(value.toByteArray()))

    override fun equals(other: Any?): Boolean {
        return other is MilestoneType && other.id == id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    companion object {
        val CompositionChange = MilestoneType("Composition changed")
        val Retrospective = MilestoneType("Retrospective")
        fun custom(value: String) = MilestoneType(value)
    }
}*/

enum class MilestoneType {
    CompositionChange,
    Retrospective,
    Meeting,
    Other
}