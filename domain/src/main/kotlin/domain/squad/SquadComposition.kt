package domain.squad

import domain.sharedkernel.EventBus
import domain.sharedkernel.model.ID
import domain.squad.api.ComposeSquad
import domain.squad.api.GetSquad
import domain.squad.model.Member
import domain.squad.model.Squad
import domain.squad.model.commands.NewSquadCommand
import domain.squad.model.events.MemberHasJoinedDomainEvent
import domain.squad.model.events.MemberHasLeftDomainEvent
import domain.squad.model.events.NewSquadCreatedDomainEvent
import domain.squad.spi.ClockService
import domain.squad.spi.SquadRepository
import domain.squad.model.SquadResult
import domain.squad.model.SquadResult.*
import domain.squad.model.SquadResult.Error.*


class SquadComposition(
    private val repository: SquadRepository,
    private val eventBus: EventBus,
    private val clockService: ClockService
) : ComposeSquad, GetSquad {
    override fun join(squad: ID<Squad>, member: ID<Member>): SquadResult {
        val existingSquad = repository.find(squad) ?: return NotFound

        existingSquad.join(member).let {
            return when (it) {
                is Error -> it
                is Ok -> {
                    val updated = it.value
                    eventBus.publish(MemberHasJoinedDomainEvent(updated.id, member = member, date = clockService.now()))
                    repository.save(existingSquad)
                    it
                }
            }
        }
    }

    override fun leave(squad: ID<Squad>, member: ID<Member>): SquadResult {
        val existingSquad = repository.find(squad) ?: return NotFound

        return existingSquad.leave(member).also {
            when (it) {
                is Error -> {}
                is Ok -> {
                    val updated = it.value
                    eventBus.publish(MemberHasLeftDomainEvent(updated.id, member = member, date = clockService.now()))
                    repository.save(existingSquad)
                }
            }
        }
    }

    override operator fun get(`identified by`: ID<Squad>): SquadResult =
        repository.find(`identified by`)?.let { Ok(it) }?: NotFound

    override fun all(): List<Squad> = repository.findAll().filter { it.isStillExisting() }

    override fun `described as`(command: NewSquadCommand): SquadResult {
        val squad = Squad(
            name = command.name,
            members = command.members
        )

        repository.save(squad)
        eventBus.publish(
            NewSquadCreatedDomainEvent(
                id = squad.id,
                name = squad.name,
                members = squad.members,
                date = clockService.now()
            )
        )

        return Ok(squad)
    }
}