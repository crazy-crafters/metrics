package domain.squad.model.errors

import domain.squad.model.Member
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name


sealed interface SquadError

data class SquadDoesNotExist(val squad: ID<Squad>): SquadError
data class MemberAlreadyJoinedTheTeam(val squad: ID<Squad>, val member: ID<Member>): SquadError
data class MemberIsNotPartOfTheSquad(val squad: ID<Squad>, val member: ID<Member>): SquadError
data class NewSquadIsEmptyError(val name: Name<Squad>): SquadError