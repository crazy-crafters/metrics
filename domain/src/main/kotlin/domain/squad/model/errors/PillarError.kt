package domain.squad.model.errors

import domain.squad.model.Pillar
import domain.squad.model.Score
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

sealed interface PillarError
data class PillarAlreadyExists(val squad: ID<Squad>, val title: Title<Pillar>): PillarError
data class PillarDoesNotExist(val id: ID<Pillar>): PillarError
data class PillarCannotBeEvaluated(val id: ID<Pillar>, val score: Score): PillarError
