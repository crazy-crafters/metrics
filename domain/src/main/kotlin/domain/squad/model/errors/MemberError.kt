package domain.squad.model.errors

import domain.squad.model.Member
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID

sealed interface MemberError
data class MemberAlreadyOnboarded(val email: Email): MemberError
data class MemberDoesNotExist(val id: ID<Member>): MemberError