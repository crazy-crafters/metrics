package domain.squad.model

import domain.squad.MilestoneType
import domain.sharedkernel.model.Title
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.DateTime

data class Milestone(
    val squad: ID<Squad>,
    val title: Title<Milestone>,
    val type: MilestoneType,
    val description: String,
    val date: DateTime
)