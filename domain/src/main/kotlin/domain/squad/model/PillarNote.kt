package domain.squad.model

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

class PillarNote(
    val id: ID<PillarNote>,
    val title: Title<PillarNote>,
    val description: String,
    val attachedFiles: List<AttachedFileDescriptor>
) {
    constructor(
        title: Title<PillarNote>,
        description: String,
        attachedFiles: List<AttachedFileDescriptor>): this(ID(), title, description, attachedFiles)

    override fun toString(): String {
        return "PillarNote(id=$id, title=$title, description='$description', attachedFiles=$attachedFiles)"
    }
}
