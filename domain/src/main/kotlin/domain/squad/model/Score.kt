package domain.squad.model

data class Score(val value: Int, val maximum: Int) {

    val valid: Boolean
        get() = value <= maximum

    companion object {
        fun NoScoreOn(maximum: Int) = Score(maximum + 1, maximum)
    }
}