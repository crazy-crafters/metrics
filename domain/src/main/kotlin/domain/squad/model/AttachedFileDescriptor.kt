package domain.squad.model

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name

class AttachedFileDescriptor(val id: ID<AttachedFile>, val name: Name<AttachedFile>) {
    constructor(name: Name<AttachedFile>): this(ID(), name)

    override fun equals(other: Any?): Boolean {
        return other is AttachedFileDescriptor && other.id == id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}