package domain.squad.model

import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name


data class Member(val id: ID<Member>,
                  val name: Name<Member>,
                  val email: Email
) {
    constructor(name: Name<Member>, email: Email): this(ID<Member>(), name, email)
}
