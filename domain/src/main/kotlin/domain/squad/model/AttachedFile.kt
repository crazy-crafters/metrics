package domain.squad.model

class AttachedFile(
    val descriptor: AttachedFileDescriptor,
    val content: ByteArray
) {
    override fun equals(other: Any?): Boolean {
        return other is AttachedFile && descriptor == other.descriptor
    }

    override fun hashCode(): Int {
        return descriptor.hashCode()
    }
}