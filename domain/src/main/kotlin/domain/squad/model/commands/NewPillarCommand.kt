package domain.squad.model.commands

import domain.squad.model.Pillar
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

data class NewPillarCommand(val squad: ID<Squad>,
                            val title: Title<Pillar>,
                            val description: String,
                            val scale: Int)
