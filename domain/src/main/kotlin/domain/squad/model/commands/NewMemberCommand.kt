package domain.squad.model.commands

import domain.squad.model.Member
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.Name

data class NewMemberCommand(
    val name: Name<Member>,
    val email: Email
)