package domain.squad.model.commands

import domain.squad.model.Member
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name

data class NewSquadCommand(
    val name: Name<Squad>,
    val members: List<ID<Member>>
)
