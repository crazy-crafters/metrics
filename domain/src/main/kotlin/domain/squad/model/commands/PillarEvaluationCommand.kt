package domain.squad.model.commands

import domain.squad.model.Pillar
import domain.sharedkernel.model.ID

data class PillarEvaluationCommand(val pillar: ID<Pillar>, val score: Int)