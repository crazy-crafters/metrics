package domain.squad.model.commands

import domain.squad.MilestoneType
import domain.squad.model.Milestone
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.sharedkernel.model.DateTime

data class NewMilestoneCommand(
    val title: Title<Milestone>,
    val description: String,
    val date: DateTime,
    val type: MilestoneType,
    val squad: ID<Squad>
)