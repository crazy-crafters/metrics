package domain.squad.model.commands

import domain.squad.model.AttachedFileDescriptor
import domain.squad.model.Pillar
import domain.squad.model.PillarNote
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

data class PillarNoteCommand(
    val pillar: ID<Pillar>,
    val title: Title<PillarNote>,
    val description: String,
    val files: List<AttachedFileDescriptor>)
