package domain.squad.model

import domain.squad.model.errors.PillarCannotBeEvaluated
import domain.squad.model.errors.PillarError
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

class Pillar(
    val id: ID<Pillar>,
    val squad: ID<Squad>,
    val title: Title<Pillar>,
    val description: String,
    val score: Score,
    val notes: List<PillarNote> = emptyList()
) {

    constructor(squad: ID<Squad>,
                title: Title<Pillar>,
                description: String,
                score: Score,
                allNotes: MutableList<PillarNote> = mutableListOf()): this(ID<Pillar>(), squad, title, description, score, allNotes)

    override fun equals(other: Any?): Boolean {
        return other is Pillar && id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun reevaluated(score: Int): Either<Pillar, PillarError> {
        val newScore = this.score.copy(value = score)
        if (!newScore.valid)
            return Either.fail(PillarCannotBeEvaluated(id, newScore))
        return Either.success(
            Pillar(
            id = id,
            squad = squad,
            title = title,
            description = description,
            score = newScore
        )
        )
    }

    fun adding(note: PillarNote): Pillar {
        return Pillar(
            id = id,
            squad = squad,
            title = title,
            description = description,
            score = score,
            notes = notes + note
        )
    }


}