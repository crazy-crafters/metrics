package domain.squad.model

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.model.SquadResult.*
import domain.squad.model.SquadResult.Error.*

sealed interface SquadResult {
    data class Ok(val value: Squad): SquadResult

    sealed interface Error : SquadResult {
        data object NotFound : Error
        data object MemberAlreadyJoined : Error
        data object MemberIsNotPartOfTheSquad : Error
    }
}


    class Squad(val id: ID<Squad>, val name: Name<Squad>) {
        constructor(id: ID<Squad>, name: Name<Squad>, members: List<ID<Member>>) : this(id, name) {
            squadComposition.addAll(members)
        }

        constructor(name: Name<Squad>, members: List<ID<Member>>) : this(ID<Squad>(), name, members)

        private val squadComposition = mutableListOf<ID<Member>>()
        val members: List<ID<Member>>
            get() = squadComposition


        override fun toString() = "Squad($id)"
        override fun equals(other: Any?) = other is Squad && this.id == other.id
        override fun hashCode() = id.hashCode()
        fun join(member: ID<Member>): SquadResult {
            if (squadComposition.contains(member)) {
                return MemberAlreadyJoined
            }
            squadComposition.add(member)
            return Ok(this)
        }

        fun leave(member: ID<Member>): SquadResult {
            if (!squadComposition.contains(member)) {
                return MemberIsNotPartOfTheSquad
            }
            squadComposition.remove(member)

            return Ok(this)
        }

        fun isStillExisting(): Boolean = squadComposition.isNotEmpty()
    }