package domain.squad.model.events


import domain.sharedkernel.model.DateTime
import domain.squad.model.Member
import domain.squad.model.Squad
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name

sealed interface SquadDomainEvent: DomainEvent {
    override val id: ID<Squad>
}

data class NewSquadCreatedDomainEvent(override val id: ID<Squad>, override val date: DateTime, val name: Name<Squad>, val members: List<ID<Member>>):
    SquadDomainEvent
data class MemberHasJoinedDomainEvent(override val id: ID<Squad>, override val date: DateTime, val member: ID<Member>):
    SquadDomainEvent
data class MemberHasLeftDomainEvent(override val id: ID<Squad>, override val date: DateTime, val member: ID<Member>):
    SquadDomainEvent
