package domain.squad.model.events

import domain.sharedkernel.model.DateTime
import domain.squad.model.Member
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name

data class NewMemberHasBeenOnboardedDomainEvent(override val id: ID<Member>, val name: Name<Member>, val email: Email, override val date: DateTime):
    DomainEvent
