package domain.squad.model.events

import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DomainEvent
import domain.squad.model.AttachedFile
import domain.squad.model.Pillar
import domain.squad.model.PillarNote
import domain.squad.model.Score
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

data class NewPillarDefinedEvent(override val id: ID<Pillar>, val title: Title<Pillar>, override val date: DateTime):
    DomainEvent

data class PillarHasBeenEvaluatedEvent(
    override val id: ID<Pillar>,
    val score: Score,
    override val date: DateTime
): DomainEvent


data class NoteHasBeenAddedEvent(
    override val id: ID<PillarNote>,
    val pillar: ID<Pillar>,
    val title: Title<PillarNote>,
    val files: List<ID<AttachedFile>>,
    override val date: DateTime
): DomainEvent