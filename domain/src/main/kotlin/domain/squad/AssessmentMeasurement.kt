package domain.squad

import domain.sharedkernel.EventBus
import domain.squad.api.CreatePillar
import domain.squad.api.EvaluatePillar
import domain.squad.api.GetAssessment
import domain.squad.model.Pillar
import domain.squad.model.PillarNote
import domain.squad.model.Score
import domain.squad.model.Squad
import domain.squad.model.commands.NewPillarCommand
import domain.squad.model.commands.PillarEvaluationCommand
import domain.squad.model.commands.PillarNoteCommand
import domain.squad.model.errors.PillarAlreadyExists
import domain.squad.model.errors.PillarDoesNotExist
import domain.squad.model.errors.PillarError
import domain.squad.model.events.NewPillarDefinedEvent
import domain.squad.model.events.NoteHasBeenAddedEvent
import domain.squad.model.events.PillarHasBeenEvaluatedEvent
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.squad.spi.AssessmentRepository
import domain.squad.spi.ClockService

class AssessmentMeasurement(
    private val repository: AssessmentRepository,
    private val bus: EventBus,
    private val clockService: ClockService
): CreatePillar, GetAssessment, EvaluatePillar {
    override fun execute(command: NewPillarCommand): Either<Pillar, PillarError> {

        if (repository.find(`for squad` = command.squad, named = command.title) != null) {
            return Either.fail(PillarAlreadyExists(command.squad, command.title))
        }

        val newPillar = Pillar(
            squad = command.squad,
            title = command.title,
            description = command.description,
            score = Score.NoScoreOn(command.scale)
        )
        repository.save(newPillar)
        bus.publish(
            NewPillarDefinedEvent(
                id = newPillar.id,
                title = newPillar.title,
                date = clockService.now()
            )
        )

        return Either.success(newPillar)
    }

    override fun assessment(`for the squad`: ID<Squad>): List<Pillar> {
        return repository.findCurrentAssessment(`for the squad`)
    }

    override fun byId(id: ID<Pillar>): Either<Pillar, PillarError> {
        val pillar = repository.find(id) ?: return Either.fail(PillarDoesNotExist(id))
        return Either.success(pillar)
    }

    override fun evaluate(command: PillarEvaluationCommand): Either<Pillar, PillarError> {
        val pillar = repository.find(command.pillar) ?: return Either.fail(
            PillarDoesNotExist(
                command.pillar
            )
        )

        return pillar.reevaluated(command.score).onSuccess {
            bus.publish(
                PillarHasBeenEvaluatedEvent(it.id, it.score, clockService.now())
            )
            repository.save(it)
        }
    }

    override fun addNote(command: PillarNoteCommand): Either<Pillar, PillarError> {
        val note = PillarNote(
            command.title,
            command.description,
            command.files
        )

        val pillar =
            (repository.find(command.pillar) ?: return Either.fail(PillarDoesNotExist(command.pillar)))
            .adding(note)

            bus.publish(
                NoteHasBeenAddedEvent(
                    id = note.id,
                    pillar = pillar.id,
                    title = note.title,
                    files = note.attachedFiles.map { it.id },
                    date = clockService.now()
                )
            )
            repository.save(pillar)

        return Either.success(pillar)
    }
}