package domain.squad

import domain.squad.api.GetMember
import domain.squad.api.OnboardMember
import domain.squad.model.Member
import domain.squad.model.commands.NewMemberCommand
import domain.squad.model.errors.MemberAlreadyOnboarded
import domain.squad.model.errors.MemberDoesNotExist
import domain.squad.model.errors.MemberError
import domain.sharedkernel.EventBus
import domain.squad.model.events.NewMemberHasBeenOnboardedDomainEvent
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.squad.spi.ClockService
import domain.squad.spi.MemberRepository

class StaffingManagement(
    private val memberRepository: MemberRepository,
    private val eventsBus: EventBus,
    private val clockService: ClockService
):
    OnboardMember,
    GetMember {

    override operator fun invoke(newMember: NewMemberCommand): Either<Member, MemberError> {
        if (memberRepository.findByEmail(newMember.email) != null)
            return Either.fail(MemberAlreadyOnboarded(newMember.email))

        val member = Member(
            name = newMember.name,
            email = newMember.email
        )

        memberRepository.save(member)
        eventsBus.publish(
            NewMemberHasBeenOnboardedDomainEvent(member.id, member.name, member.email, clockService.now())
        )

        return Either.success(member)
    }

    override fun all() = memberRepository.findAll().sortedBy { it.name }
    override fun by(id: ID<Member>): Either<Member, MemberError> {
        val member = memberRepository.findBy(id) ?: return Either.fail(MemberDoesNotExist(id))
        return Either.success(member)
    }
}
