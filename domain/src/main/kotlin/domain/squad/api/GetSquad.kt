package domain.squad.api

import domain.sharedkernel.model.ID
import domain.squad.model.Squad
import domain.squad.model.SquadResult

interface GetSquad {
    fun get(`identified by`: ID<Squad>): SquadResult
    fun all(): List<Squad>
}
