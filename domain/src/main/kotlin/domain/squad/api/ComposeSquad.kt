package domain.squad.api

import domain.sharedkernel.model.ID
import domain.squad.model.Member
import domain.squad.model.SquadResult
import domain.squad.model.Squad
import domain.squad.model.commands.NewSquadCommand

interface ComposeSquad {
    fun join(squad: ID<Squad>, member: ID<Member>): SquadResult
    fun leave(squad: ID<Squad>, member: ID<Member>): SquadResult
    fun `described as`(command: NewSquadCommand): SquadResult
}