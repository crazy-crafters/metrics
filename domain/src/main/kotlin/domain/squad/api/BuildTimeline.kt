package domain.squad.api

import domain.squad.Timeline
import domain.squad.model.Squad
import domain.squad.model.errors.SquadError
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID

interface BuildTimeline {
    fun getAll(of: ID<Squad>): Either<Timeline, SquadError>
}