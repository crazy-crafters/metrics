package domain.squad.api

import domain.squad.model.Pillar
import domain.squad.model.commands.PillarEvaluationCommand
import domain.squad.model.commands.PillarNoteCommand
import domain.squad.model.errors.PillarError
import domain.sharedkernel.model.Either

interface EvaluatePillar {
    fun evaluate(command: PillarEvaluationCommand): Either<Pillar, PillarError>
    fun addNote(pillarNoteCommand: PillarNoteCommand): Either<Pillar, PillarError>
}