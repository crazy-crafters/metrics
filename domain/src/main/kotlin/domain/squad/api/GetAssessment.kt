package domain.squad.api

import domain.squad.model.Pillar
import domain.squad.model.Squad
import domain.squad.model.errors.PillarError
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID

interface GetAssessment {
    fun assessment(`for the squad`: ID<Squad>): List<Pillar>
    fun byId(id: ID<Pillar>): Either<Pillar, PillarError>
}