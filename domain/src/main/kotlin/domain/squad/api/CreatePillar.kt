package domain.squad.api

import domain.squad.model.Pillar
import domain.squad.model.commands.NewPillarCommand
import domain.squad.model.errors.PillarError
import domain.sharedkernel.model.Either


interface CreatePillar {
    fun execute(command: NewPillarCommand): Either<Pillar, PillarError>
}