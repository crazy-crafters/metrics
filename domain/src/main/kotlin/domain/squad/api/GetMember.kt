package domain.squad.api

import domain.squad.model.errors.MemberError
import domain.squad.model.Member
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID

interface GetMember {
    fun all(): List<Member>
    fun by(id: ID<Member>): Either<Member, MemberError>
}