package domain.squad.api

import domain.squad.model.errors.MemberError
import domain.squad.model.commands.NewMemberCommand
import domain.squad.model.Member
import domain.sharedkernel.model.Either

interface OnboardMember {
    operator fun invoke(newMember: NewMemberCommand): Either<Member, MemberError>
}