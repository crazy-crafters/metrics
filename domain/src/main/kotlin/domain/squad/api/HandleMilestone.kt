package domain.squad.api

import domain.squad.model.commands.NewMilestoneCommand


interface HandleMilestone {
    fun declare(command: NewMilestoneCommand)
}