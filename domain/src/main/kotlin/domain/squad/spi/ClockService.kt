package domain.squad.spi

import domain.sharedkernel.model.DateTime

interface ClockService {
    fun now(): DateTime
}