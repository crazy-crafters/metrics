package domain.squad.spi

import domain.squad.model.AttachedFile
import domain.sharedkernel.model.ID

interface AttachedFileRepository {
    fun save(file: AttachedFile)
    fun find(id: ID<AttachedFile>): AttachedFile?
}

