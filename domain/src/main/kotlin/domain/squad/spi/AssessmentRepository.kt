package domain.squad.spi

import domain.squad.model.Pillar
import domain.squad.model.Squad
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

interface AssessmentRepository {
    fun save(pillar: Pillar)
    fun find(`for squad`: ID<Squad>, named: Title<Pillar>): Pillar?
    fun find(id: ID<Pillar>): Pillar?
    fun findCurrentAssessment(`for squad`: ID<Squad>): List<Pillar>
}