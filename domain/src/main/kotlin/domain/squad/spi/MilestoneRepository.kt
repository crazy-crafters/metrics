package domain.squad.spi

import domain.squad.model.Milestone
import domain.sharedkernel.model.ID

interface MilestoneRepository {
    fun findAll(id: ID<*>): List<Milestone>
    fun save(milestone: Milestone)
}