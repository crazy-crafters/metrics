package domain.squad.spi

import domain.squad.model.Member
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID

interface MemberRepository {
    fun findBy(id: ID<Member>): Member?
    fun findByEmail(email: Email): Member?
    fun save(newMember: Member)
    fun findAll(): List<Member>
}