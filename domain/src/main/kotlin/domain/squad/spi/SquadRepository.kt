package domain.squad.spi


import domain.sharedkernel.model.ID
import domain.squad.model.Squad

interface SquadRepository {
    fun find(squad: ID<Squad>): Squad?
    fun save(squad: Squad)
    fun findAll(): List<Squad>
}