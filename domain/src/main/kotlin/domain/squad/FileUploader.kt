package domain.squad

import domain.squad.model.AttachedFile
import domain.squad.model.AttachedFileDescriptor
import domain.sharedkernel.model.Name
import domain.squad.spi.AttachedFileRepository

data class NewAttachedFile(
    val name: Name<AttachedFile>,
    val content: ByteArray
)

interface UploadFile {
    fun upload(command: NewAttachedFile): AttachedFileDescriptor
}



class FileUploader(private val repository: AttachedFileRepository): UploadFile {
    override fun upload(command: NewAttachedFile): AttachedFileDescriptor {
        val f = AttachedFile(
            AttachedFileDescriptor(command.name),
            command.content
        )
        repository.save(f)

        return f.descriptor
    }
}