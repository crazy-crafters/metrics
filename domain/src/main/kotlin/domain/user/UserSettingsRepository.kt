package domain.user

import domain.sharedkernel.model.ID

interface UserSettingsRepository {
    fun save(userSettings: UserSettings)
    fun find(user: ID<User>): UserSettings?
}