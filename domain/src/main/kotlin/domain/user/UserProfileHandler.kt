package domain.user

import domain.sharedkernel.model.ID

class UserProfileHandler(val repository: UserSettingsRepository) : HandleUserProfile {
    override fun save(userSettings: UserSettings) {
        repository.save(userSettings)
    }

    override fun get(user: ID<User>): UserProfile {
        return repository.find(user)?.let { UserProfile.Found(it) } ?: UserProfile.NotFound
    }

}