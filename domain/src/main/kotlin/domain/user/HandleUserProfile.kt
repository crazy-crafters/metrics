package domain.user

import domain.sharedkernel.model.ID

interface HandleUserProfile {
    fun save(userSettings: UserSettings)
    fun get(user: ID<User>): UserProfile
}