package domain.user

import domain.project.model.Project
import domain.sharedkernel.model.ID

interface User

data class UserSettings(val user: ID<User>, val project: ID<Project>)

sealed interface UserProfile {
    data class Found(val settings: UserSettings): UserProfile
    data object NotFound: UserProfile
}
