package domain.statistics

import kotlin.math.ceil
import kotlin.math.sqrt

class Distribution private constructor(private val values: List<Long>) {
    companion object {
        fun empty(): Distribution = Distribution(listOf())

        fun on(values: Iterable<Long>) = Distribution(values.sorted().toList())

        fun monteCarlo(cycles: Long, generator: () -> Long): Distribution {
            return on((0..cycles).map { generator() })
        }
    }

    val size: Int
        get() = values.size

    fun at(probability: Double): Long? {
        if (values.isNotEmpty()) {
            val index = ceil((values.size - 1) * probability).toInt();
            return values[index]
        }
        return null;
    }

    fun combine(other: Distribution): Distribution {
        return on(values + other.values)
    }

    fun min(): Long? = values.minOrNull()
    fun max(): Long? = values.maxOrNull()
    fun average(): Double? {
        if (values.isNotEmpty()) {
            return values.average()
        }
        return null;
    }

    fun median(): Long? {
        if (values.isNotEmpty()) {
            return values[values.size / 2]
        }
        return null
    }

    fun deviation(): Double? {
        if (values.isNotEmpty()) {
            val a = average()!!
            return (values.map { v -> v * v }.average()) - (a * a)
        }
        return null;
    }

    fun standardDeviation(): Double? {
        return deviation()?.let { sqrt(it) }
    }

}