package domain.sharedkernel

import domain.sharedkernel.model.DomainEvent


typealias Listener = (DomainEvent) -> Unit

interface EventBus {
    fun publish(event: DomainEvent)
    fun subscribe(subscriber: Listener)
}