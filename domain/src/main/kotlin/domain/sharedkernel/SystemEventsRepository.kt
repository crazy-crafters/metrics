package domain.sharedkernel

import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID

interface SystemEventsRepository {
    fun findAll(id: ID<*>): List<DomainEvent>
}