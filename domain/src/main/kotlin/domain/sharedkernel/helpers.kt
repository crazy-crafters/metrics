package domain.sharedkernel

infix fun String.or(other: String) = listOf(this, other)
infix fun List<String>.or(other: String) = this + other
