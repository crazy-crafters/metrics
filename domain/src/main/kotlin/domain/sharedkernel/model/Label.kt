package domain.sharedkernel.model

class Label<T> private constructor(val value: String) : Comparable<Label<T>> {
    override fun compareTo(other: Label<T>): Int =
        value.compareTo(other.value, ignoreCase = true)

    override fun equals(other: Any?) =
        other is Label<*> && value.equals(other.value, ignoreCase = true)

    override fun hashCode(): Int {
        return value.lowercase().hashCode()
    }

    override fun toString(): String {
        return value
    }

    companion object {
        fun <T> from(label: String): Label<T> {
            assert(label.isNotBlank()) { "The label cannot be blank" }
            return Label(label)
        }
    }
}