package domain.sharedkernel.model

class Name<T> private constructor(val value: String) : Comparable<Name<T>> {
    override fun compareTo(other: Name<T>): Int =
        value.compareTo(other.value, ignoreCase = true)

    override fun equals(other: Any?) =
        other is Name<*> && value.equals(other.value, ignoreCase = true)

    override fun hashCode(): Int {
        return value.lowercase().hashCode()
    }

    override fun toString(): String {
        return value
    }

    companion object {
        fun <T> from(fullname: String): Name<T> {
            assert(fullname.isNotBlank()) { "The full name cannot be blank" }
            return Name(fullname)
        }
    }
}