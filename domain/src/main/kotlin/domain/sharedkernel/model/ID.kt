package domain.sharedkernel.model

import java.util.*
@JvmInline value class ID<out T>(val value: String) {

    constructor(id: UUID): this(id.toString())

    override fun toString() = value
    constructor(): this(UUID.randomUUID().toString())

    fun <U> remap(): ID<U> = from(this.value)

    companion object {
        fun <T> from(s: String) = ID<T>(s)
        fun <T> from(s: UUID) = ID<T>(s.toString())
        fun <T> invalid(): ID<T> = from<T>("00000000-0000-0000-0000-000000000000")
    }
}
