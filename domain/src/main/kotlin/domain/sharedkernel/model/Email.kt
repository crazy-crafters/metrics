package domain.sharedkernel.model

@ConsistentCopyVisibility
data class Email private constructor(val value: String) {
    override fun equals(other: Any?): Boolean {
        return other is Email && this.value.equals(other.value, ignoreCase = true)
    }

    override fun hashCode(): Int {
        return value.lowercase().hashCode()
    }

    companion object {
        const val MAX_SIZE = 254
        private val emailRx = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$".toRegex()
        fun from(value: String): Email {
            if(value.trim().length > MAX_SIZE)
                throw EmailSizeExceededException()
            if (emailRx.matches(value))
                return Email(value)
            throw MalFormedEmailException(value)
        }
    }
}


sealed class EmailContractException(message: String): RuntimeException(message)
class EmailSizeExceededException: EmailContractException("Email cannot exceed ${Email.MAX_SIZE} characters")
class MalFormedEmailException(val value: String): EmailContractException("The email address '$value' does not comply with the OWASP validation email structure")