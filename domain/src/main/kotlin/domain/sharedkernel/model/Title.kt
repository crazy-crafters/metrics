package domain.sharedkernel.model

import java.util.*

class Title<out T>(val value: String) {
    override fun equals(other: Any?): Boolean {
        return other is Title<*> && value.equals(other.value, ignoreCase = true)
    }

    override fun hashCode(): Int {
        return value.lowercase(Locale.getDefault()).hashCode()
    }

    override fun toString(): String {
        return value
    }

    companion object {
        fun <T> from(value: String) = Title<T>(value)
    }
}