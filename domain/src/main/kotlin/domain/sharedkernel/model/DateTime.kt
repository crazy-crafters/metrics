package domain.sharedkernel.model

import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoUnit
import java.util.*

data class DateTime (val value: Instant) : Comparable<DateTime> {
    val dayOfMonth: Int
        get() = value.atZone(ZoneOffset.UTC).dayOfMonth

    companion object {
        val MIN: DateTime = DateTime(Instant.MIN)
        val MAX: DateTime = DateTime(Instant.MAX)
        val INVALID: DateTime = MAX

        fun String.toDateTime() = DateTime.parse(this)

        fun parse(s: String): DateTime {
            return if (s.contains("T")) {
                tryParse(s)
            } else {
                tryParse("${s}T00:00:00")
            }
        }

        private fun tryParse(dateString: String): DateTime {
            val formats = listOf(
                DateTimeFormatterBuilder()
                    .appendPattern("yyyy-MM-dd'T'HH:mm:ss")
                    .optionalStart()
                    .appendPattern(".SSS")
                    .optionalEnd()
                    .optionalStart()
                    .appendPattern(".SS")
                    .optionalEnd()
                    .optionalStart()
                    .appendPattern(".S")
                    .optionalEnd()
                    .optionalStart()
                    .appendOffset("+HHmm", "Z")
                    .optionalEnd()
                    .toFormatter(),
                DateTimeFormatter.ISO_INSTANT,
                DateTimeFormatter.ISO_LOCAL_DATE_TIME,
                DateTimeFormatter.ISO_LOCAL_DATE,
                DateTimeFormatter.ISO_OFFSET_DATE_TIME
            )

            for (format in formats) {
                try {
                    val localDateTime = OffsetDateTime.parse(dateString, format)
                    return DateTime(localDateTime.toInstant())
                } catch (_: Exception) {
                }
                try {
                    val instant = Instant.parse(dateString)
                    return DateTime(instant)
                } catch (_: Exception) {
                }
            }

            try {
                val dt = LocalDateTime.parse(dateString)
                return DateTime(dt.toInstant(ZoneOffset.UTC))
            } catch (_: Exception) {
                throw IllegalArgumentException("Could not parse: $dateString")
            }

        }

        //fun now() = DateTime(Instant.now().truncatedTo(ChronoUnit.MICROS))
    }

    fun until(endInclusive: DateTime, interval: Duration): Sequence<DateTime> {
        return generateSequence(this) { current ->
            val next = current.plus(interval)
            if (next <= endInclusive) next else null
        }
    }

    fun until(endExclusive: DateTime, interval: ChronoUnit) =
        value.until(endExclusive.value, interval)


    fun plus(duration: Duration): DateTime {
        return DateTime(value.plus(duration))
    }

    override operator fun compareTo(other: DateTime): Int {
        return value.compareTo(other.value)
    }

    operator fun rangeTo(that: DateTime) = DateTimeRange(this, that)

    fun toDateString(): String = format("yyyy-MM-dd")

    fun isSameDay(other: DateTime): Boolean {
        val thisDate = LocalDate.ofInstant(this.value, ZoneOffset.UTC)
        val otherDate = LocalDate.ofInstant(other.value, ZoneOffset.UTC)
        return thisDate == otherDate
    }

    fun startOfDay(): DateTime {
        return DateTime(
            LocalDateTime.of(
                LocalDate.ofInstant(value, ZoneOffset.UTC),
                LocalTime.MIN
            ).toInstant(ZoneOffset.UTC)
        )
    }

    fun noon(): DateTime {
        return DateTime(
            LocalDateTime.of(
                LocalDate.ofInstant(value, ZoneOffset.UTC),
                LocalTime.NOON
            ).toInstant(ZoneOffset.UTC)
        )
    }

    fun endOfDay(): DateTime {
        return DateTime(
            LocalDateTime.of(
                LocalDate.ofInstant(value, ZoneOffset.UTC),
                LocalTime.MAX
            ).toInstant(ZoneOffset.UTC)
        )
    }

    fun format(s: String): String {
        if (value == Instant.MAX)
            return ""
        return LocalDateTime.ofInstant(value, ZoneOffset.UTC).format(
            DateTimeFormatter.ofPattern(
                s,
                Locale.US
            )
        )
    }

    operator fun inc(): DateTime {
        return plus(Duration.ofDays(1L))
    }
}


class DateTimeRange(
    override val start: DateTime,
    override val endInclusive: DateTime
) : ClosedRange<DateTime>, Iterable<DateTime> {

    override fun iterator(): Iterator<DateTime> {
        return DateTimeIterator(start, endInclusive)
    }
}


class DateTimeIterator(val start: DateTime, private val endInclusive: DateTime) :
    Iterator<DateTime> {

    private var initValue = start

    override fun hasNext(): Boolean {
        return initValue <= endInclusive
    }

    override fun next(): DateTime {
        if (initValue > endInclusive) {
            throw NoSuchElementException("No additional element available!")
        }
        return initValue++
    }
}