package domain.sharedkernel.model

interface DomainEvent {
    val date: DateTime
    val id: ID<*>
}