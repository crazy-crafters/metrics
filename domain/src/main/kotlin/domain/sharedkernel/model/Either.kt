package domain.sharedkernel.model

interface Either<out Val, out Err> {
    val isError: Boolean
    val isSuccess: Boolean

    fun <U> fold(onSuccess: (value: Val) -> U, onFailure: (error: Err) -> U): U
    fun <V, E> map(onSuccess: (value: Val) -> V, onFailure: (error: Err) -> E): Either<V, E>

    fun onSuccess(statement: (Val) -> Unit): Either<Val, Err>
    fun onFailure(statement: (Err) -> Unit): Either<Val, Err>

    fun orNull(): Val?

    companion object {
        fun <Val, Err> fail(value: Err): Error<Val, Err> {
            return Error(value)
        }

        fun <Val, Err> success(value: Val): Value<Val, Err> {
            return Value(value)
        }

        fun <Val, Err> catch(onException: (exception: RuntimeException) -> Err, f: () -> Val): Either<Val, Err> {
            return try {
                success(f())
            } catch (e: RuntimeException) {
                fail(onException(e))
            }
        }

        fun <Val, Err> conditionally(b: Boolean, ifTrue: () -> Val, ifFalse: () -> Err): Either<Val, Err> {
            return if (b)
                    success(ifTrue())
                  else
                      fail(ifFalse())
        }

        fun <Val, Err> notNull(f: () -> Val?, ifNull: () -> Err): Either<Val, Err> {
            return success(f() ?: return fail(ifNull()))
        }
    }

    @ConsistentCopyVisibility
    data class Error<Val, Err> internal constructor(internal val value: Err): Either<Val, Err> {
        override val isError = true
        override val isSuccess = false

        override fun <U> fold(onSuccess: (value: Val) -> U, onFailure: (error: Err) -> U): U {
            return onFailure(value)
        }

        override fun <V, E> map(onSuccess: (value: Val) -> V, onFailure: (error: Err) -> E): Either<V, E> {
            return fail(onFailure(this.value))
        }

        override fun onSuccess(statement: (Val) -> Unit): Either<Val, Err> {
            return this
        }

        override fun onFailure(statement: (Err) -> Unit): Either<Val, Err> {
            statement(value)
            return this
        }

        override fun orNull(): Val? = null

        override fun toString(): String {
            return "Error{$value}"
        }
    }

    @ConsistentCopyVisibility
    data class Value<Val, Err> internal constructor(internal val value: Val): Either<Val, Err> {
        override val isError = false
        override val isSuccess = true

        override fun <U> fold(onSuccess: (value: Val) -> U, onFailure: (error: Err) -> U): U {
            return onSuccess(value)
        }


        override fun <V, E> map(onSuccess: (value: Val) -> V, onFailure: (error: Err) -> E): Either<V, E> {
            return success(onSuccess(this.value))
        }

        override fun onSuccess(statement: (Val) -> Unit): Either<Val, Err> {
            statement(value)
            return this
        }

        override fun onFailure(statement: (Err) -> Unit): Either<Val, Err> {
            return this
        }

        override fun orNull(): Val? = value

        override fun toString(): String {
            return "Value{$value}"
        }

    }
}

fun <Val, Err, DV: Val, DE: Err> Either<DV, DE>.generify(): Either<Val, Err> {
    return this.fold(
            { Either.success(it) },
            { Either.fail(it) }
    )
}

fun <Val, Err, U, DE: Err> Either<Val, DE>.mapOnSuccess(onValue: (value: Val) -> Either<U, Err>): Either<U, Err> {
    return fold(
            {onValue(it).generify()},
            { Either.fail(it) }
    )
}

fun <U, E> List<Either<U, E>>.merge(): Either<List<U>, E> {
        return this.firstOrNull { it.isError }?.map(
                {listOf()}, // Cannot be reached
                {it}
        ) ?: Either.success(this.map { (it as Either.Value<U, E>).value })
}


fun <V, E> V.success() = Either.success<V, E>(this)
fun <V, E> E.failure() = Either.fail<V, E>(this)