package domain.teamwork.issue.model

import domain.sharedkernel.model.ID

sealed interface IssueError
data class NoReportOnOpenIssueError(val id: ID<Issue>): IssueError
data object NoIssueToCreateError : IssueError
data class NoIssueCreationEventError(val id: ID<Issue>): IssueError