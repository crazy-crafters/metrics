package domain.teamwork.issue.model

data class Workload(
    val points: PointsOfComplexity,
    val count: Int
) {
    val issues = mutableListOf<Issue>()
    constructor(issues: List<Issue>): this(
        issues.fold(PointsOfComplexity.of(0)) { acc, issue -> acc + issue.estimation },
        issues.count()
    ) {
        this.issues.addAll(issues)
    }
    operator fun minus(other: Workload) = Workload(
    this.points - other.points,
    this.count - other.count
    )
    operator fun plus(other: Workload) = Workload(
        this.points + other.points,
        this.count + other.count
    )
}