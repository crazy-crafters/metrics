package domain.teamwork.issue.model

import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.project.model.Project
import domain.teamwork.status.model.Status
import domain.teamwork.shared.events.*
import java.time.temporal.ChronoUnit
import kotlin.math.absoluteValue
import kotlin.math.max


class Issue private constructor(
    val id: ID<Issue>,
    val project: ID<Project>,
    val creationDate: DateTime,
    val events: List<IssueEvent>
) {
    val title: Title<Issue>
        get() = rebuildTitle()

    val estimation: PointsOfComplexity
        get() = rebuildEstimation()

    val tags: Tags
        get() = rebuildTags()

    val isClosed: Boolean
        get() = rebuildState(DateTime.MAX) == IssueState.Closed

    val leadTime: TimeDuration
        get() = leadTime(DateTime.MAX)

    val cycleTime: TimeDuration
        get() {
            val route = route().steps
            if (route.isEmpty()) {
                return TimeDuration.Invalid
            }
            return cycleTime(route.first().status, route.last().status)
        }

    val efficiency: Efficiency
        get() {
            if (!this.isClosed) {
                return Efficiency.Invalid
            }

            val waiting = Stopwatch()
            val working = Stopwatch()

            events.forEach { event ->
                when (event) {
                    is IssueAdded -> {
                        when (event.status) {
                            is Status.Waiting -> {
                                waiting.start(event.date)
                                working.stop(event.date)
                            }

                            is Status.Working -> {
                                working.start(event.date)
                                waiting.stop(event.date)
                            }

                            else -> {}
                        }
                    }

                    is IssueMoved -> {
                        when (event.status) {
                            is Status.Waiting -> {
                                waiting.start(event.date)
                                working.stop(event.date)
                            }

                            is Status.Planned -> {
                                if (working.isRunning()) {
                                    waiting.start(event.date)
                                    working.stop(event.date)
                                }
                            }

                            is Status.Working -> {
                                working.start(event.date)
                                waiting.stop(event.date)
                            }

                            is Status.Done -> {
                                waiting.stop(event.date)
                                working.stop(event.date)
                            }

                            else -> {}
                        }
                    }

                    is IssueClosed -> {
                        waiting.stop(event.date)
                        working.stop(event.date)
                    }

                    else -> {}
                }
            }
            val total = working.duration + waiting.duration
            if (total == TimeDuration.seconds(0))
                return Efficiency.of(100)
            return Efficiency.of(((working.duration / total) * 100.0).toInt())
        }


    fun apply(event: Event): Issue {
        if (event is IssueEvent)
            return Issue(id, project, creationDate, events + event)
        return this
    }

    fun status(on: DateTime): Status {
        return rebuildStatus(events.filter { it.date <= on })
    }

    fun isClosed(on: DateTime): Boolean {
        val s = rebuildStatus(on)
        return s is Status.Closed || s is Status.Done
    }

    fun isDone(on: DateTime) = rebuildStatus(on) is Status.Done
    fun isCancelled(on: DateTime) = !isDone(on) && isClosed(on)

    fun route() = rebuildRoute(DateTime.MAX)

    fun `is in iteration`(from: DateTime, to: DateTime): Boolean {
        return startsBefore(to) &&
                (!isClosed(from) ||
                        rebuildRoute(to).hasBeenOpenedBetween(from, to))
    }

    fun leadTime(on: DateTime): TimeDuration {
        var closed = false
        var done = false
        var date = DateTime.MAX

        events.filter { it.date <= on }.forEach { event ->
            when (event) {
                is IssueAdded -> {
                    closed = false
                }

                is IssueClosed -> {
                    closed = true
                    if (!done)
                        date = event.date
                }

                is IssueMoved -> {
                    if (event.status is Status.Done) {
                        done = true
                        if (!closed)
                            date = event.date
                    } else if (event.status is Status.Closed) {
                        closed = true
                        if (!done)
                            date = event.date
                    } else {
                        done = false
                    }
                }

                else -> {}
            }
        }

        if (date == DateTime.MAX)
            return TimeDuration.Invalid
        return date - creationDate
    }

    fun cycleTime(between: Status, and: Status): TimeDuration {

        val allChangeEvents = events.filterIsInstance<IssueMoved>()

        val firstA = allChangeEvents.firstOrNull { it.status == between }?.date
        val firstB = allChangeEvents.firstOrNull { it.status == and }?.date

        if (firstA == null || firstB == null)
            return TimeDuration.seconds(0)

        val lastA = allChangeEvents.last { it.status == between }.date
        val lastB = allChangeEvents.last { it.status == and }.date

        return TimeDuration.seconds(
            max(
                firstA.until(lastB, ChronoUnit.SECONDS),
                firstB.until(lastA, ChronoUnit.SECONDS)
            )
        )
    }


    private fun startsBefore(date: DateTime): Boolean {
        val firstStatus = events.first()
        return firstStatus.date <= date && status(date) != Status.OutOfScope
    }


    private fun rebuildTitle(): Title<Issue> {
        return events.fold(Title.from("")) { title, event ->
            when (event) {
                is IssueAdded -> event.title
                is IssueRenamed -> event.title
                else -> title
            }
        }
    }

    private fun rebuildStatus(on: DateTime) = rebuildStatus(events.filter { it.date <= on })
    private fun rebuildStatus(events: List<IssueEvent>): Status {
        return events.fold(Status.OutOfScope as Status) { status, event ->
            when (event) {
                is IssueAdded -> event.status
                is IssueMoved -> event.status
                is IssueClosed -> if (status is Status.Done) status else Status.Closed.default
                else -> status
            }
        }
    }

    private fun rebuildEstimation(): PointsOfComplexity {
        return events.fold(PointsOfComplexity.of(0)) { complexity, event ->
            when (event) {
                is IssueAdded -> event.estimation
                is IssueEstimated -> event.estimation
                else -> complexity
            }
        }
    }

    private fun rebuildTags(): Tags {
        return events.fold(Tags.empty()) { tags, event ->
            when (event) {
                is IssueTagAdded -> tags.add(event.tag)
                is IssueTagRemoved -> tags.remove(event.tag)
                else -> tags
            }
        }
    }

    private fun rebuildState(date: DateTime): IssueState {
        return rebuildRoute(date).state
    }

    override fun equals(other: Any?): Boolean {
        return other is Issue && id == other.id && project == other.project
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + project.hashCode()
        return result
    }

    override fun toString(): String {
        return "Issue($title)"
    }

    private fun rebuildRoute(date: DateTime): Route {

        if (events.count() { it.date <= date } == 0)
            return Route(emptyList(), IssueState.OutOfScope)

        var closed = false
        var done = false

        val steps = mutableListOf<StatusChange>()

        events.filter { it.date <= date }.forEach { event ->
            when (event) {
                is IssueAdded -> closed = false
                is IssueClosed -> closed = true
                is IssueMoved -> {
                    steps.add(StatusChange(date = event.date, status = event.status))
                    if (!closed) {
                        done = event.status is Status.Done || event.status is Status.Closed
                    }
                }

                else -> {}
            }
        }
        return Route(steps, if (done || closed) IssueState.Closed else IssueState.Open)
    }

    companion object {
        fun from(vararg events: IssueEvent) = from(events.toList())
        fun from(events: List<IssueEvent>): Either<Issue, IssueError> {
            if (events.isEmpty())
                return Either.fail(NoIssueToCreateError)

            val sorted = events.sortedWith(
                compareBy(
                    { it.date },
                    {
                        when (it) {
                            is IssueAdded -> 0
                            is IssueMoved -> 1
                            is IssueEstimated -> 2
                            is IssueRenamed -> 2
                            is IssueClosed -> 3
                            is IssueTagAdded -> 1
                            is IssueTagRemoved -> 1
                        }
                    })
            )
            val start = sorted.first()

            if (start !is IssueAdded)
                return Either.fail(NoIssueCreationEventError(start.entityId))

            return Either.success(
                Issue(
                    id = start.entityId,
                    project = start.project,
                    creationDate = start.date,
                    events = sorted
                )
            )
        }


        fun allFrom(events: List<Event>): List<Issue> {
            return events.filterIsInstance<IssueEvent>().groupBy { it.entityId to it.project }
                .mapNotNull { (_, events) ->
                    from(events).fold(
                        onSuccess = { it },
                        onFailure = { null }
                    )
                }
        }
    }
}

operator fun DateTime.minus(other: DateTime) =
    TimeDuration.seconds(other.until(this, ChronoUnit.SECONDS).absoluteValue)


