package domain.teamwork.issue.model

import domain.sharedkernel.model.DateTime

class Stopwatch private constructor(private var accumulated: TimeDuration, private var lastStart: DateTime) {
    constructor(): this(TimeDuration.seconds(0L), lastStart= DateTime.MAX)

    val duration: TimeDuration
        get() = accumulated

    fun start(date: DateTime) {
        if (isRunning()) return
        lastStart = date
    }

    fun stop(date: DateTime) {
        if (!isRunning()) return

        accumulated += TimeDuration.seconds((date - lastStart).seconds)
        lastStart = DateTime.MAX
    }

    fun isRunning() = lastStart != DateTime.MAX
}