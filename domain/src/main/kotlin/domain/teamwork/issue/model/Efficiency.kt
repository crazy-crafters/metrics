package domain.teamwork.issue.model

@JvmInline
value class Efficiency private constructor(val fractionOf100: Int) {

    override fun toString(): String = "$fractionOf100 %"

    companion object {
        fun of(value: Int) = if (value < 0) Efficiency(-1) else Efficiency(value)

        val Invalid = of(-1)
    }
}


fun List<Efficiency>.average() = this.map { it.fractionOf100 }.average().let { Efficiency.of(it.toInt()) }