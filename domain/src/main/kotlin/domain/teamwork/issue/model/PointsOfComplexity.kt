package domain.teamwork.issue.model

@JvmInline
value class PointsOfComplexity private constructor(val value: Int): Comparable<PointsOfComplexity> {
    operator fun plus(other: PointsOfComplexity): PointsOfComplexity = of(value + other.value)
    operator fun minus(other: PointsOfComplexity): PointsOfComplexity = of(value - other.value)
    companion object {
        fun of(points: Int) = PointsOfComplexity(points)
    }
    override fun compareTo(other: PointsOfComplexity) = this.value.compareTo(other.value)

    override fun toString() = when {
        value < 1 -> "$value point"
        else -> "$value points"
    }
}