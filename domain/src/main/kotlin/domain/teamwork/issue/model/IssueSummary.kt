package domain.teamwork.issue.model

import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

sealed interface IssueSummary {
    class Valid(issue: Issue) : IssueSummary {
        val id: ID<Issue> = issue.id
        val title: Title<Issue> = issue.title

        private val end: DateTime = issue.route().steps.last().date

        val leadTime: TimeDuration = issue.leadTime(end)

        val efficiency: Efficiency = issue.efficiency

        val timeline: Route = issue.route()
    }

    data object NoSummary : IssueSummary

    companion object {
        operator fun invoke(input: Either<Issue, IssueError>): IssueSummary =
            input.fold(
                onSuccess = {
                    when {
                        it.isClosed -> Valid(it)
                        else -> NoSummary
                    }
                },
                onFailure = { NoSummary }
            )
    }
}