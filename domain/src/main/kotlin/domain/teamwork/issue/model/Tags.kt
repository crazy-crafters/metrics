package domain.teamwork.issue.model

@ConsistentCopyVisibility
data class Tags private constructor(val tags: List<Tag>) {
    companion object {
        fun empty() : Tags = Tags(listOf())

        fun on(values: Iterable<Tag>) = Tags(values.toSet().sorted().toList())

        fun single(tag: Tag) = Tags(listOf(tag))
    }

    fun combine(other: Tags): Tags {
        return on(tags + other.tags)
    }

    fun add(tag: Tag): Tags {
        return on(tags + listOf(tag))
    }

    fun remove(tag: Tag): Tags {
        return on(tags.filter { it != tag })
    }
}