package domain.teamwork.issue.model

@ConsistentCopyVisibility
data class Tag private constructor(val value: String) : Comparable<Tag> {
    companion object {
        fun on(value: String) = Tag(value)

        fun on(key: String, value: String) = on("$key:$value")
    }

    override fun compareTo(other: Tag): Int {
        return value.compareTo(other.value)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Tag

        return value == other.value
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }
}