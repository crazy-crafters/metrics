package domain.teamwork.issue.model

enum class IssueState {
    Open,
    Closed,
    OutOfScope
}