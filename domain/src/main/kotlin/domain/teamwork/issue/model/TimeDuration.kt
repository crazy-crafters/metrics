package domain.teamwork.issue.model

import kotlin.math.absoluteValue



@JvmInline
value class TimeDuration private constructor(val seconds: Long): Comparable<TimeDuration> {
    val inDays: Long
        get() = seconds / (24 * 60 * 60L)

    operator fun plus(other: TimeDuration): TimeDuration = of(seconds + other.seconds)

    operator fun div(right: TimeDuration): Double {
        return this.seconds.toDouble() / right.seconds.toDouble()
    }

    override fun toString(): String {
        var u = seconds
        val days = (u / (24 * 3600))
        if (days != 0L) u %= (days * 24 * 3600)
        val hours = u / 3600
        if (hours != 0L) u %= (hours * 3600)
        val minutes = u / 60
        if (minutes != 0L) u %= (minutes * 60)

        if (days != 0L) {
            return "${days}d ${hours}h ${minutes}m ${u}s (${seconds}s)"
        }

        if (hours != 0L) {
            return "${hours}h ${minutes}m ${u}s (${seconds}s)"
        }

        if (minutes != 0L) {
            return "${minutes}m ${u}s (${seconds}s)"
        }

        return "${u}s"
    }

    companion object {
        private fun of(seconds: Long) = TimeDuration(seconds.absoluteValue)
        fun seconds(seconds: Long) = TimeDuration(seconds.absoluteValue)
        fun days(days: Long): TimeDuration {
            return of(days * 24 * 60 * 60L)
        }

        val Invalid = seconds(-1)
    }

    override fun compareTo(other: TimeDuration): Int {
        return this.seconds.compareTo(other.seconds)
    }
}

fun List<TimeDuration>.average() = map { it.seconds }.average().let { TimeDuration.seconds(it.toLong()) }