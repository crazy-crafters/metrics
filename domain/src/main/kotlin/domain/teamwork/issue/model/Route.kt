package domain.teamwork.issue.model

import domain.sharedkernel.model.DateTime
import domain.teamwork.status.model.Status

data class StatusChange(val date: DateTime, val status: Status)
data class Route(val steps: List<StatusChange>, val state: IssueState) {
    fun hasRework() = steps.map { it.status }.count() != steps.map { it.status }.distinct().count()
    fun hasBeenOpenedBetween(from: DateTime, to: DateTime): Boolean {
        return steps.filter { it.date in from..to }.any { it.status !is Status.Closed && it.status !is Status.Done }
    }
}