package domain.teamwork.issue.model

import domain.sharedkernel.model.DateTime

sealed interface Expression {
    fun evaluate(issue: Issue): Boolean

    data object Always : Expression {
        override fun evaluate(issue: Issue): Boolean = true
    }

    data object Never : Expression {
        override fun evaluate(issue: Issue): Boolean = false
    }

    class Not(private val child: Expression) : Expression {
        override fun evaluate(issue: Issue): Boolean = !child.evaluate(issue)
    }

    class And(private val left: Expression, private val right: Expression) : Expression {
        override fun evaluate(issue: Issue): Boolean = left.evaluate(issue) && right.evaluate(issue)

    }

    class Or(private val left: Expression, private val right: Expression) : Expression {
        override fun evaluate(issue: Issue): Boolean = left.evaluate(issue) || right.evaluate(issue)
    }

    class HasTag(private val tag: Tag) : Expression {
        override fun evaluate(issue: Issue): Boolean {
            return issue.tags.tags.contains(tag) // TODO: demeter!
        }
    }

    class After(private val date: DateTime) : Expression {
        override fun evaluate(issue: Issue): Boolean {
            return true // TODO
        }
    }

    class Before(private val date: DateTime) : Expression {
        override fun evaluate(issue: Issue): Boolean {
            return true // TODO
        }
    }
}