package domain.teamwork.issue.model

import domain.sharedkernel.model.DateTime

class Snapshot(val from: DateTime, val to: DateTime, val issues: List<Issue>)