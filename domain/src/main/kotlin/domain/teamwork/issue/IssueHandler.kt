package domain.teamwork.issue

import domain.project.model.Project
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.teamwork.issue.api.GetIssue
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.IssueSummary
import domain.teamwork.issue.model.Snapshot
import domain.teamwork.issue.model.Tag
import domain.teamwork.shared.events.IssueEvent
import domain.teamwork.shared.spi.EventRepository

class IssueHandler(val eventRepository: EventRepository): GetIssue {

    override fun snapshot(project: ID<Project>, from: DateTime, to: DateTime, tags: List<Tag>): Snapshot {
        val issues = getIssues(project, from, to, tags)
        return Snapshot(from, to, issues)
    }

    override fun summary(from: ID<Project>, of: ID<Issue>): IssueSummary {
        val events = eventRepository.findAll(from, of).filterIsInstance<IssueEvent>()
        return IssueSummary(Issue.from(events))
    }

    private fun getIssues(
        project: ID<Project>,
        from: DateTime,
        to: DateTime,
        tags: List<Tag>
    ): List<Issue> {
        val issues = Issue.allFrom(eventRepository.all(project))
            .filter { it.`is in iteration`(from, to) }
            .let {
                when {
                    tags.isEmpty() -> it
                    else -> it.filter { issue -> issue.tags.tags.containsAll(tags) }
                }
            }
        return issues
    }
}