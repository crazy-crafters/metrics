package domain.teamwork.issue.api

import domain.project.model.Project
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.IssueSummary
import domain.teamwork.issue.model.Snapshot
import domain.teamwork.issue.model.Tag

interface GetIssue {
    fun snapshot(project: ID<Project>, from: DateTime, to: DateTime, tags: List<Tag> = emptyList()): Snapshot
    fun summary(from: ID<Project>, of: ID<Issue>): IssueSummary
}