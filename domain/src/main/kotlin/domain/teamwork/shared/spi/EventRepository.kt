package domain.teamwork.shared.spi

import domain.project.model.Project
import domain.teamwork.shared.events.Event
import domain.sharedkernel.model.ID
import domain.teamwork.issue.model.Issue

interface EventRepository {
    fun all(of: ID<Project>): List<Event>
    fun findAll(of: ID<Project>, issue: ID<Issue>): List<Event>
    fun clear(project: ID<Project>)
}