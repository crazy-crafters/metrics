package domain.teamwork.shared.events


import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.project.model.Project
import domain.teamwork.status.model.Status
import domain.sharedkernel.model.DateTime
import domain.teamwork.issue.model.Tag
import domain.teamwork.issue.model.Tags

sealed interface IssueEvent: Event {
    override val entityId: ID<Issue>
}

data class IssueAdded(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>,
    val status: Status,
    val title: Title<Issue>,
    val estimation: PointsOfComplexity,
    val tags: Tags,
) : IssueEvent

data class IssueRenamed(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>,
    val title: Title<Issue>,
) : IssueEvent

data class IssueMoved(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>,
    val status: Status,
) : IssueEvent

data class IssueClosed(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>
) : IssueEvent


data class IssueEstimated(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>,
    val estimation: PointsOfComplexity,
) : IssueEvent

data class IssueTagAdded(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>,
    val tag: Tag,
) : IssueEvent

data class IssueTagRemoved(
    override val date: DateTime,
    override val entityId: ID<Issue>,
    override val project: ID<Project>,
    val tag: Tag,
) : IssueEvent
