package domain.teamwork.shared.events

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.teamwork.configuration.model.Board
import domain.project.model.Project
import domain.sharedkernel.model.DateTime

sealed interface BoardEvent: Event {
    override val entityId: ID<Board>
}

data class BoardCreated(
    override val date: DateTime,
    override val entityId: ID<Board>,
    override val project: ID<Project>,
    val title: Title<Board>,
) : BoardEvent

data class BoardRenamed(
    override val date: DateTime,
    override val entityId: ID<Board>,
    override val project: ID<Project>,
    val title: Title<Board>,
) : BoardEvent