package domain.teamwork.shared.events

import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.sharedkernel.model.DateTime


sealed interface Event {
    val entityId: ID<*>
    val date: DateTime
    val project: ID<Project>
}


