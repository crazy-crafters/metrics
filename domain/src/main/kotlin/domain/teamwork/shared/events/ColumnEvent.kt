package domain.teamwork.shared.events

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.teamwork.configuration.model.Column
import domain.project.model.Project
import domain.sharedkernel.model.DateTime

sealed interface ColumnEvent: Event {
    override val entityId: ID<Column>
}

data class ColumnAdded(
    override val date: DateTime,
    override val entityId: ID<Column>,
    override val project: ID<Project>,
    val title: Title<Column>,
) : ColumnEvent

data class ColumnRenamed(
    override val date: DateTime,
    override val entityId: ID<Column>,
    override val project: ID<Project>,
    val title: Title<Column>,
) : ColumnEvent

data class ColumnRemoved(
    override val date: DateTime,
    override val entityId: ID<Column>,
    override val project: ID<Project>,
) : ColumnEvent