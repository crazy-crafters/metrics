package domain.teamwork.shared.events

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.teamwork.configuration.Iteration
import domain.project.model.Project
import domain.sharedkernel.model.DateTime


sealed interface IterationEvent: Event {
    override val entityId: ID<Iteration>
}

data class IterationStarted(
    override val date: DateTime,
    override val entityId: ID<Iteration>,
    override val project: ID<Project>,
    val name: Name<Iteration>,
) : IterationEvent

data class IterationFinished(
    override val date: DateTime,
    override val entityId: ID<Iteration>,
    override val project: ID<Project>,
) : IterationEvent