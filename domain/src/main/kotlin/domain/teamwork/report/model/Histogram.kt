package domain.teamwork.report.model

import domain.teamwork.issue.model.TimeDuration

class Histogram(private val durations: List<TimeDuration>) {
    data class Value(val count: Int, val days: Int)

    val countByDuration: List<Value>
        get() = durations.groupBy { it.inDays.toInt() }
            .map { (_, value) -> Value(value.count(), value.first().inDays.toInt()) }
            .sortedBy { it.count }

    operator fun plus(right: Histogram): Histogram {
        return Histogram(durations + right.durations)
    }

    companion object {
        val Empty: Histogram = Histogram(emptyList())
    }


}