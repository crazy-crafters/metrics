package domain.teamwork.report.model

import domain.sharedkernel.model.DateTime
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Workload
import domain.teamwork.status.model.Status

sealed interface GeneralStatsReport {
    val from: DateTime
    val to: DateTime


    data class NotFound(
        override val from: DateTime,
        override val to: DateTime
    ) : GeneralStatsReport

    class Valid(
        override val from: DateTime,
        override val to: DateTime,
        potentialIssues: List<Issue>
    ) : GeneralStatsReport {

        val issues = potentialIssues.filter { it.`is in iteration`(from, to) }

        val unfinished: Workload
            get() = Workload(issues.filter { it.status(to) is Status.Working || it.status(to) is Status.Waiting /*&& it.status(to) != Status.Ready*/ })

        val opened: Workload
            get() = Workload(issues.filter { !it.isClosed(on = to) && it.status(on = to) != Status.OutOfScope })

        val done: Workload
            get() = Workload(issues.filter { it.isDone(to) })

        val closed: Workload
            get() = Workload(issues.filter { it.isClosed(on = to) })

        val velocity: PointsOfComplexity
            get() = issues.filter { it.isDone(to) }.map { it.estimation }
                .fold(PointsOfComplexity.of(0)) { velocity, estimation -> velocity + estimation }
    }

    companion object {
        operator fun invoke(
            from: DateTime,
            to: DateTime,
            potentialIssues: List<Issue>
        ) = Valid(from, to , potentialIssues)
    }
}
