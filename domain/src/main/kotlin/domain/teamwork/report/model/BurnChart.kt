package domain.teamwork.report.model

import domain.sharedkernel.model.DateTime
import domain.teamwork.issue.model.Workload

sealed interface BurnChart {
    data class Found (val values: List<Value>) : Iterable<Value>, BurnChart {
        constructor() : this(emptyList())

        fun with(date: DateTime, value: Workload) = Found(values + Value(date, value))
        override fun iterator() = values.iterator()
    }

    data object Empty: BurnChart {
        fun with(date: DateTime, value: Workload) = Found(listOf(Value(date, value)))
    }

    data class Value(val date: DateTime, val value: Workload)

}

