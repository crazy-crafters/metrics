package domain.teamwork.report.model

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.sharedkernel.model.DateTime
import domain.project.model.Project
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.Route

sealed interface ReviewReport {
    val from: DateTime
    val to: DateTime
    val project: ID<Project>

    class Valid(
        override val from: DateTime,
        override val to: DateTime,
        override val project: ID<Project>,
        potentialIssues: List<Issue>
    ) : ReviewReport {

        val withRework: List<IssueRework>
        val withoutRework: List<Issue>

        init {
            val group = potentialIssues
                .filter { it.`is in iteration`(from, to) }
                .map { it to it.route() }
                .groupBy { it.second.hasRework() }
            withRework = group[true]!!.map { IssueRework(it.first.id, it.first.title, it.second) }
            withoutRework = group[false]!!.map { it.first }
        }
    }

    data class NoReport(
        override val from: DateTime,
        override val to: DateTime,
        override val project: ID<Project>
    ) : ReviewReport

    companion object {
        operator fun invoke(
            from: DateTime,
            to: DateTime,
            project: ID<Project>,
            potentialIssues: List<Issue>
        ) = when {
            potentialIssues.isEmpty() -> NoReport(from, to, project)
            else -> Valid(from, to, project, potentialIssues)
        }
    }
}

data class IssueRework(val id: ID<Issue>, val title: Title<Issue>, val route: Route)
