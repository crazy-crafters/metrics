package domain.teamwork.report.model

import domain.sharedkernel.model.DateTime
import domain.teamwork.issue.model.*
import domain.teamwork.status.model.Status

class WIP(values: List<Int>) {
    val min = values.minOrNull() ?: 0
    val max = values.maxOrNull() ?: 0
    val average = values.average().let { if (it.isNaN()) 0.0 else it }
}


sealed interface FlowReport {
    val from: DateTime
    val to: DateTime

    class Valid(
        override val from: DateTime, override val to: DateTime, potentialIssues: List<Issue>
    ) : FlowReport {

        val issues = potentialIssues.filter { it.`is in iteration`(from, to) }

        fun efficiency(): Efficiency {
            return issues.map { it.efficiency }.filter { it != Efficiency.Invalid }.average()
        }

        fun leadTimes(): TimeDuration {
            return issues.filter { it.isDone(on = to) }.map { it.leadTime(to) }.filter { it != TimeDuration.Invalid }
                .average()
        }

        fun cancelLeadTimes(): TimeDuration {
            return issues.filter { it.isCancelled(on = to) }.map { it.leadTime(to) }
                .filter { it != TimeDuration.Invalid }.average()
        }

        fun wip(): WIP {
            val wip = (from..to).map { d ->
                issues.map { it.status(d) }.count { it is Status.Working/* || it is Status.Waiting*/ }
            }

            return WIP(wip)
        }

        fun estimateCompletion(): CompletionEstimation {
            val opened = Workload(issues.filter { !it.isClosed(on = to) })
            val done = Workload(issues.filter { it.isDone(to) })

            if (opened.count == 0) return CompletionEstimation(
                to,
                to
            ) // Everything is done
            if (done.count == 0) return CompletionEstimation(
                DateTime.MAX, DateTime.MAX
            ) // Nothing has been done, yet

            val durationInDays = (to - from).inDays
            val p0 = this.scope(on = from) // Stock at start
            val pI = this.scope(on = to) // remaining stocks at end
            val r = done

            return CompletionEstimation.of(from, durationInDays, p0, pI, r)
        }

        private fun scope(on: DateTime): Workload {
            return Workload(issues.filter {
                val s = it.status(on)
                s !is Status.OutOfScope && s !is Status.Closed
            })
        }
    }

    data class NoReport(override val from: DateTime, override val to: DateTime) :
        FlowReport

    companion object {
        operator fun invoke(from: DateTime, to: DateTime, potentialIssues: List<Issue>) = when {
            potentialIssues.isNotEmpty() -> Valid(from, to, potentialIssues)
            else -> NoReport(from, to)
        }
    }
}