package domain.teamwork.report.model

import domain.sharedkernel.model.DateTime
import domain.teamwork.issue.model.Workload
import java.time.Duration

data class CompletionEstimation(
    val `considering issues`: DateTime,
    val `considering complexity`: DateTime
) {
    companion object {
        fun of(from: DateTime, durationInDays: Long, p0: Workload, pI: Workload, r: Workload): CompletionEstimation {
            val inCount = ( durationInDays * p0.count ) / (r.count + p0.count - pI.count)
                if ((r.points.value + p0.points.value - pI.points.value) != 0) {
                    val inComplexity = (durationInDays * p0.points.value) / (r.points.value + p0.points.value - pI.points.value)
                    return CompletionEstimation(from.plus(Duration.ofDays(inCount)), from.plus(Duration.ofDays(inComplexity)))
                } else {
                    return CompletionEstimation(from.plus(Duration.ofDays(inCount)), DateTime.INVALID)
                }
        }
    }
}