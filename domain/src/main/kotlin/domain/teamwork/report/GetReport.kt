package domain.teamwork.report

import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.statistics.Distribution
import domain.teamwork.report.api.GetReport
import domain.teamwork.configuration.Iteration
import domain.project.model.Project
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Tag
import domain.teamwork.issue.model.Workload
import domain.teamwork.shared.events.Event
import domain.teamwork.shared.events.IssueEvent
import domain.teamwork.shared.events.IterationEvent
import domain.teamwork.report.model.*
import domain.teamwork.shared.spi.EventRepository
import domain.teamwork.status.model.Status
import java.time.Duration

class GetReport(
    private val eventRepository: EventRepository,
) : GetReport {
    override fun generalStats(of: ID<Project>, from: DateTime, to: DateTime, tags: List<Tag>): GeneralStatsReport {
        val issues = getIssues(of, from, to, tags)
        if (issues.isEmpty())
            return GeneralStatsReport.NotFound(from, to)

        return GeneralStatsReport.Valid(from, to, issues)
    }

    override fun flow(of: ID<Project>, from: DateTime, to: DateTime, tags: List<Tag>): FlowReport {
        val issues = getIssues(of, from, to, tags)
        return FlowReport(from, to, issues)
    }

    private fun getIssues(
        of: ID<Project>,
        from: DateTime,
        to: DateTime,
        tags: List<Tag>
    ): List<Issue> {
        val issues = eventRepository.all(of).buildIssues()
            .filter { it.`is in iteration`(from, to) }
            .let {
                when {
                    tags.isEmpty() -> it
                    else -> it.filter { issue -> issue.tags.tags.containsAll(tags) }
                }
            }
        return issues
    }

    override fun rework(of: ID<Project>, from: DateTime, to: DateTime): ReviewReport {
        val issues = eventRepository.all(of).buildIssues().filter { it.`is in iteration`(from, to) }
        return ReviewReport(from, to, of, issues)
    }

    override fun iterations(of: ID<Project>): List<Iteration> {
        return eventRepository.all(of).filter { it.project == of }.buildIterations()
    }

    override fun burnChart(
        project: ID<Project>,
        from: DateTime,
        to: DateTime
    ): BurnChart =
        when (val report = generalStats(project, from, to)) {
            is GeneralStatsReport.Valid -> from.until(to, Duration.ofDays(1)).toList()
                .fold(BurnChart.Found()) { chart, date ->
                    report.issues.filter { issue -> issue.status(on = date) is Status.Done }.let { issues ->
                        chart.with(
                            date,
                            Workload(
                                points = issues.map { it.estimation }.sum(),
                                count = issues.count()
                            )
                        )
                    }
                }
            is GeneralStatsReport.NotFound -> BurnChart.Empty
        }

    override fun leadTimes(of: ID<Project>, from: DateTime, to: DateTime): Distribution {
        val issues = eventRepository.all(of).buildIssues().filter { it.`is in iteration`(from, to) }
        if (issues.isEmpty())
            return Distribution.empty()

        val report = Distribution.on(issues.filter { it.`is in iteration`(from, to) }
            .map { it.leadTime.inDays })//Histogram(issues.filter{ it.`is in iteration`(from, to) }.map { it.leadTime })
        return report
    }
}


private fun List<Event>.buildIssues(): List<Issue> {
    return this.filterIsInstance<IssueEvent>().groupBy { it.entityId to it.project }
        .mapNotNull { (_, events) ->
            Issue.from(events).fold(
                onSuccess = { it },
                onFailure = { null }
            )
        }
}

private fun List<Event>.buildIterations(): List<Iteration> {
    return this.filterIsInstance<IterationEvent>().groupBy { it.entityId to it.project }
        .mapNotNull { (_, events) ->
            Iteration.from(events).fold(
                onSuccess = { it },
                onFailure = { null }
            )
        }
}

private fun Iterable<PointsOfComplexity>.sum(): PointsOfComplexity =
    fold(PointsOfComplexity.of(0)) { acc, value -> acc + value }