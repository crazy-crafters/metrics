package domain.teamwork.report.api

import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.statistics.Distribution
import domain.teamwork.configuration.Iteration
import domain.project.model.Project
import domain.teamwork.issue.model.Tag
import domain.teamwork.report.model.BurnChart
import domain.teamwork.report.model.FlowReport
import domain.teamwork.report.model.GeneralStatsReport
import domain.teamwork.report.model.ReviewReport

interface GetReport {
    fun generalStats(of: ID<Project>, from: DateTime, to: DateTime, tags: List<Tag> = emptyList()): GeneralStatsReport

    fun flow(of: ID<Project>, from: DateTime, to: DateTime, tags: List<Tag> = emptyList()): FlowReport
    fun rework(of: ID<Project>, from: DateTime, to: DateTime): ReviewReport

    fun iterations(of: ID<Project>): List<Iteration>
    fun burnChart(project: ID<Project>, from: DateTime, to: DateTime): BurnChart

    fun leadTimes(of: ID<Project>, from: DateTime, to: DateTime): Distribution
}