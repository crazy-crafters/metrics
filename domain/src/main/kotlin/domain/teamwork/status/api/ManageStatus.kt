package domain.teamwork.status.api

import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.model.ExistingStatus
import domain.teamwork.status.model.Status

interface ManageStatus {
    fun statuses(of: ID<Project>): List<Status>
    fun update(project: ID<Project>, id: ID<Status>, state: Status.State): ExistingStatus
}