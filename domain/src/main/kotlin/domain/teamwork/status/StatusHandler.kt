package domain.teamwork.status

import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.api.ManageStatus
import domain.teamwork.status.model.ExistingStatus
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository

class StatusHandler(
    private val statusRepository: StatusRepository
    ) : ManageStatus {
    override fun statuses(of: ID<Project>): List<Status> {
        return statusRepository.findAll(of)
    }

    override fun update(project: ID<Project>, id: ID<Status>, state: Status.State): ExistingStatus {
        val status = statusRepository.find(project, id) ?: return ExistingStatus.NotFound
        val u = when(state) {
            Status.State.Planned -> Status.Planned(status.id, status.label)
            Status.State.Waiting -> Status.Waiting(status.id, status.label)
            Status.State.Working -> Status.Working(status.id, status.label)
            Status.State.Done -> Status.Done(status.id, status.label)
            Status.State.Closed -> Status.Closed(status.id, status.label)
            else -> TODO()
        }
        statusRepository.save(project, u)
        return ExistingStatus.Found(u)
    }
}