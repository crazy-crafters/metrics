package domain.teamwork.status.model

data class Progression(val current: Int, val total: Int)