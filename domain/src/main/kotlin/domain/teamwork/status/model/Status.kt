package domain.teamwork.status.model

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label


sealed interface ExistingStatus {
    class Found(val status: Status) : ExistingStatus
    data object NotFound : ExistingStatus
}

sealed class Status private constructor(val id: ID<Status>, val label: Label<Status>) {
    enum class State {
        Undefined,
        Planned,
        Waiting,
        Working,
        Done,
        Closed,
        OutOfScope
    }

    infix fun or(other: Status) = listOf(this, other)

    override fun equals(other: Any?): Boolean {
        return other is Status && id == other.id
    }
    override fun hashCode() = id.hashCode()
    override fun toString() = "${this::class.simpleName}($label)"


    class Undefined(id: ID<Status>, label: Label<Status>) : Status(id, label)
    class Planned(id: ID<Status>, label: Label<Status>) : Status(id, label)
    class Waiting(id: ID<Status>, label: Label<Status>) : Status(id, label)
    class Working(id: ID<Status>, label: Label<Status>) : Status(id, label)
    class Done(id: ID<Status>, label: Label<Status>) : Status(id, label)
    class Closed(id: ID<Status>, label: Label<Status>) : Status(id, label) {
        companion object {
            val default = Closed(ID.from("id"), Label.from("Closed"))
        }
    }

    object OutOfScope: Status(ID.from("-"), Label.from("Out of scope"))

    companion object {
        operator fun invoke(f: String): Status {
            return OutOfScope
        }
    }
}