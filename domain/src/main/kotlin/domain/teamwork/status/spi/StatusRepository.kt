package domain.teamwork.status.spi

import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.model.Status

interface StatusRepository {
    fun save(project: ID<Project>, status: Status)
    fun find(project: ID<Project>, id: ID<Status>): Status?
    fun findAll(of: ID<Project>): List<Status>

}