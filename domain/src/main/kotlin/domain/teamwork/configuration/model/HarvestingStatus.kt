package domain.teamwork.configuration.model

data class HarvestingStatus(val count: Int)