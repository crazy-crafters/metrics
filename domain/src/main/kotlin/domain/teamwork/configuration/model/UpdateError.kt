package domain.teamwork.configuration.model

sealed interface UpdateError
data class InvalidConfigurationError(val configuration: BoardConfiguration): UpdateError
data class NoConnectorForTypeError(val type: BoardType): UpdateError