package domain.teamwork.configuration.model

enum class BoardType {
    Jira,
    Github,
    Azuredevops,
    Trello
}