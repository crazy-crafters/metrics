package domain.teamwork.configuration.model

import domain.project.model.Project
import domain.sharedkernel.model.ID

data class Login(val user: String, val pass: String)


sealed interface BoardConfiguration {
    data class Valid(
        val project: ID<Project>,
        val type: BoardType,
        val url: String,
        val board: String,
        val login: Login
    ) : BoardConfiguration

    data object None : BoardConfiguration

    companion object {
        fun default(project: ID<Project>) = Valid(
            project = project,
            type = BoardType.Jira,
            url = "",
            board = "",
            login = Login(user = "", pass = "")
        )
    }
}



