package domain.teamwork.configuration.api

import domain.teamwork.configuration.Updater
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.spi.HarvestRemoteDashboard
import domain.teamwork.status.model.Progression

interface AnalyzeRemote {
    fun register(`for board from`: BoardType, use: HarvestRemoteDashboard): Updater
    suspend operator fun invoke(configuration: BoardConfiguration, progression: (Progression) -> Unit)
}