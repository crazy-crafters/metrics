package domain.teamwork.configuration.api

import domain.project.model.Project
import domain.teamwork.configuration.model.BoardConfiguration
import domain.sharedkernel.model.ID

interface HandleConfiguration {
    fun get(of: ID<Project>): BoardConfiguration
    fun save(of: ID<Project>, configuration: BoardConfiguration): BoardConfiguration
}