package domain.teamwork.configuration

import domain.project.model.Project
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.teamwork.shared.events.IterationEvent
import domain.teamwork.shared.events.IterationFinished
import domain.teamwork.shared.events.IterationStarted
import domain.sharedkernel.model.DateTime

sealed interface IterationError

data object NoIterationToCreateError : IterationError
data class NoIterationCreationEventError(val id: ID<Iteration>): IterationError

data class Iteration(val id: ID<Iteration>,
                     val project: ID<Project>,
                     val name: Name<Iteration>,
                     val from: DateTime,
                     val to: DateTime,
                     val closed: Boolean) {

    companion object {
        fun from(vararg events: IterationEvent) = from(events.toList())
        fun from(events: List<IterationEvent>): Either<Iteration, IterationError> {
            if (events.isEmpty())
                return Either.fail(NoIterationToCreateError)

            val sorted = events.sortedBy { it.date }
            val start = sorted.first()

            if (start !is IterationStarted)
                return Either.fail(NoIterationCreationEventError(start.entityId))

            val end = sorted.lastOrNull { it is IterationFinished }

            return Either.success(
                Iteration(
                    id = start.entityId,
                    project = start.project,
                    name = start.name,
                    from = start.date,
                    to = end?.date ?: DateTime.MAX,
                    closed = end != null
                )
            )
        }
    }
}