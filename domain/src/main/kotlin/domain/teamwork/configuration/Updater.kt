package domain.teamwork.configuration

import domain.teamwork.configuration.api.AnalyzeRemote
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.spi.HarvestRemoteDashboard
import domain.teamwork.status.model.Progression

class Updater: AnalyzeRemote {
    private val getters = mutableMapOf<BoardType, HarvestRemoteDashboard>()

    override fun register(`for board from`: BoardType, use: HarvestRemoteDashboard): Updater {
        getters[`for board from`] = use
        return this
    }

    override suspend operator fun invoke(configuration: BoardConfiguration, progression: (Progression) -> Unit) {
        if (configuration !is BoardConfiguration.Valid)
            return

        getters[configuration.type]?.invoke(configuration, progression)
    }
}