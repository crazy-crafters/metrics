package domain.teamwork.configuration.spi

import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.status.model.Progression

interface HarvestRemoteDashboard {
    suspend operator fun invoke(
        configuration: BoardConfiguration.Valid,
        progression: (Progression) -> Unit
    )
}