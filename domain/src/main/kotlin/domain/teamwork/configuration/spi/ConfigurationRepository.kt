package domain.teamwork.configuration.spi

import domain.project.model.Project
import domain.teamwork.configuration.model.BoardConfiguration
import domain.sharedkernel.model.ID

interface ConfigurationRepository {
    fun save(project: ID<Project>, configuration: BoardConfiguration)
    fun find(project: ID<Project>): BoardConfiguration?
    fun all(): List<BoardConfiguration>
}