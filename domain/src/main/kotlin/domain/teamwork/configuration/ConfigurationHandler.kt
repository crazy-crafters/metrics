package domain.teamwork.configuration

import domain.project.model.Project
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.spi.ConfigurationRepository

class ConfigurationHandler(private val configurationRepository: ConfigurationRepository):
    HandleConfiguration {

    override fun get(of: ID<Project>): BoardConfiguration {
        val configuration = configurationRepository.find(of) ?: return BoardConfiguration.None
        return configuration
    }

    override fun save(of: ID<Project>, configuration: BoardConfiguration): BoardConfiguration {
        configurationRepository.save(of, configuration)
        return configurationRepository.find(of)!!
    }
}