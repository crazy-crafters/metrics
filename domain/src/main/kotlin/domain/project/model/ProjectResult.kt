package domain.project.model


import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.squad.model.Squad

sealed interface Status {
    data class Open(val on: DateTime) : Status
    data class Closed(val on: DateTime) : Status
}


val noSquad = ID.from<Squad>("00000000-0000-0000-0000-000000000000")

sealed interface ProjectResult {

    sealed interface NotFound : ProjectResult {
        data object NoEvents : NotFound
        data object NoStartEvent : NotFound
    }

    data class Found(val project: Project): ProjectResult
}