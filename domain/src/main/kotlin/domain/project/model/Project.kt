package domain.project.model

import domain.project.model.ProjectResult.Found
import domain.project.model.ProjectResult.NotFound
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.model.Squad

class Project(
    val id: ID<Project>,
    named: Name<Project>,
    `described as`: String,
    `started on`: DateTime,
    events: List<ProjectEvent>
) {
    val name = named
    val description = `described as`
    val start = `started on`

    private val events: MutableList<ProjectEvent> = events.toMutableList()

    val squad: ID<Squad>
        get() = events.fold(noSquad) { id, event ->
            when (event) {
                is ProjectStarted -> id
                is ProjectAssigned -> event.squad
                is ProjectEnded -> id
            }
        }

    val status: Status
        get() = events.fold(Status.Open(start) as Status) { status, event ->
            when (event) {
                is ProjectAssigned -> status
                is ProjectEnded -> Status.Closed(
                    event.date
                )
                is ProjectStarted -> Status.Open(
                    event.date
                )
            }
        }

    override fun equals(other: Any?): Boolean {
        return other is Project && id == other.id
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    fun apply(event: ProjectEvent): Project {
        events.add(event)
        return this
    }

    companion object {
        fun from(vararg events: ProjectEvent) =
            from(events.toList())
        fun from(events: List<ProjectEvent>): ProjectResult {
            if (events.isEmpty())
                return NotFound.NoEvents

            val sorted = events.sortedBy { it.date }
            val start = sorted.first()

            if (start !is ProjectStarted)
                return NotFound.NoStartEvent

            return Found(Project(
                id = start.id,
                named = start.name,
                `described as` = start.description,
                `started on` = start.date,
                events = sorted
            ))
        }
    }
}