package domain.project.model

import domain.sharedkernel.model.Name
import domain.sharedkernel.model.DateTime

data class NewProjectCommand(
    val name: Name<Project>,
    val description: String,
    val startDate: DateTime
)
