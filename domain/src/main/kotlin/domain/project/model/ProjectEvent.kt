package domain.project.model


import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.sharedkernel.model.DateTime
import domain.squad.model.Squad

sealed interface ProjectEvent: DomainEvent {
    override val id: ID<Project>
}

data class ProjectStarted(
    override val id: ID<Project> = ID(),
    val name: Name<Project>,
    val description: String,
    override val date: DateTime
) : ProjectEvent


data class ProjectAssigned(
    override val id: ID<Project> = ID(),
    val squad: ID<Squad>,
    override val date: DateTime
) : ProjectEvent


data class ProjectEnded(
    override val id: ID<Project> = ID(),
    override val date: DateTime
) : ProjectEvent