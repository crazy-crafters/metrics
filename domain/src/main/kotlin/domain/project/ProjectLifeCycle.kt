package domain.project

import domain.project.api.GetProject
import domain.project.api.HandleProject
import domain.project.model.*
import domain.project.spi.ProjectEventRepository
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.squad.model.Squad

class GetProject(private val repository: ProjectEventRepository) : GetProject {
    override fun all(): List<Project> {
        return repository.all().buildIssues().filterIsInstance<Project>()
    }

    override fun information(about: ID<Project>): ProjectResult {
        return Project.from(repository.find(about))
    }
}


class HandleProject(private val repository: ProjectEventRepository) : HandleProject {
    override fun start(newProject: NewProjectCommand): ProjectResult {
        val event = ProjectStarted(
            id = ID(),
            name = newProject.name,
            description = newProject.description,
            date = newProject.startDate
        )
        repository.save(event)
        return Project.from(event)
    }

    override fun close(project: ID<Project>, on: DateTime): ProjectResult {
        return when (val it = get(about = project)) {
            is ProjectResult.Found -> {
                val event = ProjectEnded(
                    id = project,
                    date = on
                )
                repository.save(event)
                it.also { (found) ->
                    found.apply(event)
                }
            }

            else -> it
        }
    }

    override fun assign(project: ID<Project>, to: ID<Squad>, since: DateTime): ProjectResult {
        return when (val it = get(about = project)) {
            is ProjectResult.Found -> {
                val event: ProjectEvent = ProjectAssigned(
                    id = project,
                    squad = to,
                    date = since,
                )
                repository.save(event)
                it.also { (found) ->
                    found.apply(event)
                }
            }

            else -> it
        }
    }

    private fun get(about: ID<Project>) =  Project.from(repository.find(about))
}


private fun List<ProjectEvent>.buildIssues(): List<Project> {
    return this.groupBy { it.id }
        .map { (_, events) ->
            Project.from(events)
        }.filterIsInstance<Project>()
}