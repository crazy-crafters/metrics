package domain.project.spi

import domain.sharedkernel.model.DateTime

interface ClockService {
    fun now(): DateTime
}