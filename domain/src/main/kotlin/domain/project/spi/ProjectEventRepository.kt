package domain.project.spi

import domain.project.model.Project
import domain.project.model.ProjectEvent
import domain.sharedkernel.model.ID


interface ProjectEventRepository {
    fun find(from: ID<Project>): List<ProjectEvent>
    fun all(): List<ProjectEvent>
    fun save(event: ProjectEvent)
}

