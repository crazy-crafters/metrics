package domain.project.stubs


import domain.project.model.Project
import domain.project.model.ProjectEvent
import domain.project.model.ProjectResult
import domain.project.model.ProjectStarted
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Name

fun Project.Companion.get(vararg events: ProjectEvent) =
    (from(*events) as ProjectResult.Found).project

val voyager: Project
    get() = Project.get(
        ProjectStarted(
            id = "Voyager".generateProjectID(),
            name = Name.from("Voyager"),
            description = "Voyager probes are iconic NASA missions exploring the outer planets of the solar system and now in interstellar space",
            date = DateTime.parse("1977-01-01T12:00:00"))
    )
val pathfinder: Project
    get() = Project.get(
        ProjectStarted(
            id = "Pathfinder".generateProjectID(),
            name = Name.from("Pathfinder"),
            description = "this NASA mission, led by Jennifer Trosper, successfully deployed the Sojourner rover on Mars, demonstrating the feasibility of robotic exploration of the Red Planet.",
            date = DateTime.parse("1996-01-01T12:00:00")
        )
    ) as Project

val apolloEvents = listOf(
    ProjectStarted(
        id = "Apollo".generateProjectID(),
        name = Name.from("Apollo"),
        description = "The onboard flight guidance software from NASA's Apollo program",
        date = DateTime.parse("1969-01-01T12:00:00")
    )
)
val apollo: Project
    get() = Project.get(*apolloEvents.toTypedArray())