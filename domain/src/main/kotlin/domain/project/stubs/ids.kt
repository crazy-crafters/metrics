package domain.project.stubs



import domain.project.model.Project
import domain.sharedkernel.model.ID
import java.util.*

fun String.generateProjectID() = ID<Project>(UUID.nameUUIDFromBytes(this.toByteArray()))

val unknown = "Unknown".generateProjectID()


