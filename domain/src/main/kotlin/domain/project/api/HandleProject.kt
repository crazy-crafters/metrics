package domain.project.api


import domain.project.model.NewProjectCommand
import domain.project.model.Project
import domain.project.model.ProjectResult
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.squad.model.Squad

interface HandleProject {
    fun start(newProject: NewProjectCommand): ProjectResult
    fun assign(project: ID<Project>, to: ID<Squad>, since: DateTime): ProjectResult
    fun close(project: ID<Project>, on: DateTime): ProjectResult
}