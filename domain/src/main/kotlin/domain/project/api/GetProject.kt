package domain.project.api

import domain.project.model.Project
import domain.project.model.ProjectResult
import domain.sharedkernel.model.ID


interface GetProject {
    fun all(): List<Project>
    fun information(about: ID<Project>): ProjectResult
}