package domain.project.model

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name


@DisplayName("Project Model")
class ProjectTest {
    @Test
    fun `build an issue from creation event`() {
        Project.from(startProjectEvent(id = "1", date = "2024-01-12")).shouldBeTypeOf<ProjectResult.Found>().also { (project) ->
            project.id.shouldBe(ID.from("1"))
            project.name.shouldBe(Name.from("Project 1"))
            project.start.shouldBe(DateTime.parse("2024-01-12T12:00:00"))
        }
    }

    @Test
    fun `raise an error when there is no 'issue added' event`() {
        Project.from(projectAssignedEvent(to = "squad 1")) shouldBe ProjectResult.NotFound.NoStartEvent
    }

    @Test
    fun `raise an error when there is no event at all`() {
        Project.from() shouldBe ProjectResult.NotFound.NoEvents
    }

    @Test
    fun `get squad of a new project`() {
        Project.from(startProjectEvent(id = "1", date = "2024-01-12")).shouldBeTypeOf<ProjectResult.Found>().also { (it) ->
            it.squad shouldBe noSquad
        }
    }

    @Test
    fun `assign a squad to a project`() {
        Project.from(
            startProjectEvent(id = "1", date = "2024-01-12"),
            projectAssignedEvent(to = "squad 1", since = "2024-01-13")
        ).shouldBeTypeOf<ProjectResult.Found>().also { (it) ->
            it.squad shouldBe ID.from("squad 1")
        }
    }

    @Test
    fun `close a project`() {
        Project.from(
            startProjectEvent(id = "1", date = "2024-01-12"),
            endProjectEvent(id = "1", on = "2024-10-06"),
        ).shouldBeTypeOf<ProjectResult.Found>().also { (it) ->
            it.status shouldBe Status.Closed(on = DateTime.parse("2024-10-06T12:00:00"))
        }
    }

    @Test
    fun `assert equality`() {
        createProjectIdentity("A") shouldBe createProjectIdentity("A")

        createProjectIdentity("A").hashCode() shouldBe createProjectIdentity("A").hashCode()
    }

    @Test
    fun `assert inequality`() {
        createProjectIdentity("A") shouldNotBe createProjectIdentity("B")
        createProjectIdentity("A").hashCode() shouldNotBe createProjectIdentity("B").hashCode()
    }
}


private fun startProjectEvent(id: String, date: String) = ProjectStarted(
    id = ID.from(id),
    name = Name.from("Project $id"),
    description = "This is the Project $id",
    date = DateTime.parse("${date}T12:00:00")
)

private fun projectAssignedEvent(id: String = "1", to: String, since: String = "2024-01-12") = ProjectAssigned(
    id = ID.from(id),
    squad = ID.from(to),
    date = DateTime.parse("${since}T12:00:00")
)

private fun endProjectEvent(id: String = "1", on: String = "2024-01-12") = ProjectEnded(
    id = ID.from(id),
    date = DateTime.parse("${on}T12:00:00")
)

private fun createProjectIdentity(id: String, date: String = "2024-01-12") =
    Project.from(startProjectEvent(id = id, date = date))
