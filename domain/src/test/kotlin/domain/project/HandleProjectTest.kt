package domain.project


import domain.project.doubles.ProjectEventRepositoryDouble
import domain.project.model.*
import domain.project.stubs.apollo
import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.materials.shouldContainIgnoringFields
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.sharedkernel.model.Name
import domain.squad.model.Squad
import domain.squad.squad1
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test


class ProjectLifeCycleTestHelper : TestContext {
    private val projectRepositoryDouble = ProjectEventRepositoryDouble()
    private val handleProject = HandleProject(projectRepositoryDouble)

    fun `the events`(vararg events: ProjectEvent) = events.forEach { projectRepositoryDouble.save(it) }
    fun `we assign the project`(project: Project, to: Squad, since: DateTime) =
        handleProject.assign(project.id, to.id, since)

    fun `we close the project`(project: Project, since: DateTime) = handleProject.close(project.id, since)
}

class HandleProjectTest : ContextualizedTest<ProjectLifeCycleTestHelper>(ProjectLifeCycleTestHelper()) {
    @Test
    fun `assign a project`() {
        given {
            `the events`(
                apollo.`has started`(),
            )
        } `when` {
            `we assign the project`(apollo, to = squad1, "2024-01-15".toDateTime())
        } then {
            it.shouldBeTypeOf<ProjectResult.Found>().also { (it) ->
                it.squad shouldBe squad1.id
            }
        }
    }

    @Test
    fun `get an error when assigning a unknown project`() {
        given {
            `the events`()
        } `when` {
            `we assign the project`(apollo, to = squad1, "2024-01-15".toDateTime())
        } then {
            it shouldBe ProjectResult.NotFound.NoEvents
        }
    }

    @Test
    fun `stop a project`() {
        given {
            `the events`(
                apollo.`has started`(),
            )
        } `when` {
            `we close the project`(apollo, "2024-01-15".toDateTime())
        } then {
            it.shouldBeTypeOf<ProjectResult.Found>().also { (it) ->
                it.status.shouldBe(Status.Closed("2024-01-15".toDateTime()))
            }
        }
    }

    @Test
    fun `get an error when closing a unknown project`() {
        given {
            `the events`()
        } `when` {
            `we close the project`(apollo, "2024-01-15".toDateTime())
        } then {
            it shouldBe ProjectResult.NotFound.NoEvents
        }
    }

    @Test
    fun `starting a new project`() {
        val projectStub = ProjectEventRepositoryDouble()
        val registerProject = HandleProject(projectStub)

        val project = registerProject.start(
            NewProjectCommand(
                name = Name.from("Apollo"),
                description = "A project used to monitor and report the status of Apolloes",
                startDate = DateTime.MIN
            )
        )

        project.shouldBeTypeOf<ProjectResult.Found>().also { (project) ->
            project.name.value shouldBe "Apollo"
            project.description shouldBe "A project used to monitor and report the status of Apolloes"
            `based on side effects` {
                projectStub.items.shouldContainIgnoringFields(
                    listOf(
                        ProjectStarted(
                            project.id,
                            project.name,
                            project.description,
                            DateTime.MIN,
                        )
                    ), "id", "timetag"
                )
            }
        }
    }
}

private fun Project.`has started`(): ProjectEvent {
    return ProjectStarted(
        id = this.id,
        name = this.name,
        description = this.description,
        date = this.start
    )
}