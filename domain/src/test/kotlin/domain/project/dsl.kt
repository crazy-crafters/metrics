package domain.project


import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import domain.project.doubles.SurveyEventRepositoryDouble
import domain.project.model.Project
import domain.project.spi.ClockService
import domain.project.stubs.generateProjectID
import domain.survey.SurveyMeasurement
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.survey.model.*

class NPSDSL(private val sut: SurveyMeasurement) {
    lateinit var result: Survey
    fun `when I ask for NPS`(of: String, on: String): NPSDSL {
        result = sut.measured(of.generateProjectID(), DateTime.parse(on))
        return this
    }

    fun `when I open a survey for`(project: String): NPSDSL {
        result = sut.openSurvey(
            NewNPSSurvey(project.generateProjectID(), sut.clock.now())
        )
        return this
    }

    fun `when I close the survey for`(project: String, on: String): NPSDSL {
        result = sut.closeSurvey(project.generateProjectID(), DateTime.parse(on))
        return this
    }

    fun `when I vote`(`for the project`: String, vote: Int, on: DateTime): NPSDSL {
        result = sut.registerVote(NPSVote(`for the project`.generateProjectID(), on, vote))
        return this
    }

    fun `then I should get`(
        score: Survey.NPSSurvey.Scores,
        `starting on`: DateTime,
        `closing on`: DateTime
    ) {
        result.shouldBeTypeOf<Survey.NPSSurvey>()
        (result as Survey.NPSSurvey).scores.shouldBe(score)
        (result as Survey.NPSSurvey).from.shouldBe(`starting on`)
        (result as Survey.NPSSurvey).to.shouldBe(`closing on`)
    }


    fun `then the survey is closed`() {
        result.shouldBeTypeOf<Survey.NPSSurvey>()
        (result as Survey.NPSSurvey).isOpen shouldBe false
    }

    inline fun <reified T : Any> `then I should get an error`() {
        result.shouldBeTypeOf<T>()
    }


}

val fakeClock = object : ClockService {
    override fun now() = d("2024-01-12")
}

fun `given the NPS`(description: String, clock: ClockService = fakeClock, f: NPSDSL.() -> Unit) {

    val groups = description.trimIndent().split("\n").splitProjects()

    val provider = SurveyEventRepositoryDouble(*(groups.flatMap { it.toNPS() }.toTypedArray()))

    NPSDSL(
        SurveyMeasurement(provider, clock)
    ).f()
}

fun List<String>.splitProjects(): List<List<String>> {
    val projects = this.mapIndexed { i, _ -> i }.filter { !this[it].startsWith(" ") }
    return projects.zip(projects.drop(1) + listOf(count())) { a, b -> a to b }.map { (first, last) ->
        this.subList(first, last)
    }
}

fun String.toNPS(): List<SurveyEvent> {
    val parts = this.trimIndent().split(": ")
    val project = parts.first().replace(":", "").trim().generateProjectID()
    return parts.last().toNPS(project)
}

fun String.toNPS(project: ID<Project>): List<SurveyEvent> {
    val parts = this.trimIndent().split(" to ", " -> ", ", ")
    val from = DateTime.parse("${parts.first()}T12:00:00")

    val to = if (parts[1] == "-") DateTime.INVALID else DateTime.parse("${parts[1]}T12:00:00")

    val promoters = parts.first { it.contains("promoters") }.split(" ").first().toInt()
    val passives = parts.first { it.contains("passives") }.split(" ").first().toInt()
    val detractors = parts.first { it.contains("detractors") }.split(" ").first().toInt()

    return listOf(
        OpenSurvey(date = from, id = project)
    ) +
            MutableList(promoters) { Vote(date = from, id = project, score = 10) } +
            MutableList(passives) { Vote(date = from, id = project, score = 7) } +
            MutableList(detractors) { Vote(date = from, id = project, score = 0) } +
            if (to != DateTime.INVALID) {
                listOf(CloseSurvey(date = to, id = project))
            } else {
                emptyList()
            }
}

fun List<String>.toNPS(): List<SurveyEvent> {
    val projectName = this.first().replace(":", "").trim()
    val project = projectName.generateProjectID()
    return this.drop(1).flatMap { if (it.contains("No NPS yet", ignoreCase = true)) emptyList() else it.toNPS(project) }
}

private fun d(s: String): DateTime {
    return if (s.contains("T")) {
        DateTime.parse(s)
    } else {
        DateTime.parse("${s}T12:00:00")
    }
}