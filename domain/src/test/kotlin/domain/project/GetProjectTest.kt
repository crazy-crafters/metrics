package domain.project


import domain.project.doubles.ProjectEventRepositoryDouble
import domain.project.model.*
import domain.project.stubs.apollo
import domain.project.stubs.pathfinder
import domain.project.stubs.voyager
import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test


class GetProjectTestHelper : TestContext {
    private val projectRepositoryDouble = ProjectEventRepositoryDouble()
    private val getProject = GetProject(projectRepositoryDouble)

    fun `the events`(vararg events: ProjectEvent) = events.forEach { projectRepositoryDouble.save(it) }
    fun `we get the project`(project: Project) = getProject.information(project.id)
    fun `we get all the projects`() = getProject.all()
}

class GetProjectTest : ContextualizedTest<GetProjectTestHelper>(GetProjectTestHelper()) {

    @Test
    fun `get a project by id`() {
        val events = ProjectEventRepositoryDouble()
        val getProject = GetProject(events)

        events.apply {
            apollo.`has started`()
            voyager.`has started`()
            pathfinder.`has started`()
        }

        getProject.information(about =voyager.id) shouldBe ProjectResult.Found(voyager)
    }

    /*@Test
    fun `getting a project`() {
        given {
            `the events`(
                apollo.`has started`(),
                voyager.`has started`(),
                pathfinder.`has started`()
            )
        } `when` {
            `we get the project`(voyager)
        } then {
            it shouldBe voyager
        }
    }

    @Test
    fun `listing all projects`() {
        given {
            `the events`(
                apollo.`has started`(),
                voyager.`has started`(),
                pathfinder.`has started`()
            )
        } `when` {
            `we get all the projects`()
        } then {
            it.shouldContainExactly(apollo, voyager, pathfinder)
        }
    }

    @Test
    fun `getting an error when there is no project`() {
        given {
            `the events`(
                //voyager.`has started`()
            )
        } `when` {
            `we get the project`(voyager)
        } then {
            it shouldBe Project.NotFound.NoEvents
        }
    }

    @Test
    fun `getting an error when the project does not exist`() {
        given {
            `the events`(
                apollo.`has started`()
            )
        } `when` {
            `we get the project`(voyager)
        } then {
            it shouldBe Project.NotFound.NoEvents
        }
    }*/
}