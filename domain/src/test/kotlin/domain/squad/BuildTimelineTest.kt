package domain.squad

import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.materials.`should be success and`
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.Title
import domain.squad.doubles.MilestoneRepositoryDouble
import domain.squad.doubles.SystemEventsRepositoryDouble
import domain.squad.model.Milestone
import domain.squad.model.Squad
import domain.squad.model.commands.NewMilestoneCommand
import domain.squad.model.errors.SquadError
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test


class EventsTestContext: TestContext {
    private val eventsRepository = SystemEventsRepositoryDouble()
    private val milestoneRepository = MilestoneRepositoryDouble()

    val handler = TimelineBuilder(eventsRepository, milestoneRepository)

    fun `the events`(vararg events: DomainEvent) {
        eventsRepository.add(events.toList())
    }

    fun `asking for the events of`(squad: Squad): Either<Timeline, SquadError> {
        return handler.getAll(squad.id)
    }

    fun Squad.`has declared a retrospective on`(date: DateTime) {
        milestoneRepository.add(
            Milestone(
            squad = this.id,
            title = Title("Retrospective"),
            description = "This is the minutes of our retro",
            date = date,
            type = MilestoneType.Retrospective
        )
        )
    }

    fun Squad.`declares a retrospective on`(date: DateTime) {
        handler.declare(
            NewMilestoneCommand(
                squad = this.id,
                title = Title("Retrospective"),
                description = "This is the minutes of our retro",
                date = date,
                type = MilestoneType.Retrospective
            )
        )
    }
}


fun Either<Timeline, SquadError>.`should be the timeline`(`starting on`: DateTime, vararg events: TimelineItem) {
    `should be success and` {
        this.items.shouldContainExactly(*events)
        this.startingOn.shouldBe(`starting on`)
    }
}

class BuildTimelineTest: ContextualizedTest<EventsTestContext>(EventsTestContext()) {
    @Test
    fun `getting all events for a given squad`() {
        given {
            `the events`(
                squad1.hasBeenCreated("2024-01-01".toDateTime()),
                grace.joined(squad1, on= "2024-01-12".toDateTime()),
                squad2.hasBeenCreated("2024-01-12".toDateTime()),
            )
        } `when` {
            `asking for the events of`(squad1)
        } then {
            it.`should be the timeline`(
                `starting on`= "2024-01-01".toDateTime(),
                squadCompositionChange("2024-01-12".toDateTime(), joining = listOf(grace.id))
            )
        }
    }

    @Test
    fun `a member has declared a milestone`() {
        given {
            `the events`(
                squad1.hasBeenCreated("2024-01-01".toDateTime()),
                grace.joined(squad1, on= "2024-01-12".toDateTime()),
                squad2.hasBeenCreated("2024-01-12".toDateTime()),
                janis.joined(squad1, on= "2024-01-31".toDateTime()),
            )
            squad1.`has declared a retrospective on`(d("2024-01-20"))
        } `when` {
            `asking for the events of`(squad1)
        } then {
            it.`should be the timeline`(
                `starting on`= d("2024-01-01"),
                squadCompositionChange(d("2024-01-12"), joining = listOf(grace.id)),
                retrospective(d("2024-01-20")),
                squadCompositionChange(d("2024-01-31"), joining = listOf(janis.id)),
            )
        }
    }

    @Test
    fun `a member declares a milestone`() {
        given {
            `the events`(
                squad1.hasBeenCreated(d("2024-01-01")),
                grace.joined(squad1, on= d("2024-01-12")),
                squad2.hasBeenCreated(d("2024-01-12")),
                janis.joined(squad1, on= d("2024-01-31")),
            )
        } `when` {
            squad1.`declares a retrospective on`(d("2024-01-20"))
        }
    }
}


fun d(s: String) = s.toDateTime()