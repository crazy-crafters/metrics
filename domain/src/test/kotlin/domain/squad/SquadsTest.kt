package domain.squad

import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.materials.shouldContainIgnoringFields
import domain.sharedkernel.model.DomainEvent
import domain.squad.*
import domain.squad.doubles.ClockServiceDouble
import domain.squad.doubles.EventsBusDouble
import domain.squad.doubles.SquadRepositoryDouble
import domain.squad.model.Member
import domain.squad.model.Squad
import domain.squad.model.SquadResult
import domain.squad.model.commands.NewSquadCommand
import domain.squad.model.events.MemberHasJoinedDomainEvent
import domain.squad.model.events.MemberHasLeftDomainEvent
import domain.squad.model.events.NewSquadCreatedDomainEvent
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.collections.shouldContainInOrder
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test

class HandleSquadsTestHelper : TestContext {
    private val bus = EventsBusDouble()
    val squadRepository = SquadRepositoryDouble()
    val handler = SquadComposition(squadRepository, bus, ClockServiceDouble())

    fun `the squads`(vararg squad: Squad) = squad.forEach { squadRepository.save(it) }
    infix fun Member.joins(squad: Squad) = handler.join(squad.id, this.id)
    infix fun Member.leaves(squad: Squad) = handler.leave(squad.id, this.id)
    infix fun Member.`should be in`(squad: Squad) = squadRepository.check(this, `should be in` = squad.id)
    infix fun Member.`should not be in`(squad: Squad) =
        squadRepository.check(this, `should not be in` = squad.id)

    fun `the following events should have been published`(vararg events: DomainEvent) {
        bus.events.shouldContainIgnoringFields(events.toList(), "date")
    }

    fun `we get the squad`(squad: Squad) = handler.get(squad.id)
    fun `we get all the squads`() = handler.all()

    fun `we want to create the squad`(squad: Squad): SquadResult {
        val command = NewSquadCommand(
            name = squad.name,
            members = squad.members
        )
        return handler.`described as`(command)
    }
}


class SquadTest : ContextualizedTest<HandleSquadsTestHelper>(HandleSquadsTestHelper()) {

    @Test
    fun `a member joins a squad`() {
        given {
            `the squads`(squad1, squad2)
        } `when` {
            grace joins squad1
        } then {
            `based on side effects` {
                grace `should be in` squad1
                `the following events should have been published`(
                    MemberHasJoinedDomainEvent(squad1.id, member = grace.id, date = ClockServiceDouble.now)
                )
            }

        }
    }

    @Test
    fun `a member cannot join a squad twice`() {
        given {
            `the squads`(squad1.with(grace), squad2)
        } `when` {
            grace joins squad1
        } then {
            it shouldBe SquadResult.Error.MemberAlreadyJoined
        }
    }

    @Test
    fun `getting a squad`() {
        given {
            `the squads`(squad1, squad2)
        } `when` {
            `we get the squad`(squad1)
        } then {
            it.shouldBeTypeOf<SquadResult.Ok>()
            it.value.id shouldBe squad1.id
        }
    }

    @Test
    fun `getting a squad that does not exist generates an error`() {
        given {
            `the squads`(squad1, squad2)
        } `when` {
            `we get the squad`(squad3)
        } then {
            it shouldBe SquadResult.Error.NotFound
        }
    }

    @Test
    fun `getting all the squads`() {
        given {
            `the squads`(squad1.with(janis, grace), squad2.with(patti))
        } `when` {
            `we get all the squads`()
        } then {
            it.shouldContainExactly(squad1, squad2)
        }
    }

    @Test
    fun `only squads that still exist must be listed`() {
        given {
            `the squads`(squad1.with(janis, grace), squad2.with(patti), squad3)
        } `when` {
            `we get all the squads`()
        } then {
            it.shouldContainExactly(squad1, squad2)
        }
    }

    @Test
    fun `creating a new squad`() {
        given {
            `the squads`()
        } `when` {
            `we want to create the squad`(squad1.with(janis, grace))
        } then {
            it.shouldBeTypeOf<SquadResult.Ok>()

            it.value.name.shouldBe(squad1.name)
            it.value.members.shouldContainInOrder(janis.id, grace.id)

            `based on side effects` {
                squadRepository.findAll().shouldContainIgnoringFields(
                    listOf(squad1.with(janis, grace)),
                    "id", "events"
                )
            }
        }
    }

    @Test
    fun `when a new squad is created, an event is triggerred`() {
        given {
            `the squads`()
        } `when` {
            `we want to create the squad`(squad1.with(janis, grace))
        } then {
            it.shouldBeTypeOf<SquadResult.Ok>()
            `based on side effects` {
                `the following events should have been published`(
                    NewSquadCreatedDomainEvent(
                        it.value.id,
                        name = it.value.name,
                        members = it.value.members,
                        date = ClockServiceDouble.now
                    )
                )
            }
        }
    }

    @Test
    fun `a member leaves their squad`() {
        given {
            `the squads`(squad1.with(grace, patti, janis), squad2)
        } `when` {
            grace leaves squad1
        } then {
            `based on side effects` {
                grace `should not be in` squad1
                `the following events should have been published`(
                    MemberHasLeftDomainEvent(squad1.id, member = grace.id, date = ClockServiceDouble.now)
                )
            }

        }
    }

    @Test
    fun `a member can only leave their own squad `() {
        given {
            `the squads`(squad1.with(grace), squad2)
        } `when` {
            grace leaves squad2
        } then {
            it shouldBe SquadResult.Error.MemberIsNotPartOfTheSquad
        }
    }
}

//fun Squad.with(vararg member: Member) = Squad(this.id, this.name, member.map { it.id }.toList())
