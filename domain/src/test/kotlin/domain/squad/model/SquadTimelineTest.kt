package domain.squad.model

import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.model.DateTime
import domain.squad.*
import domain.squad.model.events.MemberHasJoinedDomainEvent
import domain.squad.model.events.MemberHasLeftDomainEvent
import domain.squad.model.events.NewSquadCreatedDomainEvent
import domain.teamwork.d
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test


class SquadSquadTimelineTestContext: TestContext {
    val timeline = Timeline(squad1.id)

    fun `the squad has been created on`(date: DateTime) {
        timeline.add(NewSquadCreatedDomainEvent(squad1.id, date, squad1.name, emptyList()))
    }

    infix fun Member.`joined on`(date: DateTime) {
        timeline.add(MemberHasJoinedDomainEvent(squad1.id, date, this.id))
    }

    infix fun Member.`left on`(date: DateTime) {
        timeline.add(MemberHasLeftDomainEvent(squad1.id, date, this.id))
    }

    fun `getting the events`() = timeline.items
}

class SquadTimelineTest: ContextualizedTest<SquadSquadTimelineTestContext>(SquadSquadTimelineTestContext()) {
    @Test
    fun `should define the creation date from event`() {
        val timeline = Timeline(squad1.id)

        timeline.add(NewSquadCreatedDomainEvent(squad1.id, d("2024-01-01"), squad1.name, emptyList()))

        timeline.startingOn shouldBe d("2024-01-01")
    }

    @Test
    fun `should define the staffing change from event`() {
        given {
            `the squad has been created on`(d("2024-01-01"))
            grace `joined on` d("2024-01-12")
        } `when` {
            `getting the events`()
        } then {
            it.shouldContain(
                squadCompositionChange(d("2024-01-12"), joining = listOf(grace.id))
            )
        }
    }

    @Test
    fun `should create a single event for composition at same date`() {
        given {
            `the squad has been created on`(d("2024-01-01"))
            alison `joined on` d("2024-01-01")

            grace `joined on` d("2024-01-12")
            janis `joined on` d("2024-01-12")

            janis `left on` d("2024-01-20")

            patti `joined on` d("2024-01-31")
            alison `left on` d("2024-01-31")

        } `when` {
            `getting the events`()
        } then {
            it.shouldContainExactly(
                squadCompositionChange(d("2024-01-01"), joining = listOf(alison.id)),
                squadCompositionChange(d("2024-01-12"), joining = listOf(grace.id, janis.id)),
                squadCompositionChange(d("2024-01-20"), leaving = listOf(janis.id)),
                squadCompositionChange(d("2024-01-31"), joining = listOf(patti.id), leaving = listOf(alison.id))
            )
        }
    }
}