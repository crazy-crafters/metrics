package domain.squad.model

import domain.sharedkernel.materials.toId
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Test

class AttachedFileTest {

    private val descriptorForFile1: AttachedFileDescriptor
        get() = AttachedFileDescriptor(
            id = "file 1".toId(),
            name = Name.from("file 1")
        )

    private val descriptorForFile2: AttachedFileDescriptor
        get() = AttachedFileDescriptor(
            id = "file 2".toId(),
            name = Name.from("file 2")
        )

    private val descriptorForAnotherFile1: AttachedFileDescriptor
        get() = AttachedFileDescriptor(
            id = ID(),
            name = Name.from("file 1")
        )

    @Test
    fun `assert equality on descriptiors`() {
        descriptorForFile1 shouldBe descriptorForFile1
        descriptorForFile1.hashCode() shouldBe descriptorForFile1.hashCode()
    }

    @Test
    fun `assert inequality on descriptiors`() {
        descriptorForFile1 shouldNotBe descriptorForFile2
        descriptorForFile1.hashCode() shouldNotBe descriptorForFile2.hashCode()
    }

    @Test
    fun `two files can have the same name`() {
        descriptorForFile1 shouldNotBe descriptorForAnotherFile1
        descriptorForFile1.hashCode() shouldNotBe descriptorForAnotherFile1.hashCode()
    }

    @Test
    fun `assert equality on files`() {
        AttachedFile(descriptorForFile1, byteArrayOf()) shouldBe AttachedFile(descriptorForFile1, byteArrayOf())
        AttachedFile(descriptorForFile1, byteArrayOf()).hashCode() shouldBe AttachedFile(descriptorForFile1, byteArrayOf()).hashCode()
    }

    @Test
    fun `assert inequality on files`() {
        AttachedFile(descriptorForFile1, byteArrayOf()) shouldNotBe AttachedFile(descriptorForFile2, byteArrayOf())
        AttachedFile(descriptorForFile1, byteArrayOf()).hashCode() shouldNotBe AttachedFile(descriptorForFile2, byteArrayOf()).hashCode()
    }
}