package domain.squad.model

import domain.squad.*
import domain.squad.model.SquadResult.Error.MemberIsNotPartOfTheSquad
import domain.squad.model.SquadResult.Ok
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test

class SquadTest {

    @Test
    fun `assert equality between entities`() {
        squad1 shouldBe squad1
        squad1.hashCode() shouldBe squad1.hashCode()
    }

    @Test
    fun `assert inequality between entities`() {
        squad1 shouldNotBe squad2
        squad1.hashCode() shouldNotBe squad2.hashCode()
    }

    @Test
    fun `should add a new member`() {
        val result = squad1.join(grace.id)

        result.shouldBeTypeOf<Ok>()
        result.value.members.shouldContain(grace.id)
    }

    @Test
    fun `should not add a member twice`() {
        val team = squad1

        team.join(grace.id)
        team.join(grace.id)

        team.members.shouldContain(grace.id)
    }

    @Test
    fun `should consider a squad as existing when having at least one member`() {
        val team = squad1

        team.isStillExisting() shouldBe false
        team.join(grace.id)
        team.isStillExisting() shouldBe true
    }

    @Test
    fun `should remove a member`() {
        val squad = squad1.with(grace, patti)

        val result = squad.leave(grace.id)
        result.shouldBeTypeOf<Ok>()
        result.value.members.shouldContain(patti.id)
    }

    @Test
    fun `should raise an error when a member leaves another team`() {
        val squad = squad1.with(grace, patti)

        val result = squad.leave(alison.id)
        result shouldBe MemberIsNotPartOfTheSquad
    }
}