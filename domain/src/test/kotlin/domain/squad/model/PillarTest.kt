package domain.squad.model

import domain.sharedkernel.materials.toId
import domain.sharedkernel.model.Title
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Test

class PillarTest {

    private val pillar1: Pillar
        get() = Pillar(
            "1".toId(),
            "squad1".toId(),
            Title("Collaboration over Protection"),
            "Collab over Protect",
            Score(5, 10)
        )

    private val pillar2: Pillar
        get() = Pillar(
            "2".toId(),
            "squad1".toId(),
            Title("Collaboration over Protection"),
            "Collab over Protect",
            Score(5, 10)
        )

    @Test
    fun `assert equality`() {
        pillar1 shouldBe pillar1
        pillar1.hashCode() shouldBe pillar1.hashCode()
    }

    @Test
    fun `assert inequality`() {
        pillar1 shouldNotBe pillar2
        pillar1.hashCode() shouldNotBe pillar2.hashCode()
    }

    @Test
    fun `should add a note to a pillar`() {
        val note = PillarNote(
            "note1".toId(),
            Title("Dummy title"),
            "Dummy description",
            emptyList()
        )

        pillar1.adding(note).notes.shouldContain(note)
    }
}