package domain.squad

import domain.sharedkernel.model.Name
import domain.squad.doubles.AttachedFileRepositoryDouble
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class UploadFileTest {

    @Test
    fun `should create a file descriptor for new file`() {
        val repository = AttachedFileRepositoryDouble()
        val newFile = NewAttachedFile(
            Name.from("New File"),
            byteArrayOf()
        )

        val result = FileUploader(repository).upload(newFile)

        result.name.shouldBe(Name.from("New File"))
    }
}