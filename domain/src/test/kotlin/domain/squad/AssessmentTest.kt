package domain.squad

import domain.sharedkernel.materials.*
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.Title
import domain.squad.doubles.AssessmentRepositoryDouble
import domain.squad.doubles.ClockServiceDouble
import domain.squad.doubles.EventsBusDouble
import domain.squad.doubles.SquadRepositoryDouble
import domain.squad.model.*
import domain.squad.model.commands.NewPillarCommand
import domain.squad.model.commands.PillarNoteCommand
import domain.squad.model.errors.PillarAlreadyExists
import domain.squad.model.errors.PillarCannotBeEvaluated
import domain.squad.model.errors.PillarDoesNotExist
import domain.squad.model.events.NewPillarDefinedEvent
import domain.squad.model.events.NoteHasBeenAddedEvent
import domain.squad.model.events.PillarHasBeenEvaluatedEvent
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test


private fun pillar(`for the squad`: Squad, title: String, score: Int = 3) = Pillar(
    id = title.toId(),
    squad = `for the squad`.id,
    title = Title(title),
    description = "This is an pillar",
    score = Score(score, 10)
)

private fun `a note`(title: String = "New meeting", description: String = "Minutes of the meeting") =
    PillarNote(
        Title(title),
        description,
        listOf()
    )

class AssessmentTestHelper : TestContext {
    private val bus = EventsBusDouble()
    private val squadRepository = SquadRepositoryDouble()
    val assessmentRepository = AssessmentRepositoryDouble()
    private val sut = AssessmentMeasurement(assessmentRepository, bus, ClockServiceDouble())

    fun `the squads`(vararg squad: Squad) = squad.forEach { squadRepository.save(it) }

    fun Member.`displays their assessment`() =
        sut.assessment(`for the squad` = squadRepository.squads.first { it.members.contains(this.id) }.id)

    fun Member.`displays the pillar`(name: String) = sut.byId(name.toId())
    fun Member.evaluate(name: String, to: Int) = sut.evaluate(
        domain.squad.model.commands.PillarEvaluationCommand(
            name.toId(),
            to
        )
    )

    fun Member.`add a note to`(pillar: String, title: String, description: String) = sut.addNote(
        PillarNoteCommand(
            pillar.toId(),
            Title.from(title),
            description,
            listOf()
        )
    )

    fun Member.`defines a new pillar as following`(title: String, description: String, maximum: Int) = sut.execute(
        NewPillarCommand(
            squadRepository.squads.first { it.members.contains(this.id) }.id,
            title = Title(title),
            description = description,
            scale = maximum
        )
    )

    fun `the pillar`(`for the squad`: Squad, title: String, score: Int = 3) {
        assessmentRepository.save(
            pillar(`for the squad`, title, score)
        )
    }

    fun `the following events should have been published`(vararg events: DomainEvent) {
        bus.events.shouldContainIgnoringFields(events.toList(), "date")
    }

}


class PillarTest : ContextualizedTest<AssessmentTestHelper>(AssessmentTestHelper()) {

    @Test
    fun `a member defines an pillar for their squad`() {
        given {
            `the squads`(squad1.with(grace))
        } `when` {
            grace.`defines a new pillar as following`(
                title = "New pillar",
                description = "The is an pillar",
                maximum = 10
            )
        } then {
            val expected = Pillar(
                squad = squad1.id,
                title = Title("New pillar"),
                description = "This is an pillar",
                score = Score.NoScoreOn(10)
            )
            it.`should be success and` {
                this.title.shouldBe(expected.title)
                this.squad.shouldBe(expected.squad)
                `based on side effects` {
                    assessmentRepository.`should contain`(expected)
                    `the following events should have been published`(
                        NewPillarDefinedEvent(
                            id = id,
                            title = Title("New pillar"),
                            date = ClockServiceDouble.now
                        )
                    )
                }
            }

        }
    }

    @Test
    fun `a pillar cannot be named as an already existing one`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(
                `for the squad` = squad1,
                "Old pillar"
            )
        } `when` {
            grace.`defines a new pillar as following`(
                title = "Old pillar",
                description = "The is an pillar",
                maximum = 10
            )
        } then {
            it.`should be failure and` {
                this shouldBe PillarAlreadyExists(squad1.id, Title("Old pillar"))
            }
        }
    }


    @Test
    fun `a member lists their pillars of their own squad`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(`for the squad` = squad1, "pillar 1")
            `the pillar`(`for the squad` = squad1, "pillar 2")
            `the pillar`(`for the squad` = squad1, "pillar 3")
            `the pillar`(`for the squad` = squad1, "pillar 4")

            `the pillar`(`for the squad` = squad2, "pillar 5")
            `the pillar`(`for the squad` = squad2, "pillar 6")
        } `when` {
            grace.`displays their assessment`()
        } then {
            it.shouldContainIgnoringFields(
                listOf(
                    pillar(`for the squad` = squad1, "pillar 1"),
                    pillar(`for the squad` = squad1, "pillar 2"),
                    pillar(`for the squad` = squad1, "pillar 3"),
                    pillar(`for the squad` = squad1, "pillar 4")
                ),
                "id"
            )
        }
    }

    @Test
    fun `a member gets details of a given pillar`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(`for the squad` = squad1, "pillar 1")
            `the pillar`(`for the squad` = squad1, "pillar 2")
        } `when` {
            grace.`displays the pillar`("pillar 1")
        } then {
            it.`should be success and` {
                this.shouldBe(pillar(`for the squad` = squad1, "pillar 1"))
            }
        }
    }

    @Test
    fun `a member evaluates an pillar`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(`for the squad` = squad1, "pillar 1", score = 1)
        } `when` {
            grace.evaluate("pillar 1", to = 4)
        } then {
            it `should be success and` {
                this.score.shouldBe(Score(4, 10))
                `based on side effects` {
                    assessmentRepository.`should contain`(
                        pillar(`for the squad` = squad1, "pillar 1", score = 4)
                    )

                    `the following events should have been published`(
                        PillarHasBeenEvaluatedEvent(id, score = score, date = ClockServiceDouble.now)
                    )
                }
            }
        }
    }

    @Test
    fun `a member cannot evaluate an unknown pillar`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(`for the squad` = squad1, "pillar 1", score = 1)
        } `when` {
            grace.evaluate("pillar 2", to = 4)
        } then {
            it `should be failure and` {
                this.shouldBeTypeOf<PillarDoesNotExist>()
            }
        }
    }

    @Test
    fun `a member cannot give an invalid evaluation`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(`for the squad` = squad1, "pillar 1", score = 1)
        } `when` {
            grace.evaluate("pillar 1", to = 999)
        } then {
            it `should be failure and` {
                this.shouldBeTypeOf<PillarCannotBeEvaluated>()
            }
        }
    }

    @Test
    fun `a member adds a new note to a pillar`() {
        given {
            `the squads`(squad1.with(grace))
            `the pillar`(`for the squad` = squad1, "pillar 1", score = 1)
        } `when` {
            grace.`add a note to`("pillar 1", "New meeting", "Minutes of the meeting")
        } then {
            it `should be success and` {
                this.notes.shouldContainIgnoringFields(
                    listOf(
                        `a note`()
                    ),
                    "id"
                )
                `based on side effects` {
                    `the following events should have been published`(
                        NoteHasBeenAddedEvent(
                            id = notes.first().id,
                            pillar = id,
                            title = notes.first().title,
                            files = emptyList(),
                            date = ClockServiceDouble.now
                        )
                    )
                }
            }
        }
    }

    @Test
    fun `getting an error when displaying a pillar that does not exist`() {
        given {
            `the squads`(squad1.with(grace))
        } `when` {
            grace.`displays the pillar`("unknown")
        } then {
            it `should be failure and` {
                this.shouldBeTypeOf<PillarDoesNotExist>()
            }
        }
    }

    @Test
    fun `getting an error when adding a note to a pillar that does not exist`() {
        given {
            `the squads`(squad1.with(grace))
        } `when` {
            grace.`add a note to`("pillar 1", "New meeting", "Minutes of the meeting")
        } then {
            it `should be failure and` {
                this.shouldBeTypeOf<PillarDoesNotExist>()
            }
        }
    }
}

