package domain.squad

import domain.sharedkernel.materials.*
import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID
import domain.squad.doubles.ClockServiceDouble
import domain.squad.doubles.EventsBusDouble
import domain.squad.doubles.MemberRepositoryDouble
import domain.squad.model.Member
import domain.squad.model.commands.NewMemberCommand
import domain.squad.model.errors.MemberAlreadyOnboarded
import domain.squad.model.errors.MemberDoesNotExist
import domain.squad.model.events.NewMemberHasBeenOnboardedDomainEvent
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

fun Member.toCommand() = NewMemberCommand(
    name = this.name,
    email = this.email
)


class OnboardingTestContext: TestContext {

    private val userRepositoryFake = MemberRepositoryDouble()
    private val eventsBus = EventsBusDouble()
    private val staffing = StaffingManagement(userRepositoryFake, eventsBus, ClockServiceDouble())

    fun `there is not member yet`() {
        userRepositoryFake.clear()
    }

    fun `the members`(vararg member: Member) {
        userRepositoryFake.members.addAll(member)
    }

    fun `the org onboards`(member: Member) = staffing(member.toCommand())
    fun `we list all members`() = staffing.all()
    fun `we retrieve`(id: ID<Member>) = staffing.by(id)

    fun `the following events should have been published`(vararg events: DomainEvent) {
        eventsBus.events.shouldContainIgnoringFields(events.toList(), "id", "timetag")
    }
}

class OnboardingTest: ContextualizedTest<OnboardingTestContext>(OnboardingTestContext()) {

    @Test
    fun `onboarding a new member`() {
        given {
            `there is not member yet`()
        } `when` {
            `the org onboards`(grace)
        } then {
            //it.`should be the value`(grace.copy(id=it.))
            it.`should be success and` {
                this shouldBe grace.copy(id=this.id)
            }
            `based on side effects` {
                `the following events should have been published`(
                    NewMemberHasBeenOnboardedDomainEvent(grace.id, grace.name, grace.email, date = ClockServiceDouble.now)
                )
            }

        }
    }

    @Test
    fun `refusing to onboard a member twice`() {
        given {
            `the members`(
                grace
            )
        } `when` {
            `the org onboards`(grace)
        } then {
            it.`should be the error`(MemberAlreadyOnboarded(grace.email))
        }
    }

    @Test
    fun `listing all members in alphabetical order`() {
        given {
            `the members`(
                patti, grace, janis, alison
            )
        } `when` {
            `we list all members`()
        } then {
            it.shouldContainExactly(alison, grace, janis, patti)
        }
    }

    @Test
    fun `getting a member by its id`() {
        given {
            `the members`(
                patti, grace, janis, alison
            )
        } `when` {
            `we retrieve`(grace.id)
        } then {
            it.`should be the value`(grace)
        }
    }

    @Test
    fun `getting an error when the id does not exist`() {
        given {
            `the members`()
        } `when` {
            `we retrieve`(grace.id)
        } then {
            it.`should be the error`(MemberDoesNotExist(grace.id))
        }
    }
}