package domain.user

import domain.project.model.Project
import domain.sharedkernel.materials.ContextualizedTest
import domain.sharedkernel.materials.TestContext
import domain.sharedkernel.model.ID
import domain.user.doubles.UserSettingsRepositoryDouble
import io.kotest.assertions.fail
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class UserSettingsTest: ContextualizedTest<SettingsTestContext>(SettingsTestContext()) {

    @Test
    fun `save settings`() {
        given {
            `I am`("Grace")
        } `when` {
            `I save this default settings`("apollo")
        } then {
            `based on side effects` {
                `my settings should be`(
                    project = "apollo"
                )
            }
        }
    }

    @Test
    fun `get settings for the given user`() {
        given {
            `I am`("Grace", `my default project is` = "apollo")
        } `when` {
            `I get default settings`()
        } then {

        }
    }
}

class SettingsTestContext: TestContext {
    var user: ID<User>? = null
    var userSettings: UserSettings? = null

    val repository: UserSettingsRepositoryDouble = UserSettingsRepositoryDouble()
    val settingsHandler: HandleUserProfile = UserProfileHandler(repository)

    fun `I am`(name: String, `my default project is`: String? = null) {
        user = ID.from(name)

        if (`my default project is` != null) {
            userSettings = UserSettings(user!!, ID.from(`my default project is`))
        }

        if (userSettings != null)
            repository.save(userSettings!!)
    }

    fun `I save this default settings`(project: String) {
        val project = ID.from<Project>(project)
        val userSettings = UserSettings(user!!, project = project)
        settingsHandler.save(userSettings)
    }

    fun `I get default settings`() = settingsHandler.get(user!!)

    fun `my settings should be`(project: String) {
        repository.items.firstOrNull { it.user == user }?.project?.shouldBe(ID.from(project)) ?: fail("expecting settings for $user. But no settings has been found")
    }
}