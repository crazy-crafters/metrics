package domain.survey


import org.junit.jupiter.api.Test
import domain.project.`given the NPS`
import domain.survey.model.Survey
import domain.survey.model.Survey.NPSSurvey
import domain.sharedkernel.model.DateTime

class SurveyMeasurementTest {
    @Test
    fun `opening a NPS to vote`() {
        `given the NPS`(
            """
            Apollo:
                No NPS yet
            """
        ) {
            `when I open a survey for`("Apollo")
                .`then I should get`(
                    score = NPSSurvey.Scores(promoters = 0, passives = 0, detractors = 0),
                    `starting on` = DateTime.parse("2024-01-12").noon(),
                    `closing on` = DateTime.INVALID
                )
        }
    }

    @Test
    fun `refusing to open a vote when another one is still valid`() {
        `given the NPS`(
            """
            Apollo:
                2024-01-12 to - -> 10 promoters, 0 passives, 0 detractors
            """
        ) {
            `when I open a survey for`("Apollo")
                .`then I should get an error`<Survey.Error.OpeningOverlapping>()
        }
    }

    @Test
    fun `getting last NPS for the given project`() {
        `given the NPS`(
            """
            Apollo
                2022-12-01 to 2022-12-15 -> 10 promoters, 10 passives, 10 detractors
                2023-01-01 to 2023-01-15 -> 20 promoters, 10 passives, 10 detractors
                2023-02-01 to 2023-02-15 -> 45 promoters, 10 passives, 5 detractors
            Marketplace
                2023-01-01 to 2023-01-15 -> 100 promoters, 100 passives, 100 detractors
                2023-02-01 to 2023-02-15 -> 200 promoters, 100 passives, 100 detractors
                2023-02-01 to 2023-02-15 -> 450 promoters, 120 passives, 50 detractors
            """
        ) {
            `when I ask for NPS`(of = "Apollo", on = "2023-02-20")
                //.`then I should get`("Apollo : 2023-02-01 to 2023-02-15 -> 45 promoters, 10 passives, 5 detractors")
                .`then I should get`(
                    score = NPSSurvey.Scores(promoters = 45, passives = 10, detractors = 5),
                    `starting on` = DateTime.parse("2023-02-01T12:00:00Z"),
                    `closing on` = DateTime.parse("2023-02-15T12:00:00Z"),
                )
        }
    }


    @Test
    fun `getting last NPS for the given project at the given date`() {
        `given the NPS`(
            """
            Apollo
                2022-12-01 to 2022-12-15 -> 10 promoters, 10 passives, 10 detractors
                2023-01-01 to 2023-01-15 -> 20 promoters, 10 passives, 10 detractors
                2023-02-01 to 2023-02-15 -> 45 promoters, 10 passives, 5 detractors
            Marketplace
                2023-01-01 to 2023-01-15 -> 100 promoters, 100 passives, 100 detractors
                2023-02-01 to 2023-02-15 -> 200 promoters, 100 passives, 100 detractors
                2023-02-01 to 2023-02-15 -> 450 promoters, 120 passives, 50 detractors
            """
        ) {
            `when I ask for NPS`(of = "Apollo", on = "2023-01-10")
                //.`then I should get`("Apollo : 2023-01-01 to 2023-01-15 -> 20 promoters, 10 passives, 10 detractors")
                .`then I should get`(
                    score = NPSSurvey.Scores(promoters = 20, passives = 10, detractors = 10),
                    `starting on` = DateTime.parse("2023-01-01").noon(),
                    `closing on` = DateTime.parse("2023-01-15").noon(),
                )
        }
    }


    @Test
    fun `getting an error if there is no NPS`() {
        `given the NPS`(
            """
        Apollo
            2022-12-01 to 2022-12-15 -> 10 promoters, 10 passives, 10 detractors
            2023-01-01 to 2023-01-15 -> 20 promoters, 10 passives, 10 detractors
            2023-02-01 to 2023-02-15 -> 45 promoters, 10 passives, 5 detractors
        """
        ) {
            `when I ask for NPS`(of = "Marketplace", on = "2023-01-10")
                .`then I should get an error`<Survey.Error.NoSurvey>()
        }
    }

    @Test
    fun `an user votes for a NPS`() {
        `given the NPS`(
            """
            Apollo
                2024-01-01 to - -> 45 promoters, 10 passives, 5 detractors
            """
        ) {
            `when I vote`("Apollo", 10, on = d("2024-01-12"))
                //.`then I should get`("Apollo: 2024-01-01 to 2024-06-30 -> 46 promoters, 10 passives, 5 detractors")
                .`then I should get`(
                    score = NPSSurvey.Scores(promoters = 46, passives = 10, detractors = 5),
                    `starting on` = DateTime.parse("2024-01-01").noon(),
                    `closing on` = DateTime.INVALID,
                )
        }
    }

    @Test
    fun `getting an error when voting while there is no survey`() {
        `given the NPS`(
            """
            Apollo
            """
        ) {
            `when I vote`("Apollo", 10, on = d("2024-01-12"))
                .`then I should get an error`<Survey.Error.Closed>()
        }
    }
    
    @Test
    fun `close a survey`() {
        `given the NPS`(
            """
            Apollo
                2022-12-01 to - -> 45 promoters, 10 passives, 5 detractors
            Marketplace
                2023-01-01 to 2023-01-15 -> 100 promoters, 100 passives, 100 detractors
                2023-02-01 to 2023-02-15 -> 200 promoters, 100 passives, 100 detractors
                2023-02-01 to 2023-02-15 -> 450 promoters, 120 passives, 50 detractors
            """
        ) {
            `when I close the survey for`("Apollo", "2024-01-12")
             .`then the survey is closed`()
        }
    }
}

private fun d(s: String): DateTime {
    return if (s.contains("T")) {
        DateTime.parse(s)
    } else {
        DateTime.parse("${s}T12:00:00")
    }
}