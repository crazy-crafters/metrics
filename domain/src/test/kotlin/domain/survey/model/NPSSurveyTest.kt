package domain.survey.model

import domain.project.stubs.apollo
import domain.sharedkernel.model.DateTime
import domain.survey.model.Survey.NPSSurvey
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test

fun NPSSurvey.`should have promoters`(i: Int) {
    this.scores.promoters.shouldBe(i)
}

fun NPSSurvey.`should have passives`(i: Int) {
    this.scores.passives.shouldBe(i)
}

fun NPSSurvey.`should have detractors`(i: Int) {
    this.scores.detractors.shouldBe(i)
}

fun NPSSurvey.`should have a level of`(expected: NPSLevel) {
    this.scores.level.shouldBe(expected)
}


val monday = DateTime.parse("2025-01-06")
val tuesday = DateTime.parse("2025-01-07")
val wednesday = DateTime.parse("2025-01-08")
val thursday = DateTime.parse("2025-01-09")
val friday = DateTime.parse("2025-01-10")
val saturday = DateTime.parse("2025-01-11")
val sunday = DateTime.parse("2025-01-12")

private fun voteEvent(on: DateTime, score: Int) = Vote(on, apollo.id, score)
private fun openEvent(on: DateTime) = OpenSurvey(on, apollo.id)
private fun closeEvent(on: DateTime) = CloseSurvey(on, apollo.id)

class NPSSurveyTest {
    @Test
    fun `a new survey should start with a Open event`() {
        NPSSurvey.rebuild().shouldBeTypeOf<Survey.Error.NoSurvey>()
        NPSSurvey.rebuild(voteEvent(on = monday, 9)).shouldBeTypeOf<Survey.Error.NoSurvey>()
    }

    @Test
    fun `open a NPS survey`() {
        NPSSurvey.rebuild(openEvent(on = monday)).shouldBeTypeOf<NPSSurvey>().let {
            it.isOpen shouldBe true
            it.scores.shouldBe(NPSSurvey.Scores(0, 0, 0))
        }
    }

    @Test
    fun `add a vote to a NPS survey`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 9)
        ).shouldBeTypeOf<NPSSurvey>().scores.shouldBe(NPSSurvey.Scores(1, 0, 0))
    }

    @Test
    fun `a vote of 9 or 10 is a promoter`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 9),
            voteEvent(tuesday, 10)
        ).shouldBeTypeOf<NPSSurvey>().scores.shouldBe(NPSSurvey.Scores(2, 0, 0))
    }

    @Test
    fun `a vote between 0 and 6 is a detractor`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 0),
            voteEvent(tuesday, 1),
            voteEvent(tuesday, 2),
            voteEvent(tuesday, 3),
            voteEvent(tuesday, 4),
            voteEvent(tuesday, 5)
        ).shouldBeTypeOf<NPSSurvey>().also {
            it.scores.shouldBe(NPSSurvey.Scores(0, 0, 6))
        }
    }

    @Test
    fun `a vote between of 7 and 8 is a passive`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 7),
            voteEvent(tuesday, 8)
        ).shouldBeTypeOf<NPSSurvey>().also {
            it.scores.shouldBe(NPSSurvey.Scores(0, 2, 0))
        }
    }

    @Test
    fun `close a survey`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 0),
            voteEvent(tuesday, 8),
            closeEvent(on = friday)
        ).shouldBeTypeOf<NPSSurvey>().let {
            it.isOpen shouldBe false
        }
    }

    @Test
    fun `a NPS between -100 and 0 is Poor (more detractors than promoters)`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 0),
            voteEvent(tuesday, 0),
            voteEvent(tuesday, 10),
            closeEvent(on = friday)
        ).shouldBeTypeOf<NPSSurvey>().also {
            it.scores.level.shouldBe(NPSLevel.Poor)
        }
    }

    @Test
    fun `a NPS between 0 and 30 is Average`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 4),
            voteEvent(tuesday, 10),
            closeEvent(on = friday)
        ).shouldBeTypeOf<NPSSurvey>().also {
            it.scores.level.shouldBe(NPSLevel.Average)
        }
    }

    @Test
    fun `a NPS between 31 and 70 is Good`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 8),
            voteEvent(tuesday, 10),
            closeEvent(on = friday)
        ).shouldBeTypeOf<NPSSurvey>().also {
            it.scores.level.shouldBe(NPSLevel.Good)
        }
    }

    @Test
    fun `a NPS between more than 71 is Excellent`() {
        NPSSurvey.rebuild(
            openEvent(on = monday),
            voteEvent(tuesday, 10),
            closeEvent(on = friday)
        ).shouldBeTypeOf<NPSSurvey>().also {
            it.scores.level.shouldBe(NPSLevel.Excellent)
        }
    }

    @Test
    fun `build multiple surveys`() {
        NPSSurvey.rebuildAll(
            openEvent(on = monday),
            voteEvent(tuesday, 6),
            closeEvent(on = wednesday),
            openEvent(on = tuesday),
            voteEvent(friday, 10),
            closeEvent(on = saturday)
        ).map { it.scores }.shouldContainExactly(
            NPSSurvey.Scores(0, 0, 1),
            NPSSurvey.Scores(1, 0, 0)
        )
    }
}
