package domain.sharedkernel

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import domain.sharedkernel.materials.`should be failure and`
import domain.sharedkernel.materials.`should be success and`
import org.junit.jupiter.api.Test
import domain.sharedkernel.model.Either

class EitherAssertTest {

    @Test
    fun `should execute lambda on success with extension function`() {
        var called = false
        Either.success<Int, String>(12).`should be success and` {
            called = true
        }

        called shouldBe true
    }

    @Test
    fun `should execute lambda on success`() {
        var called = false

        val either = Either.success<Int, String>(12)

        either `should be success and` {
            called = true
        }

        called shouldBe true
    }

    @Test
    fun `should execute lambda on error with extension function`() {
        var called = false

        val either = Either.fail<Int, String>("Error")

        either.`should be failure and` {
            called = true
        }

        called shouldBe true
    }

    @Test
    fun `should execute lambda on error`() {
        var called = false

        val either = Either.fail<Int, String>("Error")

        either `should be failure and` {
            called = true
        }

        called shouldBe true
    }

    @Test
    fun `A success either should fail when _should be failure of_ is called`() {
        val either = Either.success<Int, String>(12)

        shouldThrow<AssertionError> {
            either.`should be failure and` {}
        }
    }

    @Test
    fun `A failure either should fail when _should be success of_ is called`() {
        val either = Either.fail<Int, String>("Error")

        shouldThrow<AssertionError> {
            either.`should be success and` {}
        }
    }
}
