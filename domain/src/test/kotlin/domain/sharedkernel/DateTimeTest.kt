package domain.sharedkernel

import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.time.Duration.Companion.days

class DateTimeTest {
    @Test
    fun `get day of month`() {

        (1..31).forEach {
            val dt = "2025-05-${it.toString().padStart(2, '0')}T12:00:00".toDateTime()
            dt.dayOfMonth shouldBe it
        }
    }

    @ParameterizedTest
    @ValueSource(strings = [
        "2025-03-05T12:00:00",
        "2025-03-05T12:00:00.0",
        "2025-03-05T12:00:00.00",
        "2025-03-05T12:00:00.000",
        "2025-03-05T12:00:00.00Z",
        "2025-03-05T12:00:00.00Z",
        "2025-03-05T12:00:00.000Z",
        "2025-03-05T13:00:00+01:00",
        "2025-03-05T13:00:00.0+01:00",
        "2025-03-05T13:00:00.00+01:00",
        "2025-03-05T13:00:00.000+01:00",
        "2025-03-05T14:00:00.00+02:00",
        "2025-03-05T14:00:00.00+02:00",
        "2025-03-05T14:00:00.000+02:00",
        "2025-03-05T14:00:00.000+0200",
    ])
    fun `parse different formats including offset`(input: String) {
        input.toDateTime().value shouldBe Instant.parse("2025-03-05T12:00:00Z")
    }

    @Test
    fun `throw exception when that's not a date`() {
        shouldThrow<IllegalArgumentException> {
            DateTime.parse("foobar")
        }
    }

    @Test
    fun `get diff between dates`() {
        val start = "2025-03-01T00:00:00".toDateTime()
        val end =  "2025-03-31T00:00:00".toDateTime()

        start.until(end, ChronoUnit.DAYS) shouldBe 30L

        start.until(end, ChronoUnit.HOURS) shouldBe 30L * 24

        start.until(end, ChronoUnit.MINUTES) shouldBe 30L * 24 * 60

        start.until(end, ChronoUnit.SECONDS) shouldBe 30L * 24 * 60 * 60
    }

    @Test
    fun `iterate through dates`() {
        val start = "2025-03-01T00:00:00".toDateTime()
        val end =  "2025-03-31T00:00:00".toDateTime()

        start.until(end, Duration.of(1, ChronoUnit.DAYS)).fold(0) { acc, _ -> acc + 1 } shouldBe 31

        (start .. end).fold(0) { acc, _ -> acc + 1 } shouldBe 31
    }

    @Test
    fun `iterate through dates day by day`() {
        val start = "2025-03-01T00:00:00".toDateTime()
        val end =  "2025-03-31T00:00:00".toDateTime()

        (start .. end).fold(0) { acc, _ -> acc + 1 } shouldBe 31
    }

    @Test
    fun `two dates are on same day no matter the time`() {
        val d1 = "2025-03-01T08:00:00".toDateTime()
        val d2 =  "2025-03-01T20:00:00".toDateTime()

        d1.isSameDay(d2) shouldBe true
        d2.isSameDay(d1) shouldBe true

        d1.isSameDay(d1.plus(Duration.ofDays(12))) shouldBe false
    }

    @Test
    fun `get date same day at midnight`() {
        "2025-03-01T08:00:00".toDateTime().startOfDay() shouldBe "2025-03-01T00:00:00".toDateTime()
    }

    @Test
    fun `get date same day at the end of day`() {
        "2025-03-01T08:00:00".toDateTime().endOfDay() shouldBe "2025-03-01T23:59:59.999999999Z".toDateTime()
    }

    @Test
    fun `get date same day at noon`() {
        "2025-03-01T08:00:00".toDateTime().noon() shouldBe "2025-03-01T12:00:00".toDateTime()
    }
}