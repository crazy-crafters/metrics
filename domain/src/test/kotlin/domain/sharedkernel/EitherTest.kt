package domain.sharedkernel

import io.kotest.matchers.booleans.shouldBeFalse
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import domain.sharedkernel.model.Either
import domain.sharedkernel.model.generify
import domain.sharedkernel.model.mapOnSuccess
import domain.sharedkernel.model.merge

sealed class GeneralError
data class Error(val message: String="this is an error"): GeneralError()

sealed class GeneralValue
data class Value(val value: Int=12): GeneralValue()

class EitherTest {

    @Test
    fun `should fold`() {
        error.fold(
                { Assertions.fail("Expecting an error but got the value") },
                { it }
        ).shouldBe(Error())
        
        value.fold(
                { it },
                { Assertions.fail("Expecting a value but got the error") }
        ).shouldBe(Value())
    }

    @Test
    fun `should map`() {
        error.map<Double, Double>(
                { Assertions.fail("Expecting an error but got the value") },
                { 1.2 }
        ).shouldBe(Either.fail<Value, Double>(1.2))
        value.map<Double, Double>(
                { 1.2 },
                { Assertions.fail("Expecting an error but got the error") }
        ).shouldBe(Either.success<Double, Error>(1.2))
    }

    @Test
    fun `should display`() {
        error.toString().shouldBe("Error{Error(message=this is an error)}")
        value.toString().shouldBe("Value{Value(value=12)}")
    }

    @Test
    fun `should generify`() {
        var generified: Either<GeneralValue, GeneralError> = error.generify()
        generified.shouldBe(Either.fail<GeneralValue, GeneralError>(Error()))
        generified = value.generify()
        generified.shouldBe(Either.success<GeneralValue, GeneralError>(Value()))
    }

    @Test
    fun `should merge values`() {
        listOf(
            Either.success(Value(1)),
            Either.success<Value, Error>(Value(2)),
            Either.success(Value(3)),
            Either.success(Value(4))
        ).merge().shouldBe(Either.success(listOf(Value(1), Value(2), Value(3), Value(4))))
    }

    @Test
    fun `should stop merge on error`() {
        listOf(
            Either.success<Value, Error>(Value(1)),
            Either.success(Value(2)),
            Either.fail(Error()),
            Either.success(Value(4))
        ).merge().shouldBe(Either.fail(Error()))
    }

    @Test
    fun `should map a value`() {
        value.mapOnSuccess { Either.success<Double, GeneralError>(1.2) }
            .shouldBe(Either.success(1.2))
    }

    @Test
    fun `mapValue should return the error`() {
        error.mapOnSuccess { Either.success<Double, GeneralError>(1.2) }
            .shouldBe(error)
    }

    @Test
    fun `should apply on success`() {
        value.onSuccess {
            value -> value.shouldBe(Value(12))
        }
        .onFailure {
                fail("Should not call onFailure when it's a value")
        }
    }

    @Test
    fun `should apply on failure`() {
        error.onSuccess {
            fail("Should not call onSuccess when it's an error")
        }
            .onFailure {
                value -> value.shouldBe(Error("this is an error"))
            }
    }

     @Test
     fun `should return value or null`() {
         value.orNull().shouldBe(Value(12))
         error.orNull().shouldBe(null)
     }

    @Test
    fun `should flag as value or error`() {
        value.isSuccess.shouldBeTrue()
        value.isError.shouldBeFalse()
        error.isSuccess.shouldBeFalse()
        error.isError.shouldBeTrue()
    }

    @Test
    fun `should return a value if no exception has been raised`() {
        val result = Either.catch(
            onException = { e -> e.message },
        ) {
            12
        }

        result.isSuccess.shouldBeTrue()
        result.onSuccess {
            it.shouldBe(12)
        }
    }

    @Test
    fun `should catch an exception`() {
        val result = Either.catch(
            onException = { e -> e.message },
        ) {
            throw IllegalArgumentException("error received")
        }

        result.isError.shouldBeTrue()
        result.onFailure {
            it.shouldBe("error received")
        }
    }

    @Test
    fun `should return a success according to the predicate`() {
        val result = Either.conditionally(true,
            { 12 },
            { "abc" })

        result.isSuccess.shouldBeTrue()
    }

    @Test
    fun `should return a failure according to the predicate`() {
        val result = Either.conditionally(false,
            { 12 },
            { "abc" })

        result.isError.shouldBeTrue()
    }


    private val error = Either.fail<Value, Error>(Error("this is an error"))
    private val value = Either.success<Value, Error>(Value(12))
}
