package domain.sharedkernel

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import domain.sharedkernel.model.Name

class NameTest {
    @Test
    fun `a name cannot be blank`() {
        assertThrows<AssertionError> {
            Name.from<String>("   ")
        }
    }

    @Test
    fun `the name is case insensitive`() {
        Name.from<String>("Grace Slick").shouldBe(Name.from("grace SLICK"))
        Name.from<String>("Grace Slick").hashCode().shouldBe(Name.from<String>("grace SLICK").hashCode())
    }
}