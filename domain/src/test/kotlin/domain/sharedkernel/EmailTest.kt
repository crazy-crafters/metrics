package domain.sharedkernel

import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.EmailSizeExceededException
import domain.sharedkernel.model.MalFormedEmailException
import kotlin.random.Random

class EmailTest {
    @Test
    fun `an email should be case insensitive`() {
        Email.from("GRACE@spam.com").shouldBe(Email.from("grace@spam.COM"))
        Email.from("GRACE@spam.com").hashCode().shouldBe(Email.from("grace@spam.COM").hashCode())
    }

    @Test
    fun `an malformed email should throw an error`() {
        shouldThrow<MalFormedEmailException> {
            Email.from("grace")
        }
    }

    @Test
    fun `an email is valid when length is under 254 characters`() {
        shouldNotThrowAny {
            Email.from("a".repeat(Random.nextInt(1,249)).plus("@x.com"))
        }
    }

    @Test
    fun `an email can not exceed 254 characters`() {
        shouldThrow<EmailSizeExceededException> {
            Email.from("a".repeat(255))
        }
    }
}
