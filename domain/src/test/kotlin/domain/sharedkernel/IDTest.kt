package domain.sharedkernel

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.Test
import domain.sharedkernel.model.ID


interface DummyType

class IDTest {

    private fun makeValue() =        ID.from<DummyType>("00000000-0000-0000-0000-000000000001")
    private fun makeAnotherValue() = ID.from<DummyType>("00000000-0000-0000-0000-000000000002")
    @Test
    fun `assert equality`() {
        makeValue().shouldBe(makeValue())
        makeValue().hashCode().shouldBe(makeValue().hashCode())
    }

    @Test
    fun `assert inequality`() {
        makeValue().shouldNotBe(makeAnotherValue())
        makeValue().hashCode().shouldNotBe(makeAnotherValue().hashCode())
    }
}