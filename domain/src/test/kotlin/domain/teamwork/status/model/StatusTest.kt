package domain.teamwork.status.model

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label

@DisplayName("Model Status")
class StatusTest {
    @Test
    fun `assert equality on id`() {
        Status.Undefined(ID.from("foobar"), Label.from("foobar")) shouldBe Status.Undefined(ID.from("foobar"), Label.from("FOOBAR"))
        Status.Undefined(ID.from("foobar"), Label.from("foobar")).hashCode() shouldBe Status.Undefined(ID.from("foobar"), Label.from("FOOBAR")).hashCode()
    }

    @Test
    fun `assert inequality on different values`() {
        Status.Undefined(ID.from("foobar"), Label.from("foobar")) shouldNotBe "spam"
        Status.Undefined(ID.from("foobar"), Label.from("foobar")) shouldNotBe Status.Undefined(ID.from("spam"), Label.from("spam"))
        Status.Undefined(ID.from("foobar"), Label.from("foobar")).hashCode() shouldNotBe Status.Undefined(ID.from("spam"), Label.from("spam")).hashCode()
    }

    @Test
    fun `check if a status is in the list`() {
        val planned = Status.Planned(ID.from("planned"), Label.from("foobar"))
        val waiting = Status.Waiting(ID.from("waiting"), Label.from("foobar"))
        val working = Status.Working(ID.from("working"), Label.from("foobar"))

        (planned in planned or working) shouldBe true
        (planned in waiting or working) shouldBe false
    }
}