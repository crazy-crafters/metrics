package domain.teamwork.status

import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.doubles.StatusRepositoryDouble
import domain.teamwork.status.model.ExistingStatus
import domain.teamwork.status.model.Status

@DisplayName("Status")
class StatusTest {
    @Test
    fun `list all status for the given project`() {
        val repository = StatusRepositoryDouble().with(
            project= anId.ofApollo(),
            Status.Waiting(id = ID.from("1"), label = Label.from("Ready")),
            Status.Working(id = ID.from("2"), label = Label.from("In Progress")),
            Status.Done(id = ID.from("3"), label = Label.from("Done"))
        ).with(
            project= anId.of("ariane"),
            Status.Waiting(id = ID.from("1"), label = Label.from("To Do")),
            Status.Working(id = ID.from("5"), label = Label.from("Doing")),
            Status.Done(id = ID.from("6"), label = Label.from("Closed"))
        )

        val sut = StatusHandler(repository)

        sut.statuses(of= anId.ofApollo()).shouldContainExactly(
            Status.Waiting(id = ID.from("1"), label = Label.from("Ready")),
            Status.Working(id = ID.from("2"), label = Label.from("In Progress")),
            Status.Done(id = ID.from("3"), label = Label.from("Done"))
        )
    }

    @Test
    fun `change state of a given status`() {
        val repository = StatusRepositoryDouble().with(
            project= anId.ofApollo(),
            Status.Undefined(id = ID.from("1"), label = Label.from("Ready")),
        ).with(
            project= anId.of("ariane"),
            Status.Waiting(id = ID.from("1"), label = Label.from("To Do")),
        )

        val sut = StatusHandler(repository)

        sut.update(project= anId.ofApollo(), id= ID.from("1"), domain.teamwork.status.model.Status.State.Waiting)

        sut.statuses(of= anId.ofApollo()).shouldContainExactly(
            Status.Waiting(id = ID.from("1"), Label.from("Ready"))
        )
    }

    @Test
    fun `change state of an unknown status will raise an error`() {
        val repository = StatusRepositoryDouble().with(
            project= anId.ofApollo(),
            Status.Undefined(id = ID.from("1"), Label.from("Ready")),
        ).with(
            project= anId.of("ariane"),
            Status.Waiting(id = ID.from("1"), Label.from("To Do")),
        )

        val sut = StatusHandler(repository)

        val result = sut.update(project= anId.ofApollo(), id = ID.from("123"), domain.teamwork.status.model.Status.State.Waiting)
        result.shouldBeTypeOf<ExistingStatus.NotFound>()
    }
}
