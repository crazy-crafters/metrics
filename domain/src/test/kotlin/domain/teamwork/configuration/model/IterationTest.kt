package domain.teamwork.configuration.model

import domain.sharedkernel.model.DateTime.Companion.toDateTime
import io.kotest.matchers.shouldBe
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.teamwork.configuration.Iteration
import domain.teamwork.configuration.NoIterationCreationEventError
import domain.teamwork.configuration.NoIterationToCreateError
import domain.teamwork.shared.events.IterationFinished
import domain.teamwork.shared.events.IterationStarted
import org.junit.jupiter.api.Test

class IterationTest {

    @Test
    fun `rebuild an iteration`() {
        Iteration.from(
            IterationStarted("2024-01-10".toDateTime(), ID.from("dummy"), ID.from("dummy"), Name.from("dummy")),
            IterationFinished("2024-01-20".toDateTime(), ID.from("dummy"), ID.from("dummy"))
        ).fold({
           it.from shouldBe "2024-01-10".toDateTime()
           it.to shouldBe "2024-01-20".toDateTime()
        }, {TODO()})
    }

    @Test
    fun `when there is no event, we should get an error`() {
        Iteration.from().fold({
            TODO()
        }, {
         it shouldBe NoIterationToCreateError
        })
    }

    @Test
    fun `when there is no started event, we should get an error`() {
        Iteration.from(
            IterationFinished("2024-01-20".toDateTime(), ID.from("dummy"), ID.from("dummy"))
        ).fold({TODO()}, {
           it shouldBe NoIterationCreationEventError(ID.from("dummy"))
        })
    }
}