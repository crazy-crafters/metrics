package domain.teamwork.configuration

import io.kotest.matchers.shouldBe
import domain.teamwork.doubles.ConfigurationRepositoryDouble
import domain.teamwork.configuration.model.BoardConfiguration
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.teamwork.aConfiguration
import domain.project.model.Project
import domain.teamwork.toConfiguration

class ConfigurationHandlerTest {
    @Test
    fun `getting current configuration for the given project`() {
        val configurationRepositoryDouble = ConfigurationRepositoryDouble()
        configurationRepositoryDouble.save(anId.ofApollo(), aConfiguration.of())
        configurationRepositoryDouble.save(anId.of("ariane"), anId.of<Project>("ariane").toConfiguration())

        val configure = ConfigurationHandler(configurationRepositoryDouble)
       configure.get(anId.ofApollo()) shouldBe aConfiguration.of()
    }

    @Test
    fun `getting none when no configuration has been set`() {
        val configurationRepositoryDouble = ConfigurationRepositoryDouble()

        val configure = ConfigurationHandler(configurationRepositoryDouble)
       configure.get(anId.ofApollo()) shouldBe BoardConfiguration.None
    }

    @Test
    fun `saving configuration`() {
        val configurationRepositoryDouble = ConfigurationRepositoryDouble()
        val configure = ConfigurationHandler(configurationRepositoryDouble)

        configure.save(anId.ofApollo(), aConfiguration.of())

        configurationRepositoryDouble.`should have a configuration for`(anId.ofApollo())
    }

}



