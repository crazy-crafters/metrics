package domain.teamwork.issue

import io.kotest.matchers.collections.shouldContainExactly
import domain.teamwork.d
import domain.teamwork.doubles.EventRepositoryDouble
import domain.teamwork.doubles.toIssue
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId

class IssueHandlerTest {
    val issue1 = "Issue 1: 2 points -> ready 2024-01-01, in_progress 2024-01-02, done 2024-01-04".toIssue(anId.ofApollo())
    val issue2 = "Issue 2: 5 points -> ready 2024-01-01, in_progress 2024-01-03, done 2024-01-07".toIssue(anId.ofApollo())
    val issue3 = "Issue 3: 2 points -> ready 2024-01-05, in_progress 2024-01-09, done 2024-01-10".toIssue(anId.ofApollo())

    val eventRepository: EventRepositoryDouble = EventRepositoryDouble().let {
        it.contains(
            issue1,
            issue2,
            issue3
        )

        it
    }

    val getSnapshot = IssueHandler(eventRepository)

    @Test
    fun `finding all issues in a time frame`() {
        getSnapshot.snapshot(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"),).issues.shouldContainExactly(
            issue1,
            issue2,
            issue3
        )
    }


    @Test
    fun `finding all issues in a time frame excluding issues after`() {
        getSnapshot.snapshot(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-03"),).issues.shouldContainExactly(
            issue1,
            issue2,
        )
    }

    @Test
    fun `finding all issues in a time frame excluding issues before`() {
        getSnapshot.snapshot(anId.ofApollo(), from = d("2024-01-05"), to = d("2024-01-15"),).issues.shouldContainExactly(
            issue2,
            issue3,
        )
    }
}

