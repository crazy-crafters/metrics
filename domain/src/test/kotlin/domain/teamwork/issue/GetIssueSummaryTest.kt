package domain.teamwork.issue

import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.ID
import domain.teamwork.doubles.EventRepositoryDouble
import domain.teamwork.issue.model.IssueSummary

class GetIssueSummaryTest {
    @Test
    fun `getting a single issue`() {
        val eventRepository = EventRepositoryDouble()
        val getSnapshot = IssueHandler(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-08 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
        """
        )

        val result = getSnapshot.summary(anId.ofApollo(), ID.from("Issue 1"))

        result.shouldBeTypeOf<IssueSummary.Valid>()

        result.id shouldBe ID.from("Issue 1")
    }

    @Test
    fun `getting an error when the issue does not exist`() {
        val eventRepository = EventRepositoryDouble()
        val getSnapshot = IssueHandler(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-08 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
        """
        )

        val result = getSnapshot.summary(anId.ofApollo(), ID.from("Issue ZZZZZ"))

        result.shouldBeTypeOf<IssueSummary.NoSummary>()
    }

    @Test
    fun `getting an error when the issue is not closed`() {
        val eventRepository = EventRepositoryDouble()
        val getSnapshot = IssueHandler(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02 
        """
        )

        val result = getSnapshot.summary(anId.ofApollo(), ID.from("Issue 1"))

        result.shouldBeTypeOf<IssueSummary.NoSummary>()
    }
}
