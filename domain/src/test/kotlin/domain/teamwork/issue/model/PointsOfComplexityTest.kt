package domain.teamwork.issue.model

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class PointsOfComplexityTest {
    @Test
    fun `assert equality`() {
        PointsOfComplexity.of(13) shouldBe PointsOfComplexity.of(13)

        PointsOfComplexity.of(13).hashCode() shouldBe PointsOfComplexity.of(13).hashCode()
    }

    @Test
    fun `sum points of complexity`() {
        PointsOfComplexity.of(13) + PointsOfComplexity.of(21) shouldBe PointsOfComplexity.of(34)
    }

    @Test
    fun `minus points of complexity`() {
        PointsOfComplexity.of(13) - PointsOfComplexity.of(5) shouldBe PointsOfComplexity.of(8)
    }

    @Test
    fun `compare points of complexity`() {
        PointsOfComplexity.of(13).compareTo(PointsOfComplexity.of(8)) shouldBe 1

        PointsOfComplexity.of(8).compareTo(PointsOfComplexity.of(8)) shouldBe 0

        PointsOfComplexity.of(8).compareTo(PointsOfComplexity.of(13)) shouldBe -1
    }
}
