package domain.teamwork.issue.model

import io.kotest.matchers.shouldBe
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.sharedkernel.model.DateTime
import domain.teamwork.doubles.*
import domain.teamwork.shared.events.*
import domain.teamwork.startOfDay
import domain.teamwork.status.model.Status
import org.junit.jupiter.api.Test

class IssueMetricsTest {
    @Test
    fun `getting cycle time`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.cycleTime(
            between = Status.Ready,
            and = Status.isDone
        ) shouldBe (startOfDay("2024 01 10") - startOfDay("2024 01 20"))
    }

    @Test
    fun `getting overall cycle time`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.cycleTime shouldBe (startOfDay("2024 01 10") - startOfDay("2024 01 20"))
    }

    @Test
    fun `getting cycle time between unknown states will be 0`() {
        make(startOfDay("2024 01 02")).cycleTime(
            between = Status.Ready,
            and = Status.isDone
        ) shouldBe TimeDuration.seconds(0)

        make(startOfDay("2024 01 02")).with(Status.Ready, startOfDay("2024 01 02"))
            .cycleTime(between = Status.Ready, and = Status.isDone) shouldBe TimeDuration.seconds(0)
    }

    @Test
    fun `getting lead time when issue is done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.isClosed shouldBe true
        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 10") - startOfDay("2024 01 20"))
    }


    @Test
    fun `lead time is invalid when the issue is not closed nor done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))

        issue.isClosed shouldBe false
        issue.leadTime(startOfDay("2024 01 30")) shouldBe (TimeDuration.Invalid)
    }

    @Test
    fun `getting lead time when issue is closed but not done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .close(on = startOfDay("2024 01 20"))

        issue.isClosed shouldBe true
        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 10") - startOfDay("2024 01 20"))
    }

    @Test
    fun `getting lead time when no events exist`() {
        val issue = make(startOfDay("2024 01 10"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (TimeDuration.Invalid)
    }


    @Test
    fun `getting lead time when issue is added but never moved or closed`() {
        val issue = make(startOfDay("2024 01 10"))

issue.leadTime(startOfDay("2024 01 30")) shouldBe TimeDuration.Invalid
    }

    @Test
    fun `getting lead time when issue is closed without being moved to Done`() {
        val issue = make(startOfDay("2024 01 10"))
            .close(on = startOfDay("2024 01 20"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 20") - startOfDay("2024 01 10"))
    }

    @Test
    fun `getting lead time when issue is moved to Done then closed`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))
            .close(on = startOfDay("2024 01 25"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 20") - startOfDay("2024 01 10"))
    }

    @Test
    fun `getting lead time when issue is moved through multiple statuses before Done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 14"))
            .with(Status.isDone, on = startOfDay("2024 01 18"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 18") - startOfDay("2024 01 10"))
    }

    @Test
    fun `getting lead time when issue is moved directly to Closed without Done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 14"))
            .close(on = startOfDay("2024 01 18"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 18") - startOfDay("2024 01 10"))
    }

    @Test
    fun `getting lead time when issue is Done but moved again before closing`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .with(Status.isDone, on = startOfDay("2024 01 18"))
            .with(Status.InProgress, on = startOfDay("2024 01 22"))
            .close(on = startOfDay("2024 01 25"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 25") - startOfDay("2024 01 10"))
    }

    @Test
    fun `getting lead time when issue is closed immediately after Done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .with(Status.isDone, on = startOfDay("2024 01 18"))
            .close(on = startOfDay("2024 01 18"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 18") - startOfDay("2024 01 10"))
    }

    @Test
    fun `getting lead time when issue is closed reopened and closed again`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .with(Status.isDone, on = startOfDay("2024 01 18"))
            .close(on = startOfDay("2024 01 20"))
            .added(on = startOfDay("2024 01 22"))
            .with(Status.InProgress, on = startOfDay("2024 01 22"))
            .close(on = startOfDay("2024 01 25"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 25") - startOfDay("2024 01 10"))
    }


    @Test
    fun `getting lead time when issue is closed before being done`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 12"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .close(on = startOfDay("2024 01 20"))
            .with(Status.isDone, on = startOfDay("2024 01 22"))

        issue.leadTime(startOfDay("2024 01 30")) shouldBe (startOfDay("2024 01 20") - startOfDay("2024 01 10"))
    }


    @Test
    fun `getting time between two states in a workflow`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.cycleTime(between = Status.Ready, and = Status.isDone) shouldBe (startOfDay("2024 01 10") - startOfDay("2024 01 20"))
    }

    @Test
    fun `getting longest time between two states in a workflow when issue made a rollback`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.Ready, on = startOfDay("2024 01 13"))
            .with(Status.InProgress, on = startOfDay("2024 01 14"))
            .with(Status.isDone, on = startOfDay("2024 01 16"))
            .with(Status.InProgress, on = startOfDay("2024 01 17"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.cycleTime(between = Status.Ready, and = Status.isDone) shouldBe (startOfDay("2024 01 10") - startOfDay("2024 01 20"))
    }

    @Test
    fun `get efficiency when no waiting period`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 10"))
            .with(Status.isDone, on = startOfDay("2024 01 15"))

        issue.efficiency shouldBe (Efficiency.of(100))
    }

    @Test
    fun `efficiency does not take into account first Planned status`() {
        val issue = make(startOfDay("2024 01 09"))
            .with(Status.Planned, on = startOfDay("2024 01 09"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.efficiency shouldBe (Efficiency.of(50))
    }

    @Test
    fun `efficiency considers Planned status as waiting time when the issue has started`() {
        val issue = make(startOfDay("2024 01 09"))
            .with(Status.Planned, on = startOfDay("2024 01 09"))
            .with(Status.InProgress, on = startOfDay("2024 01 15"))
            .with(Status.Planned, on = startOfDay("2024 01 16"))
            .with(Status.InProgress, on = startOfDay("2024 01 18"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.efficiency shouldBe (Efficiency.of(60))
    }

    @Test
    fun `efficiency is available only for issue closed`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))

        issue.efficiency shouldBe (Efficiency.Invalid)
    }

    @Test
    fun `get efficiency when no working period`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.isDone, on = startOfDay("2024 01 15"))

        issue.efficiency shouldBe (Efficiency.of(0))
    }

    @Test
    fun `efficiency when there are a waiting and a working period`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.isDone, on = startOfDay("2024 01 15"))

        issue.efficiency shouldBe (Efficiency.of((3.0 / 5.0 * 100).toInt()))
    }

    @Test
    fun `efficiency when there there is rework`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.Ready, on = startOfDay("2024 01 10"))
            .with(Status.InProgress, on = startOfDay("2024 01 12"))
            .with(Status.isDone, on = startOfDay("2024 01 15"))
            .with(Status.InProgress, on = startOfDay("2024 01 17"))
            .with(Status.isDone, on = startOfDay("2024 01 20"))

        issue.efficiency shouldBe (Efficiency.of((6.0 / (10.0 - 2) * 100).toInt()))
    }
}


fun make(creation: String): Issue = make(startOfDay(creation))

fun make(creation: DateTime): Issue =
    Issue.from(
        IssueAdded(
            entityId = ID.from("dummy"),
            project = ID.from("dummy"),
            title = Title.from("dummy"),
            date = creation,
            status = Status.Backlog,
            estimation = PointsOfComplexity.of(1),
            tags = Tags.empty()

        ),
    ).fold({ it }, { TODO() })

fun Issue.with(status: Status, on: String): Issue = with(status, startOfDay(on))
fun Issue.with(status: Status, on: DateTime): Issue =
    apply(
        IssueMoved(
            date = on,
            entityId = ID.from("dummy"),
            project = ID.from("dummy"),
            status = status
        )
    )

fun Issue.close(on: String): Issue = close(startOfDay(on))
fun Issue.close(on: DateTime): Issue =
    apply(
        IssueClosed(
            date = on,
            entityId = ID.from("dummy"),
            project = ID.from("dummy"),
        )
    )

fun Issue.added(on: String): Issue = added(startOfDay(on))
fun Issue.added(on: DateTime): Issue =
    apply(
        IssueAdded(
            date = on,
            entityId = ID.from("dummy"),
            project = ID.from("dummy"),
            title = Title.from("dummy"),
            status = Status.Backlog,
            estimation = PointsOfComplexity.of(1),
            tags = Tags.empty()
        )
    )

fun Issue.renamed(on: String, title: String): Issue =
    apply(
        IssueRenamed(
            date = startOfDay(on),
            entityId = ID.from("dummy"),
            project = ID.from("dummy"),
            title = Title.from(title),
        )
    )

fun Issue.estimate(on: String, complexity: Int): Issue =
    apply(
        IssueEstimated(
            date = startOfDay(on),
            entityId = ID.from("dummy"),
            project = ID.from("dummy"),
            estimation = PointsOfComplexity.of(complexity),
        )
    )