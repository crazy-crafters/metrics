package domain.teamwork.issue.model

import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.sharedkernel.materials.`should be failure and`
import domain.sharedkernel.materials.`should be success and`
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.sharedkernel.model.Title
import domain.teamwork.*
import domain.teamwork.doubles.InProgress
import domain.teamwork.doubles.Ready
import domain.teamwork.doubles.`an issue with`
import domain.teamwork.doubles.isDone
import domain.teamwork.shared.events.IterationStarted
import domain.teamwork.status.model.Status

class IssueFromEventsTest {

    @Test
    fun `build an issue from creation event`() {
        Issue.from(makeCreateEvent(id="1", date="2024-01-12", estimation = 5, title = "Issue 1")).`should be success and` {
            id shouldBe (ID.from("1"))
            title shouldBe (Title.from("Issue 1"))
            creationDate shouldBe (DateTime.parse("2024-01-12T12:00:00"))
            estimation shouldBe (PointsOfComplexity.of(5))
        }
    }

    @Test
    fun `raise an error when there is no 'issue added' event`() {
        Issue.from(makeMoveEvent(id="1", date="2024-01-12")).`should be failure and` {
            this.shouldBeTypeOf<NoIssueCreationEventError>()
        }
    }

    @Test
    fun `raise an error when there is no event at all`() {
        Issue.from(makeMoveEvent(id="1", date="2024-01-12")).`should be failure and` {
            this.shouldBeTypeOf<NoIssueCreationEventError>()
        }
    }

    @Test
    fun `get status from a new issue`() {
        Issue.from(makeCreateEvent(id="1", date="2024-01-12", estimation = 5, title = "Issue 1")).`should be success and` {
            status("2024-01-13".toDateTime()) shouldBe (Status.OutOfScope)
        }
    }

    @Test
    fun `get status from an issue on the given date`() {
        Issue.from(
            makeCreateEvent(id="1", date="2024-01-12", estimation = 5, title = "Issue 1"),
            makeMoveEvent(id="1", date="2024-01-14", newStatus = "in progress"),
            makeMoveEvent(id="1", date="2024-01-16", newStatus = "done")
        ).`should be success and` {
            status("2024-01-15".toDateTime()) shouldBe (Status.InProgress)
            status("2024-01-17".toDateTime()) shouldBe (Status.isDone)
        }
    }

    @Test
    fun `status before issue has been created is out of scope`() {
        Issue.from(
            makeCreateEvent(id="1", date="2024-01-12", estimation = 5, title = "Issue 1"),
            makeMoveEvent(id="1", date="2024-01-14", newStatus = "in progress"),
            makeMoveEvent(id="1", date="2024-01-16", newStatus = "done")
        ).`should be success and` {
            status("1953-06-06".toDateTime()) shouldBe (Status.OutOfScope)
        }
    }

    private fun createIssueIdentity(id: String, project: String, date: String = "2024-01-12")
            = Issue.from(
                makeCreateEvent(id = id, date = date, project = project)
            )

    @Test
    fun `assert equality`() {
        createIssueIdentity("A", project = "project 1") shouldBe (createIssueIdentity("A", project = "project 1")
        )

        createIssueIdentity("A", project = "project 1").hashCode() shouldBe (createIssueIdentity("A", project = "project 1").hashCode()
        )
    }

    @Test
    fun `assert inequality`() {
        createIssueIdentity("A", project = "project 1") shouldNotBe (createIssueIdentity("B", project = "project 1")
        )

        createIssueIdentity("A", project = "project 1").hashCode() shouldNotBe (createIssueIdentity("B", project = "project 1").hashCode()
        )

        createIssueIdentity("A", project = "project 1") shouldNotBe (createIssueIdentity("A", "project 2")
        )

        createIssueIdentity("A", project = "project 1").hashCode() shouldNotBe (createIssueIdentity("A", "project 2").hashCode()
        )
    }

    val issue = """
        | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 10 | 11 | 12 | 13  
--------+----+----+----+----+----+----+----+----+----+----+----+----+----
Issue 1 |    |    |    | C  |    |    |  R |    |    | P  |    |    | D       

""".trimIndent().toEvents().get("Issue 1")

    @Test
    fun `the issue is not in iteration before being created`() {
        issue.`is in iteration`(startOfDay("2023-12-01"), endOfDay("2023-12-03")) shouldBe false
    }

    @Test
    fun `the issue is not in iteration before having a state`() {
        issue.`is in iteration`(startOfDay("2024-01-01"), endOfDay("2024-01-05")) shouldBe false
    }

    @Test
    fun `the issue is not in iteration after being done`() {
        issue.`is in iteration`(startOfDay("2024-01-15"), endOfDay("2024-01-25")) shouldBe false
    }

    @Test
    fun `the issue is in iteration when it is ready`() {
        issue.`is in iteration`(startOfDay("2024-01-06"), endOfDay("2024-01-08")) shouldBe true
    }

    @Test
    fun `the issue is in iteration when it is ready then in progress`() {
        issue.`is in iteration`(startOfDay("2024-01-06"), endOfDay("2024-01-11")) shouldBe true
    }

    @Test
    fun `the issue is in iteration when it is done before end`() {
        issue.`is in iteration`(startOfDay("2024-01-06"), endOfDay("2024-01-14")) shouldBe true
    }

    @Test
    fun `the issue is in iteration when it is already in progress before`() {
        issue.`is in iteration`(startOfDay("2024-01-11"), endOfDay("2024-01-14")) shouldBe true
    }

    @Test
    fun `getting state at a given date`() {
        val issue = make("2024 01 01")
            .with(Status.Ready, "2024 01 10")
            .with(Status.InProgress, "2024 01 12")
            .with(Status.Ready, "2024 01 13")
            .close("2024 06 16")

        issue.status(startOfDay("1999 01 01")) shouldBe (Status.OutOfScope)
        issue.status(startOfDay("2024 01 10")) shouldBe (Status.Ready)
        issue.status(startOfDay("2024 01 11")) shouldBe (Status.Ready)
        issue.status(startOfDay("2024 01 12")) shouldBe (Status.InProgress)
        issue.status(startOfDay("2024 01 12")) shouldBe (Status.InProgress)
        issue.status(startOfDay("2024 01 31")) shouldBe (Status.Ready)
        issue.status(startOfDay("2024 06 30")) shouldBe (Status.Closed.default)
    }

    @Test
    fun `applying an event not related to the issue has no effect`() {
        val issue = `an issue with`(
            project = anId.ofApollo(),
            `created on` = startOfDay("2024 01 01"),
            changes = listOf(
                startOfDay("2024 01 10") to Status.Ready,
                startOfDay("2024 01 12") to Status.InProgress,
                startOfDay("2024 01 13") to Status.Ready,
            )
        )

        issue.apply(
            IterationStarted("2024-01-10".toDateTime(), ID.from("dummy"), ID.from("dummy"), Name.from("dummy"))
        ) shouldBe issue
    }


    @Test
    fun `rename an issue`() {
        val issue = make(startOfDay("2024 01 10"))
            .renamed(on= "2024 01 17", "new title")

        issue.title shouldBe (Title.from("new title"))
    }

    @Test
    fun `moving an issue has no effect on title`() {
        val issue = make(startOfDay("2024 01 10"))
            .with(Status.InProgress, on= startOfDay("2024 01 17"))

        issue.title shouldBe (make(startOfDay("2024 01 10")).title)
    }

    @Test
    fun `estimate an issue`() {
        val issue = make(startOfDay("2024 01 10"))
            .estimate(complexity = 8, on= "2024 01 15")
            .estimate(complexity = 13, on= "2024 01 20")

        issue.estimation shouldBe (PointsOfComplexity.of(13))
    }
}




