package domain.teamwork.issue.model

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test


class WorkloadTest {
    @Test
    fun `sum Workload`() {
        (
            Workload(PointsOfComplexity.of(5), 2)
            +
            Workload(PointsOfComplexity.of(21), 4)
        ) shouldBe Workload(PointsOfComplexity.of(5 + 21), 2 + 4)
    }

    @Test
    fun `minus Workload`() {
        (
            Workload(PointsOfComplexity.of(21), 5)
                    -
            Workload(PointsOfComplexity.of(8), 2)
        ) shouldBe Workload(PointsOfComplexity.of(21 - 8), 5 - 2)
    }
}