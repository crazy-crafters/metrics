package domain.teamwork

import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.doubles.InProgress
import domain.teamwork.doubles.Ready
import domain.teamwork.doubles.isDone
import domain.teamwork.doubles.issues
import domain.teamwork.status.model.Status
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test

class BoardIssueCheckTest {
    @Test
    fun `issue 1 verification`() {
        val issue = issues.get("Issue 1")

        issue.status(endOfDay("2024 01 01")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 02")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 03")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 04")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 05")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 06")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 07")) shouldBe
                Status.Waiting(
                    ID.from("testing"),
                    Label.from("Test")
                )
        issue.status(endOfDay("2024 01 08")) shouldBe Status.isDone
        issue.status(endOfDay("2024 01 09")) shouldBe Status.isDone
        issue.status(endOfDay("2024 01 10")) shouldBe Status.isDone

        issue.`is in iteration`(
            startOfDay("2024 01 01"),
            endOfDay("2024 01 10")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 02"),
            endOfDay("2024 01 11")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 03"),
            endOfDay("2024 01 12")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 04"),
            endOfDay("2024 01 13")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 05"),
            endOfDay("2024 01 14")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 15")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 07"),
            endOfDay("2024 01 16")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 08"),
            endOfDay("2024 01 17")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 09"),
            endOfDay("2024 01 18")
        ) shouldBe false
    }

    @Test
    fun `issue 5 verification`() {
        val issue = issues.get("Issue 5")

        issue.status(endOfDay("2024 01 08")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 09")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 10")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 11")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 12")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 13")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 14")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 15")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 16")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 17")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 18")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 19")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 20")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 21")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 22")) shouldBe Status.InProgress
        issue.status(endOfDay("2024 01 23")) shouldBe Status.isDone

        issue.`is in iteration`(
            startOfDay("2024 01 01"),
            endOfDay("2024 01 10")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 02"),
            endOfDay("2024 01 11")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 03"),
            endOfDay("2024 01 12")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 04"),
            endOfDay("2024 01 13")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 05"),
            endOfDay("2024 01 14")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 15")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 07"),
            endOfDay("2024 01 16")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 08"),
            endOfDay("2024 01 17")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 09"),
            endOfDay("2024 01 18")
        ) shouldBe true
    }

    @Test
    fun `issue 2 verification`() {
        val issue = issues.get("Issue 2")

        issue.status(endOfDay("2024 01 01")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 02")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 03")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 04")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 05")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 06")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 07")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 08")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 09")) shouldBe Status.Ready
        issue.status(endOfDay("2024 01 10")) shouldBe Status.Ready

        issue.`is in iteration`(
            startOfDay("2024 01 01"),
            endOfDay("2024 01 10")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 02"),
            endOfDay("2024 01 11")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 03"),
            endOfDay("2024 01 12")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 04"),
            endOfDay("2024 01 13")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 05"),
            endOfDay("2024 01 14")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 15")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 07"),
            endOfDay("2024 01 16")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 08"),
            endOfDay("2024 01 17")
        ) shouldBe true

        issue.`is in iteration`(
            startOfDay("2024 01 09"),
            endOfDay("2024 01 18")
        ) shouldBe true
    }
}