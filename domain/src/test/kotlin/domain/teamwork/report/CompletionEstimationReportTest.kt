package domain.teamwork.report

import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.teamwork.d
import domain.teamwork.doubles.EventRepositoryDouble
import domain.teamwork.report.model.CompletionEstimation
import domain.teamwork.report.model.FlowReport


class CompletionEstimationReportTest {
    @Test
    fun `getting completion estimation for a given project`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-08 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
            Issue 4: 8 points -> ready 01-01, in_progress 01-06 
            Issue 5: 8 points -> ready 01-01
            Issue 6: 0 points -> ready 01-01, cancelled 01-05
        """
        )

        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.estimateCompletion() shouldBe CompletionEstimation(
            `considering issues` = "2024-01-22T12:00".toDateTime(),
            `considering complexity` = "2024-02-08T12:00".toDateTime()
        )
    }

    @Test
    fun `completion estimation when there is no estimation in points should be invalid`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 0 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 0 points -> ready 01-01, in_progress 01-02, done 01-08 
            Issue 3: 0 points -> ready 01-01, in_progress 01-04, done 01-10 
            Issue 4: 0 points -> ready 01-01, in_progress 01-06 
            Issue 5: 0 points -> ready 01-01
            Issue 6: 0 points -> ready 01-01, cancelled 01-05
        """
        )

        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.estimateCompletion() shouldBe CompletionEstimation(
            `considering issues` = "2024-01-22T12:00".toDateTime(),
            `considering complexity` = DateTime.INVALID
        )
    }
}