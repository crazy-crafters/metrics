package domain.teamwork.report.model

import io.kotest.matchers.collections.shouldContainExactly
import org.junit.jupiter.api.Test
import domain.teamwork.issue.model.TimeDuration


class HistogramTest {

    @Test
    fun `create a distribution`() {
        val d = Histogram(
            listOf(
                TimeDuration.days(0L),
                TimeDuration.days(1L),
                TimeDuration.days(1L)
            )
        )

        d.countByDuration.shouldContainExactly(
            Histogram.Value(1, 0),
            Histogram.Value(2, 1),
        )
    }

    @Test
    fun `sum distributions`() {
        val d1 = Histogram(
            listOf(
                TimeDuration.days(0L),
                TimeDuration.days(1L),
                TimeDuration.days(1L)
            )
        )

        val d2 = Histogram(
            listOf(
                TimeDuration.days(1L),
                TimeDuration.days(2L),
                TimeDuration.days(3L)
            )
        )

        val d = d1 + d2

        d.countByDuration.shouldContainExactly(
            Histogram.Value(1, 0),
            Histogram.Value(1, 2),
            Histogram.Value(1, 3),
            Histogram.Value(3, 1),
        )
    }
}