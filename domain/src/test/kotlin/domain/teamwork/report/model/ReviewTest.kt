package domain.teamwork.report.model

import domain.sharedkernel.model.DateTime.Companion.toDateTime
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import domain.teamwork.doubles.*
import domain.teamwork.issue.model.*
import domain.teamwork.doubles.`an issue with`
import domain.teamwork.status.model.Status

class ReviewTest {

    @Test
    fun `get route of an issue`() {
        `an issue with`(
            `created on` = "2024 01 01",
            "2024 01 10" to Status.Ready,
            "2024 01 11" to Status.InProgress,
            "2024 01 12" to Status.isDone,
        ).route() shouldBe Route(
            listOf(
                StatusChange(date = "2024-01-10".toDateTime(), status = Status.Ready),
                StatusChange(date = "2024-01-11".toDateTime(), status = Status.InProgress),
                StatusChange(date = "2024-01-12".toDateTime(), status = Status.isDone)
            ), IssueState.Closed
        )
    }

    @Test
    fun `get route of an issue not closed`() {
        `an issue with`(
            `created on` = "2024 01 01",
            "2024 01 10" to Status.Ready,
            "2024 01 11" to Status.InProgress,
        ).route() shouldBe Route(
            listOf(
                StatusChange(date = "2024-01-10".toDateTime(), status = Status.Ready),
                StatusChange(date = "2024-01-11".toDateTime(), status = Status.InProgress)
            ), IssueState.Open
        )
    }

    @Test
    fun `get route of an issue closed`() {
        `an issue with`(
            `created on` = "2024 01 01",
            "2024 01 10" to Status.Ready,
            "2024 01 11" to Status.InProgress,
        ).close("2024 01 15").route() shouldBe Route(
            listOf(
                StatusChange(date = "2024-01-10".toDateTime(), status = Status.Ready),
                StatusChange(date = "2024-01-11".toDateTime(), status = Status.InProgress)
            ), IssueState.Closed
        )
    }

    @Test
    fun `get route of an issue closed and reopen`() {
        `an issue with`(
            `created on` = "2024 01 01",
            "2024 01 10" to Status.Ready,
            "2024 01 11" to Status.InProgress,
        ).close("2024 01 15")
            .added("2024 01 15")
            .route() shouldBe Route(
            listOf(
                StatusChange(date = "2024-01-10".toDateTime(), status = Status.Ready),
                StatusChange(date = "2024-01-11".toDateTime(), status = Status.InProgress)
            ), IssueState.Open
        )
    }

    @Test
    fun `an issue without rework`() {
            `an issue with`(
                `created on` = "2024 01 01",
                "2024 01 10" to Status.Ready,
                "2024 01 10" to Status.InProgress,
                "2024 01 11" to Status.isDone,
            ).route().hasRework() shouldBe false
    }

    @Test
    fun `an issue with rework`() {
            `an issue with`(
                `created on` = "2024 01 01",
                "2024 01 10" to Status.Ready,
                "2024 01 10" to Status.InProgress,
                "2024 01 11" to Status.isDone,
                "2024 01 12" to Status.InProgress,
            ).route().hasRework() shouldBe true
    }
}