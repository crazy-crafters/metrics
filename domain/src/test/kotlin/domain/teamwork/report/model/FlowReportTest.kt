package domain.teamwork.report.model


import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import domain.teamwork.endOfDay
import domain.teamwork.startOfDay
import domain.teamwork.doubles.issues
import domain.teamwork.issue.model.Efficiency
import domain.teamwork.issue.model.TimeDuration

class FlowReportTest {
    @Test
    fun `get efficiency`() {
        val board = FlowReport.Valid(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 31"),
            issues.all()
        )
        board.efficiency() shouldBe Efficiency.of(47)
    }

    @Test
    fun `get lead times`() {
        val board = FlowReport.Valid(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 31"),
            issues.all()
        )
        board.leadTimes() shouldBe TimeDuration.days(9)
    }

    @Test
    fun `get canceled lead times`() {
        val board = FlowReport.Valid(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 31"),
            issues.all()
        )
        board.cancelLeadTimes() shouldBe TimeDuration.days(4)
    }

    @Test
    fun `get estimated completion`() {
        val board = FlowReport.Valid(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 16"),
            issues.all()
        )
        board.estimateCompletion() shouldBe CompletionEstimation(`considering complexity` = startOfDay("2024-01-26"), `considering issues` = startOfDay("2024-01-26"))
    }

}


