package domain.teamwork.report.model


import io.kotest.matchers.collections.shouldBeEmpty
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.teamwork.doubles.*
import domain.teamwork.endOfDay
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Workload
import domain.teamwork.startOfDay
import domain.teamwork.status.model.Status


class GeneralStatsReportTest {
    @Test
    fun `don't count in progress issues as done`() {

            GeneralStatsReport(
                startOfDay("2024 01 01"),
                endOfDay("2024 01 05"),
                listOf(issues.get("Issue 1"))
            ).done.count shouldBe 0
    }

    @Test
    fun `count issues done during the iteration`() {
            GeneralStatsReport(
                startOfDay("2024 01 01"),
                endOfDay("2024 01 08"),
                listOf(issues.get("Issue 1"))
            ).done.count shouldBe 1
    }

    @Test
    fun `count all opened issues (not closed)`() {
            GeneralStatsReport(
                startOfDay("2024 01 01"),
                endOfDay("2024 01 10"),
                issues.get(1, 2, 3, 4, 5, 6)
            ).opened.count shouldBe 3
    }


    @Test
    fun `ignore issues done before the iteration`() {
            GeneralStatsReport(
                startOfDay("2024 01 15"),
                endOfDay("2024 01 20"),
                listOf(issues.get("Issue 1"))
            ).issues.shouldBeEmpty()
    }

    @Test
    fun `consider issues done on first day of the iteration`() {
            GeneralStatsReport(
                startOfDay("2024 01 08"),
                endOfDay("2024 01 20"),
                listOf(issues.get("Issue 1"))
            ).done shouldBe Workload(PointsOfComplexity.of(5), count = 1)
    }

    @Test
    fun `don't consider issues created but not planned`() {
            GeneralStatsReport(
                startOfDay("2024 01 08"),
                endOfDay("2024 01 20"),
                listOf(issues.get("Issue 6"))
            ).issues.shouldBeEmpty()
    }

    @Test
    fun `don't consider issues created after iteration`() {
            GeneralStatsReport(
                startOfDay("2024 01 02"),
                endOfDay("2024 01 04"),
                listOf(issues.get("Issue 5"))
            ).issues.shouldBeEmpty()
    }



    @Test
    fun `velocity on a board`() {
        val board = GeneralStatsReport(
            startOfDay("2024 01 04"), endOfDay("2024 01 20"),
            listOf(
                `an issue with`(
                    project = anId.ofApollo(),
                    `created on` = startOfDay("2024 01 01"),
                    changes = listOf(
                        startOfDay("2024 01 01") to Status.Planned,
                        startOfDay("2024 01 10") to Status.Ready,
                        startOfDay("2024 01 12") to Status.InProgress,
                        startOfDay("2024 01 20") to Status.isDone,
                    ),
                    estimation = 5
                ),
                `an issue with`(
                    project = anId.ofApollo(),
                    `created on` = startOfDay("2024 01 01"),
                    changes = listOf(
                        startOfDay("2024 01 01") to Status.Planned,
                        startOfDay("2024 01 10") to Status.Ready,
                        startOfDay("2024 01 12") to Status.InProgress,
                        startOfDay("2024 01 13") to Status.isDone,
                    ),
                    estimation = 1
                )
            )
        )
        board.velocity shouldBe PointsOfComplexity.of(5 + 1)
    }

    @Test
    fun `velocity on a board should consider Done issue only`() {
        val board = GeneralStatsReport(
            startOfDay("2024 01 04"), endOfDay("2024 01 20"),
            listOf(
                `an issue with`(
                    project = anId.ofApollo(),
                    `created on` = startOfDay("2024 01 01"),
                    estimation = 5,
                    changes = listOf(
                        startOfDay("2024 01 01") to Status.Planned,
                        startOfDay("2024 01 10") to Status.Ready,
                        startOfDay("2024 01 12") to Status.InProgress,
                        startOfDay("2024 01 20") to Status.isDone
                    )
                ),
                `an issue with`(
                    project = anId.ofApollo(),
                    `created on` = startOfDay("2024 01 01"),
                    changes = listOf(
                        startOfDay("2024 01 01") to Status.Planned,
                        startOfDay("2024 01 10") to Status.Ready,
                        startOfDay("2024 01 12") to Status.InProgress
                    ),
                    estimation = 40
                )
            )
        )
        board.velocity shouldBe PointsOfComplexity.of(5)
    }

    @Test
    fun `get board for the given iterations`() {
        val board = GeneralStatsReport(
            startOfDay("2024 01 06"),
            endOfDay("2024 01 10"),
            issues.all()
        )

        board.velocity shouldBe PointsOfComplexity.of(10)
        board.opened shouldBe Workload(issues.get(2, 3, 5))
        board.unfinished shouldBe Workload(issues.get(2,3,5))
        board.done shouldBe Workload(issues.get(1, 4))
        board.closed shouldBe Workload(issues.get(1, 4))
    }
}


