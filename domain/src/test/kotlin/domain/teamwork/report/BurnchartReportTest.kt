package domain.teamwork.report

import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.teamwork.d
import domain.teamwork.doubles.EventRepositoryDouble
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Workload
import domain.teamwork.report.model.BurnChart


class BurnChartReportTest {
    @Test
    fun `getting a burn chart for a given project`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-04 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
            Issue 4: 8 points -> ready 01-01, in_progress 01-06 
            Issue 5: 8 points -> ready 01-01  
        """
        )

        val result = getReport.burnChart(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<BurnChart.Found>()

        result.values.shouldContainExactly(
            BurnChart.Value(date = d("2024-01-01"), value = Workload(count = 0, points = PointsOfComplexity.of(0))),
            BurnChart.Value(date = d("2024-01-02"), value = Workload(count = 0, points = PointsOfComplexity.of(0))),
            BurnChart.Value(date = d("2024-01-03"), value = Workload(count = 1, points = PointsOfComplexity.of(2))),
            BurnChart.Value(date = d("2024-01-04"), value = Workload(count = 2, points = PointsOfComplexity.of(7))),
            BurnChart.Value(date = d("2024-01-05"), value = Workload(count = 2, points = PointsOfComplexity.of(7))),
            BurnChart.Value(date = d("2024-01-06"), value = Workload(count = 2, points = PointsOfComplexity.of(7))),
            BurnChart.Value(date = d("2024-01-07"), value = Workload(count = 2, points = PointsOfComplexity.of(7))),
            BurnChart.Value(date = d("2024-01-08"), value = Workload(count = 2, points = PointsOfComplexity.of(7))),
            BurnChart.Value(date = d("2024-01-09"), value = Workload(count = 2, points = PointsOfComplexity.of(7))),
            BurnChart.Value(date = d("2024-01-10"), value = Workload(count = 3, points = PointsOfComplexity.of(9))),
            BurnChart.Value(date = d("2024-01-11"), value = Workload(count = 3, points = PointsOfComplexity.of(9))),
            BurnChart.Value(date = d("2024-01-12"), value = Workload(count = 3, points = PointsOfComplexity.of(9))),
            BurnChart.Value(date = d("2024-01-13"), value = Workload(count = 3, points = PointsOfComplexity.of(9))),
            BurnChart.Value(date = d("2024-01-14"), value = Workload(count = 3, points = PointsOfComplexity.of(9))),
            BurnChart.Value(date = d("2024-01-15"), value = Workload(count = 3, points = PointsOfComplexity.of(9)))
        )
    }
}