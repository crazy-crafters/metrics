package domain.teamwork.report

import io.kotest.matchers.doubles.plusOrMinus
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import domain.teamwork.d
import domain.teamwork.doubles.EventRepositoryDouble
import domain.teamwork.issue.model.Efficiency
import domain.teamwork.issue.model.TimeDuration
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.teamwork.issue.model.Tag
import domain.teamwork.report.model.FlowReport
import domain.teamwork.report.model.GeneralStatsReport


val commonBoard = EventRepositoryDouble().also {
    it.contains(
        anId.ofApollo(),
        """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-08 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
            Issue 4: 8 points -> ready 01-01, in_progress 01-06 
            Issue 5: 8 points -> ready 01-01
            Issue 6: 0 points -> ready 01-01, cancelled 01-05
            
            Feature 1 : 01-01 Issue 1 01-02, Issue 2 01-01
            Feature 2 : 01-01 Issue 3 01-02, Issue 4 01-02, Issue 5 01-02, Issue 6 01-02
        """
    )
}


class GetFlowReportTest {

    @Test
    fun `get flow for a given project`() {
        val getReport = GetReport(commonBoard)
        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.issues.count() shouldBe 6
        result.cancelLeadTimes() shouldBe TimeDuration.days(4)
        result.leadTimes() shouldBe TimeDuration.days(6)
        result.efficiency() shouldBe Efficiency.of(50)
    }


    @Test
    fun `getting an error when we get the flow report and there is no any issues`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result shouldBe FlowReport.NoReport(d("2024-01-01"), to = d("2024-01-15"))
    }


    @Test
    fun `getting flow for a given project`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-08 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
            Issue 4: 8 points -> ready 01-01, in_progress 01-06 
            Issue 5: 8 points -> ready 01-01
            Issue 6: 0 points -> ready 01-01, cancelled 01-05
        """
        )

        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.issues.count() shouldBe 6
        result.cancelLeadTimes() shouldBe TimeDuration.days(4)
        result.leadTimes() shouldBe TimeDuration.days(6)
        result.efficiency() shouldBe Efficiency.of(50)
    }

    @Test
    fun `get wip`() {
        val getReport = GetReport(commonBoard)
        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-11"))

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.wip().let {
            it.min shouldBe 0
            it.max shouldBe 3
            it.average shouldBe ((0 + 2 + 1 + 2 + 2 + 3 + 3 + 2 + 2 + 1 + 1) / 11.0 plusOrMinus (0.01))
        }
    }

    @Test
    fun `wip on a single date`() {
        val getReport = GetReport(commonBoard)
        val result = getReport.flow(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-11"))

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.wip().average shouldBe (1.72 plusOrMinus (0.1))
    }

    @Test
    fun `filter flow report issues on tags`() {
        val getReport = GetReport(commonBoard)
        val result = getReport.flow(
            anId.ofApollo(),
            from = d("2024-01-01"),
            to = d("2024-01-11"),
            tags = listOf(Tag.on("Feature:1"))
        )

        result.shouldBeTypeOf<FlowReport.Valid>()
        result.issues.map { it.id.value } shouldBe listOf("Issue 1", "Issue 2")
    }

    @Test
    fun `filter general stats issues on tags`() {
        val getReport = GetReport(commonBoard)
        val result = getReport.generalStats(
            anId.ofApollo(),
            from = d("2024-01-01"),
            to = d("2024-01-11"),
            tags = listOf(Tag.on("Feature:1"))
        )

        result.shouldBeTypeOf<GeneralStatsReport.Valid>()
        result.issues.map { it.id.value } shouldBe listOf("Issue 1", "Issue 2")
    }

    /*@Test
    fun `get distribution by time duration`() {
        val getReport = ReportHandler(commonBoard)
        val result = getReport.leadTimes(anId.ofApollo<Project>(), from = d("2024-01-01"), to = d("2024-01-11"))

        assertThat(result.countByDuration).containsExactly(
            Histogram.Value(count=1, days=2),
            Histogram.Value(count=1, days=7),
            Histogram.Value(count=1, days=9),
            Histogram.Value(count=1, days=4),
            Histogram.Value(count=2, days=0),
        )
    }*/
}