package domain.teamwork.report

import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.teamwork.configuration.Iteration
import domain.teamwork.d
import domain.teamwork.doubles.EventRepositoryDouble
import domain.teamwork.endOfDay
import domain.teamwork.issue.model.*
import domain.teamwork.report.model.GeneralStatsReport
import domain.teamwork.shared.events.IterationFinished
import domain.teamwork.shared.events.IterationStarted
import domain.teamwork.startOfDay


class GetGeneralStatsTest {
    @Test
    fun `getting general stats for a given project`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.contains(
            anId.ofApollo(),
            """
            Issue 1: 2 points -> ready 01-01, in_progress 01-02, done 01-03 
            Issue 2: 5 points -> ready 01-01, in_progress 01-02, done 01-04 
            Issue 3: 2 points -> ready 01-01, in_progress 01-04, done 01-10 
            Issue 4: 8 points -> ready 01-01, in_progress 01-06 
            Issue 5: 8 points -> ready 01-01  
        """
        )

        val result = getReport.generalStats(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<GeneralStatsReport.Valid>()
        result.issues.count() shouldBe 5
        result.done shouldBe Workload(PointsOfComplexity.of(9), 3)
        result.opened shouldBe Workload(PointsOfComplexity.of(16), 2)

    }

    @Test
    fun `getting an error when the given project does not have any issues`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        val result = getReport.generalStats(anId.ofApollo(), from = d("2024-01-01"), to = d("2024-01-15"))

        result.shouldBeTypeOf<GeneralStatsReport.NotFound>()
    }

    @Test
    fun `list all iterations`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.save(
            IterationStarted(
                startOfDay("2024-05-10"),
                ID.from("it 1"),
                anId.ofApollo(),
                Name.from("Sprint 1")
            )
        )
        eventRepository.save(IterationFinished(endOfDay("2024-05-20"), ID.from("it 1"), anId.ofApollo()))

        eventRepository.save(
            IterationStarted(
                startOfDay("2024-05-21"),
                ID.from("it 2"),
                anId.ofApollo(),
                Name.from("Sprint 2")
            )
        )
        eventRepository.save(IterationFinished(endOfDay("2024-05-31"), ID.from("it 2"), anId.ofApollo()))

        val iterations = getReport.iterations(anId.ofApollo())
        iterations.shouldContainExactly(
            Iteration(
                id = ID.from("it 1"),
                project = anId.ofApollo(),
                name = Name.from("Sprint 1"),
                from = startOfDay("2024-05-10"),
                to = endOfDay("2024-05-20"),
                closed = true
            ),
            Iteration(
                id = ID.from("it 2"),
                project = anId.ofApollo(),
                name = Name.from("Sprint 2"),
                from = startOfDay("2024-05-21"),
                to = endOfDay("2024-05-31"),
                closed = true
            )
        )
    }

    @Test
    fun `compare results to Jira reports`() {
        val eventRepository = EventRepositoryDouble()
        val getReport = GetReport(eventRepository)

        eventRepository.containsFromLogs(
            anId.ofApollo(), """
            MT-1 - List all issues                 =  13 points -> Ready 2024-05-20T12:29:24.397, In_Progress 2024-05-22T21:58:16.173, Done 2024-05-22T23:39:27.120
            
            MT-2 - Get issue duration              =   8 points -> Ready 2024-05-20T12:29:24.397, In_Progress 2024-05-26T22:19:04.119, Done 2024-06-08T19:54:49.055
            MT-3 - Calculate Velocity              =   5 points -> Ready 2024-05-20T12:29:24.397, In_Progress 2024-06-01T21:09:52.684, Done 2024-06-01T21:10:01.538
            MT-4 - Calculate Lead Time             =   2 points -> Ready 2024-05-20T12:29:24.397, In_Progress 2024-06-01T21:09:56.592, Done 2024-06-08T19:54:45.515
            MT-5 - Calculate Cycle Time            =   2 points -> Ready 2024-05-20T12:29:24.397, In_Progress 2024-06-01T21:09:59.258, Done 2024-06-08T19:54:43.834
            MT-6 - Calculate Throughput            =   3 points -> Ready 2024-05-20T12:29:24.397
            MT-7 - Calculate WIP                   =   5 points -> 
            MT-8 - Calculate burn chart            =   8 points -> Ready 2024-05-20T12:29:24.397
            MT-9 - Calculate estimation gap        =   1 points -> Ready 2024-05-20T12:29:24.397
            MT-10 - Calculate done percentage      =   1 points -> Ready 2024-05-20T12:29:24.397
            MT-11 - Configure Jira for a given project=   5 points -> 
            MT-12 - Display velocity               =   3 points -> Ready 2024-06-08T17:54:33.344, In_Progress 2024-06-15T23:31:10.525, Done 2024-06-22T00:51:41.642
            MT-13 - List all iterations            =   3 points -> Ready 2024-06-08T17:54:33.344, In_Progress 2024-06-08T19:55:15.068, Done 2024-06-15T23:29:46.668
            MT-14 - Define Iteration content       =   5 points -> Ready 2024-06-18T07:07:19.424, In_Progress 2024-06-19T18:33:14.094, Done 2024-06-22T00:26:56.241
            """
        )

        val result = getReport.generalStats(
            anId.ofApollo(),
            from = DateTime.parse("2024-06-08T17:54:33.344"),
            to = DateTime.parse("2024-06-18T07:06:59.684")
        )

        result.shouldBeTypeOf<GeneralStatsReport.Valid>()
        result.done shouldBe Workload(PointsOfComplexity.of(15), 4)
        result.opened shouldBe Workload(PointsOfComplexity.of(16), 5)
    }
}