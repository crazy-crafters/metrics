package domain.teamwork.report


import io.kotest.matchers.collections.shouldContainInOrder
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import org.junit.jupiter.api.Test
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.teamwork.doubles.*
import domain.teamwork.issue.model.*
import domain.teamwork.report.model.*
import domain.teamwork.shared.events.IssueAdded
import domain.teamwork.shared.events.IssueMoved
import domain.teamwork.status.model.Status
import org.junit.jupiter.api.DisplayName
import java.time.Duration

@DisplayName("compute Completion estimation")
class StatisticsTest {

    @Test
    fun `given a backlog of 100 and 20 points made in 10 days, then the backlog should be done in 40 days`() {
        val start = DateTime.parse("2024-01-01T00:00:00")
        val stats = flowOf(
            all = 100,
            done = 20,
            duration = 10,
            `started on` = start
        )

        stats.shouldBeTypeOf<FlowReport.Valid>()
        stats.estimateCompletion() shouldBe CompletionEstimation(
            start.plus(Duration.ofDays(50)),
            start.plus(Duration.ofDays(50))
        )
    }

    @Test
    fun `when everything is already done, the backlog should be done today`() {
        val start = DateTime.parse("2024-01-01T00:00:00")
        val stats = flowOf(
            all = 100,
            done = 100,
            duration = 10,
            `started on` = start
        )

        stats.shouldBeTypeOf<FlowReport.Valid>()
        stats.estimateCompletion() shouldBe CompletionEstimation(
            DateTime.parse("2024-01-11T23:59:59.999"),
            DateTime.parse("2024-01-11T23:59:59.999")
        )
    }

    @Test
    fun `when nothing has been done yet, we cannot estimate the end`() {
        val start = DateTime.parse("2024-01-01T00:00:00")
        val stats = flowOf(
            all = 100,
            done = 0,
            duration = 10,
            `started on` = start
        )
        stats.shouldBeTypeOf<FlowReport.Valid>()
        stats.estimateCompletion() shouldBe CompletionEstimation(
            DateTime.MAX,
            DateTime.MAX
        )
    }

    val issues =
        """
        | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 
--------+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----
Issue 1 | R  |    |    | P  |    |    | T  | D  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 2 | R  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 3 | R  |    |    |    | P  |    |    |    |    |    | D  |    |    |    |  P |    |    |    |    | D  |    |    |    |    |    |    |    |    |    |         
Issue 4 |    |    |    | R  |    | P  |    | D  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 5 |    |    |    |    |    |    |    | R  |    |    |    |    |    | P  |    |    | R  |    |    |    | P  |    | D  |    |    |    |    |    |    |    
Issue 6 |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    
""".toIssueEvents()

    @Test
    fun `find all issues which have rework`() {
        val report = ReviewReport.Valid(
            "2024-01-01".toDateTime(),
            "2024-01-31".toDateTime(),
            anId.of("uuu"),
            issues.all()
        )

        report.withRework.shouldContainInOrder(
            IssueRework(
                ID.from("Issue 3"), Title.from("Issue 3"), Route(
                    steps = listOf(
                        StatusChange(date = "2024-01-01T12:00:00Z".toDateTime(), status = Status.Ready),
                        StatusChange(date = "2024-01-05T12:00:00Z".toDateTime(), status = Status.InProgress),
                        StatusChange(date = "2024-01-11T12:00:00Z".toDateTime(), status = Status.isDone),
                        StatusChange(date = "2024-01-15T12:00:00Z".toDateTime(), status = Status.InProgress),
                        StatusChange(date = "2024-01-20T12:00:00Z".toDateTime(), status = Status.isDone)
                    ), IssueState.Closed
                )
            ),
            IssueRework(
                ID.from("Issue 5"), Title.from("Issue 5"), Route(
                    steps = listOf(
                        StatusChange(date = "2024-01-08T12:00:00Z".toDateTime(), status = Status.Ready),
                        StatusChange(date = "2024-01-14T12:00:00Z".toDateTime(), status = Status.InProgress),
                        StatusChange(date = "2024-01-17T12:00:00Z".toDateTime(), status = Status.Ready),
                        StatusChange(date = "2024-01-21T12:00:00Z".toDateTime(), status = Status.InProgress),
                        StatusChange(date = "2024-01-23T12:00:00Z".toDateTime(), status = Status.isDone)
                    ), IssueState.Closed
                )
            ),
        )
    }

    /*@Test
    fun `calculate efficiency when report contains only one issue done in time`() {
        val report = Report(
            "2024-01-01".toDateTime(),
            "2024-01-31".toDateTime(),
            listOf(issues.get("Issue 1"))
        )

        assertThat(report.efficiency()).isEqualTo()
    }*/


    fun done(vararg v: Int) = listOf(*v.toTypedArray())

    fun of(all: Int, duration: Int, done: Int, `started on`: DateTime): GeneralStatsReport {
        val start = `started on`
        val end = DateTime.parse("2024-01${duration + 1}T23:59:59.999")
        val issues: List<Issue> = IntRange(0, all - 1).mapIndexedNotNull() { index, i ->
            Issue.from(
                IssueAdded(
                    entityId = ID.from("$start"),
                    project = ID.from("dummy"),
                    title = Title.from("dummy"),
                    date = start,
                    status = Status.Backlog,
                    estimation = PointsOfComplexity.of(1),
                    tags = Tags.empty()
                ),
                IssueMoved(
                    entityId = ID.from("$start"),
                    project = ID.from("dummy"),
                    date = start,
                    status = Status.Ready
                ),
                *index.let {
                    if (it < done) {
                        listOf(
                            IssueMoved(
                                entityId = ID.from("$start"),
                                project = ID.from("dummy"),
                                date = end,
                                status = Status.isDone
                            )
                        )
                    } else {
                        listOf()
                    }
                }.toTypedArray()
            ).fold({ it }, { null })
        }

        return GeneralStatsReport(start, end, issues)
    }

    private fun flowOf(all: Int, duration: Int, done: Int, `started on`: DateTime): FlowReport {
        val start = `started on`
        val end = DateTime.parse("2024-01-${duration + 1}T23:59:59.999")
        //val end = DateTime.of(LocalDate.of(2024, 1, duration + 1), LocalTime.MAX)
        val issues: List<Issue> = IntRange(0, all - 1).mapIndexedNotNull() { index, i ->
            Issue.from(
                IssueAdded(
                    entityId = ID.from("$start"),
                    project = ID.from("dummy"),
                    title = Title.from("dummy"),
                    date = start,
                    status = Status.Backlog,
                    estimation = PointsOfComplexity.of(1),
                    tags = Tags.empty()
                ),
                IssueMoved(
                    entityId = ID.from("$start"),
                    project = ID.from("dummy"),
                    date = start,
                    status = Status.Ready
                ),
                *index.let {
                    if (it < done) {
                        listOf(
                            IssueMoved(
                                entityId = ID.from("$start"),
                                project = ID.from("dummy"),
                                date = end,
                                status = Status.isDone
                            )
                        )
                    } else {
                        listOf()
                    }
                }.toTypedArray()
            ).fold({ it }, { null })
        }

        return FlowReport(start, end, issues)
    }
}