package domain.statistics

import io.kotest.matchers.comparables.shouldBeEqualComparingTo
import io.kotest.matchers.doubles.plusOrMinus
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Test


class DistributionTest {
    @Test
    fun `min of a distribution`() {
        distribution(0, 4, 1).min().shouldBe(0)
    }

    @Test
    fun `max of a distribution`() {
        distribution(0, 4, 1).max().shouldBe(4)
    }

    @Test
    fun `average of a distribution`() {
        distribution(0, 4).average()!!.shouldBeEqualComparingTo(2.0)
    }

    @Test
    fun `median of a distribution`() {
        distribution(0, 1, 4).median().shouldBe(1)
    }

    @Test
    fun `deviation of a distribution`() {
        distribution(0, 1, 2, 1, 4).deviation()!!.shouldBe(1.839 plusOrMinus(0.01))
    }

    @Test
    fun `standard deviation of a distribution`() {
        distribution(0, 1, 2, 1, 4).standardDeviation()!!.shouldBe(1.356 plusOrMinus(0.01))
    }

    @Test
    fun `at of a distribution`() {
        val d = distribution(0, 1, 1, 1, 3, 3, 3, 5, 5, 5)
        d.at(0.3).shouldBe(1)
        d.at(0.4).shouldBe(3)
        d.at(0.5).shouldBe(3)
        d.at(0.6).shouldBe(3)
        d.at(0.7).shouldBe(5)
        d.at(0.8).shouldBe(5)
    }

    @Test
    fun `combine of two distributions`() {
        val d1 = distribution(1, 1, 3, 3, 5)
        val d2 = distribution(0, 1, 1, 3, 5, 5, 5)
        d1.combine(d2).size.shouldBe(12)
    }

    private fun distribution(vararg values: Long): Distribution {
        return Distribution.on(values.asIterable())
    }
}