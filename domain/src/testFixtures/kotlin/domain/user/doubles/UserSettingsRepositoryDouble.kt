package domain.user.doubles

import domain.sharedkernel.model.ID
import domain.user.UserSettings
import domain.user.User
import domain.user.UserSettingsRepository

class UserSettingsRepositoryDouble : UserSettingsRepository {
    val items = mutableListOf<UserSettings>()
    override fun save(userSettings: UserSettings) {
        items.removeIf { it.user == userSettings.user }
        items.add(userSettings)
    }

    override fun find(user: ID<User>) = items.firstOrNull { it.user == user }

}