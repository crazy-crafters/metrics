package domain.sharedkernel.materials

import domain.sharedkernel.model.ID

object anId {
    fun <T> of(s: String): ID<T> = ID.from(s)
    fun <T> ofApollo(): ID<T> = ID.from("apollo")

    operator fun <T> invoke(): ID<T> = ID()
}
