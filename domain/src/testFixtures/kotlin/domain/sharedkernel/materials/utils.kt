package domain.sharedkernel.materials

import domain.sharedkernel.model.ID
import io.kotest.matchers.collections.shouldContainAllIgnoringFields
import kotlin.reflect.KProperty

fun <T> String.toId() = ID<T>(this.lowercase())



inline fun <reified T : Any> List<T>.shouldContainIgnoringFields(
    expected: List<T>,
    ignoreField: String,
    vararg ignoreFields: String
) {
    val props = getAllProperties<T>(listOf(ignoreField, *ignoreFields))
    this.shouldContainAllIgnoringFields(
        expected,
        props.first(),
        *props.drop(1).toTypedArray()
    )
}

inline fun <reified T: Any> getAllProperties(ignoreFields: List<String>): List<KProperty<*>> {
    val u = T::class.members.filterIsInstance<KProperty<*>>()
    return u.filter { it.name in ignoreFields }
}
