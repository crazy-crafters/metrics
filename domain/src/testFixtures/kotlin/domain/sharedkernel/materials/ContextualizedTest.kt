package domain.sharedkernel.materials

import io.kotest.matchers.shouldBe

import domain.sharedkernel.model.Either
import kotlin.test.fail

interface TestContext

class ContextualizedTestResult<R, T: TestContext>(val result: R?, private val env: T, private val exception: RuntimeException?) {
    infix fun then(f: T.(R) -> Unit) {
        if (result != null) {
            f(env, result)
        } else {
            fail("Expecting a value, but got the error $exception")
        }
    }

    infix fun `then it will throw an exception`(f: T.(RuntimeException) -> Unit) {
        if (exception != null) {
            f(env, exception)
        } else {
            fail("Expecting an exception, but got the value $result")
        }
    }
}

abstract class ContextualizedTest<T: TestContext>(private val context: T) {
    fun given(init: T.() -> Unit): T {
        context.init()
        return context
    }
    infix fun <R> T.`when`(f: T.() -> R): ContextualizedTestResult<R, T> {
        return try {
            ContextualizedTestResult(context.f(), context, null)
        } catch (ex: RuntimeException) {
            ContextualizedTestResult(null, context, ex)
        }
    }

    fun `based on side effects`(f: T.() -> Unit) {
        context.f()
    }
}

fun <E> Either<*, E>.`should be the error`(expected: E) {
    this.`should be failure and` {
        this shouldBe expected
    }
}

fun <V> Either<V, *>.`should be the value`(expected: V) {
    `should be success and` {
        this shouldBe expected
    }
}