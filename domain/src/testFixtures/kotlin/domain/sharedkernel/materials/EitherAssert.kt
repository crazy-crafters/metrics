package domain.sharedkernel.materials

import io.kotest.matchers.types.shouldBeTypeOf
import domain.sharedkernel.model.Either
import kotlin.test.fail


infix fun <SUCCESS, FAILURE> Either<SUCCESS, FAILURE>.`should be success and`(successAssertion: SUCCESS.() -> Unit) {
   this.onSuccess {
        successAssertion(it)
    }.onFailure {
        fail("Expected a valid result but got an error: $it")
    }
}

infix fun <SUCCESS, FAILURE> Either<SUCCESS, FAILURE>.`should be failure and`(failureAssertion: FAILURE.() -> Unit) {
    this.onFailure {
        failureAssertion(it)
    }.onSuccess {
        fail("Expected a valid result but got an error: $it")
    }
}

inline fun <SUCCESS, FAILURE, reified T: Any> Either<SUCCESS, FAILURE>.`should be failure of`() {
    `should be failure and` {
        this.shouldBeTypeOf<T>()
    }
}

/*class EitherAssert<SUCCESS, FAILURE>(
    actual: Either<SUCCESS, FAILURE>,
) : AbstractAssert<EitherAssert<SUCCESS, FAILURE>, Either<SUCCESS, FAILURE>>(actual, EitherAssert::class.java) {

    companion object {
        fun <SUCCESS, ERROR> makeAssertion(actual: Either<SUCCESS, ERROR>): EitherAssert<SUCCESS, ERROR> {
            return EitherAssert(actual)
        }
    }

    fun isSuccessOf(successAssertion: (SUCCESS) -> Unit) {
        actual.onSuccess {
            successAssertion(it)
        }.onFailure {
            Assertions.fail("Expected a valid result but got an error: $it")
        }
    }

    fun isFailureOf(errorAssertion: (FAILURE) -> Unit) {
        actual.onSuccess {
            Assertions.fail("Expected an error but got a valid result: $it")
        }.onFailure {
            errorAssertion(it)
        }
    }
}

infix fun <SUCCESS, FAILURE> Either<SUCCESS, FAILURE>.`should be success and`(successAssertion: SUCCESS.() -> Unit) {
    EitherAssert.makeAssertion(this).isSuccessOf {
        it.successAssertion()
    }
}

infix fun <SUCCESS, FAILURE> Either<SUCCESS, FAILURE>.`should be failure and`(failureAssertion: FAILURE.() -> Unit) {
    EitherAssert.makeAssertion(this).isFailureOf {
        it.failureAssertion()
    }
}

infix fun <SUCCESS, FAILURE> Either<SUCCESS, FAILURE>.`should be failure of`(T: KClass<*>) {
    EitherAssert.makeAssertion(this).isFailureOf {
        assertThat(it).isInstanceOf(T.java)
    }
}

infix fun <SUCCESS, FAILURE> Either<SUCCESS, FAILURE>.`should be failure of`(expected: FAILURE) {
    EitherAssert.makeAssertion(this).isFailureOf {
        it) shouldBe expected
    }
}
*/