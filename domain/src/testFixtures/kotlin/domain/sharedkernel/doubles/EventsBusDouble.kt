package domain.sharedkernel.doubles

import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.EventBus
import domain.sharedkernel.Listener

class EventsBusDouble: EventBus {

    val events = mutableListOf<DomainEvent>()
    override fun publish(event: DomainEvent) {
        events.add(event)
    }

    override fun subscribe(subscriber: Listener) {}

}