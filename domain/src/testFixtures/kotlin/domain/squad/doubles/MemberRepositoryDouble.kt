package domain.squad.doubles

import domain.squad.model.Member
import domain.sharedkernel.model.Email
import domain.sharedkernel.model.ID
import domain.squad.spi.MemberRepository

class MemberRepositoryDouble(vararg member: Member): MemberRepository {
    val members: MutableSet<Member> = member.toMutableSet()
    override fun findBy(id: ID<Member>): Member? {
        return members.firstOrNull { it.id == id }
    }

    override fun findByEmail(email: Email): Member? {
        return members.firstOrNull { it.email == email }
    }

    override fun save(newMember: Member) {
        members.add(newMember)
    }

    override fun findAll(): List<Member> = members.toList()

    fun clear() {
        members.clear()
    }

    fun addAll(member: List<Member>) {
        members.addAll(member)
    }
}