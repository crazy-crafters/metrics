package domain.squad.doubles

import domain.squad.model.PillarNote
import domain.squad.spi.NoteRepository

class NoteRepositoryDouble(vararg notes: PillarNote): NoteRepository {
    val items = mutableListOf(*notes)

}