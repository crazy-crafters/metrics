package domain.squad.doubles

import domain.sharedkernel.model.DomainEvent
import domain.sharedkernel.model.ID
import domain.sharedkernel.SystemEventsRepository

class SystemEventsRepositoryDouble(vararg events: DomainEvent) : SystemEventsRepository {
    val events = mutableListOf<DomainEvent>()

    init {
        this.events.addAll(events.toList())
    }

    fun add(e: List<DomainEvent>) {
        events.addAll(e)
    }

    override fun findAll(id: ID<*>) = events.filter { it.id == id }
}