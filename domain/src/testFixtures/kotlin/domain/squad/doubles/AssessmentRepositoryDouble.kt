package domain.squad.doubles

import io.kotest.matchers.collections.shouldNotBeEmpty
import domain.squad.model.Pillar
import domain.squad.model.Squad
import domain.squad.spi.AssessmentRepository
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title

class AssessmentRepositoryDouble: AssessmentRepository {
    val pillars = mutableListOf<Pillar>()

    fun `should contain`(expected: Pillar) {
        pillars.filter {
            it.title == expected.title
         && it.squad == expected.squad
         && it.score == expected.score
        }.shouldNotBeEmpty()
    }

    override fun save(pillar: Pillar) {
        pillars.remove(pillar)
        pillars.add(pillar)
    }

    override fun find(`for squad`: ID<Squad>, named: Title<Pillar>): Pillar? {
        return pillars.firstOrNull { it.title == named && it.squad == `for squad` }
    }

    override fun find(id: ID<Pillar>): Pillar? {
        return pillars.firstOrNull { it.id == id }
    }

    override fun findCurrentAssessment(`for squad`: ID<Squad>): List<Pillar> {
        return pillars.filter { it.squad == `for squad` }
    }

    fun idOf(indicator: String): ID<Pillar>? = pillars.firstOrNull { it.title == Title<Pillar>(indicator) }?.id
}