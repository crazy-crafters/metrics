package domain.squad.doubles

import domain.squad.model.Milestone
import domain.sharedkernel.model.ID
import domain.squad.spi.MilestoneRepository

class MilestoneRepositoryDouble: MilestoneRepository {
    val events = mutableListOf<Milestone>()

    init {
        this.events.addAll(events.toList())
    }

    fun add(e: List<Milestone>) {
        events.addAll(e)
    }
    fun add(e: Milestone) {
        events.add(e)
    }

    override fun findAll(id: ID<*>) = events.filter { it.squad == id }
    override fun save(milestone: Milestone) {
        add(milestone)
    }
}
