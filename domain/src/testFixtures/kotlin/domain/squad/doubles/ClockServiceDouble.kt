package domain.squad.doubles

import domain.sharedkernel.model.DateTime
import domain.squad.spi.ClockService

class ClockServiceDouble: ClockService {
    override fun now(): DateTime = now

    companion object {
        val now = DateTime.parse("2024-10-06T12:00:00.000Z")
    }
}