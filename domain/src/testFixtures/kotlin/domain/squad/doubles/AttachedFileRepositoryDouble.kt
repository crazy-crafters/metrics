package domain.squad.doubles

import domain.squad.model.AttachedFile
import domain.squad.model.AttachedFileDescriptor
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.spi.AttachedFileRepository
import java.util.*

fun <T> String.toId() = ID<T>(UUID.nameUUIDFromBytes(this.lowercase().toByteArray()))

fun file(name: String) = AttachedFile(
    AttachedFileDescriptor(name.toId(),
    Name.from(name)),
    name.toByteArray()
)


class AttachedFileRepositoryDouble(vararg files: AttachedFile): AttachedFileRepository {
    val items = mutableListOf(*files)

    override fun save(file: AttachedFile) {
        items.add(file)
    }

    override fun find(id: ID<AttachedFile>): AttachedFile? {
        return items.firstOrNull { it.descriptor.id == id }
    }

    fun create(filename: String): AttachedFile {
        val f = AttachedFile(
            AttachedFileDescriptor(
                id = filename.toId(),
                name = Name.from(filename)
            ),
            content = filename.toByteArray()
        )
        items.add(f)
        return f
    }
}