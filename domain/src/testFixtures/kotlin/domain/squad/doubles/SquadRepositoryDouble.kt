package domain.squad.doubles

import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldNotContain
import io.kotest.matchers.shouldNotBe
import domain.sharedkernel.model.ID
import domain.squad.model.Member
import domain.squad.model.Squad
import domain.squad.spi.SquadRepository

class SquadRepositoryDouble(vararg squads: Squad): SquadRepository {
    val squads: MutableList<Squad> = squads.toMutableList()
    override fun find(squad: ID<Squad>): Squad? {
        return squads.firstOrNull { it.id == squad }
    }

    override fun save(squad: Squad) {
        squads.add(squad)
    }

    override fun findAll() = squads

    fun check(member: Member, `should be in`: ID<Squad>? = null, `should not be in`: ID<Squad>? = null): SquadRepositoryDouble {
        if (`should be in` != null) {
            val s = find(`should be in`)
            s shouldNotBe null
            s!!.members.shouldContain(member.id)
        }
        if (`should not be in` != null) {
            val s = find(`should not be in`)
            s shouldNotBe null
            s!!.members.shouldNotContain(member.id)
        }
        return this
    }
}