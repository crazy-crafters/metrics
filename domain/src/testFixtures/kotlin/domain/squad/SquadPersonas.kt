package domain.squad

import domain.sharedkernel.model.*
import domain.squad.model.Member
import domain.squad.model.Squad
import domain.squad.model.events.MemberHasJoinedDomainEvent
import domain.squad.model.events.MemberHasLeftDomainEvent
import domain.squad.model.events.NewSquadCreatedDomainEvent

val squad1: Squad
    get() = Squad(ID.from("SQUAD 1"), Name.from("Squad 1"))
val squad2: Squad
    get() = Squad(ID.from("SQUAD 2"), Name.from("Squad 2"))

val squad3: Squad
    get() = Squad(ID.from("SQUAD 3"), Name.from("Squad 3"))
val grace: Member
    get() = Member(
        ID.from("GRACE"),
        Name.from("Grace Slick"),
        Email.from("grace.slick@foo.bar")
    )
val janis: Member
    get() = Member(
        ID.from("JANIS"),
        Name.from("Janis Joplin"),
        Email.from("janis.joplin@foo.bar")
    )
val patti: Member
    get() = Member(
        ID.from("PATTI"),
        Name.from("Patti Smith"),
        Email.from("patti.smith@foo.bar")
    )
val alison: Member
    get() = Member(
        ID.from("Alison"),
        Name.from("Alison Mosshart"),
        Email.from("Alison.mosshart@foo.bar")
    )

fun Squad.with(vararg members: Member): Squad {
    members.forEach {
        this.join(it.id)
    }
    return this
}

fun createMember(name: String) = Member(
    ID.from(name),
    Name.from(name),
    Email.from(name.lowercase().replace(" ", ".") + "@foo.bar")
)
val carolyn: Member
    get() = createMember("Carolyn Porco")
val margaret: Member
    get() = createMember("Margaret Hamilton")
val jennifer: Member
    get() = createMember("Jennifer Trosper")


fun Squad.hasBeenCreated(on: DateTime) = NewSquadCreatedDomainEvent(this.id, on, this.name, this.members)
fun Member.joined(squad: Squad, on: DateTime) = MemberHasJoinedDomainEvent(squad.id, on, this.id)
fun Member.left(squad: Squad, on: DateTime) = MemberHasLeftDomainEvent(squad.id, on, this.id)



internal fun retrospective(date: DateTime): TimelineItem {
    return TimelineItem(
        title = Title("Retrospective"),
        description = CustomDescription("This is the minutes of our retro"),
        date = date,
        type = MilestoneType.Retrospective
    )
}