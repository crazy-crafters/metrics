package domain.project.doubles

import domain.project.model.Project
import domain.sharedkernel.model.ID
import domain.survey.model.SurveyEvent
import domain.survey.spi.SurveyEventRepository

class SurveyEventRepositoryDouble(vararg event: SurveyEvent): SurveyEventRepository {
    val events = mutableListOf(*event)

    override fun findAll(project: ID<Project>): List<SurveyEvent> {
        return events.filter { it.id == project }
    }

    override fun save(event: SurveyEvent) {
        events.add(event)
    }
}