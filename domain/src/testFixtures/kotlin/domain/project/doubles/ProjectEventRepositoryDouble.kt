package domain.project.doubles

import domain.project.model.Project
import domain.project.model.ProjectEvent
import domain.project.model.ProjectStarted
import domain.project.spi.ProjectEventRepository
import domain.sharedkernel.model.ID


class ProjectEventRepositoryDouble(vararg project: ProjectEvent) : ProjectEventRepository {
    val items = mutableListOf(*project)
    override fun find(from: ID<Project>): List<ProjectEvent> {
        return items.filter { it.id == from }
    }

    override fun all(): List<ProjectEvent> {
        return items
    }

    override fun save(event: ProjectEvent) {
        items.add(event)
    }

    fun Project.`has started`() {
        items.add(
            ProjectStarted(
                id = this.id,
                name = this.name,
                description = this.description,
                date = this.start
            )
        )
    }
}