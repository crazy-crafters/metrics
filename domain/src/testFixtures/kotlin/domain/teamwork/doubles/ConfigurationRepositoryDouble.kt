package domain.teamwork.doubles


import io.kotest.matchers.shouldNotBe
import domain.teamwork.configuration.spi.ConfigurationRepository
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login
import domain.project.model.Project
import domain.sharedkernel.model.ID

class ConfigurationRepositoryDouble: ConfigurationRepository {
    val data = mutableMapOf<ID<Project>, BoardConfiguration>()

    override fun save(project: ID<Project>, configuration: BoardConfiguration) {
        data[project] = configuration
    }

    override fun find(project: ID<Project>): BoardConfiguration? {
        return data[project]
    }

    override fun all(): List<BoardConfiguration> {
        return data.values.toList()
    }


    fun add(project: ID<Project>, type: BoardType) {
        data[project] = BoardConfiguration.Valid(project, type, url = "http://localhost", board = project.toString(), Login("", ""))
    }

    fun `should have a configuration for`(project: ID<Project>) {
        val c = data[project]
        c shouldNotBe null
        c shouldNotBe BoardConfiguration.None
    }


}