package domain.teamwork.doubles

import domain.teamwork.shared.spi.EventRepository
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Label
import domain.project.model.Project
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Tag
import domain.teamwork.issue.model.Tags
import domain.teamwork.shared.events.*
import domain.teamwork.shared.events.Event
import domain.teamwork.shared.events.IssueAdded
import domain.teamwork.shared.events.IssueClosed
import domain.teamwork.shared.events.IssueMoved
import domain.teamwork.status.model.Status
import java.util.*

class EventRepositoryDouble : EventRepository {
    val events = mutableListOf<Event>()
    override fun all(of: ID<Project>): List<Event> {
        return events.filter { e -> e.project == of }
    }

    override fun findAll(of: ID<Project>, issue: ID<Issue>): List<Event> {
        return all(of).filter { it.entityId == issue }
    }

    fun save(event: Event) {
        events.add(event)
    }

    override fun clear(project: ID<Project>) {
        events.removeIf { it.project == project }
    }

    fun contains(project: ID<Project>, s: String) {
        val lines = s.trimIndent().trim().split("\n").map { it.trim() }.filter { it.isNotBlank() }

        lines.filter { it.startsWith("Issue", ignoreCase = true) }.flatMap { line ->
            line.toIssue(project).events
        }.forEach {
            events.add(it)
        }

        lines.filter { it.startsWith("Feature", ignoreCase = true) }.flatMap { line ->
            line.toFeature(project)
        }.forEach {
            events.add(it)
        }
    }

    fun contains(vararg issues: Issue) {
        issues.flatMap { it.events }.forEach {
            events.add(it)
        }
    }

    private fun String.toFeature(project: ID<Project>): List<IssueEvent> {
        // Feature 1 : 01-01 Issue 1 01-02, Issue 2 01-01
        val rx = "Feature (\\d+) : (\\d{2}-\\d{2}) (.+)".toRegex()
        val gr = rx.find(this)?.groups
        if (gr.isNullOrEmpty())
            return emptyList()
        val (_, id, date, issues) = gr.mapNotNull { it?.value }

        issues.split(",")
            .map { it.replace("Issue", "").trim().split(" ") }
            .forEach { (issue, date) ->
                val date = "2024-${date}T12:00:00".toDateTime()
                events.add(
                    IssueTagAdded(date = date, project = project, entityId = ID.from("Issue $issue"), tag = Tag.on("Feature:$id"))
                )
            }

        return emptyList()
    }

    private fun String.toChangelogFromLogs(): List<Pair<Status, DateTime>> {
        if (this.isBlank()) return emptyList()
        return this.split(",").map { it ->
            val (state, date) = it.trim().split(" ")
            val id = state.lowercase(Locale.getDefault()).replace("_", "").replace(" ", "")
            Status.of(id = ID.from(id.replace("_", "")), Label.from(state)) to date.toDateTime()
        }
    }

    fun containsFromLogs(project: ID<Project>, s: String) {
        s.trimIndent().trim().split("\n").filter { it.isNotBlank() }.flatMap { line ->
            val (name, points, states) = line.split("=", "->").map { it.trim() }
            val changes = states.toChangelogFromLogs()
            listOf(
                IssueAdded(
                    entityId = ID.from(name),
                    project = project,
                    title = Title.from(name),
                    date = changes.firstOrNull()?.second ?: DateTime.MIN,
                    status = Status.OutOfScope,
                    estimation = PointsOfComplexity.of(points.replace(" points", "").toInt()),
                    tags = Tags.empty()
                )
            ) + changes.map {
                IssueMoved(
                    entityId = ID.from(name), project = project, date = it.second, status = it.first
                )
            }
        }.forEach {
            events.add(it)
        }
    }
}

fun Status.Companion.of(id: ID<Status>, value: Label<Status>): Status {
    return when (id) {
        Status.OutOfScope.id -> Status.OutOfScope
        Backlog.id -> Backlog
        Planned.id -> Planned
        Ready.id -> Ready
        InProgress.id -> InProgress
        isDone.id -> isDone
        Closed.id -> Closed
        else -> Status.Undefined(label = value, id = id)
    }
}


fun String.toIssue(
    project: ID<Project>
): Issue {
    val (name, points, states) = this.split(":", "->").map { it.trim() }
    val changelog = states.toChangelog()
    return Issue.from(
        listOf(
            IssueAdded(
                entityId = ID.from(name),
                project = project,
                title = Title.from(name),
                date = changelog.firstOrNull()?.second ?: DateTime.MIN,
                status = Status.Backlog,
                estimation = PointsOfComplexity.of(points.replace(" points", "").toInt()),
                tags = Tags.empty()
            )
        ) + changelog.map {
            if (it.first == Status.of(id = ID.from("cancelled"), Label.from("Cancelled"))) {
                IssueClosed(
                    entityId = ID.from(name),
                    project = project,
                    date = it.second,
                )
            } else {
                IssueMoved(
                    entityId = ID.from(name), project = project, date = it.second, status = it.first
                )
            }
        }).fold({ it }, { TODO(it.toString()) })
}

private fun String.toChangelog(): List<Pair<Status, DateTime>> {
    if (this.isBlank()) return emptyList()
    return this.split(",").map {
        val (state, date) = it.trim().split(" ")
        val dateStr = if (date.count { it == '-' } == 1) {
            "2024-${date}T12:00:00".toDateTime()
        } else {
            "${date}T12:00:00".toDateTime()
        }
        val id = state.lowercase(Locale.getDefault()).replace("_", "").replace(" ", "")
        Status.of(id = ID.from(id), value = Label.from(state)) to dateStr
    }
}

val Status.Companion.Backlog: Status
    get() = Status.Planned(label = Label.from("backlog"), id = ID.from("backlog"))
val Status.Companion.Planned: Status
    get() = Status.Planned(label = Label.from("Created"), id = ID.from("created"))
val Status.Companion.Ready: Status
    get() = Status.Waiting(label = Label.from("Ready"), id = ID.from("ready"))
val Status.Companion.InProgress: Status
    get() = Status.Working(label = Label.from("In Progress"), id = ID.from("inprogress"))
val Status.Companion.isDone: Status
    get() = Status.Done(label = Label.from("Done"), id = ID.from("done"))
val Status.Companion.Closed: Status
    get() = Status.Closed(label = Label.from("Closed"), id = ID.from("closed"))