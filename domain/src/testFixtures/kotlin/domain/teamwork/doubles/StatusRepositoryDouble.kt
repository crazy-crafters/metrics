package domain.teamwork.doubles

import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.model.Status
import domain.teamwork.status.spi.StatusRepository

class StatusRepositoryDouble: StatusRepository {
    val items = mutableMapOf<ID<Project>, List<Status>>()

    override fun save(project: ID<Project>, status: Status) {
        items[project] = items.getOrDefault(project, emptyList()).filter { it.id != status.id } + status
    }

    override fun find(project: ID<Project>, id: ID<Status>): Status? {
        return items.getOrDefault(project, emptyList()).firstOrNull { it.id == id }
    }

    override fun findAll(of: ID<Project>): List<Status> {
        return items.getOrDefault(of, emptyList())
    }

    fun with(project: ID<Project>, vararg of: Status): StatusRepositoryDouble {
        of.forEach {
            save(project, it)
        }
        return this
    }
}


