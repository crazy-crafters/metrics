package domain.teamwork.doubles

import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Title
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Label
import domain.project.model.Project
import domain.teamwork.issue.model.*
import java.util.*
import domain.teamwork.shared.events.*
import domain.teamwork.status.model.Status
import domain.teamwork.startOfDay

private val aTypicalBoard =
"""
        | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 
--------+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----
Issue 1 | R  |    |    | P  |    |    | T  | D  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 2 | R  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 3 | R  |    |    |    | P  |    |    |    |    |    | D  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 4 |    |    |    | R  |    | P  |    | D  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |         
Issue 5 |    |    |    |    |    |    |    | R  |    |    |    |    |    | P  |    |    | R  |    |    |    | P  |    | D  |    |    |    |    |    |    |    
Issue 6 |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
Issue 7 |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |  R |  P |  R | P  | C  |    |
""".trimIndent()
val issues = aTypicalBoard.toIssueEvents()

fun `an issue with`(`created on`: String, vararg changes: Pair<String, Status>)
    = `an issue with`(ID.from("dummy"), startOfDay(`created on`), changes.map { (l, r) -> startOfDay(l) to r }.toList())

fun `an issue with`(project: ID<Project>, `created on`: DateTime, changes: List<Pair<DateTime, Status>>, estimation: Int = 5): Issue {
    val id = ID.from<Issue>(UUID.randomUUID())
    return Issue.from(
        IssueAdded(
            entityId = id,
            project = project,
            title = Title.from("title"),
            date = `created on`,
            status = Status.Backlog,
            estimation = PointsOfComplexity.of(estimation),
            tags = Tags.empty()
        ),
        *changes.map { (date, status) ->
            IssueMoved(
                entityId = id,
                project = project,
                date = date,
                status = status
            )
        }.toTypedArray()
    ).fold({it}, {null})!!
}

fun String.toIssueEvents() =
    IssueEventPicker(
        this.trimIndent().split("\n").filter { it.isNotBlank() }.drop(2)
            .flatMap {
                val parts = it.split("|")
                val name = parts[0].trim()
                val changes = parts.drop(1).mapIndexed { index, value -> index + 1 to value.trim() }
                    .filter { (_, value) -> value.isNotBlank() }
                    .map { (day, value) ->
                        val date = DateTime.parse("2024-01-${day.toString().padStart(2, '0')}T12:00:00")
                        when (value) {
                            "B" -> Status.Backlog
                            "C" -> Status.Closed(id = ID.from("cancelled"), Label.from("Cancelled"))
                            "R" -> Status.Ready
                            "P" -> Status.InProgress
                            "D" -> Status.isDone
                            "T" -> Status.Waiting(id = ID.from("testing"), Label.from("Tests"))
                            else -> Status.of(id= ID.from(value), Label.from(value))
                        } to date
                    }
                listOf(
                    IssueAdded(
                        entityId = ID.from(name),
                        project = anId.ofApollo(),
                        title = Title.from(name),
                        date = changes.firstOrNull()?.second ?: DateTime.parse("2024-01-01T00:00:00"),
                        status = Status.OutOfScope,
                        estimation = PointsOfComplexity.of(5),
                        tags = Tags.empty()
                    )
                ) + changes.map {
                    if (it.first.id == ID.from<Status>("cancelled")) {
                        IssueClosed(
                            entityId = ID.from(name),
                            project = anId.ofApollo(),
                            date = it.second
                        )
                    } else {
                        IssueMoved(
                            entityId = ID.from(name),
                            project = anId.ofApollo(),
                            date = it.second,
                            status = it.first
                        )
                    }
                }
            }
    )

class IssueEventPicker(val all: List<Event>) {

    fun all(): List<Issue> {
        return all.map { it.entityId.value }.distinct().map { get(it) }
    }

    fun get(vararg names: String): List<Issue> {
        return names.map {name ->
            get(name)
        }
    }

    fun get(name: String): Issue {
        return Issue.from(all.filter { it.entityId.value == name }.filterIsInstance<IssueEvent>())
            .fold({it}, {TODO()})
    }

    fun get(name: Int): List<Issue> {
        return listOf(get("Issue $name"))
    }

    fun get(vararg name: Int): List<Issue> {
        return get(*name.map { "Issue $it" }.toTypedArray())
    }
}