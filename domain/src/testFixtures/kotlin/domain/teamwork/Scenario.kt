package domain.teamwork

import domain.sharedkernel.materials.anId
import domain.teamwork.configuration.model.BoardConfiguration
import domain.project.model.Project

object aConfiguration {
    fun of(s: String = "apollo"): BoardConfiguration = anId.of<Project>(s).toConfiguration()
}