package domain.teamwork

import domain.project.model.Project
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.sharedkernel.model.Title
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.BoardType
import domain.teamwork.configuration.model.Login
import domain.teamwork.doubles.of
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Tags
import domain.teamwork.shared.events.IssueAdded
import domain.teamwork.shared.events.IssueEvent
import domain.teamwork.shared.events.IssueMoved
import domain.teamwork.status.model.Status
import java.util.*

fun d(s: String) = "${s}T12:00:00".toDateTime()

fun makeCreateEvent(id: String, date: String, project: String = anId.ofApollo<Project>().value, title: String = "Issue $id", estimation: Int = 1): IssueEvent {
    return IssueAdded(
        entityId = ID.from(id),
        project = ID.from(project),
        title = Title.from(title),
        estimation = PointsOfComplexity.of(estimation),
        status = Status.OutOfScope,
        date = DateTime.parse("${date}T12:00:00"),
        tags = Tags.empty()
    )
}

fun makeMoveEvent(id: String = "1", date: String, newStatus:  String = "in progress"): IssueEvent {
    return IssueMoved(
        entityId = ID.from(id),
        project = anId.ofApollo(),
        date = DateTime.parse("${date}T12:00:00"),
        status = toStatus(newStatus)
    )
}

fun String.toEvents(): List<IssueEvent> =
    this.split("\n").filter { it.isNotBlank() }.drop(2)
    .flatMap {
        val parts = it.split("|")
        val name = parts[0].trim()
        parts.drop(1).mapIndexed { index, value -> index + 1 to value.trim() }
            .filter { (_, value) -> value.isNotBlank() }
            .map { (day, value) ->
                val date = "2024-01-${day.toString().padStart(2, '0')}"
                when (value) {
                    "C" -> makeCreateEvent(id = name, date = date) //Status.Created
                    else -> makeMoveEvent(id = name, date = date, newStatus = value) //Status.of(value, IssueState.Open)
                }
            }
    }

fun toStatus(value: String) =

    when(value) {
        "R" -> Status.of(ID.from("ready"), Label.from("Ready"))
        "P" -> Status.of(ID.from("inprogress"), Label.from("InProgress"))
        "D" -> Status.of(ID.from("done"), Label.from("Done"))
        else -> Status.of(
            ID.from(value.lowercase(Locale.getDefault()).replace("_", "").replace(" ", "")),
            Label.from(value)
        )
    }

fun startOfDay(s: String) = DateTime.parse(s.replace(" ", "-") + "T00:00:00")
fun endOfDay(s: String) = DateTime.parse(s.replace(" ", "-") + "T23:59:59.999")
fun List<IssueEvent>.get(id: String): Issue = Issue.from(filter { it.entityId == ID.from<Issue>(id) }).fold({it}, {TODO()})

fun ID<Project>.toConfiguration() = BoardConfiguration.Valid(
    project = this,
    type = BoardType.Jira,
    url = "http://localhost",
    board = this.value,
    login = Login("domain/user", "pass")
)