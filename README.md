# Metrics Application

## How to run

### Prerequisites

- Gradle 8.0
- Kotlin 2.0
- node v22.11.0
- Stenciljs v4.13.0

### Configuration

- In `ssr` project, copy the *application.conf* to *application.secrets.conf*
- Fill the secrets with your own Google client ID & secrets

To disable the authentication part (and app security), add `profile = "no-auth"` to the */application.secrets.conf*

> DO NOT COMMIT THE FILE CONTAINING YOUR SECRETS

### Init dev environment

The UI components requires [Stencils.js](https://stenciljs.com/)

```shell
# If you use nvm to manage node versions, you can switch to node version in .nvmrc file
nvm use

npm install --global @stencil/core@latest --save-exact
```

### Build

- Step 1: build UI component (from root directory)

```shell
cd platform/ssr/src/ui-components
npm install
npm run build
```

- Step 2: Build app (from root directory)

```shell
./gradlew clean check
./gradlew jar
```

## Run 

### Console

```shell
java -jar apps/cli/build/libs/cli-all.jar --help
```

By default, data is stored in `$HOME/.blackbox`

### SSR

```shell
java -jar apps/cli/build/libs/cli-all.jar -config=apps/ssr/src/main/resources/application.secrets.conf
```

##### Run locally and demo mode

Configuration example:

```
datasource = {
    type = "filesystem"
    path=""
    demo-mode=false
}
```

In the configuration file *application.conf*, change the `type` to:
- `in-memory`: runs the app using volatile data source. Everything will be lost when the app will close
- `filesystem`: runs the app using json files saved on the local file system. Set `path` to the directory where you want
  to store the data.

If `demo-mode` is set to `true`, the app will load fake data. It is recommended to use the demo mode with in-memory source

## Tests

```shell
./gradlew clean check
```

## About UI

This project uses [Bulma](https://bulma.io/documentation/) & [Kotlin DSL](https://kotlinlang.org/docs/type-safe-builders.html) to build most of the pages. \
For UI components requiring more than html & vanilla JS, it uses [Stencil](https://stenciljs.com/) (see *ui-components* directory)

### Overriding Bulma

- Open the file *ui-components/src/global.scss*
- Follow the instructions from [Bulma documentation](https://bulma.io/documentation/customize/with-sass/)
  - Ensure that your modification occurs before including bulma component
- From the ui-components directory, run `npm run build`
- Restart the app
