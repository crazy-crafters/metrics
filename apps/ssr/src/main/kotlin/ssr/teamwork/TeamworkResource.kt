package ssr.teamwork

import io.ktor.resources.*

@Resource("/teamwork/{project}")
data class TeamworkResource(val project: String) {

    @Resource("configuration")
    class Configure(val parent: TeamworkResource) {
        constructor(project: String): this(TeamworkResource(project))
        operator fun component1() = parent.project

        @Resource("status")
        class Status(val parent: Configure) {
            constructor(project: String): this(Configure(project))
            operator fun component1() = parent.parent.project
        }

        @Resource("board")
        class Board(val parent: Configure) {
            constructor(project: String): this(Configure(project))
            operator fun component1() = parent.parent.project
        }
    }

    @Resource("update")
    class Update(val parent: TeamworkResource) {
        constructor(project: String): this(TeamworkResource(project))
        operator fun component1() = parent.project
    }

    @Resource("report")
    class Report(val parent: TeamworkResource) {
        constructor(project: String): this(TeamworkResource(project))

        @Resource("general")
        class GeneralStats(val parent: Report, val from: String? =null, val to: String? = null) {
            constructor(project: String): this(Report(project))
        }

        @Resource("review")
        class Review(val parent: Report, val from: String? =null, val to: String? = null) {
            constructor(project: String): this(Report(project))
        }

        @Resource("flow")
        class Flow(val parent: Report, val from: String? =null, val to: String? = null) {
            constructor(project: String): this(Report(project))
        }
    }

}