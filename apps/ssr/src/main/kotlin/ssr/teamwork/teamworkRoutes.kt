package ssr.teamwork


import io.ktor.server.application.*
import ssr.teamwork.ui.configuration.configurationRoutes
import ssr.teamwork.ui.reports.reportRoutes
import ssr.teamwork.ui.status.statusRoutes


fun Application.teamworkRoutes() {
    statusRoutes()
    reportRoutes()
    configurationRoutes()
}

