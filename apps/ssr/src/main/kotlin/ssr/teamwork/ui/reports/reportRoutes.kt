package ssr.teamwork.ui.reports

import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import ssr.configuration.privateGet
import ssr.configuration.privatePost
import ssr.teamwork.TeamworkResource


fun Application.reportRoutes() {
    routing {
        privateGet<TeamworkResource.Report.GeneralStats> { r, user ->
            displayGeneralReportPage(from=r.from, to=r.to, settings = user.userSettings)
        }

        privatePost<TeamworkResource.Report.GeneralStats> { r, user ->
            val params = call.receiveParameters()
            val from = params["from"]!!
            val to = params["to"]!!

            val url: String = href(TeamworkResource.Report.GeneralStats(r.parent, from=from, to=to))
            call.respondRedirect(url)
        }

        privateGet<TeamworkResource.Report.Flow> { r, user ->
            displayFlowReportPage(from=r.from, to=r.to, settings = user.userSettings)
        }

        privatePost<TeamworkResource.Report.Flow> { r, user ->
            val params = call.receiveParameters()
            val from = params["from"]!!
            val to = params["to"]!!

            val url: String = href(TeamworkResource.Report.Flow(r.parent, from=from, to=to))
            call.respondRedirect(url)
        }
    }
}