package ssr.teamwork.ui.status

import io.ktor.server.routing.*
import kotlinx.html.*
import ssr.teamwork.ui.pageWithMenuProjectHeader
import domain.user.UserSettings


val RoutingContext.statusPage: (settings: UserSettings, statuses: List<domain.teamwork.status.model.Status>, validateUrl: String) -> HTML.() -> Unit
    get() = { settings, statuses, validateUrl ->
        pageWithMenuProjectHeader(settings) {
            h3("title is-3") {
                +"Status"
            }
            div("section") {
                form(action = validateUrl, method = FormMethod.post) {
                    div("is-flex is-flex-direction-column") {
                        button(classes = "button is-dark ml-auto", type = ButtonType.submit) { +"Save" }
                        table("table mt-4") {
                            tbody {
                                statuses.map {
                                    val selectColor = when (it) {
                                        is domain.teamwork.status.model.Status.Planned -> "is-info"
                                        is domain.teamwork.status.model.Status.Waiting -> "is-warning"
                                        is domain.teamwork.status.model.Status.Working -> "is-info"
                                        is domain.teamwork.status.model.Status.Closed -> "is-success"
                                        is domain.teamwork.status.model.Status.Done -> "is-success"
                                        else -> ""
                                    }
                                    val rowColumn = if (it is domain.teamwork.status.model.Status.Undefined) "has-text-danger" else ""
                                    tr() {
                                        td("is-vcentered $rowColumn") { +it.id.toString() }
                                        td("is-vcentered $rowColumn") { +it.label.toString() }
                                        td {
                                            div("select control") {
                                                select("select $selectColor") {
                                                    name = it.id.toString()
                                                    option {
                                                        value = "Undefined"
                                                        selected = it is domain.teamwork.status.model.Status.Undefined
                                                        +"Undefined"
                                                    }
                                                    option {
                                                        value = "Planned"
                                                        selected = it is domain.teamwork.status.model.Status.Planned
                                                        +"Planned"
                                                    }
                                                    option {
                                                        value = "Waiting"
                                                        selected = it is domain.teamwork.status.model.Status.Waiting
                                                        +"Waiting"
                                                    }
                                                    option {
                                                        value = "Working"
                                                        selected = it is domain.teamwork.status.model.Status.Working
                                                        +"Working"
                                                    }
                                                    option {
                                                        value = "Done"
                                                        selected = it is domain.teamwork.status.model.Status.Done
                                                        +"Done"
                                                    }
                                                    option {
                                                        value = "Closed"
                                                        selected = it is domain.teamwork.status.model.Status.Closed
                                                        +"Closed"
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }