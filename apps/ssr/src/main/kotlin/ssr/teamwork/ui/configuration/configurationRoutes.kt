package ssr.teamwork.ui.configuration

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import domain.project.api.GetProject
import domain.project.model.Project
import domain.project.model.ProjectResult
import domain.sharedkernel.model.ID
import ssr.configuration.href
import ssr.configuration.privateGet
import ssr.configuration.privatePost
import ssr.teamwork.TeamworkResource
import ssr.ui.base.inject
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.Login
import infra.teamwork.providers.UpdateBoard
import domain.user.HandleUserProfile
import domain.user.UserProfile
import domain.user.UserSettings

fun Application.configurationRoutes() {
    routing {
        privateGet<TeamworkResource.Configure.Board> { _, user ->
            val getUserProfile: HandleUserProfile by inject()

            displayBoardConfigurationPage(
                settings = (getUserProfile.get(user.id) as UserProfile.Found).settings,
                projectId = ID.from(call.parameters["project"]!!)
            )
        }

        privatePost<TeamworkResource.Configure.Board> { (project), user ->
            val form = call.receiveParameters()
            val projectId = ID.from<Project>(project)
            val configuration = BoardConfiguration.Valid(
                project = projectId.remap(),
                type = domain.teamwork.configuration.model.BoardType.valueOf(form["type"]!!),
                board = form["board"]!!,
                url = form["url"]!!,
                login = Login(
                    user = form["username"]!!,
                    pass = form["token"]!!
                )
            )
            val configurationController: HandleConfiguration by inject()

            configurationController.save(projectId.remap(), configuration)

            call.respondRedirect(href(TeamworkResource.Configure.Board(project)))
        }

        privatePost<TeamworkResource.Update> { (project), user ->
            val updateBoard: UpdateBoard by inject()
            updateBoard.run(project)
            call.respondRedirect(href(TeamworkResource.Configure.Board(project)))
        }

        /*post<TeamworkResource> {
            val project = call.parameters["project"]!!
            val form = call.receiveParameters()
            val configuration = BoardConfigurationDTO(
                type = BoardType.valueOf(form["type"]!!),
                board = form["board"]!!,
                url = form["url"]!!,
                username = form["username"]!!,
                token = form["token"]!!,
                isValid = true
            )
            val configurationController: DeliveryController by inject()
            configurationController.save(project, configuration)

            val ui: UIStructure by inject()
            call.respondRedirect(ui.urlTo<BoardConfigurationDTO>(project))
        }

        post<TeamworkResource.Update> {
            val updateController: UpdateController by inject()
            val ui: UIStructure by inject()

            val id = call.parameters["id"]!!
            updateController.run(UpdateCommandDTO(id))
            call.respondRedirect(ui.urlTo<BoardConfigurationDTO>(id))
        }*/
    }
}

suspend fun RoutingContext.displayBoardConfigurationPage(
    settings: UserSettings, projectId: ID<Project>
) {
    val handler: HandleConfiguration by inject()
    val getProject: GetProject by inject()

    val configuration = when (val conf = handler.get(projectId.remap())) {
        is BoardConfiguration.Valid -> conf
        is BoardConfiguration.None -> BoardConfiguration.default(projectId.remap())
    }

    when (val project = getProject.information(projectId)) {
        is ProjectResult.Found ->call.respondHtml(
            HttpStatusCode.OK, boardConfigurationPage(
                settings, project.project, configuration
            )
        )

        else -> call.respondRedirect("/")
    }
}

