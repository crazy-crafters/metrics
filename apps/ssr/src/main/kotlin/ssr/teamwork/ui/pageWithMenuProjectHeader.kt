package ssr.teamwork.ui

import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlinx.html.DIV
import kotlinx.html.HTML
import domain.project.api.GetProject
import domain.project.model.Project
import ssr.ui.Header
import ssr.ui.NavigationParameters
import ssr.ui.base.inject
import ssr.ui.base.pageWithMenuAndHeader
import domain.user.UserSettings

fun RoutingContext.pageWithMenuProjectHeader(
    settings: UserSettings,
    refresh: Boolean = false,
    bodyF: DIV.() -> Unit,
): HTML.() -> Unit {
    val getProject: GetProject by inject()
    val project = getProject.information(settings.project.remap())

    return pageWithMenuProjectHeader(settings, project as Project, refresh, bodyF)
}

fun RoutingContext.pageWithMenuProjectHeader(
    settings: UserSettings,
    project: Project,
    refresh: Boolean = false,
    bodyF: DIV.() -> Unit
): HTML.() -> Unit {
    val params = NavigationParameters(call.request.uri, settings, Header(project.name.toString(), project.description))

    return pageWithMenuAndHeader(params, refresh, bodyF=bodyF)
}
