package ssr.teamwork.ui.reports

import io.ktor.http.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.html.*
import domain.sharedkernel.model.DateTime
import ssr.Palette
import ssr.configuration.href
import ssr.setMessage
import ssr.teamwork.TeamworkResource
import ssr.teamwork.ui.pageWithMenuProjectHeader
import ssr.ui.base.Message
import ssr.ui.base.inject
import ssr.ui.uiComponent
import domain.teamwork.report.api.GetReport
import domain.teamwork.report.model.GeneralStatsReport
import infra.teamwork.UndefinedStatusException
import domain.user.UserSettings
import java.time.Duration
import java.time.temporal.ChronoUnit

suspend fun RoutingContext.displayGeneralReportPage(settings: UserSettings, from: String?, to: String?) {
    val controller: GetReport by inject()

    val dateFrom = from?.let { DateTime.parse(it) } ?: DateTime.now().plus(Duration.ofDays(-365))
    val dateTo = to?.let { DateTime.parse(it) } ?: DateTime.now()

    try {
        call.respondHtml(
            HttpStatusCode.OK,
            generalReportPage(
                settings,
                controller.generalStats(settings.project.remap(), dateFrom, dateTo)
            )
        )
    } catch (e: UndefinedStatusException) {
        call.setMessage(Message(Message.Level.Danger, e.message!!))
        call.respondRedirect(href(TeamworkResource.Configure.Status(settings.project.toString())))
    }
}

val RoutingContext.generalReportPage: (
    settings: UserSettings,
    generalInfo: GeneralStatsReport
) -> HTML.() -> Unit
    get() = { settings, generalInfo ->
        pageWithMenuProjectHeader(settings) {
            uiComponent(
                "sprint-picker",
                "from" to "'${generalInfo.from}'",
                "to" to "'${generalInfo.to}'",
                "validateUrl" to "'${call.request.uri}'"
            )
            displayGeneralInformation(generalInfo)
        }
    }

fun DIV.displayGeneralInformation(report: GeneralStatsReport) {
    when(report) {
        is GeneralStatsReport.NotFound -> return
        is GeneralStatsReport.Valid -> {
            fun Double.rounded() = (this * 1000).toLong().toDouble() / 1000.0
            val historyLength = report.durationInDays()
            val velocityCount = (report.done.count.toDouble() / historyLength).rounded()
            val velocityPoints = (report.done.points.value.toDouble() / historyLength).rounded()

            section("block") {
                h3("title is-3") {
                    +"General Statistics"
                }

                table("table is-fullwidth report") {
                    thead {
                        tr {
                            td {
                                style = "width: 150px;"
                            }

                            td {
                                style = "width: 200px;"
                                div("is-flex is-justify-content-center") {
                                    h4("title is-4") {
                                        +"Issues"
                                    }
                                }
                            }
                            td {
                                style = "width: 200px;"
                                div("is-flex is-justify-content-center") {
                                    h4("title is-4") {
                                        +"Points"
                                    }
                                }
                            }
                        }
                    }
                    tbody {
                        tr {
                            td {}
                            td {
                                buildChart(
                                    unfinished = report.unfinished.count,
                                    toDo = report.opened.count - report.unfinished.count,
                                    done = report.done.count,
                                    cancelled = report.closed.count - report.done.count
                                )
                            }
                            td {
                                buildChart(
                                    unfinished = report.unfinished.points.value,
                                    toDo = (report.opened.points - report.unfinished.points).value,
                                    done = report.done.points.value,
                                    cancelled = (report.closed.points - report.done.points).value
                                )
                            }
                        }
                        tr {
                            td {
                                buildLegend("To Do", Palette.Blue)
                            }
                            td { +"${report.opened.count - report.unfinished.count}" }
                            td { +"${report.opened.points - report.unfinished.points}" }
                        }
                        tr {
                            td {
                                buildLegend("In Progress", Palette.Yellow)
                            }
                            td { +"${report.unfinished.count}" }
                            td { +"${report.unfinished.points}" }
                        }
                        tr {
                            td {
                                buildLegend("Done", Palette.Green)
                            }
                            td { +"${report.done.count}" }
                            td { +"${report.done.points}" }
                        }
                        tr {
                            td {
                                buildLegend("Cancelled", Palette.Red)
                            }
                            td { +"${report.closed.count - report.done.count}" }
                            td { +"${report.closed.points - report.done.points}" }
                        }
                        tr {
                            td {
                                style = "border: none"
                                div("pt-4") { }
                            }
                            td { style = "border: none" }
                            td { style = "border: none" }
                        }
                        tr {
                            td { +"Closed" }
                            td { +"${report.closed.count}" }
                            td { +"${report.closed.points}" }
                        }
                        tr {
                            td { +"Opened" }
                            td { +"${report.opened.count}" }
                            td { +"${report.opened.points}" }
                        }
                        tr {
                            td { +"Velocity (/day)" }
                            td { +"$velocityCount" }
                            td { +"$velocityPoints" }
                        }
                    }
                }
            }
        }
    }

}

private fun TD.buildChart(
    unfinished: Int,
    toDo: Int,
    done: Int,
    cancelled: Int
) {
    buildChart(
        asData(
            unfinished to Palette.Yellow,
            toDo to Palette.Blue,
            done to Palette.Green,
            cancelled to Palette.Red
        )
    )
}

fun FlowContent.buildChart(data: String) {
    div("is-flex is-justify-content-center") {
        div {
            style = "width: 200px; height: 200px"
            uiComponent(
                "chart-donut",
                "size" to "200",
                "data" to data
            )
        }
    }
}

fun asData(vararg data: Pair<Number, Palette>) =
    data.joinToString(prefix = "[", postfix = "]") { """[${it.first}, "${it.second}"]""" }

fun TD.buildLegend(label: String, color: Palette) {
    /*div("is-flex is-align-items-center") {
        div {
            style = """margin-right: 8px; width: 30px; height: 15px; background-color: ${color}"""

        }
        div {
            +label
        }
    }*/
    //div("is-flex is-align-items-center is-flex-direction-column") {
    div {
        style = """
                text-decoration-line: underline;
                text-decoration-style: solid;
                text-decoration-thickness: 6px;
                text-underline-offset: 6px;
                text-decoration-color: $color;
            """.trimIndent()
        +label
    }
    /*div {
        style = """margin-right: 8px; width: 100%; height: 4px; background-color: ${color}"""

    }*/
    //}
}

fun GeneralStatsReport.Valid.durationInDays(): Double {
    return from.until(to, ChronoUnit.DAYS).toDouble()
}