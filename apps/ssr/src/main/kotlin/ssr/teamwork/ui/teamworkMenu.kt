package ssr.teamwork.ui

import io.ktor.server.application.*
import io.ktor.server.resources.*
import ssr.teamwork.TeamworkResource
import ssr.ui.NavigationBuilder

fun Application.teamworkMenu(): NavigationBuilder.() -> Unit = {
    "Teamwork" {
        "General Statistics" to href(TeamworkResource.Report.GeneralStats(params.settings.project.toString()))
        "Flow Report" to href(TeamworkResource.Report.Flow(params.settings.project.toString()))
        //"General Statistics" to resolver.urlTo<GeneralStatsReportDTO>(params.settings.project)
        //"Flow Report" to resolver.urlTo<FlowReportDTO>(params.settings.project)
    }
    "Settings" {
        weight = 999
        "Status" to href(TeamworkResource.Configure.Status(params.settings.project.toString()))//resolver.urlTo<StatusDTO>(params.settings.project)
        "Board" to  href(TeamworkResource.Configure.Board(params.settings.project.toString()))
    }
}
