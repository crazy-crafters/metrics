package ssr.teamwork.ui.reports

import io.ktor.http.*
import io.ktor.server.html.*
import io.ktor.server.routing.*
import kotlinx.html.*
import domain.sharedkernel.model.DateTime
import ssr.Palette
import ssr.configuration.href
import ssr.teamwork.TeamworkResource
import ssr.teamwork.ui.pageWithMenuProjectHeader
import ssr.ui.base.inject
import ssr.ui.uiComponent
import domain.teamwork.report.api.GetReport
import domain.teamwork.report.model.ReviewReport
import domain.user.UserSettings
import java.time.Duration


suspend fun RoutingContext.displayFlowReportPage(settings: UserSettings, from: String?, to: String?) {
    val getReport: GetReport by inject()

    val dateFrom = from?.let { DateTime.parse(it) } ?: DateTime.now().plus(Duration.ofDays(-365))
    val dateTo = to?.let { DateTime.parse(it) } ?: DateTime.now()

    call.respondHtml(
        HttpStatusCode.OK,
        flowReportPage(
            settings,
            getReport.flow(settings.project.remap(), dateFrom, dateTo),
            getReport.rework(settings.project.remap(), dateFrom, dateTo)
        )
    )
}

val RoutingContext.flowReportPage: (
    settings: UserSettings,
    flowReport: domain.teamwork.report.model.FlowReport,
    reworkReport: ReviewReport
) -> HTML.() -> Unit
    get() = { settings, flowReport, reworkReport ->
        pageWithMenuProjectHeader(settings) {
            uiComponent(
                "sprint-picker",
                "from" to "'${reworkReport.from}'",
                "to" to "'${reworkReport.to}'",
                "validateUrl" to "'${href(TeamworkResource.Report.Flow(reworkReport.project.value))}'"
            )

            displayFlowReport(flowReport)

            displayReviewReport(this, reworkReport)

            /*if (flowReport is FlowReport.Valid) {
                displayFlowReport(flowReport)
            }
            displayReviewReport(this, reworkReport)*/
        }
    }

fun DIV.displayFlowReport(flowReport: domain.teamwork.report.model.FlowReport) {
    when (flowReport) {
        is domain.teamwork.report.model.FlowReport.NoReport -> return
        is domain.teamwork.report.model.FlowReport.Valid -> {
            section("block mb-6") {
                h3("title is-3") { +"Flow Metrics" }
                table("table") {
                    tbody {
                        tr {
                            bordelessTd("has-text-weight-semibold") { +"Flow time" }
                            bordelessTd { +"${flowReport.leadTimes().inDays} days" }
                        }
                        tr {
                            bordelessTd("has-text-weight-semibold") { +"Efficiency" }
                            bordelessTd { +"${flowReport.efficiency().fractionOf100} %" }
                        }
                        tr {
                            bordelessTd("has-text-weight-semibold") { +"Cancel Time" }
                            bordelessTd { +"${flowReport.cancelLeadTimes().inDays} days" }
                        }
                    }
                }
            }
        }
    }
}

fun RoutingContext.displayReviewReport(parent: DIV, report: ReviewReport) {
    parent.apply {
        section("block mt-4") {
            h3("title is-3") { +"Rework" }
            when (report) {
                is ReviewReport.Valid -> {
                    buildChart(
                        asData(
                            report.withoutRework.count() to Palette.Yellow,
                            report.withRework.count() to Palette.Green
                        )
                    )
                    table("table report") {
                        tbody {
                            tr {
                                bordelessTd("has-text-weight-semibold") {
                                    buildLegend("Issues with rework", Palette.Yellow)
                                }
                                bordelessTd {
                                    +"${report.withRework.count()}"
                                }
                            }
                            tr {
                                bordelessTd("has-text-weight-semibold") {
                                    buildLegend("Issues without rework", Palette.Green)
                                }
                                bordelessTd {
                                    +"${report.withoutRework.count()}"
                                }
                            }
                        }
                    }
                }
                is ReviewReport.NoReport -> {}
            }
        }
    }
}

fun TR.bordelessTd(classes: String = "", f: TD.() -> Unit) {
    td(classes) {
        style = "border: none"
        f()
    }
}