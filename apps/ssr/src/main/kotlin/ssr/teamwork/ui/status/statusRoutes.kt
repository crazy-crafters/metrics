package ssr.teamwork.ui.status


import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*
import domain.sharedkernel.model.ID
import ssr.configuration.href
import ssr.configuration.privateGet
import ssr.configuration.privatePost
import ssr.teamwork.TeamworkResource
import ssr.ui.base.inject
import domain.teamwork.status.api.ManageStatus
import domain.user.HandleUserProfile
import domain.project.model.Project
import domain.user.UserProfile
import domain.user.UserSettings

fun Application.statusRoutes() {
    routing {

        privateGet<TeamworkResource.Configure.Status> { (_), user ->
            val getUserProfile: HandleUserProfile by inject()

            displayStatusPage((getUserProfile.get(user.id) as UserProfile.Found).settings)
        }

        privatePost<TeamworkResource.Configure.Status> { _, _ ->
            val statuses = call.receiveParameters().toMap().entries
                .map { (status, value) -> status to domain.teamwork.status.model.Status.State.valueOf(value.first()) }
            val project = call.parameters["project"]!!
            saveStatus(project, statuses.toMap())
        }
    }
}


private suspend fun RoutingContext.displayStatusPage(settings: UserSettings) {
    val workflowHandler: ManageStatus by inject()
    call.respondHtml(
        HttpStatusCode.OK,
        statusPage(
            settings,
            workflowHandler.statuses(settings.project.remap()),
            href(TeamworkResource.Configure.Status(settings.project.toString()))
        )
    )
}

suspend fun RoutingContext.saveStatus(project: String, statuses: Map<String, domain.teamwork.status.model.Status.State>) {
    val workflowHandler: ManageStatus by inject()
    val projectId = ID.from<Project>(project)
    statuses.forEach { (status, value) ->
        if (value != domain.teamwork.status.model.Status.State.Undefined)
            workflowHandler.update(projectId, ID.from(status), value)
    }

    call.respondRedirect(href(TeamworkResource.Configure.Status(project)))
}