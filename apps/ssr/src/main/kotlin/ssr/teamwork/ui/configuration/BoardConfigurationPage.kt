package ssr.teamwork.ui.configuration

import domain.project.model.Project
import io.ktor.server.routing.*
import kotlinx.html.*
import ssr.configuration.href
import ssr.teamwork.TeamworkResource
import ssr.teamwork.ui.pageWithMenuProjectHeader
import ssr.ui.base.inject
import domain.teamwork.configuration.model.BoardConfiguration
import infra.teamwork.UpdateStatus
import infra.teamwork.providers.UpdateStatusRepository
import domain.user.UserSettings


val RoutingContext.boardConfigurationPage: (
    settings: UserSettings, project: Project, configuration: BoardConfiguration.Valid
) -> HTML.() -> Unit
    get() = { settings, project, configuration ->
        val manageStatus: UpdateStatusRepository by inject()

        val updateStatus = manageStatus.get(project.id.remap())
        pageWithMenuProjectHeader(settings, project, refresh=(updateStatus is UpdateStatus.Pending)) {
            form {
                action = href(TeamworkResource.Update(project.id.toString()))
                method = FormMethod.post
                input(type = InputType.hidden, name = "project") { id="project"; value=project.id.toString() }
                button(
                    type = ButtonType.submit
                ) {
                    if (updateStatus is UpdateStatus.Pending) {
                        classes = setOf("button", "is-dark", "is-loading")
                    } else {
                        classes = setOf("button", "is-dark")
                    }
                    id = "update-board-button"
                    span { +"Update" }
                }
            }

            form(action = href(TeamworkResource.Configure.Board(project.id.toString())), method = FormMethod.post) {
                div("is-flex is-flex-direction-column") {
                    div("field") {
                        label("label") {
                            +"Type of board"
                        }
                        div("select") {
                            select {
                                name = "type"
                                option {
                                    value = "Azuredevops"
                                    selected = configuration.type.toString() == "Azure Devops"
                                    +"Azure Devops"
                                }
                                option {
                                    value = "Jira"
                                    selected = configuration.type.toString() == "Jira"
                                    +"Jira"
                                }
                                option {
                                    value = "Trello"
                                    selected = configuration.type.toString() == "Trello"
                                    +"Trello"
                                }
                            }
                        }
                    }

                    div("field") {
                        label("label") {
                            +"URL"
                        }
                        div("control") {
                            input(InputType.text, classes = "input") {
                                placeholder = "http://example.com"
                                value = configuration.url
                                id = "url"
                                name = "url"
                                required = true
                            }
                        }
                    }

                    div("field") {
                        label("label") {
                            +"Board"
                        }
                        div("control") {
                            input(InputType.text, classes = "input") {
                                placeholder = "1234"
                                value = configuration.board
                                id = "board"
                                name = "board"
                                required = true
                            }
                        }
                    }

                    div("field") {
                        label("label") {
                            +"Username"
                        }
                        div("control") {
                            input(InputType.text, classes = "input") {
                                placeholder = "user"
                                value = configuration.login.user
                                id = "username"
                                name = "username"
                                required = true
                            }
                        }
                    }

                    div("field") {
                        label("label") {
                            +"Token"
                        }
                        div("control") {
                            passwordInput(classes = "input") {
                                id = "token"
                                value = configuration.login.pass
                                name = "token"
                                required = true
                            }
                        }
                    }
                    div("is-flex is-justify-content-space-around") {
                        style = "width: 25rem"
                        button(classes = "button is-dark", type = ButtonType.submit) { +"Save" }
                        a(
                            classes = "button is-outlined is-dark",
                        ) {
                            href = ""
                            +"Cancel"
                        }
                    }
                }
            }
        }
    }