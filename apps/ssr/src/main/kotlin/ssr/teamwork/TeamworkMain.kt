package ssr.teamwork

import io.ktor.server.application.*
import io.ktor.server.netty.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.dsl.module
import infra.project.projectModule
import ssr.configuration.href
import ssr.configuration.privateGet
import ssr.ssrModule
import ssr.teamwork.ui.teamworkMenu
import ssr.ui.NavigationBuilder
import infra.teamwork.teamworkModule
import infra.user.userModule
import java.nio.file.Path


val teamworkSSRModule = module {
    val path = (System.getProperty("LOCAL_STORAGE") ?: System.getenv("LOCAL_STORAGE") ?: "").let {
        if (it.isBlank())
            Path.of(System.getProperty("user.home")).resolve(".blackbox")
        else
            Path.of(it)
    }

    includes(
        teamworkModule(path),
        projectModule(),
        userModule(path)
    )
}


fun Application.module() {
    ssrModule(
        teamworkSSRModule,
        module {
            single<NavigationBuilder> { params ->
                val menu = NavigationBuilder(params[0])
                teamworkMenu()(menu)
                menu
            }
        }
    )


    routing {
        privateGet("/") {user ->
            call.respondRedirect(href(TeamworkResource.Report.GeneralStats(user.userSettings.project.toString())))
        }
        teamworkRoutes()
    }
}


fun main(args: Array<String>): Unit = EngineMain.main(args)



