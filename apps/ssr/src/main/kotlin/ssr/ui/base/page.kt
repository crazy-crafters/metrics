package ssr.ui.base


import io.ktor.server.request.*
import io.ktor.server.routing.*
import ssr.takeMessage
import ssr.ui.uiComponent
import kotlinx.html.*
import org.koin.core.parameter.parametersOf
import org.koin.java.KoinJavaComponent
import ssr.ui.Navigation

fun buildNavigation(currentURI: String, f: Navigation.() -> Unit): Navigation {
    val navigation = Navigation()
    navigation.f()
    navigation.current(currentURI)
    return navigation
}

fun RoutingContext.page(includeHeader: HEAD.() -> Unit = {}, bodyF: DIV.() -> Unit): HTML.() -> Unit
= page({}, includeHeader, bodyF)

fun RoutingContext.page(withMenu: Navigation.() -> Unit, includeHeader: HEAD.() -> Unit = {}, bodyF: DIV.() -> Unit): HTML.() -> Unit {
        return {
            val navigation = buildNavigation(call.request.uri, withMenu)
            val message = call.takeMessage()
            pageStructure("root-content", includeHeader) {
                div("container is-fullwidth") {
                    div {
                        aside("menu menu-left") {
                            navigation.toHtml()()
                        }
                    }
                    div("page-content") {
                        div("container is-widescreen") {
                            bodyF()

                            if (message != Message.None) {
                                uiComponent(
                                    "message-toast",
                                    "message" to "'${message.content}'",
                                    "level" to "'${message.level}'"
                                )
                            }
                        }
                    }
                }
            }
        }
    }


inline fun <reified T> inject() = KoinJavaComponent.inject<T>(T::class.java)
inline fun <reified T> inject(vararg params: Any) = KoinJavaComponent.inject<T>(T::class.java, parameters = { parametersOf(*params) })