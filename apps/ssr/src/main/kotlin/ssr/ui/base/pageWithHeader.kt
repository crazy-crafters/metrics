package ssr.ui.base

import io.ktor.server.routing.*
import kotlinx.html.*
import ssr.takeMessage
import ssr.ui.Header
import ssr.ui.uiComponent

fun RoutingContext.pageWithHeader(
    header: Header,
    refresh: Boolean = false,
    includeHeader: HEAD.() -> Unit = {},
    bodyF: DIV.() -> Unit
): HTML.() -> Unit {
    return {
        val (title, subtitle) = header
        val message = call.takeMessage()
        attributes["data-theme"] = "light"
        head {
            includeHeader()
            link {
                rel = "stylesheet"; href =
                "https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0"
            }
            link { rel = "stylesheet"; href = "/static/global.css" }
            link { rel = "stylesheet"; href = "/static/components/ui-components.css" }
            link { rel = "stylesheet"; href = "/static/components/bulma-calendar.css" }
            link { rel = "stylesheet"; href = "https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css" }
            script(type = "text/javascript", src = "/static/components/bulma-calendar.js") {}
            script(type = "text/javascript", src = "/static/js/modal.js") {}
            script(type = "module", src = "/static/components/ui-components.esm.js") { }

            if (refresh) {
                meta {
                    httpEquiv="Refresh"
                    content="5"
                }
                //<meta http-equiv="refresh" content="5"; URL=." />

            }
        }
        body {
            div("is-flex is-flex-direction-column") {
                style = "min-height: 100vh"
                section("hero is-small header") {
                    div("hero-body ml-6") {
                        p("title has-text-grey-light") { +title }
                        p("subtitle has-text-grey") { +subtitle }
                    }
                }
                div("root-content") {
                    bodyF()

                    if (message != Message.None) {
                        uiComponent(
                            "message-toast",
                            "message" to "'${message.content.replace("'", "\\'")}'",
                            "level" to "'${message.level}'"
                        )
                    }
                }
            }
        }
    }
}