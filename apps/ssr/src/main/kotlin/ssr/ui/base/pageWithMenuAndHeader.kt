package ssr.ui.base

import io.ktor.server.routing.*
import kotlinx.html.*
import ssr.ui.NavigationBuilder
import ssr.ui.NavigationParameters

/*fun RoutingContext.pageWithMenuAndHeader(
    params: NavigationParameters,
    includeHeader: HEAD.() -> Unit = {},
    bodyF: DIV.() -> Unit
): HTML.() -> Unit {
    return {
        val navigation: NavigationBuilder by inject(params)
        val (title, subtitle) = params.header
        val message = call.takeMessage()
        attributes["data-theme"] = "light"
        head {
            includeHeader()
            link {
                rel = "stylesheet"; href =
                "https://fonts.googleapis.com/css2?family=Material+Symbols+Outlined:opsz,wght,FILL,GRAD@24,400,0,0"
            }
            link { rel = "stylesheet"; href = "/static/global.css" }
            link { rel = "stylesheet"; href = "/static/components/ui-components.css" }
            link { rel = "stylesheet"; href = "/static/components/bulma-calendar.css" }
            link { rel = "stylesheet"; href = "https://cdn.jsdelivr.net/npm/charts.css/dist/charts.min.css" }
            script(type = "text/javascript", src = "/static/components/bulma-calendar.js") {}
            script(type = "text/javascript", src = "/static/js/modal.js") {}
            script(type = "module", src = "/static/components/ui-components.esm.js") { }
        }
        body {
            div("is-flex is-flex-direction-column") {
                style = "height: 100vh"
                section("hero is-small header") {
                    div("hero-body ml-6") {
                        p("title has-text-grey-light") { +title }
                        p("subtitle has-text-grey") { +subtitle }
                    }
                }
                div("root-content") {
                    div("columns") {
                        style = "height: 100%"
                        div("column is-narrow") {
                            aside("menu") {
                                navigation.toHtml()()
                            }
                        }
                        div("column") {
                            div("container is-widescreen page-content p-6") {
                                bodyF()

                                if (message != Message.None) {
                                    uiComponent(
                                        "message-toast",
                                        "message" to "'${message.content}'",
                                        "level" to "'${message.level}'"
                                    )
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}*/

fun RoutingContext.pageWithMenuAndHeader(
    params: NavigationParameters,
    refresh: Boolean = false,
    includeHeader: HEAD.() -> Unit = {},
    bodyF: DIV.() -> Unit
): HTML.() -> Unit {
    val navigation: NavigationBuilder by inject(params)
    return pageWithHeader(params.header, refresh, includeHeader) {
            div("columns") {
                style = "height: 100%"
                div("column is-narrow") {
                    aside("menu") {
                        navigation.toHtml()()
                    }
                }
                div("column") {
                    div("container is-widescreen page-content p-6") {
                        bodyF()
                    }
                }
            }
        }
}