package ssr.ui.base

import kotlinx.serialization.Serializable

@Serializable
data class Message(val level: Level, val content: String) {
    enum class Level {
        Success,
        Warning,
        Danger,
        None
    }

    companion object {
        val None = Message(Level.None, "")
    }
}