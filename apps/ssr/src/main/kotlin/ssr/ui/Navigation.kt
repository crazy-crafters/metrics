package ssr.ui

import kotlinx.html.*
import domain.user.UserSettings


class NavigationItem(val name: String, val url: String, var active: Boolean) {
    fun convert(): UL.() -> Unit = {
        li {
            a(classes = "") {
                +"Composition"
            }
        }
    }
}

class NavigationCategory(val name: String, var weight: Int = 0) {
    val items = mutableListOf<NavigationItem>()
    infix fun String.to(url: String) {
        if (url.isBlank())
            return
        items.add(
            NavigationItem(this, url, false)
        )
    }

    fun activate(url: String) {
        items.forEach {
            it.active = it.url == url
        }
    }
}

class Navigation {
    private var backUrl = ""
    val items = mutableListOf<NavigationCategory>()

    fun current(url: String): Navigation {
        items.forEach { it.activate(url) }
        return this
    }

    infix fun String.to(url: String) {
        if (url.isBlank())
            return
        val navigationCategory = NavigationCategory("")
        navigationCategory.items.add(
            NavigationItem(this, url, false)
        )
        items.add(navigationCategory)
    }

    operator fun String.invoke(f: NavigationCategory.() -> Unit) {
        val navigationCategory = NavigationCategory(this)
        navigationCategory.f()
        items.add(navigationCategory)
    }

    fun toHtml(): ASIDE.() -> Unit = {
        div {
            style = "height: 80px;"
            if (backUrl.isNotBlank()) {
                a(
                    classes = "is-white",
                    href = backUrl
                ) {
                    style = "padding-left: 32px;"
                    span("material-symbols-outlined has-text-white") {
                        style = "margin-bottom: 64px; font-size: 72px;"
                        +"arrow_back"
                    }
                }
            }
        }
        items.forEach { category ->
            if (category.name == "-") {
                p("menu-label") {
                }
            } else if (category.name.isNotBlank()) {
                p("menu-label has-text-grey-light") {
                    +category.name
                }
            }


            ul("menu-list" + if (category.name.isNotBlank()) " sub-menu" else "") {
                category.items.forEach { item ->
                    li {
                        a(classes = if (item.active) "is-active" else "", href = item.url) {
                            +item.name

                        }
                    }
                }
            }
        }
    }
}

data class Header(val title: String, val subtitle: String) {
    companion object {
        val default = Header("", "")
    }
}

data class NavigationParameters(
    val currentUri: String,
    val settings: UserSettings,
    val header: Header = Header.default
)

class NavigationBuilder(val params: NavigationParameters) {
    val items = mutableListOf<NavigationCategory>()

    infix fun String.to(url: String) {
        if (url.isBlank())
            return
        val navigationCategory = NavigationCategory("")
        navigationCategory.items.add(
            NavigationItem(this, url, false)
        )
        items.add(navigationCategory)
    }

    operator fun String.invoke(f: NavigationCategory.() -> Unit) {
        val navigationCategory = NavigationCategory(this)
        navigationCategory.f()
        val existing = items.find { it.name == navigationCategory.name }
        if (existing != null) {
            existing.items.addAll(navigationCategory.items)
        } else {
            items.add(navigationCategory)
        }
        items.forEach { it.activate(params.currentUri) }
        items.sortBy { it.weight }
    }

    fun toHtml(): ASIDE.() -> Unit = {
        div("pt-6 pl-4") {
            items.forEach { category ->
                div("pt-4") {
                    if (category.name == "-") {
                        p("mb-0 menu-label") {
                        }
                    } else if (category.name.isNotBlank()) {
                        p("mb-0 menu-label has-text-grey") {
                            +category.name
                        }
                    }


                    ul("menu-list" + if (category.name.isNotBlank()) " sub-menu" else "") {
                        category.items.forEach { item ->
                            li {
                                a(classes = if (item.active) "is-active" else "", href = item.url) {
                                    +item.name

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}


