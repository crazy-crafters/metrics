package ssr.ui

import kotlinx.html.*
import kotlin.random.Random
import kotlin.random.nextUInt

fun HTMLTag.uiComponentScripted(name: String, vararg data: Pair<String, String>, script: (id: UInt) -> String = {""}) {
    val id = Random.nextUInt()
    unsafe {
        +"""
            <$name id='$id'></$name>
            <script>
            ${
                data.joinToString("\n") { (key, value) -> "document.getElementById('$id').$key = $value;" }
            }
            ${script(id)}
            </script>
        """.trimIndent()
    }
}

fun HTMLTag.uiComponent(name: String, vararg data: Pair<String, String>) {
    val id = Random.nextUInt()
    unsafe {
        +"""
            <$name id='$id'></$name>
            <script>
            ${
            data.joinToString("\n") { (key, value) -> "document.getElementById('$id').$key = $value;" }
        }
            </script>
        """.trimIndent()
    }
}

fun HTMLTag.uiComponent(name: String, vararg data: Pair<String, String>, slot: HTMLTag.() -> Unit) {
    val id = Random.nextUInt()
    unsafe {
        +"""<$name id='$id'>"""
    }

    slot()

    unsafe{
        +"""</$name>
            <script>
            ${
            data.joinToString("\n") { (key, value) -> "document.getElementById('$id').$key = $value;" }
        }
            </script>
        """.trimIndent()
    }
}


fun DIV.slot(name: String, classes: String = "", content: DIV.() -> Unit) {
    div(classes) {
        attributes["slot"] = name
        content()
    }
}