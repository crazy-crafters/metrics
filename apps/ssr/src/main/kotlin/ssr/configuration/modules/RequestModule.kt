package ssr.configuration.modules

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.sessions.*
import org.koin.core.module.Module
import org.koin.dsl.module
import ssr.UserCookie
import ssr.configuration.UserSession
import ssr.configuration.security

data class RuntimeEnvironment(val withSecurity: Boolean)

val Application.requestModule: Module
    get() = module {
        val httpClient = HttpClient(CIO) {
            install(ContentNegotiation) {
                json()
            }
        }

        val profile = environment.config.propertyOrNull("profile")?.getString() ?: ""
        val withSecurity = !profile.equals("no-auth", ignoreCase = true)

        install(Sessions) {
            cookie<UserSession>("user_session")
            cookie<UserCookie>("user_cookie")
        }

        single<RuntimeEnvironment> {
            RuntimeEnvironment(withSecurity=withSecurity)
        }

        if (withSecurity) {
            security(httpClient)
        } else {
        }
    }