package ssr.configuration

import io.ktor.http.*

class HttpGetError(val code: HttpStatusCode): RuntimeException()