package ssr.configuration

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.resources.post
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import kotlinx.html.a
import kotlinx.html.body
import kotlinx.html.p
import domain.sharedkernel.model.ID
import ssr.configuration.modules.RuntimeEnvironment
import ssr.ui.base.inject
import domain.user.HandleUserProfile
import domain.user.UserProfile
import domain.user.User
import kotlin.collections.set

val applicationHttpClient = HttpClient(CIO) {
    install(ContentNegotiation) {
        json()
    }
}

fun Application.security(httpClient: HttpClient = applicationHttpClient) {

    val clientId = environment.config.property("google.client-id").getString()
    val clientSecret = environment.config.property("google.client-secret").getString()
    val scopes = environment.config.property("google.scopes").getList()
    val authorizeUrl = environment.config.property("google.authorize-url").getString()
    val accessTokenUrl = environment.config.property("google.access-token-url").getString()

    val redirects = mutableMapOf<String, String>()
    install(Authentication) {
        oauth("auth-oauth-google") {
            // Configure oauth authentication
            urlProvider = { "http://localhost:8080/callback" }
            providerLookup = {
                OAuthServerSettings.OAuth2ServerSettings(
                    name = "google",
                    authorizeUrl = authorizeUrl,
                    accessTokenUrl = accessTokenUrl,
                    requestMethod = HttpMethod.Post,
                    clientId = clientId,
                    clientSecret = clientSecret,
                    defaultScopes = scopes,
                    extraAuthParameters = listOf("access_type" to "offline"),
                    onStateCreated = { call, state ->
                        //saves new state with redirect url value
                        call.request.queryParameters["redirectUrl"]?.let {
                            redirects[state] = it
                        }
                    }
                )
            }
            client = httpClient
        }
    }
    routing {
        authenticate("auth-oauth-google") {
            get("/login") {
                // Redirects to 'authorizeUrl' automatically
            }

            get("/callback") {
                val currentPrincipal: OAuthAccessTokenResponse.OAuth2? = call.principal()
                currentPrincipal?.let { principal ->
                    principal.state?.let { state ->
                        call.sessions.set(UserSession(state, principal.accessToken))
                        redirects[state]?.let { redirect ->
                            call.respondRedirect(redirect)
                            return@get
                        }
                    }
                }
                call.respondRedirect("/home")
            }
        }
        get("/") {
            call.respondHtml {
                body {
                    p {
                        a("/login") { +"Login with Google" }
                    }
                }
            }
        }
    }
}

/*
fun Route.with_auth(method: HttpMethod, httpClient: HttpClient, path: String,
                    body: suspend RoutingContext.(CurrentUser) -> Unit
): Route {
    return route(path, method) {
        handle {
            val redirectUrl = URLBuilder("http://localhost:8080/login").run {
                parameters.append("redirectUrl", call.request.uri)
                build()
            }

            val session = getSession(call)
            if (session != null) {
                try {
                    val userInfo: CurrentUser = fetchUserInfo(httpClient, session)
                    body(userInfo)
                } catch (e: HttpGetError) {
                    call.respondRedirect(redirectUrl)
                }
            } else {
                call.respondRedirect(redirectUrl)
            }
        }
    }
}

fun Route.without_auth(method: HttpMethod, httpClient: HttpClient, path: String,
                       body: suspend RoutingContext.(CurrentUser) -> Unit
): Route {
    return route(path, method) {
        handle {
            val userInfo = CurrentUser(
                id = System.getProperty("user.name"),
                name = System.getProperty("user.name")
            )
            body(userInfo)
        }
    }
}*/


fun Route.privateGet(url: String, body: suspend RoutingContext.(CurrentUser) -> Unit): Route {
    return get(url) {
        val environment: RuntimeEnvironment by inject()
        if (environment.withSecurity)
            protected(body)
        else
            open(body)
    }
}


inline fun <reified T : Any> Route.privateGet(noinline body: suspend RoutingContext.(T, CurrentUser) -> Unit): Route {
    return get<T> { u: T ->
        val environment: RuntimeEnvironment by inject()
        if (environment.withSecurity)
            protected<T>(body, u)
        else
            open(body, u)
    }
}

inline fun <reified T : Any> Route.privatePost(noinline body: suspend RoutingContext.(T, CurrentUser) -> Unit): Route {
    return post<T> { u: T ->
        val environment: RuntimeEnvironment by inject()
        if (environment.withSecurity)
            protected<T>(body, u)
        else
            open(body, u)
    }
}

suspend inline fun <reified T : Any> RoutingContext.open(
    body: suspend RoutingContext.(T, CurrentUser) -> Unit,
    resource: T
) {
    val userController: HandleUserProfile by inject()

    val id = ID.from<User>(System.getProperty("user.name"))
    val name = System.getProperty("user.name")
    val userProfile = when (val it = userController.get(id)) {
        is UserProfile.Found -> it.settings
        UserProfile.NotFound -> throw RuntimeException("No profile")
    }
    body(resource, CurrentUser(id = id, name = name, userSettings = userProfile))
}

suspend inline fun <reified T : Any> RoutingContext.protected(
    body: suspend RoutingContext.(T, CurrentUser) -> Unit,
    resource: T
) {
    val redirectUrl = URLBuilder("http://localhost:8080/login").run {
        parameters.append("redirectUrl", call.request.uri)
        build()
    }

    val session = getSession(call)
    if (session != null) {
        try {
            val engine: HttpClientEngine by inject()

            val userController: HandleUserProfile by inject()
            val userInfo: UserInfo = fetchUserInfo(HttpClient(engine), session)
            val userProfile = when (val it = userController.get(ID.from(userInfo.id))) {
                is UserProfile.Found -> it.settings
                UserProfile.NotFound -> throw RuntimeException("No profile")
            }
            body(resource, CurrentUser(id = ID.from(userInfo.id), name = userInfo.name, userSettings = userProfile))
        } catch (e: HttpGetError) {
            call.respondRedirect(redirectUrl)
        }
    } else {
        call.respondRedirect(redirectUrl)
    }
}

suspend fun RoutingContext.open(body: suspend RoutingContext.(CurrentUser) -> Unit) {
    val userController: HandleUserProfile by inject()
    val id = ID.from<User>(System.getProperty("user.name"))
    val name = System.getProperty("user.name")
    val userProfile = when (val it = userController.get(id)) {
        is UserProfile.Found -> it.settings
        UserProfile.NotFound -> throw RuntimeException("No profile")
    }
    body(CurrentUser(id = id, name = name, userSettings = userProfile))
}

suspend fun RoutingContext.protected(body: suspend RoutingContext.(CurrentUser) -> Unit) {
    val redirectUrl = URLBuilder("http://localhost:8080/login").run {
        parameters.append("redirectUrl", call.request.uri)
        build()
    }

    val session = getSession(call)
    if (session != null) {
        try {
            val engine: HttpClientEngine by inject()
            val userController: HandleUserProfile by inject()
            val userInfo: UserInfo = fetchUserInfo(HttpClient(engine), session)
            val userProfile = when (val it = userController.get(ID.from(userInfo.id))) {
                is UserProfile.Found -> it.settings
                UserProfile.NotFound -> throw RuntimeException("No profile")
            }
            body(CurrentUser(id = ID.from(userInfo.id), name = userInfo.name, userSettings = userProfile))
        } catch (e: HttpGetError) {
            call.respondRedirect(redirectUrl)
        }
    } else {
        call.respondRedirect(redirectUrl)
    }
}

inline fun <reified T : Any> RoutingContext.href(resource: T) = call.application.href(resource)
