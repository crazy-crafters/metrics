package ssr.configuration

import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import domain.sharedkernel.model.ID
import domain.user.User
import domain.user.UserSettings

suspend fun fetchUserInfo(
    httpClient: HttpClient,
    userSession: UserSession
): UserInfo {
    val response = httpClient.get("https://www.googleapis.com/oauth2/v2/userinfo") {
        headers {
            append(HttpHeaders.Authorization, "Bearer ${userSession.token}")
        }
    }
    if (response.status != HttpStatusCode.OK)
        throw HttpGetError(response.status)

    val content = response.bodyAsText()
    return Json.decodeFromString<UserInfo>(content)
}

suspend fun getSession(
    call: ApplicationCall
): UserSession? {
    val userSession: UserSession? = call.sessions.get()
    if (userSession == null) {
        val redirectUrl = URLBuilder("http://localhost:8080/login").run {
            parameters.append("redirectUrl", call.request.uri)
            build()
        }
        call.respondRedirect(redirectUrl)
        return null
    }
    return userSession
}

@Serializable
data class UserSession(val state: String, val token: String)

data class CurrentUser(
    val id: ID<User>,
    val name: String,
    val userSettings: UserSettings
)

@Serializable
data class UserInfo(
    val id: String,
    val name: String,
)