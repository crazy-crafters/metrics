package ssr

enum class Palette(val value: String) {
    Red("#E63946"),
    Yellow("#ffbf34"),
    //Green("#2A9D8F"),
    Green("#73C167"),
    //Blue("#0094a2"),
    Blue("#007EA7"),
    Grey("#232531"),
    Disabled("#D8D2E0")
    /*GreenLight("#A8E6CF"),
    RedLight("#F4A6A8"),
    BlueLight("#84D8F0"),
    GreyLight("#D8D2E0"),*/

    ;
    //green("#12a754"),
    //red("#A2221C"),

    override fun toString() = value
}