package ssr.profile.ui


import io.ktor.server.application.*
import ssr.profile.UserResource
import ssr.ui.NavigationBuilder
import io.ktor.server.resources.href

fun Application.userMenu(): NavigationBuilder.() -> Unit = {
    "Settings" {
        weight = 999
        "Profile" to href(UserResource.Profile())
    }
}
