package ssr.profile.ui

import domain.project.model.Project
import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlinx.html.*
import domain.sharedkernel.model.ID
import ssr.ui.NavigationParameters
import ssr.ui.base.pageWithMenuAndHeader
import domain.user.UserSettings


val RoutingContext.profilePage: (settings: UserSettings, projects: List<Project>, url: String) -> HTML.() -> Unit
    get() = { settings, projects, url ->
        pageWithMenuAndHeader(NavigationParameters(call.request.uri, settings)) {
            h3("title is-3") {
                +"Profile"
            }
            div("section") {
                form(action = url, method = FormMethod.post) {
                    div("is-flex is-flex-direction-column") {
                        button(classes = "button is-dark ml-auto", type = ButtonType.submit) { +"Save" }
                    }

                    div("field") {
                        label("label") { +"Project" }
                        div("select control") {
                            select("select") {
                                name = "project"
                                option {
                                    value = ""
                                    selected = settings.project == ID.invalid<Project>()
                                    +""
                                }
                                projects.forEach {
                                    option {
                                        value = it.id.toString()
                                        selected = it.id == settings.project
                                        +it.name.toString()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }