package ssr.profile


import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import domain.project.api.GetProject
import domain.sharedkernel.model.ID
import ssr.configuration.privateGet
import ssr.configuration.privatePost
import ssr.profile.ui.profilePage
import ssr.ui.base.inject
import domain.user.HandleUserProfile
import domain.user.UserSettings

@Resource("/user")
class UserResource {
    @Resource("profile")
    class Profile(val parent: UserResource = UserResource()) {
    }
}

fun Application.profileRoutes() {
    routing {
        privateGet<UserResource.Profile> { _, user ->
            val getProject: GetProject by inject()

            val projects = getProject.all()
            call.respondHtml(
                HttpStatusCode.OK,
                profilePage(user.userSettings, projects, href(UserResource.Profile(UserResource())))
            )
        }

        privatePost<UserResource.Profile> { _, userInfo ->
            val handleUserProfile: HandleUserProfile by inject()
            val project = call.receiveParameters()["project"]!!
            val userId = userInfo.id
            val settings = UserSettings(user = userId, project = ID.from(project))

            handleUserProfile.save(settings)
            call.respondRedirect(href(UserResource.Profile(UserResource())))
        }
    }
}