package ssr.profile

import org.koin.dsl.module
import infra.project.projectModule
import infra.user.userModule
import java.nio.file.Path

val profileSSRModule = module {
    val path = (System.getProperty("LOCAL_STORAGE") ?: System.getenv("LOCAL_STORAGE") ?: "").let {
        if (it.isBlank())
            Path.of(System.getProperty("user.home")).resolve(".blackbox")
        else
            Path.of(it)
    }

    includes(
        userModule(path),
        projectModule(path),
    )
}