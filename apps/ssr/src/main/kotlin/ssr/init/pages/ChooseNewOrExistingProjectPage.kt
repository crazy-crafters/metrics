package ssr.init.pages

import domain.project.model.ProjectResult
import io.ktor.server.routing.*
import kotlinx.html.*
import ssr.configuration.href
import ssr.init.InitResource
import ssr.ui.Header
import ssr.ui.base.pageWithHeader
import domain.user.UserSettings

fun RoutingContext.initPageTemplate(
    header: Header,
    bodyF: DIV.() -> Unit
): HTML.() -> Unit {
    return pageWithHeader(header) {
        div("container is-widescreen page-content p-0") {
            div("is-flex is-align-items-center is-justify-content-center") {
                style = "height: 100%"
                bodyF()
            }
        }
    }
}

val RoutingContext.chooseNewOrExistingProjectPage: (settings: UserSettings, validateUrl: String) -> HTML.() -> Unit
    get() = { settings, validateUrl ->
        initPageTemplate(Header("Initialization", "Configure your project")) {
            div("is-flex is-align-items-center is-flex-direction-column") {
                h5("title is-5") {
                    +"Do you want to create a new project?"
                }
                div("is-flex is-justify-content-space-around") {
                    style = "width: 25rem"
                    a(
                        classes = "button is-outlined is-dark",
                    ) {
                        href = href(InitResource.Project())
                        +"Yes"
                    }
                    a(
                        classes = "button is-outlined is-dark",
                    ) {
                        href = "/"
                        +"No"
                    }
                }
            }
        }
    }


val RoutingContext.chooseConfigureBoardPage: (project: ProjectResult) -> HTML.() -> Unit
    get() = { project ->
        initPageTemplate(Header("Initialization", "Configure your project")) {
            div("is-flex is-align-items-center is-flex-direction-column") {
                h5("title is-5") {
                    +"Do you want to configure a teamwork board for?"
                }
                div("is-flex is-justify-content-space-around") {
                    style = "width: 25rem"
                    a(
                        classes = "button is-outlined is-dark",
                    ) {
                        href = href(InitResource.Teamwork())
                        +"Yes"
                    }
                    a(
                        classes = "button is-outlined is-dark",
                    ) {
                        href = "/"
                        +"No"
                    }
                }
            }
        }
    }