package ssr.init.pages

import io.ktor.server.routing.*
import kotlinx.html.*
import ssr.configuration.href
import ssr.ui.Header
import ssr.init.InitResource

val RoutingContext.createNewProjectPage: () -> HTML.() -> Unit
    get() = {
        initPageTemplate(Header("Initialization", "Create a new project")) {
            form(action = href(InitResource.Project()), method = FormMethod.post) {
                div("is-flex is-align-items-center is-flex-direction-column") {
                    div("field") {
                        label("label") {
                            +"Name"
                        }
                        div("control") {
                            input(InputType.text, classes = "input") {
                                placeholder = "Project Name"
                                id = "name"
                                name = "name"
                                required = true
                            }
                        }
                    }

                    div("field") {
                        label("label") {
                            +"Description"
                        }
                        div("control") {
                            textArea(classes="input") {
                            //input(InputType.text, classes = "input") {
                                placeholder = "Short Description"
                                id = "description"
                                name = "description"
                                required = true
                            }
                        }
                    }

                    div("is-flex is-justify-content-space-around") {
                        style = "width: 25rem"
                        button(classes = "button is-dark", type = ButtonType.submit) { +"Save" }
                        a(
                            classes = "button is-outlined is-dark",
                        ) {
                            href = ""
                            +"Cancel"
                        }
                    }
                }
            }
        }
    }