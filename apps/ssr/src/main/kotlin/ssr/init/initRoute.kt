package ssr.init

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import domain.project.api.HandleProject
import domain.project.model.NewProjectCommand
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.Name
import ssr.configuration.privateGet
import ssr.configuration.privatePost
import ssr.init.pages.chooseConfigureBoardPage
import ssr.init.pages.chooseNewOrExistingProjectPage
import ssr.init.pages.createNewProjectPage
import ssr.ui.base.inject
import domain.user.HandleUserProfile
import domain.user.UserProfile

@Resource("/init")
class InitResource {
    @Resource("project")
    class Project

    @Resource("teamwork")
    class Teamwork
}

fun Application.initRoutes() {
    routing {
        privateGet<InitResource> { _, user ->
            val userController: HandleUserProfile by inject()
            displayChooseNewOrExistingProjectPage(userController.get(user.id))
        }

        privateGet<InitResource.Project> { _, userinfo ->
            displayCreateNewProject()
        }
        privatePost<InitResource.Project> { _, userinfo ->
            handleCreateNewProject()
        }
    }
}


private suspend fun RoutingContext.displayChooseNewOrExistingProjectPage(profile: UserProfile) {
    when (profile) {
        is UserProfile.Found -> call.respondHtml(
            HttpStatusCode.OK,
            chooseNewOrExistingProjectPage(profile.settings, "/init/project")
        )
        is UserProfile.NotFound -> call.respondRedirect("/")
    }

}

private suspend fun RoutingContext.displayCreateNewProject() {

    call.respondHtml(
        HttpStatusCode.OK,
        createNewProjectPage()
    )
}

private suspend fun RoutingContext.handleCreateNewProject() {
    val projectController: HandleProject by inject()
    val parameters = call.receiveParameters()
    val newProject = projectController.start(
        NewProjectCommand(
            name = Name.from(parameters["name"] ?: ""),
            description = parameters["description"] ?: "",
            startDate = DateTime.now()
        )
    )
    call.respondHtml(
        HttpStatusCode.OK,
        chooseConfigureBoardPage(newProject)
    )
}

