package ssr

import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.plugins.calllogging.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import org.koin.core.module.Module
import org.koin.ktor.plugin.Koin
import org.slf4j.event.Level
import ssr.configuration.modules.requestModule
import io.ktor.server.resources.*
import ssr.profile.profileRoutes
import ssr.profile.profileSSRModule

fun Application.ssrModule(vararg modules: Module) {
    log.info("Loading modules")
    install(Resources)
    install(CallLogging) {
        level = Level.INFO
        filter { call ->
            call.request.path().startsWith("/api/v1")
        }
        format { call ->
            val status = call.response.status()
            val httpMethod = call.request.httpMethod.value
            val userAgent = call.request.headers["User-Agent"]
            "Status: $status, HTTP method: $httpMethod, User agent: $userAgent"
        }
    }

    install(Koin) {
        modules(
            *modules,
            requestModule,
            profileSSRModule
        )
    }

    routing {
        staticResources("/static", "static")
    }
    profileRoutes()
}