package ssr

import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.netty.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.dsl.module
import ssr.configuration.href
import ssr.configuration.privateGet
import ssr.init.initRoutes
import ssr.profile.profileSSRModule
import ssr.profile.ui.userMenu
import ssr.project.projectMenu
import ssr.project.projectRoutes
import ssr.teamwork.TeamworkResource
import ssr.teamwork.teamworkRoutes
import ssr.teamwork.teamworkSSRModule
import ssr.teamwork.ui.teamworkMenu
import ssr.ui.NavigationBuilder

fun Application.module() {
    ssrModule(
        teamworkSSRModule,
        profileSSRModule,
        module {
            factory<NavigationBuilder> { params ->
                val menu = NavigationBuilder(params[0])
                userMenu()(menu)
                teamworkMenu()(menu)
                projectMenu()(menu)
                menu
            }
        }
    )

    routing {
        privateGet<HomePage> { _, user ->
            val url = href(TeamworkResource.Report.GeneralStats(user.userSettings.project.value))
            call.respondRedirect(url)
        }

        teamworkRoutes()
        //profileRoutes()
        projectRoutes()
        initRoutes()
    }
}

@Resource("/")
class HomePage


fun main(args: Array<String>): Unit = EngineMain.main(args)



