package ssr.project

import io.ktor.server.application.*
import io.ktor.server.resources.*
import ssr.project.survey.SurveyResource
import ssr.ui.NavigationBuilder

fun Application.projectMenu(): NavigationBuilder.() -> Unit = {
    "Surveys" {
        "Net Participants Score" to href(SurveyResource.NPS(params.settings.project.toString()))
    }
}
