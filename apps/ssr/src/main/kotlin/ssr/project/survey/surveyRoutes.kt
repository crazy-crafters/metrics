package ssr.project.survey


import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.html.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import domain.project.model.Project
import domain.survey.api.GetSurvey
import domain.survey.api.HandleSurvey
import domain.survey.model.NPSVote
import domain.survey.model.NewNPSSurvey
import domain.survey.model.Survey
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import ssr.configuration.privateGet
import ssr.configuration.privatePost
import ssr.project.survey.ui.npsSurveyPage
import ssr.project.survey.ui.userMetricsPage
import ssr.setMessage
import ssr.ui.base.Message
import ssr.ui.base.inject


@Resource("/survey/{project}")
class SurveyResource(val project: String) {
    @Resource("nps")
    class NPS(val parent: SurveyResource) {
        operator fun component1() = parent.project

        constructor(project: String): this(SurveyResource(project))

        @Resource("vote")
        class Vote(val parent: NPS) {
            constructor(project: String): this(NPS(project))
            operator fun component1() = parent.parent.project
        }

        @Resource("open")
        class Open(val parent: NPS) {
            constructor(project: String): this(NPS(project))
            operator fun component1() = parent.parent.project
        }

        @Resource("close")
        class Close(val parent: NPS) {
            constructor(project: String): this(NPS(project))
            operator fun component1() = parent.parent.project
        }
    }
}

fun Application.surveyRoutes() {
    routing {
        privateGet<SurveyResource.NPS> { (project), user ->
            val getSurvey: GetSurvey by inject()
            val nps = getSurvey.measured(ID.from(project), DateTime.now())
            when (nps) {
                is Survey.NPSSurvey -> call.respondHtml(
                    HttpStatusCode.OK,
                    userMetricsPage(
                        user.userSettings,
                        nps
                    )
                )
                else -> call.respondRedirect("/")
            }

        }

        privateGet<SurveyResource.NPS.Vote> { _, user ->
            val id = ID.from<Project>(call.parameters["id"]!!)
            call.respondHtml(HttpStatusCode.OK, npsSurveyPage(user.userSettings, id))
        }

        privatePost<SurveyResource.NPS.Vote> { (project), _ ->
            val handleSurvey : HandleSurvey by inject()

            val vote = call.receiveParameters()["vote"]!!.toInt()
            val command = NPSVote(
                project = ID.from(project),
                date = DateTime.now(),
                score = vote,
            )

            handleSurvey.registerVote(command)
            call.respondRedirect(href(SurveyResource.NPS(project)))
        }

        privatePost<SurveyResource.NPS.Open> { (project), user ->
            val handleSurvey : HandleSurvey by inject()

            /*surveyController.execute(
                OpenNPSSurveyCommandDTO(
                    project = call.parameters["id"]!!,
                    from = clock.now().format("YYYY-MM-dd")
                )
            )*/
            handleSurvey.openSurvey(
                NewNPSSurvey(
                    project = ID.from(call.parameters["id"]!!),
                    from = DateTime.now()
                )
            )

            call.setMessage(Message(Message.Level.Success, "The NPS survey is now open"))

            call.respondRedirect(href(SurveyResource.NPS(project)))
        }

        privatePost<SurveyResource.NPS.Close> { (project), user ->
            val handleSurvey : HandleSurvey by inject()

            handleSurvey.closeSurvey(
                project = ID.from(call.parameters["id"]!!),
                on = DateTime.now(),
            )

            call.setMessage(Message(Message.Level.Success, "The NPS survey is now closed"))
            call.respondRedirect(href(SurveyResource.NPS(project)))
        }
    }

}

