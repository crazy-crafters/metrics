package ssr.project.survey.ui

import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlinx.html.HTML
import kotlinx.html.div
import kotlinx.html.h3
import domain.survey.model.Survey
import ssr.ui.NavigationParameters
import ssr.ui.base.pageWithMenuAndHeader
import domain.user.UserSettings

val RoutingContext.userMetricsPage: (settings: UserSettings, nps: Survey.NPSSurvey?) -> HTML.() -> Unit
    get() = { settings, nps ->
        pageWithMenuAndHeader(NavigationParameters(call.request.uri, settings)) {
            h3("title is-3") {
                +"User Metrics"
            }
            div("section") {
                nps(this@div, settings.project.remap(), nps)
            }
        }
    }