package ssr.project.survey.ui


import io.ktor.server.request.*
import io.ktor.server.routing.*
import kotlinx.html.*
import domain.project.model.Project
import domain.sharedkernel.model.ID
import ssr.configuration.href
import ssr.project.survey.SurveyResource
import ssr.ui.NavigationParameters
import ssr.ui.base.pageWithMenuAndHeader
import domain.user.UserSettings

val RoutingContext.npsSurveyPage: (settings: UserSettings, project: ID<Project>) -> HTML.() -> Unit
    get() = { settings, project ->

        val link = href(SurveyResource.NPS.Vote(project.value))

        pageWithMenuAndHeader(NavigationParameters(call.request.uri, settings)) {
            style = "margin: auto"
            h2("title is-2") {
                style = "text-align: center; margin-bottom: 16vmin"
                +"How well does the product meets your needs"
            }
            div {
                style = "display: flex; column-gap: 1vh;"
                (0..10).forEach { index ->
                    form {
                        action = link
                        method = FormMethod.post
                        input(type = InputType.hidden, name = "vote") { id = "vote"; value = index.toString() }
                        button(classes = "button is-dark has-text-light", type = ButtonType.submit) {
                            val s = "12vmin"
                            val f = "6vmin"
                            style =
                                "font-size: $f; width: $s; height: $s;"
                            +"$index"
                        }
                    }
                }
            }
        }
    }

/*fun RoutingContext.npsSurveyPage(link: String): HTML.() -> Unit = {
    singlePage() {
        div {
            style="margin: auto"
            h2("title is-2") {
                style="text-align: center; margin-bottom: 16vmin"
                +"How well do the product meets your needs"
            }
            div {
                style="display:flex"
            }
            div {
                style = "display: flex; column-gap: 1vh;"
                (0..10).forEach { index ->
                    form {
                        action = link
                        method = FormMethod.post
                        input(type = InputType.hidden, name = "vote") { id="vote"; value=index.toString() }
                        button(classes = "button is-dark has-text-light", type = ButtonType.submit) {
                            val s = "12vmin"
                            val f = "6vmin"
                            style =
                                "font-size: $f; width: $s; height: $s;"
                            +"$index"
                        }
                    }
                }
            }
        }
    }
}*/