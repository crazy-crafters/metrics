package ssr.project.survey.ui

import io.ktor.server.routing.*
import kotlinx.html.*
import domain.project.model.Project
import domain.survey.model.Survey
import domain.sharedkernel.model.ID
import ssr.configuration.href
import ssr.project.survey.SurveyResource


fun RoutingContext.nps(parent: DIV, projectId: ID<Project>, nps: Survey.NPSSurvey?) {
    parent.apply {
        h2("title is-2") {
            +"Net Participant Score"
        }
        div("box") {
            if (nps?.scores == null || nps.scores.total == 0) {
                div("nps") {
                    style = "width: 100%; height: 300px; display: flex; justify-content: center; align-items: center"
                    h5("title is-5") {
                        +"There is nothing to display yet"
                    }
                }
            } else {
                div {
                    style = "display: flex;flex-direction: column; align-items: end;"
                    h6("subtitle is-6") {
                        style = "margin-bottom: 24px"
                        displayPeriod(nps.from.toDateString(), nps.to.toDateString())
                    }
                    div("nps") {
                        div {
                            div {
                                toSvg(nps)
                            }
                            div {
                                style =
                                    "display: flex; justify-content: space-around; max-width: 50%; margin-left: auto; margin-right: auto"
                                h6("subtitle is-6 has-text-danger") { +"Poor" }
                                h6("subtitle is-6 has-text-warning") { +"Average" }
                                h6("subtitle is-6 has-text-primary") { +"Good" }
                                h6("subtitle is-6 has-text-success") { +"Excellent" }
                            }
                        }
                        div {
                            h2("title is-5") {
                                +"NPS Breakdown"
                            }
                            div {
                                div("nps-score") {
                                    npsDetail(nps.promotersPercentage, "Promoters", "success")
                                    p("subtitle is-3") {
                                        +"-"
                                    }
                                    npsDetail(nps.detractorsPercentage, "Detractors", "danger")
                                    p("title is-3") {
                                        +"="
                                    }
                                    npsDetail(nps.scores.value.toString(), "NPS Score", nps.scores.levelColor())
                                }
                                hr {}
                                div("nps-details") {
                                    p { +"Promoters" }
                                    p { +"(9 - 10)" }
                                    h6("title is-6") { +nps.scores.promoters.toString() }
                                    progress("progress is-success") {
                                        value = nps.promotersPercentage.toString(); max = "100"
                                    }
                                    p { +"Passives" }
                                    p { +"(7 - 8)" }
                                    h6("title is-6") { +nps.scores.passives.toString() }
                                    progress("progress is-warning") {
                                        value = nps.passivesPercentage.toString(); max = "100"
                                    }
                                    p { +"Detractors" }
                                    p { +"(0 - 6)" }
                                    h6("title is-6") { +nps.scores.detractors.toString() }
                                    progress("progress is-danger") {
                                        value = nps.detractorsPercentage.toString(); max = "100"
                                    }
                                    p("title is-6") {
                                        style = "grid-column-start: 1; grid-column-end: 3"
                                        +"Total Answers"
                                    }
                                    h6("title is-6") { +"${nps.scores.total}" }
                                }
                            }
                        }
                    }
                }
            }
            div {
                style = "display: flex; justify-content: space-between"
                if (nps == null || !nps.isOpen) {
                    form {
                        action = href(SurveyResource.NPS.Open(projectId.toString()))//ui.urlTo<OpenNPSSurveyCommandDTO>(projectId)
                        method = FormMethod.post
                        input(type = InputType.hidden, name = "project") { id = "project"; value = projectId.toString() }
                        button(
                            classes = "button is-dark is-rounded",
                            type = ButtonType.submit
                        ) {
                            id = "open-survey-button"
                            span("icon material-symbols-outlined") {
                                style = "align-self: baseline"
                                +"ballot"
                            }
                            span { +"Open the survey" }

                        }
                    }
                } else {
                    form {
                        action = href(SurveyResource.NPS.Close(projectId.toString()))
                        method = FormMethod.post
                        input(type = InputType.hidden, name = "project") { id = "project"; value = projectId.toString() }
                        button(
                            classes = "button is-dark is-rounded",
                            type = ButtonType.submit
                        ) {
                            id = "close-survey-button"
                            span("icon material-symbols-outlined") {
                                style = "align-self: baseline"
                                +"ballot"
                            }
                            span { +"Close the survey" }

                        }
                    }
                }
                if (nps != null && nps.isOpen) {
                    a(
                        classes = "button is-dark is-rounded",
                    ) {
                        href = href(SurveyResource.NPS.Vote(projectId.toString()))
                        span("icon material-symbols-outlined") {
                            style = "align-self: baseline"
                            +"how_to_vote"
                        }
                        span { +"Go to the survey" }
                    }
                }
            }
        }
    }
}

private fun H6.displayPeriod(from: String, to: String) {
    if (to.contains("999999")) {
        span {
            +"Measured since "
        }
        b {
            span("title is-6") {
                +from
            }
        }
        return
    }
    span {
        +"Measured from "
    }
    b {
        span("title is-6") {
            +from
        }
    }
    span {
        +" to "
    }
    b {
        span("title is-6") {
            +to
        }
    }
}


fun Survey.NPSSurvey.Scores.levelColor() = when (this?.level) {
    domain.survey.model.NPSLevel.Poor -> "danger"
    domain.survey.model.NPSLevel.Average -> "warning"
    domain.survey.model.NPSLevel.Good -> "primary"
    domain.survey.model.NPSLevel.Excellent -> "success"
    else -> "none"
}

private fun DIV.npsDetail(percentage: Int, label: String, color: String) = npsDetail("$percentage %", label, color)
private fun DIV.npsDetail(percentage: String, label: String, color: String) {
    div {
        div("block indicator-score") {
            p("title is-3 has-text-$color") {
                style = "white-space: nowrap; overflow: hidden;"
                +percentage
            }
            p("subtitle is-7") {
                +label
            }
        }
    }
}


private fun DIV.toSvg(nps: Survey.NPSSurvey) {
    unsafe {
        + """
            
        """.trimIndent()
    }
}

val Survey.NPSSurvey.promotersPercentage: Int
    get() = ((scores.promoters.toDouble() / scores.total) * 100.0).toInt()
val Survey.NPSSurvey.passivesPercentage: Int
    get() = ((scores.passives.toDouble() / scores.total) * 100.0).toInt()
val Survey.NPSSurvey.detractorsPercentage: Int
    get() = ((scores.detractors.toDouble() / scores.total) * 100.0).toInt()