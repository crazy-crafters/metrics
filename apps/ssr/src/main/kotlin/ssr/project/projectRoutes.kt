package ssr.project

import io.ktor.server.application.*
import ssr.project.survey.surveyRoutes


fun Application.projectRoutes() {
    surveyRoutes()
}