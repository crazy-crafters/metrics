package ssr

import io.ktor.server.application.*
import io.ktor.server.sessions.*
import kotlinx.serialization.Serializable
import ssr.ui.base.Message

@Serializable
data class UserCookie(val message: Message = Message.None)

fun ApplicationCall.takeMessage(): Message {
    val message = this.sessions.get<UserCookie>()?.message ?: Message.None
    this.sessions.set("user_cookie", UserCookie())
    return message
}

fun ApplicationCall.setMessage(message: Message) {
    val cookie = this.sessions.get<UserCookie>()?: UserCookie()
    this.sessions.set("user_cookie", cookie.copy(message = message))
}