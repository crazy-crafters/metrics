document.addEventListener('DOMContentLoaded', () => {
    (document.querySelectorAll('.js-copy-to-clipboard') || []).forEach(($trigger) => {
        const link = $trigger.dataset.target;
        $trigger.addEventListener('click', () => {
            navigator.clipboard.writeText(link);
            openToast("link-copied")
        });
    });
});