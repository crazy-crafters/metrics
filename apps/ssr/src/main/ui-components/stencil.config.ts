import { Config } from '@stencil/core';
import { sass } from '@stencil/sass';

export const config: Config = {
  namespace: 'ui-components',
  plugins: [
    sass()
  ],
  globalStyle: 'src/global.scss',
  outputTargets: [
    /*{
      type: 'dist',
      esmLoaderPath: '../loader',
    },
    {
      type: 'dist-custom-elements',
      customElementsExportBehavior: 'auto-define-custom-elements',
      externalRuntime: false,
    },
    {
      type: 'docs-readme',
    },*/
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      buildDir: '../../resources/static/components/',
      copy: [
        {
          src: '../node_modules/bulma-calendar/dist/js/bulma-calendar.js',
          dest: '../../resources/static/components/bulma-calendar.js'
        },
        {
          src: '../node_modules/bulma-calendar/dist/css/bulma-calendar.css',
          dest: '../../resources/static/components/bulma-calendar.css'
        },
      ]
    },
  ],
  testing: {
    browserHeadless: "new",
  },

};
