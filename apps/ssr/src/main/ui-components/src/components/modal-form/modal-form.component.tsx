import {Component, h, State, Prop} from '@stencil/core';

@Component({
  tag: 'modal-form'
})
export class ModalForm {

  @Prop() label: string = '';
  @Prop() header: string = '';
  @Prop() validateUrl: string = '';
  @Prop() cancelUrl: string = '';

  @Prop() encType: string = '';

  @State() showModal: boolean = false;
  toggleModal() {
    this.showModal = !this.showModal;
  }


  render() {
    return (
      <div>
        <button class="button is-dark" onClick={() => this.toggleModal()}>{this.label}</button>
        <div class={`modal ${this.showModal ? 'is-active' : ''}`}>
          <form action={this.validateUrl} method="POST" enctype={this.encType}>
            <div class="modal-background" onClick={() => this.toggleModal()}></div>
            <div class="modal-card">
              <header class="modal-card-head">
                <p class="modal-card-title">{this.header}</p>
                <button class="modal-close is-large" aria-label="close" onClick={() => this.toggleModal()}></button>
              </header>
              <section class="modal-card-body">
                <slot />
              </section>
              <footer class="modal-card-foot">
                <div class="buttons">
                  <div>
                    <button class="button is-dark" type="SUBMIT">Ok</button>
                    <a class="button is-light modal-close-button" href={this.cancelUrl}>Cancel</a>
                  </div>
                </div>
              </footer>
            </div>
          </form>
        </div>
      </div>
    )
  }

}
