import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'embedded-form'
})
export class EmbeddedForm {

  @Prop() label: string = '';
  @Prop() header: string = '';
  @Prop() validateUrl: string = '';
  @Prop() cancelUrl: string = '';

  @Prop() encType: string = '';

  render() {
    return (
      <div>
          <form action={this.validateUrl} method="POST" enctype={this.encType}>
              <slot />
              <div class="buttons">
                  <div>
                    <button class="button is-dark" type="SUBMIT">Ok</button>
                    <a class="button is-light modal-close-button" href={this.cancelUrl}>Cancel</a>
                  </div>
              </div>
          </form>
      </div>
    )
  }

}
