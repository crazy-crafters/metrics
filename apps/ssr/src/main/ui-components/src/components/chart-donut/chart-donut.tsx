import { Component, Prop, h } from '@stencil/core';

@Component({
  tag: 'chart-donut',
  shadow: false,
})
export class DonutChart {
  @Prop() data: [number, string][];
  @Prop() size: number = 400;

  private chartContainer: HTMLElement;

  componentDidLoad() {
    // Load the Google Charts library
    const googleScript = document.createElement('script');
    googleScript.src = 'https://www.gstatic.com/charts/loader.js';
    googleScript.onload = this.drawChart.bind(this);
    document.body.appendChild(googleScript);
  }

  drawChart() {
    if (!this.data || !this.chartContainer) return;

    (window as any).google.charts.load('current', { packages: ['corechart'] });
    (window as any).google.charts.setOnLoadCallback(() => {
      const chartData = new (window as any).google.visualization.DataTable();
      chartData.addColumn('string', 'Label');
      chartData.addColumn('number', 'Value');
      const sum = [...this.data.map(value => value[0])]
                        .reduce((sum, current) => sum + current, 0);

      let colors = [...this.data.map(value => value[1])];
      if (sum) {
        chartData.addRows([...this.data.map(value => ['', value[0]])]);
      } else {
        chartData.addRows([['', 1]]);
        colors = ["#808080"]
      }

      const options = {
        title: null,
        pieHole: 0.4, // Makes it a donut chart
        width: this.size,
        height: this.size,
        chartArea: {width: this.size, height: this.size},
        legend: 'none',
        'tooltip' : {
          trigger: 'none'
        },
        enableInteractivity: false,
        colors: colors
      };

      const chart = new (window as any).google.visualization.PieChart(
        this.chartContainer,
      );
      chart.draw(chartData, options);
    });
  }

  render() {
    return <div ref={el => (this.chartContainer = el as HTMLElement)} />;
  }
}
