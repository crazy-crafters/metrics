import {h, Component, Prop, Element} from '@stencil/core';
import bulmaCalendar from "bulma-calendar";

@Component({
  tag: 'bulma-calendar',
})
export class BulmaCalendar {

  @Element() element: HTMLElement;
  @Prop() date: Date = new Date();
  @Prop() name: string = '';

  private calendar?: HTMLInputElement;

  componentDidLoad() {

    bulmaCalendar.attach(this.calendar, {
      type: 'date',
      dateFormat: "yyyy-MM-dd",
      startDate: this.date,
      color: 'dark',
      showFooter: false,
      displayMode: 'dialog'
    });
  }

  render() {

    return (<div class="">
      <input name={this.name} type="date" ref={el => this.calendar = el as HTMLInputElement} />
    </div>);
  }
}
