import { Component, h, Prop, State } from '@stencil/core';

@Component({
  tag: 'button-with-confirmation'
})
export class ButtonWithConfirmation {

  @Prop() label: string;
  @Prop() header: string;
  @Prop() message: string;
  @Prop() validate: string;

  @State() showModal: boolean = false;

  toggleModal() {
    this.showModal = !this.showModal;
  }


  render() {
    return (
      <div>
        <button class="button is-danger is-outlined is-small" onClick={() => this.toggleModal()}>{this.label}</button>
        <div class={`modal ${this.showModal ? 'is-active' : ''}`}>
          <div class="modal-background" onClick={() => this.toggleModal()}></div>
          <div class="modal-card">
            <header class="modal-card-head">
              <p class="modal-card-title">{this.header}</p>
              <button class="modal-close is-large" aria-label="close" onClick={() => this.toggleModal()}></button>
            </header>
            <section class="modal-card-body">
              <p>{this.message}</p>
            </section>
            <footer class="modal-card-foot">
              <div class="buttons">
                <form action={this.validate} method="POST">
                  <div>
                    <button class="button is-dark" type="SUBMIT">Ok</button>
                    <a class="button is-light modal-close-button" href="">Cancel</a>
                  </div>
                </form>
              </div>
            </footer>
          </div>
        </div>
      </div>
    );
  }

}
