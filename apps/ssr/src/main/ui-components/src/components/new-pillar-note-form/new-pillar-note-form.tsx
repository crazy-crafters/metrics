import {Component, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'new-pillar-note-form'
})
export class NewPillarNoteForm {

  @Prop() validateUrl: string = '';
  @Prop() cancelUrl: string = '';

  @State() selectedFiles: string[] = [];

  onFileSelected(files: FileList) {
    const selected = [];
    for (var i = 0; i != files.length; ++i) {
      selected.push(files.item(i).name);
    }

    this.selectedFiles = [...selected];
  }

  render() {
    return (
        <modal-form
          label="Add a Note"
          header="Register a new Note"
          validateUrl={this.validateUrl}
          cancelUrl={this.cancelUrl}
          encType="multipart/form-data"
        >
          <div class="field">
            <label class="label">Short Description</label>
            <input
              name='title'
              class='input'
            />
          </div>
          <div class="field">
            <label class="label">Notes</label>
            <div class="control">
                    <textarea
                      name='description'
                      class='textarea'>
                    </textarea>
            </div>
          </div>
          <div class="field">
            <label class="label">Attached Files</label>
            <div class="file is-boxed">
              <label class="file-label">
                <input class="file-input" type="file" name="attached" onChange={($event: any) => this.onFileSelected($event.target.files)} multiple={true} />
                <span class="file-cta">
                  <span class="icon material-symbols-outlined">
                    upload
                  </span>
                  <span class="file-label"> Choose files… </span>
                </span>
              </label>
            </div>
            <div class="tags">
              {
                this.selectedFiles.map(f =>
                  <span class="tag is-dark">
                          {f}
                  </span>
                )
              }
            </div>
          </div>
        </modal-form>
    );
  }

}
