import {Component, h, Prop} from '@stencil/core';


export interface Status {
  id: string,
  name: string,
  state: string
}

@Component({
  tag: 'update-statuses'
})
export class UpdateStatuses {

  @Prop() statuses: Status[] = [];
  @Prop() validateUrl: string = '';

  render() {
    return (
      <form action={this.validateUrl} method="POST">
        <button class="button is-dark" type="SUBMIT">Save</button>
        <table class="table">
          <tbody>
          {
            this.statuses.map((u) =>
              <tr>
                <td>{u.id}</td>
                <td>{u.name}</td>
                <td>{u.state}</td>
                <td>
                  <div class="select">
                    <select name={u.id}>
                      <option value="Undefined" selected={u.state === "Undefined"}>Undefined</option>
                      <option value="Planned" selected={u.state === "Planned"}>Planned</option>
                      <option value="Waiting" selected={u.state === "Waiting"}>Waiting</option>
                      <option value="Working" selected={u.state === "Working"}>Working</option>
                      <option value="Done" selected={u.state === "Done"}>Done</option>
                      <option value="Closed" selected={u.state === "Closed"}>Closed</option>
                    </select>
                  </div>
                </td>
              </tr>)
          }
          </tbody>
        </table>
      </form>
    );
  }

}
