import {Component, Element, h, Listen, Prop, State} from '@stencil/core';
import {Item} from "./item";


@Component({
  tag: 'tags-autocomplete'
})
export class TagsAutoComplete {

  @Element() element: HTMLElement;

  @State() showSuggestions: boolean;
  @State() inputValue = '';
  @State() tags: Item[] = [];
  @State() suggestions: Item[] = [];
  @State() selected: number;

  @Prop() values: Item[] = [];
  @Prop() name: string = '';


  @Listen('click', { target: 'window' })
  checkForClickOutside(ev) {
    if (this.element.contains(ev.target)) {
      return;
    }
    this.showSuggestions = false;
  }

  findMatch = (searchTerm: string): Item[] => {
    /*if (searchTerm.length === 0) {
      return this.values;
    }*/
    const alreadyToggled = this.tags.map((it) => it.value.toLowerCase());
    return this.values.filter((item) => !alreadyToggled.includes(item.value.toLowerCase()) && item.label.toLowerCase().search(searchTerm.toLowerCase()) !== -1);
  };

  onInput = (e) => {
    this.inputValue = (e.target as any).value;
    this.suggestions = this.findMatch(this.inputValue);
    this.showSuggestions = true;
  };

  onFocus = () => {
    this.showSuggestions = true;
    this.selected = 0;
    this.suggestions = this.findMatch(this.inputValue);
  }


  onKeyDown = (e) => {
    if (this.suggestions.length <= 0) {
      return;
    }
    switch (e.key) {
      case 'ArrowUp':
          this.selected = (this.selected === 0) ? this.suggestions.length - 1 : this.selected - 1;
        break;
      case 'ArrowDown':
          this.selected = (this.selected === this.suggestions.length - 1) ? 0 : this.selected + 1;
        break;
      default:
        break;
    }
  };

  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.onSelect(this.suggestions[this.selected]);
    }
  }

  onSelect = (selection: Item) => {
    this.tags.push(selection)
    this.inputValue = '';
    this.selected = 0;
    this.showSuggestions = false;
  }

  remove = (index: number) => {
    this.tags = this.tags.slice(0, index).concat(this.tags.slice(index + 1));
  }


  render() {
    return (<div class="">
      <div class="dropdown is-active" style={{"width": "100%"}}>
        <div class="dropdown-trigger"  style={{"width": "100%"}}>
          <input
            required={true}
            id={this.name}
            name={this.name}
            type='text'
            value={this.tags.map((i) => i.value).join(';')}
            style={{'display': 'none'}}
          ></input>
          <input
            class='input'
            type="text"
            value={this.inputValue}
            onInput={e => this.onInput(e)}
            onFocus={() => this.onFocus()}
            onKeyDown={e => this.onKeyDown(e)}
            onKeyPress={e => this.onKeyPress(e)}
          />
        </div>
        <div
             class="dropdown-menu"
             style={{
               "display": this.showSuggestions ? "block": "none"
             }}
             id="dropdown-menu" role="menu">
          <div class="dropdown-content">
              {this.suggestions.map((suggestion, index) =>
                <a
                  role='listbox'
                  class={{
                    'dropdown-item': true,
                    'is-active': index === this.selected
                  }}
                  onClick={() => this.onSelect(suggestion)}
                >
                  {suggestion.label}
                </a>
              )
              }
          </div>
        </div>
      </div>
      <div class="tags" style={{'min-height': '56px', 'margin-top': '16px'}}>
        {
          this.tags.map((item, index) =>
            <span class="tag is-dark">
              <div>
              {item.label}
              </div>
              <a class="delete is-small" onClick={() => this.remove(index)}></a>
            </span>
          )
        }
      </div>
    </div>);
  }
}
