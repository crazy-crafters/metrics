import {h, Component, Prop, State, Listen, Element} from '@stencil/core';

import {Item} from "./item";

@Component({
  tag: 'input-autocomplete'
})
export class AutoComplete {

  @Element() element: HTMLElement;

  @State() showSuggestions: boolean;
  @State() inputValue = '';
  @State() selectedValue: Item = {value: '', label: ''};
  @State() suggestions: Item[] = [];
  @State() selected: number;

  @Prop() values: Item[] = [];
  @Prop() name: string = '';


  @Listen('click', { target: 'window' })
  checkForClickOutside(ev) {
    if (this.element.contains(ev.target)) {
      return;
    }
    this.showSuggestions = false;
  }

  findMatch = (searchTerm: string): Item[] => {
    if (searchTerm.length === 0) {
      return this.values;
    }
    return this.values.filter((item) => item.label.search(searchTerm) !== -1);
  };

  onInput = (e) => {
    this.inputValue = (e.target as any).value;
    this.suggestions = this.findMatch(this.inputValue);
    this.showSuggestions = true;
  };

  onFocus = () => {
    this.showSuggestions = true;
    this.selected = 0;
    this.suggestions = this.findMatch(this.inputValue);
  }

  onFocusOut = () => {
    //this.showSuggestions = false;
    //this.selected = 0;

  }


  onKeyDown = (e) => {
    if (this.suggestions.length <= 0) {
      return;
    }
    switch (e.key) {
      case 'ArrowUp':
          this.selected = (this.selected === 0) ? this.suggestions.length - 1 : this.selected - 1;
        break;
      case 'ArrowDown':
          this.selected = (this.selected === this.suggestions.length - 1) ? 0 : this.selected + 1;
        break;
      default:
        break;
    }
  };

  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault();
      this.onSelect(this.suggestions[this.selected]);
    }
  }

  onSelect = (selection: Item) => {
    this.selectedValue = selection;
    this.inputValue = selection.label;
    this.selected = 0;
    this.showSuggestions = false;
  }


  render() {
    return (<div class="">
      <input type="hidden" id={this.name} name={this.name} value={this.inputValue} />
      <div class="dropdown is-active" style={{"width": "100%"}}>
        <div class="dropdown-trigger"  style={{"width": "100%"}}>
            <input
              name = {this.name + '-user-value'}
              class='input'
              type="text"
              value={this.inputValue}
              onInput={e => this.onInput(e)}
              onFocus={() => this.onFocus()}
              onKeyDown={e => this.onKeyDown(e)}
              onKeyPress={e => this.onKeyPress(e)}
            />
        </div>
        <div
             class="dropdown-menu"
             style={{
               "display": this.showSuggestions ? "block": "none"
             }}
             id="dropdown-menu" role="menu">
          <div class="dropdown-content">
              {this.suggestions.map((suggestion, index) =>
                <a
                  role='listbox'
                  class={{
                    'dropdown-item': true,
                    'is-active': index === this.selected
                  }}
                  onClick={() => this.onSelect(suggestion)}
                >
                  {suggestion.label}
                </a>
              )
              }
          </div>
        </div>
      </div>
    </div>);
  }
}
