import {Component, h, Prop, State} from '@stencil/core';

import {Item} from "../common/item";

@Component({
  tag: 'squad-form'
})
export class SquadForm {

  @State() value: string;
  @State() action: string;

  @Prop() data: Item[];
  @Prop() validateUrl: string;
  @Prop() cancelUrl: string;

  render() {
    return (
      <modal-form label="New Squad" header="Compose a new squad" validateUrl={this.validateUrl} cancelUrl={this.cancelUrl}>
          <div class="field">
            <label class="label">Name</label>
            <div class="control">
              <input
                name='name'
                class='input'
                type="text"
              ></input>
            </div>
          </div>
          <div class="field">
            <label class="label">Members</label>
            <div class="control">
              <tags-autocomplete
                name='members'
                values={this.data}
              ></tags-autocomplete>
            </div>
          </div>
      </modal-form>
    );
  }
}
