import {Component, h, State, Prop} from '@stencil/core';
import Chart, {ChartType} from 'chart.js/auto';

const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
];




const chartType: ChartType = "line";


// @ts-ignore
const lineChartOptions = {
  maintainAspectRatio: false,
  responsive: true,
    plugins: {
    title: {
      display: true,
        text: 'Chart.js Line Chart'
    },
  },
  interaction: {
    mode: 'index',
      intersect: false
  },
  scales: {
    x: {
      display: false,
        title: {
        display: false,
      }
    },
    y: {
      display: true,
        title: {
        display: true,
          text: 'Value'
      }
    }
  }
};

// @ts-ignore
const barChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    plugins: {
      legend: {
        display: false
      },
      title: {
        display: false
      }
    },
    scales: {
      x: {
        border: {
          display: true
        },
        grid: {
          display: false,
        }
      },
      y: {
        ticks: {
          stepSize: 1
        },
        border: {
          display: true
        },
        grid: {
          display: false,
        },
      }
    }
  };

@Component({
  tag: 'burnup-chart',
  shadow: false,
})
export class BurnupChart {
  @Prop() data = {
    labels: MONTHS,
    datasets: [
      {
        label: 'Dataset 1',
        data: [10, 30, 39, 20, 25, 34, -10],
        fill: false,
      },
    ]
  };
  @State() chartRef: HTMLCanvasElement
  @State()  chart: Chart

  @State() ro: ResizeObserver;

  componentDidLoad() {

    this.ro = new ResizeObserver(_ => {
      if (this.chart) {
        this.chart.update();
      }
    });
    this.ro.observe(this.chartRef);

    if (this.chart) {
      this.chart.update();
    } else if (this.chartRef) {
      const ctx = this.chartRef.getContext("2d");
      this.chart = new Chart<ChartType>(ctx, {
        type: chartType,
        data: this.data,
        options: barChartOptions
      });
      this.chart.render()
    }
  }

  disconnectedCallback() {
    this.ro.disconnect();
  }

  render() {
    return (
        <canvas id="canvas" ref={(elem) => this.chartRef = elem}>
        </canvas>
    );
  }

}
