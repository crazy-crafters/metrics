import {Component, h, Prop, State} from '@stencil/core';
import {Item} from "../common/item";

@Component({
  tag: 'new-event-form'
})
export class NewEventForm {

  @Prop() types: Item[] = [];
  @Prop() validateUrl: string = '';
  @Prop() cancelUrl: string = '';
  @Prop() selectedType: Item = {label: '', value: ''};


  @State() showDropdownTypes: boolean = false;
  @State() showModal: boolean = false;

  toggleModal() {
    this.showModal = !this.showModal;
  }

  toggleDropdown() {
    this.showDropdownTypes = !this.showDropdownTypes;
  }

  selectType(item) {
    this.selectedType = item;
    this.toggleDropdown();
  }

    render() {
    return (
      <div>
        <button class="button is-dark" onClick={() => this.toggleModal()}>New Milestone</button>
        <div class={`modal ${this.showModal ? 'is-active' : ''}`}>
          <form action={this.validateUrl} method="POST">
            <div class="modal-background" onClick={() => this.toggleModal()}></div>
            <div class="modal-card">
              <header class="modal-card-head">
                <p class="modal-card-title">Create a new milestone</p>
                <button class="modal-close is-large" aria-label="close" onClick={() => this.toggleModal()}></button>
              </header>
              <section class="modal-card-body">
                <div class="field">
                  <label class="label">Date</label>
                  <div class="control">
                    <bulma-calendar name="date"></bulma-calendar>
                  </div>
                </div>
                <div class="field">
                  <label class="label">Title</label>
                  <div class="control">
                    <input
                      name='title'
                      class='input'
                      type="text"
                    ></input>
                  </div>
                </div>
                <div class="field">
                  <label class="label">Type of Event</label>
                  <input
                    name='type'
                    class='input'
                    type="hidden"
                    value={this.selectedType.value}
                  ></input>
                  <div class="dropdown is-active" style={{'width': '250px'}}>
                    <div class="dropdown-trigger" style={{'width': '100%'}}>
                      <button style={{'width': '100%'}} type="button" class="button" aria-haspopup="true" aria-controls="dropdown-menu" onClick={() => this.toggleDropdown()}>
                        <span>{this.selectedType.label}</span>
                        <span class="icon is-small">
                         <i class="fas fa-angle-down" aria-hidden="true"></i>
                        </span>
                      </button>
                    </div>
                    <div style={{
                      "display": this.showDropdownTypes ? "block": "none"
                        }}
                         class="dropdown-menu" id="dropdown-menu" role="menu">
                      <div class="dropdown-content">
                        {
                          this.types.map(item =>
                            <button type="button" class="dropdown-item" onClick={() => this.selectType(item)}>{item.label}</button>
                          )
                        }
                      </div>
                    </div>
                  </div>
                </div>
                <div class="field">
                  <label class="label">Description</label>
                  <div class="control">
                    <textarea
                      name='description'
                      class='textarea'>
                    </textarea>
                  </div>
                </div>
              </section>
              <footer class="modal-card-foot">
                <div class="buttons">
                  <div>
                    <button class="button is-dark" type="SUBMIT">Ok</button>
                    <a class="button is-light modal-close-button" href={this.cancelUrl}>Cancel</a>
                  </div>
                </div>
              </footer>
            </div>
          </form>
        </div>
      </div>
    )
  }

}
