import {Component, h, Host, State} from '@stencil/core';

@Component({
  tag: 'collapsible-box'
})
export class CollapsibleBox {
  @State() collapsed: boolean = true;

  toggleContent() {
    this.collapsed = !this.collapsed;
  }

  render() {
    return (
      <Host class="is-fullwidth container box pt-1 pb-1">
          <button onClick={() => this.toggleContent()} class="header is-align-items-center">
            <slot name="header"/>
            <div class="icon ml-2">
            {this.collapsed
              ? (<span class="material-symbols-outlined">expand_more</span>)
              : (<span class="material-symbols-outlined">expand_less</span>)
            }
            </div>
          </button>
        <div style={this.collapsed?{"display": "none"} : {}} class="mt-4">
          <slot name="content"/>
        </div>
      </Host>
    );
  }
}
