import {Component, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'change-indicator-score'
})
export class ChangeIndicatorScore {

  @Prop() validateUrl: string = '';
  @Prop() cancelUrl: string = '';
  @Prop() name: string = '';
  @Prop() maximum: number = 5;
  @Prop() initialValue: number = 5;

  @State() showModal: boolean = false;

  @State() value: number = this.initialValue;

  toggleModal() {
    this.showModal = !this.showModal;
  }

  handleKeyDown(event: KeyboardEvent) {
    isNaN(parseInt(event.key)) && event.preventDefault()
  }

  incrementBy(inc: number) {
    this.value = Math.min(this.maximum, Math.max(0, this.value + inc));
  }

  private onInputChangeValue(event: Event) {
    const currentValue = parseInt((event.target as HTMLInputElement).value);
    if (isNaN(currentValue)) {

      return;
    }
    this.value = currentValue;
  }


  render() {
    return (
      <div>
        <button class="button is-dark" onClick={() => this.toggleModal()}>Evaluate</button>
        <div class={`modal ${this.showModal ? 'is-active' : ''}`}>
          <form action={this.validateUrl} method="POST">
            <div class="modal-background" onClick={() => this.toggleModal()}></div>
            <div class="modal-card">
              <header class="modal-card-head">
                <p class="modal-card-title">Evaluate {this.name}</p>
                <button class="modal-close is-large" aria-label="close" onClick={() => this.toggleModal()}></button>
              </header>
              <section class="modal-card-body">
                <div class="field">
                  <label class="label">Score</label>
                  <div class="control">
                    <div style={{"display": "flex", "width": "fit-content"}} class="input is-large">
                      <button onClick={() => this.incrementBy(-1)} type="button">
                        <span class="material-symbols-outlined">remove</span>
                      </button>
                      <input name="score"
                             type="number"
                             class="is-size-3 has-text-dark"
                             max={this.maximum}
                             min={0}
                             value={this.value}
                             onInput= {e => this.onInputChangeValue(e)}
                             onKeyDown={e => this.handleKeyDown(e)}
                             style={{"border": "none", "width": "100px", "text-align": "center"}}
                      />
                      <button onClick={() => this.incrementBy(1)} type="button">
                        <span class="material-symbols-outlined">add</span>
                      </button>
                    </div>
                  </div>
                </div>
              </section>
              <footer class="modal-card-foot">
                <div class="buttons">
                  <div>
                    <button class="button is-dark" type="SUBMIT">Ok</button>
                    <a class="button is-light modal-close-button" href={this.cancelUrl}>Cancel</a>
                  </div>
                </div>
              </footer>
            </div>
          </form>
        </div>
      </div>
    )
  }

}
