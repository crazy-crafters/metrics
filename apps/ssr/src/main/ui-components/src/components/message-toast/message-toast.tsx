import {Component, h, Prop, State} from '@stencil/core';

@Component({
  tag: 'message-toast'
})
export class MessageToast {

  @Prop() level: string = "";
  @Prop() message: string = "";
  @State() visible: boolean = true;

  @State() timer: number;

  connectedCallback() {
    this.timer = window.setInterval(() => {
      this.visible = false;
      window.clearInterval(this.timer);
    }, 5000);
  }



  render() {
    let appearence = "";

    switch(this.level.toLowerCase()) {
      case "success" :
      case "warning":
      case "danger" : {
        appearence = "is-" + this.level.toLowerCase()
      }
    }

    return (
      <div class={"toaster notification " + appearence} style={this.visible ? {} : {"display": "none"}}>
        <button class="delete" onClick={() => {this.visible = false}} />
        {this.message}
      </div>

    );
  }
}


/*
div("toaster notification is-danger") {
                id="toaster"
                button(classes = "delete") {
                    onClick="closeToast('toaster')"
                }
                span("toaster-content") {
                    id="toaster-content"
                    +message.content
                }
            }
 */
