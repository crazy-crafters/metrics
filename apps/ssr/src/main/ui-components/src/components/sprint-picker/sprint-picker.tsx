import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'sprint-picker'
})
export class SprintPicker {

  @Prop() validateUrl: string = '';
  @Prop() from: string = '';
  @Prop() to: string = '';

  /*render() {
    return (

      <form action={this.validateUrl} method="POST">
        <div class="is-flex is-justify-content-center mt-6 mb-6">
          <div class="field">
            <label class="label">From</label>
            <div class="control">
              <bulma-calendar name="from"></bulma-calendar>
            </div>
          </div>
          <div class="field">
            <label class="label">To</label>
            <div class="control">
              <bulma-calendar name="to"></bulma-calendar>
            </div>
          </div>
          <button class="button is-dark" type="SUBMIT">Reload</button>
        </div>
      </form>
    );
  }*/

  render() {
    const startDate = new Date(this.from);
    const endDate = new Date(this.to);

    return (
      <form action={this.validateUrl} method="POST">
        <div class="is-flex is-justify-content-center mt-6 mb-6">
          <div class="field is-flex is-align-items-center mb-0">
            <label class="label mr-2 mb-0">From</label>
            <div class="control">
              <bulma-calendar name="from" date={startDate}></bulma-calendar>
            </div>
          </div>
          <div class="field is-flex is-align-items-center mb-0">
            <label class="label mr-2 ml-3 mb-0">To</label>
            <div class="control">
              <bulma-calendar name="to" date={endDate}></bulma-calendar>
            </div>
          </div>
          <button class="button is-ghost" type="SUBMIT">
            <span class="icon material-symbols-outlined">
              refresh
            </span>
          </button>
        </div>
      </form>
    );
  }
}

