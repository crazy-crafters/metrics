import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'board-configuration'
})
export class BoardConfiguration {

  @Prop() url: string = "";
  @Prop() type: string = "";
  @Prop() username: string = "";
  @Prop() token: string = "";
  @Prop() board: string = "";

  render() {
    return (
      <embedded-form>
        <div class="field">
          <label class="label">Type</label>
          <div class="select">
            <select name="type">
              <option value="jira">Jira</option>
              <option value="azure devops">Azure Devops</option>
            </select>
          </div>
        </div>
        <div class="field">
          <label class="label">URL</label>
          <div class="control">
            <input
              name='url'
              class='input'
              type="text"
              value={this.url}
            ></input>
          </div>
        </div>
        <div class="field">
          <label class="label">Board</label>
          <div class="control">
            <input
              name='board'
              class='input'
              type="text"
              value={this.board}
            ></input>
          </div>
        </div>
        <div class="field">
          <label class="label">Username</label>
          <p class="control has-icons-left">
            <input class="input" type="text" name="username" value={this.username}/>
            <span class="icon is-small is-left">
              <i class="fas fa-lock"></i>
            </span>
          </p>
        </div>
        <div class="field">
          <label class="label">Access Token</label>
          <p class="control has-icons-left">
            <input class="input" type="password" placeholder="Password" name="token" value={this.token} />
            <span class="icon is-small is-left">
              <i class="fas fa-lock"></i>
            </span>
          </p>
        </div>
      </embedded-form>
    );
  }

}
