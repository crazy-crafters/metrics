import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.testing.logging.TestLogEvent

plugins {
    `maven-publish`
    kotlin("plugin.serialization") version "2.1.10"
    alias(libs.plugins.kotlin.jvm)
    application
}

repositories {
    mavenCentral()
}

group = "org.crazy-crafters.blackbox.apps.cli"
version = "1.0"
base.archivesName.set("blackbox-cli")

dependencies {
    implementation(project(":domain"))
    implementation(project(":infra"))
    testImplementation(testFixtures(project(":domain")))
    testImplementation(testFixtures(project(":infra")))

    implementation("com.github.ben-manes.caffeine:caffeine:3.1.8")

    implementation(rootProject.libs.bundles.ktorClient)
    implementation(rootProject.libs.bundles.koin)
    implementation(rootProject.libs.bundles.kotlinxSerialization)
    implementation(rootProject.libs.bundles.clikt)

    testImplementation(libs.junit.jupiter.engine)
    testImplementation(libs.bundles.test)
    testImplementation("io.ktor:ktor-client-mock-jvm:${rootProject.libs.versions.ktor.get()}")
    testImplementation(rootProject.libs.koin.test.get())
}

tasks.named<Test>("test") {
    // Use JUnit Platform for unit tests.
    useJUnitPlatform()
    testLogging {
        lifecycle {
            events = mutableSetOf(TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED)
            exceptionFormat = TestExceptionFormat.FULL
            showExceptions = true
            showCauses = true
            showStackTraces = true
            showStandardStreams = true
        }
        info.events = lifecycle.events
        info.exceptionFormat = lifecycle.exceptionFormat
    }

    val failedTests = mutableListOf<TestDescriptor>()
    val skippedTests = mutableListOf<TestDescriptor>()
    addTestListener(object : TestListener {
        override fun beforeSuite(suite: TestDescriptor) {}
        override fun beforeTest(testDescriptor: TestDescriptor) {}
        override fun afterTest(testDescriptor: TestDescriptor, result: TestResult) {
            when (result.resultType) {
                TestResult.ResultType.FAILURE -> failedTests.add(testDescriptor)
                TestResult.ResultType.SKIPPED -> skippedTests.add(testDescriptor)
                else -> Unit
            }
        }

        override fun afterSuite(suite: TestDescriptor, result: TestResult) {
            if (suite.parent == null) { // root suite
                logger.lifecycle("----")
                logger.lifecycle("Test result: ${result.resultType}")
                logger.lifecycle(
                    "Test summary: ${result.testCount} tests, " +
                            "${result.successfulTestCount} succeeded, " +
                            "${result.failedTestCount} failed, " +
                            "${result.skippedTestCount} skipped")
                failedTests.takeIf { it.isNotEmpty() }?.prefixedSummary("\tFailed Tests")
                skippedTests.takeIf { it.isNotEmpty() }?.prefixedSummary("\tSkipped Tests:")
            }
        }

        private infix fun List<TestDescriptor>.prefixedSummary(subject: String) {
            logger.lifecycle(subject)
            forEach { test -> logger.lifecycle("\t\t${test.displayName()}") }
        }

        private fun TestDescriptor.displayName() = parent?.let { "${it.name} - $name" } ?: name
    })
}

tasks.jar {
    manifest.attributes["Main-Class"] = "cli.MainKt"
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree) // OR .map { zipTree(it) }
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE

    dependsOn(
        ":domain:jar",
        ":infra:jar"
    )
}