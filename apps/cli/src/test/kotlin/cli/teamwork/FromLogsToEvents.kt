package cli.teamwork

import domain.sharedkernel.model.*
import domain.sharedkernel.model.DateTime.Companion.toDateTime
import domain.teamwork.issue.model.PointsOfComplexity
import domain.teamwork.issue.model.Tags
import domain.teamwork.status.model.Status
import domain.teamwork.shared.events.*
import java.time.format.DateTimeParseException

fun String.fromLogsToEvents(): List<Event> =
    this.split("\n").map { it.trim() }.filter { it.isNotBlank() }
        .mapNotNull { line ->
            // IssueAdded(date=2024-01-01T12:00, entityId=1, project=a6c3e0ef-d0f8-3503-a045-53bf11b7d40e, title=Issue 1, complexity=PointsOfComplexity(value=2)),
            if (line.startsWith("IssueAdded")) {
                val rx =
                    "IssueAdded\\(date=(.+), entityId=(.+), project=(.+), title=(.+), complexity=PointsOfComplexity\\(value=(\\d+)\\)\\)".toRegex()
                val (date, id, project, title, complexity) = rx.find(line)!!.groups.mapNotNull { it?.value }.drop(1)
                IssueAdded(
                    entityId = ID.from(id),
                    project = ID.from(project),
                    date = date.toDateTime(),
                    status = Status.OutOfScope,
                    title = Title.from(title),
                    estimation = PointsOfComplexity.of(complexity.toInt()),
                    tags = Tags.empty()
                )
            } else if (line.startsWith("IssueMoved")) {
                val rx = "IssueMoved\\(date=(.+), entityId=(.+), project=(.+), newStatus=(.+) \\(.+\\)\\)".toRegex()
                val (date, id, project, status) = rx.find(line)!!.groups.mapNotNull { it?.value }.drop(1)
                IssueMoved(
                    entityId = ID.from(id),
                    project = ID.from(project),
                    date = date.toDateTime(),
                    status = status.toStatus()
                )
            } else if (line.startsWith("IterationStarted")) {
                val rx = "IterationStarted\\(date=(.+), entityId=(.+), project=(.+), name=(.+)\\)".toRegex()
                val (date, id, project, name) = rx.find(line)!!.groups.mapNotNull { it?.value }.drop(1)
                IterationStarted(
                    entityId = ID.from(id),
                    project = ID.from(project),
                    date = date.toDateTime(),
                    name = Name.from(name)
                )
            } else if (line.startsWith("IterationFinished")) {
                val rx = "IterationFinished\\(date=(.+), entityId=(.+), project=(.+)\\)".toRegex()
                val (date, id, project) = rx.find(line)!!.groups.mapNotNull { it?.value }.drop(1)
                IterationFinished(
                    entityId = ID.from(id),
                    project = ID.from(project),
                    date = date.toDateTime()
                )
            } else {
                null
            }
        }


private fun String.toStatus(): Status = lowercase().replace("_", "").let {
    Status.of(ID.from(it), Label.from(this))
}

fun Status.Companion.of(id: ID<Status>, value: Label<Status>): Status {
    return when(id) {
        Status.OutOfScope.id -> Status.OutOfScope
        Backlog.id -> Backlog
        Planned.id -> Planned
        Ready.id -> Ready
        InProgress.id -> InProgress
        isDone.id -> isDone
        Closed.id -> Closed
        else -> Status.Undefined(label = value, id = id)
    }
}