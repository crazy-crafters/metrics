package cli.teamwork

import buildJiraRestResponses
import cli.CLI
import com.github.ajalt.clikt.testing.test
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeTypeOf
import io.ktor.client.engine.*
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.*
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import domain.sharedkernel.materials.anId
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Label
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.Login
import domain.teamwork.shared.spi.EventRepository
import domain.teamwork.status.spi.StatusRepository
import MockedEngineWrapper
import domain.teamwork.status.model.Status
import domain.user.*
import infra.teamwork.providers.InMemoryStatusRepository
import infra.teamwork.teamworkModule
import domain.project.model.Project
import java.io.ByteArrayOutputStream
import java.io.PrintStream

val configApollo =
    BoardConfiguration.Valid(
        ID.from("apollo"),
        type = domain.teamwork.configuration.model.BoardType.Jira,
        url = "http://localhost",
        board = "apollo",
        Login("", "")
    )


@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@DisplayName("Teamwork - CLI")
class CLITest : KoinTest {
    private val configurationRepository: domain.teamwork.configuration.spi.ConfigurationRepository by inject()
    private val eventRepository: EventRepository by inject()
    private val statusRepository: StatusRepository by inject()

    private val outContent = ByteArrayOutputStream()
    private val errContent = ByteArrayOutputStream()
    private val originalOut: PrintStream = System.out
    private val originalErr: PrintStream = System.err

    @BeforeEach
    fun setUpStreams() {
        System.setOut(PrintStream(outContent))
        System.setErr(PrintStream(errContent))
        initialize()
    }

    @AfterEach
    fun restoreStreams() {
        System.setOut(originalOut)
        System.setErr(originalErr)

        stopKoin()
    }

    private fun initialize() {
        startKoin {
            modules(
                teamworkModule(),
                module {
                    allowOverride(true)
                    single<MockedEngineWrapper> {
                        val engine = MockedEngineWrapper()

                        engine.buildJiraRestResponses(
                            configApollo,
                            """
                01-01 01-10 closed
                    Issue 1: ready 01-01, in_progress 01-02, done 01-03 for 2 points
                    Issue 2: ready 01-01, in_progress 01-02, done 01-04 for 5 points
                    Issue 3: ready 01-01, in_progress 01-04, done 01-10 for 2 points
               """
                        )
                        engine
                    }

                    single<HttpClientEngine> {
                        get<MockedEngineWrapper>().engine
                    }

                    single<HandleUserProfile> {
                        UserProfileHandler(get())
                    }

                    single<UserSettingsRepository> {
                        object : UserSettingsRepository {
                            override fun save(userSettings: UserSettings) {
                                TODO("Not yet implemented")
                            }

                            override fun find(user: ID<User>): UserSettings? {
                                return UserSettings(ID.from("user"), ID.from("apollo"))
                            }

                        }
                    }

                    single<StatusRepository> {
                        InMemoryStatusRepository(
                            mapOf(
                                anId.ofApollo<Project>() to listOf(
                                    Status.OutOfScope,
                                    Status.Backlog,
                                    Status.Planned,
                                    Status.Ready,
                                    Status.InProgress,
                                    Status.isDone,
                                    Status.Closed.default,
                                )
                            )
                        )
                    }
                },
            )
        }
        configurationRepository.save(
            ID.from("apollo"),
            configApollo
        )
    }

    @Test
    @Order(1)
    fun `update cache`() = runTest {
        val result = CLI.test("teamwork update")
        result.statusCode shouldBe 0

        eventRepository.all(anId.ofApollo()).shouldContainExactly(
            *"""
            IssueAdded(date=2024-01-01T12:00, entityId=1, project=apollo, title=Issue 1, complexity=PointsOfComplexity(value=2)),
            IssueMoved(date=2024-01-01T12:00, entityId=1, project=apollo, newStatus=Ready (Open)),
            IssueMoved(date=2024-01-02T12:00, entityId=1, project=apollo, newStatus=in_progress (Open)),
            IssueMoved(date=2024-01-03T12:00, entityId=1, project=apollo, newStatus=Done (Closed)),
            IssueAdded(date=2024-01-01T12:00, entityId=2, project=apollo, title=Issue 2, complexity=PointsOfComplexity(value=5)),
            IssueMoved(date=2024-01-01T12:00, entityId=2, project=apollo, newStatus=Ready (Open)),
            IssueMoved(date=2024-01-02T12:00, entityId=2, project=apollo, newStatus=in_progress (Open)),
            IssueMoved(date=2024-01-04T12:00, entityId=2, project=apollo, newStatus=Done (Closed)),
            IssueAdded(date=2024-01-01T12:00, entityId=3, project=apollo, title=Issue 3, complexity=PointsOfComplexity(value=2)),
            IssueMoved(date=2024-01-01T12:00, entityId=3, project=apollo, newStatus=Ready (Open)),
            IssueMoved(date=2024-01-04T12:00, entityId=3, project=apollo, newStatus=in_progress (Open)),
            IssueMoved(date=2024-01-10T12:00, entityId=3, project=apollo, newStatus=Done (Closed)),
            IterationStarted(date=2024-01-01T00:00, entityId=0, project=apollo, name=Sprint 0),
            IterationFinished(date=2024-01-10T23:59:59, entityId=0, project=apollo)
            """.fromLogsToEvents().toTypedArray()
        )
    }

    @Test
    fun `get project metrics`() {
        CLI.test("teamwork update")
        val result = CLI.test("teamwork report -from 2024-01-01 -to 2024-08-31")
        result.statusCode shouldBe 0
    }

    @Test
    fun `change configuration`() {
        val result =
            CLI.test("teamwork configure --type Jira --url http://foobar.new --username aNewUsername --token aNewToken --board newBoard")
        result.statusCode shouldBe 0
        val configuration = configurationRepository.find(ID.from("apollo"))

        configuration shouldBe BoardConfiguration.Valid(
            project = ID.from("apollo"),
            type = domain.teamwork.configuration.model.BoardType.Jira,
            url = "http://foobar.new",
            board = "newBoard",
            login = Login("aNewUsername", "aNewToken")
        )
    }

    @Test
    fun `list status`() {
        val result = CLI.test("teamwork status")
        result.statusCode shouldBe 0
    }

    @Test
    fun `update status`() {
        val result = CLI.test("teamwork status update -waiting created")
        result.statusCode shouldBe 0

        statusRepository.find(anId.ofApollo(), ID.from("created"))!!.shouldBeTypeOf<domain.teamwork.status.model.Status.Waiting>()
    }

    @Test
    fun `get issue stats report`() {
        val result = CLI.test("teamwork report -issue 'Issue 1'")
        result.statusCode shouldBe 0
    }
}

val domain.teamwork.status.model.Status.Companion.Backlog: domain.teamwork.status.model.Status
    get() = domain.teamwork.status.model.Status.Planned(label = Label.from("backlog"), id = ID.from("backlog"))
val domain.teamwork.status.model.Status.Companion.Planned: domain.teamwork.status.model.Status
    get() = domain.teamwork.status.model.Status.Planned(label = Label.from("Created"), id = ID.from("created"))
val domain.teamwork.status.model.Status.Companion.Ready: domain.teamwork.status.model.Status
    get() = domain.teamwork.status.model.Status.Waiting(label = Label.from("Ready"), id = ID.from("ready"))
val domain.teamwork.status.model.Status.Companion.InProgress: domain.teamwork.status.model.Status
    get() = domain.teamwork.status.model.Status.Working(label = Label.from("In Progress"), id = ID.from("inprogress"))
val domain.teamwork.status.model.Status.Companion.isDone: domain.teamwork.status.model.Status
    get() = domain.teamwork.status.model.Status.Done(label = Label.from("Done"), id = ID.from("done"))
val domain.teamwork.status.model.Status.Companion.Closed: domain.teamwork.status.model.Status
    get() = domain.teamwork.status.model.Status.Closed(label = Label.from("Closed"), id = ID.from("closed"))