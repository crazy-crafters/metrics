package cli.user

import cli.CLI
import com.github.ajalt.clikt.testing.test
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import infra.project.projectModule
import domain.user.UserSettingsRepository
import infra.user.providers.InMemoryUserSettingsRepository
import infra.user.providers.UserSettingsDB
import infra.user.userModule
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class ProjectDTOCycleCommandTest : KoinTest {
    private val repository: UserSettingsRepository by inject()

    private val outContent = ByteArrayOutputStream()
    private val errContent = ByteArrayOutputStream()
    private val originalOut: PrintStream = System.out
    private val originalErr: PrintStream = System.err

    @BeforeEach
    fun setUpStreams() {
        System.setOut(PrintStream(outContent))
        System.setErr(PrintStream(errContent))
        initialize()
    }

    @AfterEach
    fun restoreStreams() {
        System.setOut(originalOut)
        System.setErr(originalErr)
        (repository as InMemoryUserSettingsRepository).clear()
        stopKoin()
    }

    private fun initialize() {
        startKoin {
            modules(
                userModule(),
                projectModule(),
                module {
                    allowOverride(true)
                },
            )
        }
    }

    @Test
    fun `save user settings`() {
        val result = CLI.test("set -project 'Foobar'")
        result.statusCode shouldBe 0

        (repository as InMemoryUserSettingsRepository).items.shouldContain(
            UserSettingsDB(
                user = System.getProperty("user.name"),
                project = "Foobar"
            )
        )
    }
}