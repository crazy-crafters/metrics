package cli.project

import cli.CLI
import com.github.ajalt.clikt.testing.test
import domain.project.model.ProjectAssigned
import domain.project.model.ProjectEnded
import domain.project.model.ProjectEvent
import domain.project.model.ProjectStarted
import domain.project.spi.ProjectEventRepository
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.squad1
import infra.project.projectModule
import infra.project.providers.InMemoryProjectEventRepository
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.time.Duration

class ProjectCycleCommandTest : KoinTest {
    private val eventRepository: ProjectEventRepository by inject()

    private val outContent = ByteArrayOutputStream()
    private val errContent = ByteArrayOutputStream()
    private val originalOut: PrintStream = System.out
    private val originalErr: PrintStream = System.err

    @BeforeEach
    fun setUpStreams() {
        System.setOut(PrintStream(outContent))
        System.setErr(PrintStream(errContent))
        initialize()
    }

    @AfterEach
    fun restoreStreams() {
        System.setOut(originalOut)
        System.setErr(originalErr)
        (eventRepository as InMemoryProjectEventRepository).clear()
        stopKoin()
    }

    private fun initialize() {
        startKoin {
            modules(
                projectModule(),
                module {
                    allowOverride(true)
                },
            )
        }
    }

    @Test
    fun `start a new project`() {
        val result = CLI.test("project start -name 'Foo bar' -description 'desc foo'")
        result.statusCode shouldBe 0

        eventRepository.all().filterIsInstance<ProjectStarted>().map { it.name.value to it.description }
            .shouldContain("Foo bar" to "desc foo")
    }

    @Test
    fun `stop a project`() {
        eventRepository.save(
            ProjectStarted(
                ID.from("foobar"),
                Name.from("Foo Bar"),
                "",
                DateTime.now()
            )
        )

        val result = CLI.test("project stop -id foobar")
        result.statusCode shouldBe 0

        eventRepository.all().filterIsInstance<ProjectEnded>().map { it.id.value }
            .shouldContain("foobar")
    }

    @Test
    fun `assign a project`() {
        eventRepository.save(
            ProjectStarted(
                ID.from("foobar"),
                Name.from("Foo Bar"),
                "",
                DateTime.now()
            )
        )

        val result = CLI.test("project assign -project foobar -squad spam")
        result.statusCode shouldBe 0

        eventRepository.all().filterIsInstance<ProjectAssigned>().map { it.id.value to it.squad.value }
            .shouldContain("foobar" to "spam")
    }

    @Test
    fun `display a project`() {
        eventRepository.saveAll(
            ProjectStarted(
                ID.from("foobar"),
                Name.from("Foo Bar"),
                "",
                DateTime.now().plus(Duration.ofDays(-30))
            ),
            ProjectAssigned(
                ID.from("foobar"),
                date = DateTime.now().plus(Duration.ofDays(-20)),
                squad = squad1.id
            ),
            ProjectEnded(
                ID.from("foobar"),
                date = DateTime.now().plus(Duration.ofDays(-5)),
            )
        )

        val result = CLI.test("project display -id foobar")
        result.statusCode shouldBe 0
    }

    @Test
    fun `list projects`() {
        eventRepository.saveAll(
            ProjectStarted(
                ID.from("foobar"),
                Name.from("Foo Bar"),
                "",
                DateTime.now()
            ),
            ProjectStarted(
                ID.from("baz"),
                Name.from("Baz"),
                "",
                DateTime.now()
            ),
            ProjectStarted(
                ID.from("spam"),
                Name.from("Spam"),
                "",
                DateTime.now()
            )
        )

        val result = CLI.test("project list")
        result.statusCode shouldBe 0
    }
}

fun ProjectEventRepository.saveAll(vararg events: ProjectEvent) = events.forEach {
    save(it)
}