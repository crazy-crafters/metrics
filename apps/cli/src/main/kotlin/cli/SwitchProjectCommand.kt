package cli

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.core.ProgramResult
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.mordant.rendering.TextColors
import domain.project.api.GetProject
import domain.project.model.Project
import domain.project.model.ProjectResult
import domain.sharedkernel.model.ID
import domain.user.HandleUserProfile
import domain.user.User
import domain.user.UserProfile
import domain.user.UserSettings
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class SwitchProjectCommand : CliktCommand(name = "switch"), KoinComponent {
    override fun help(context: Context) = "start a new project"

    private val id by option("-id", help = "Project ID").required()

    private val getProject: GetProject by inject()
    private val handleUserProfile: HandleUserProfile by inject()

    override fun run() {
        val user = ID.from<User>(System.getProperty("user.name"))
        val project = ID.from<Project>(id)
        getProject.information(project)
        when (getProject.information(project)) {
            is ProjectResult.Found -> {
                val settings = when (val it = handleUserProfile.get(user)) {
                    is UserProfile.Found -> it.settings.copy(project = ID.from(id))
                    is UserProfile.NotFound -> UserSettings(user, project = ID.from(""))
                }
                handleUserProfile.save(settings)
            }

            is ProjectResult.NotFound.NoEvents, is ProjectResult.NotFound.NoStartEvent -> {
                println(TextColors.red("The project '$id' does not exist"))
                throw ProgramResult(-1)
            }
        }
    }
}