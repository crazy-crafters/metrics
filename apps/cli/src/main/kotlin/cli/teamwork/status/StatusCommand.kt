package cli.teamwork.status

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import org.koin.core.component.KoinComponent

class StatusCommand private constructor() : CliktCommand(name = "status"), KoinComponent {
    override fun run() {}

    companion object {
        operator fun invoke() = StatusCommand()
            .subcommands(
                UpdateStatusCommand(),
                DisplayStatusCommand()
            )
    }
}

