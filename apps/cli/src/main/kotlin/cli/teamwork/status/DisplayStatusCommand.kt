package cli.teamwork.status

import cli.teamwork.reports.display.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.table
import com.github.ajalt.mordant.terminal.Terminal
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.api.ManageStatus
import domain.user.HandleUserProfile
import domain.user.UserProfile

class DisplayStatusCommand : CliktCommand(name = "list"), KoinComponent {
    private val getUserProfile: HandleUserProfile by inject()
    private val manageStatus: ManageStatus by inject()

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when(profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }.remap<Project>()

        val s = manageStatus.statuses(project)

        val terminal = Terminal()

        terminal.println(table {
            body {

                s.forEach { status ->
                    row {
                        cell(status.id) { cellBorders = Borders.NONE }
                        cell(status.label) { cellBorders = Borders.NONE }
                        cell(status::class.simpleName.toString()) { cellBorders = Borders.NONE }
                    }
                }
            }
        })
    }
}