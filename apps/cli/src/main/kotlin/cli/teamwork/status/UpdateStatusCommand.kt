package cli.teamwork.status

import cli.teamwork.reports.display.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.split
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.status.api.ManageStatus
import domain.user.HandleUserProfile
import domain.user.UserProfile

class UpdateStatusCommand : CliktCommand(name = "update"), KoinComponent {
    private val waiting by option("-waiting", help = "assign status to Waiting state").split(",").default(emptyList())
    private val working by option("-working", help = "assign status to Working state").split(",").default(emptyList())
    private val closed by option("-closed", help = "assign status to Closed state").split(",").default(emptyList())
    private val done by option("-done", help = "assign status to Done state").split(",").default(emptyList())

    private val manageStatus: ManageStatus by inject()
    private val getUserProfile: HandleUserProfile by inject()

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when(profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }.remap<Project>()

        if (waiting.isEmpty() && working.isEmpty() && closed.isEmpty() && done.isEmpty()) {
            throw CliktError()
        }
        waiting.map { ID.from<domain.teamwork.status.model.Status>(it) }.forEach {
            manageStatus.update(project, it, domain.teamwork.status.model.Status.State.Waiting)
        }
        working.map { ID.from<domain.teamwork.status.model.Status>(it) }.forEach {
            manageStatus.update(project, it, domain.teamwork.status.model.Status.State.Working)
        }
        closed.map { ID.from<domain.teamwork.status.model.Status>(it) }.forEach {
            manageStatus.update(project, it, domain.teamwork.status.model.Status.State.Closed)
        }
        done.map { ID.from<domain.teamwork.status.model.Status>(it) }.forEach {
            manageStatus.update(project, it, domain.teamwork.status.model.Status.State.Done)
        }
    }
}