package cli.teamwork

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.subcommands
import org.koin.core.component.KoinComponent
import cli.teamwork.board.ConfigureBoardCommand
import cli.teamwork.board.UpdateBoardCommand
import cli.teamwork.reports.DeliveryReportCommand
import cli.teamwork.reports.DisplayBurnChartCommand
import cli.teamwork.reports.SnapshotCommand
import cli.teamwork.reports.ToReviewReportCommand
import cli.teamwork.reports.DistributionReportCommand
import cli.teamwork.status.StatusCommand
import com.github.ajalt.clikt.core.Context

class TeamworkCommand private constructor() : CliktCommand(name = "teamwork"), KoinComponent {

    override val allowMultipleSubcommands = true
    override fun help(context: Context) = """Manage the Board"""

    override fun run() {}

    companion object {
        operator fun invoke() = TeamworkCommand()
            .subcommands(
                UpdateBoardCommand(),
                ConfigureBoardCommand(),
                DeliveryReportCommand(),
                DisplayBurnChartCommand(),
                StatusCommand(),
                SnapshotCommand(),
                ToReviewReportCommand(),
                DistributionReportCommand()
            )
    }
}