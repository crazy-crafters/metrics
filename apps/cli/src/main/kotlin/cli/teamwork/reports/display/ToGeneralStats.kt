package cli.teamwork.reports.display

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.rendering.TextStyles
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.table
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.report.model.GeneralStatsReport
import java.time.temporal.ChronoUnit

fun CliktCommand.toGeneralStats(report: GeneralStatsReport.Valid) {
    val historyLength = report.durationInDays()
    fun Double.rounded() = (this * 1000).toLong().toDouble() / 1000.0
    val velocityCount = (report.done.count.toDouble() / historyLength).rounded()
    val velocityPoints = (report.done.points.value.toDouble() / historyLength).rounded()

    println(TextStyles.bold("General Statistics"))
    println(table {
        header {
            row("", "Issues", "Points") { cellBorders = Borders.NONE }
        }
        body {
            row {
                cell(TextColors.green("Done")) { cellBorders = Borders.RIGHT }
                cell(TextColors.green("${report.done.count}")) { cellBorders = Borders.NONE }
                cell(TextColors.green("${report.done.points.value}")) { cellBorders = Borders.NONE }
            }
            row {
                cell(TextColors.red("In Progress")) { cellBorders = Borders.RIGHT }
                cell(TextColors.red("${report.unfinished.count}")) { cellBorders = Borders.NONE }
                cell(TextColors.red("${report.unfinished.points.value}")) { cellBorders = Borders.NONE }
            }
            row {
                cell(TextColors.yellow("Canceled")) { cellBorders = Borders.RIGHT }
                cell(TextColors.yellow("${report.closed.count - report.done.count}")) { cellBorders = Borders.NONE }
                cell(TextColors.yellow("${report.closed.points.value - report.done.points.value}")) { cellBorders = Borders.NONE }
            }
            row {
                cell("To Do") { cellBorders = Borders.RIGHT }
                cell("${report.opened.count - report.unfinished.count}") { cellBorders = Borders.NONE }
                cell("${report.opened.points.value - report.unfinished.points.value}") { cellBorders = Borders.NONE }
            }
            row {
                cell("") { cellBorders = Borders.RIGHT }
                cell("") { cellBorders = Borders.NONE }
                cell("") { cellBorders = Borders.NONE }
            }
            row {
                cell("Closed") { cellBorders = Borders.RIGHT }
                cell("${report.closed.count}") { cellBorders = Borders.NONE }
                cell("${report.closed.points.value}") { cellBorders = Borders.NONE }
            }
            row {
                cell("Opened") { cellBorders = Borders.RIGHT }
                cell("${report.opened.count}") { cellBorders = Borders.NONE }
                cell("${report.opened.points.value}") { cellBorders = Borders.NONE }
            }
            row {
                cell("Velocity (/day)") { cellBorders = Borders.RIGHT }
                cell("$velocityCount") { cellBorders = Borders.NONE }
                cell("$velocityPoints") { cellBorders = Borders.NONE }
            }
        }
    })

}

fun CliktCommand.toConfiguration(conf: BoardConfiguration) {
    when (conf) {
        is BoardConfiguration.None -> println(TextColors.gray("No board has been configured, yet"))
        is BoardConfiguration.Valid ->
            println("The ${(TextStyles.underline(conf.type.toString()))} board has been ${
                TextStyles.bold(TextColors.brightGreen("configured"))
            }")
    }
}

fun GeneralStatsReport.Valid.durationInDays(): Double {
    return from.until(to, ChronoUnit.DAYS).toDouble()
}
