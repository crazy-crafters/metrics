package cli.teamwork.reports

import cli.teamwork.reports.display.red
import cli.teamwork.reports.display.toChart
import cli.teamwork.reports.display.toConfiguration
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.options.*
import com.github.ajalt.clikt.parameters.transform.TransformContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.report.api.GetReport
import infra.teamwork.UndefinedStatusException
import domain.user.HandleUserProfile
import domain.user.UserProfile


class DisplayBurnChartCommand : CliktCommand(name = "burnchart"), KoinComponent {
    override fun help(context: Context) = "Display a burn chart between two dates"

    private val from by option("-from", help = "From this date").dateTime().required()
    private val to by option("-to", help = "To that date").dateTime().required()

    private val points by option(
        "-points",
        help = "Display the burn chart by using points of complexity instead of issues count"
    ).flag(default = false)

    private val deliveryController: GetReport by inject()
    private val handleConfiguration: HandleConfiguration by inject()
    private val getUserSettings: HandleUserProfile by inject()


    override fun run() {
        val projectId = when (val it = getUserSettings.get(ID.from(System.getProperty("user.name")))) {
            is UserProfile.Found -> it.settings.project
            is UserProfile.NotFound -> {
                println(red("No profile found for ${System.getProperty("user.name")}"))
                return
            }
        }
        try {
            val boardConfig = handleConfiguration.get(ID.from(projectId.value))

            val burnChart = deliveryController.burnChart(ID.from(projectId.value), from, to)

            toConfiguration(boardConfig)

            println("From $from")
            println("To   $to")
            println("")

            toChart(burnChart)

        } catch (e: ProjectNotFoundException) {
            println(red(e.message!!))
        } catch (e: UndefinedStatusException) {
            println(red(e.message!!))
        }
    }
}


fun RawOption.dateTime(acceptsValueWithoutName: Boolean = false): NullableOption<DateTime, DateTime> {
    val conversion: TransformContext.(String) -> DateTime =
        { DateTime.parse(it) }
    return convert({ localization.intMetavar() }, conversion = conversion)
        .copy(acceptsNumberValueWithoutName = acceptsValueWithoutName)
}

inline fun <reified T> RawOption.id(acceptsValueWithoutName: Boolean = false): NullableOption<ID<T>, ID<T>> {
    val conversion: TransformContext.(String) -> ID<T> =
        { ID.from<T>(it) }
    return convert({ localization.intMetavar() }, conversion = conversion)
        .copy(acceptsNumberValueWithoutName = acceptsValueWithoutName)
}