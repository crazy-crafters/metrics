package cli.teamwork.reports

import cli.teamwork.reports.display.bold
import cli.teamwork.reports.display.println
import cli.teamwork.reports.display.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.options.split
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.widgets.Text
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.project.model.Project
import domain.teamwork.issue.api.GetIssue
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.Route
import domain.teamwork.issue.model.Tag
import domain.user.HandleUserProfile
import domain.user.UserProfile

class SnapshotCommand : CliktCommand(name = "snapshot"), KoinComponent {
    private val controller: GetIssue by inject()
    private val getUserProfile: HandleUserProfile by inject()

    private val from by option("-from", help = "From this date").dateTime().required()
    private val to by option("-to", help = "To that date").dateTime().required()
    val tags by option("-tags").split(",")

    private val prettyprint by option("-noprettyprint", help = "enable text formatting").flag(default = false)

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when (profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }.remap<Project>()

        try {
            val snapshot = controller.snapshot(
                project = project,
                from = from,
                to = to,
                tags = tags?.map { Tag.on(it) } ?: emptyList())

            snapshot.issues.forEach {
                displayIssue(it)
            }
        } catch (e: ProjectNotFoundException) {
            println(
                Text(TextColors.brightRed(e.message!!))
            )
            throw CliktError()
        }
    }
}

fun CliktCommand.displayIssue(issue: Issue, prettyprint: Boolean = true) {
    println("ID: ${bold(issue.id.toString(), prettyprint = prettyprint)}")
    println("Title: ${bold(issue.title.toString(), prettyprint = prettyprint)}")
    println("Lead Time: ${bold(issue.leadTime.toString(), prettyprint = prettyprint)}")
    println("Cycle Time: ${bold(issue.cycleTime.toString(), prettyprint = prettyprint)}")
    println("Timeline: ${issue.route().toRepr()}")
    println("Tags: ${issue.tags.tags.joinToString(", ") { it.value }}")
    println("")
}

fun Route.toRepr() = this.steps.joinToString {
    "${it.date.toDateString()} - ${it.status.label}"
}