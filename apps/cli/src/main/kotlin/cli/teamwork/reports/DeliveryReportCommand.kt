package cli.teamwork.reports

import cli.teamwork.reports.display.*
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.groups.cooccurring
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.options.split
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.widgets.Text
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.project.model.Project
import domain.teamwork.issue.api.GetIssue
import domain.teamwork.issue.model.Issue
import domain.teamwork.issue.model.Tag
import domain.teamwork.report.api.GetReport
import domain.teamwork.report.model.GeneralStatsReport
import infra.teamwork.UndefinedStatusException
import domain.user.HandleUserProfile
import domain.user.UserProfile

class DeliveryReportCommand : CliktCommand(name = "report"), KoinComponent {
    override fun help(context: Context) =
        "Display metrics (e.g., number of done, open, velocity, estimated completion) between two dates"

    class IntervalOptions : OptionGroup() {
        val from by option("-from", help = "From this date").required()
        val to by option("-to", help = "To that date").required()
    }

    private val tags by option("-tags").split(",")

    private val interval by IntervalOptions().cooccurring()
    private val issue by option("-issue", help = "Issue ID to report")

    private val handleConfiguration: HandleConfiguration by inject()
    private val getReport: GetReport by inject()
    private val getIssue: GetIssue by inject()

    private val getUserProfile: HandleUserProfile by inject()

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when(profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }

        if (issue == null && interval == null)
            throw CliktError()

        try {
            val boardConfig = handleConfiguration.get(project.remap())

            when(boardConfig) {
                BoardConfiguration.None -> throw ProjectNotFoundException("default")
                is BoardConfiguration.Valid -> toConfiguration(boardConfig)
            }



            if (issue == null) {
                displayGeneralStatsReport(project.remap())
                return
            }

            displayIssueStatsReport(ID.from(issue!!), project.remap())


        } catch (e: ProjectNotFoundException) {
            println(
                Text(TextColors.brightRed(e.message!!))
            )
            throw e
        } catch (e: UndefinedStatusException) {
            println(
                Text(TextColors.brightRed("Error: ${e.message!!}"))
            )
            println(
                Text(TextColors.brightRed("Please, use the command status -update"))
            )
        }
    }

    private fun displayIssueStatsReport(issue: ID<Issue>, projectId: ID<Project>) {
        val report = getIssue.summary(projectId, issue)
        toIssueReport(report)
    }

    private fun displayGeneralStatsReport(projectId: ID<Project>) {
        try {
            val from = DateTime.parse(interval!!.from)
            val to = DateTime.parse(interval!!.to)

            val generalStatsReport = getReport.generalStats(projectId, from, to, tags=tags?.map { Tag.on(it) } ?: emptyList())

            println("From $from")
            println("To   $to")
            println("")

            /*if (generalStatsReport is GeneralStatsReportDTO.Valid) {
                toGeneralStats(generalStatsReport)
            }*/
            when(generalStatsReport) {
                is GeneralStatsReport.Valid -> toGeneralStats(generalStatsReport)
                is GeneralStatsReport.NotFound -> {}
            }

            val flowReport = getReport.flow(projectId, from, to, tags=tags?.map { Tag.on(it) } ?: emptyList())

            toFlow(flowReport)
            /*if (flowReport is FlowReportDTO.Valid) {
                println("")
                toFlow(flowReport)
            }*/
        } catch (e: ProjectNotFoundException) {
            println(
                Text(TextColors.brightRed(e.message!!))
            )
            throw e
        } catch (e: UndefinedStatusException) {
            println(
                Text(TextColors.brightRed("Error: ${e.message!!}"))
            )
            println(
                Text(TextColors.brightRed("Please, use the command status -update"))
            )
        }
    }

}


