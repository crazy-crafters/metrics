package cli.teamwork.reports

data class ProjectNotFoundException(val id: String) : RuntimeException("The project $id does not exist")