package cli.teamwork.reports.display

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.rendering.TextStyles
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.table
import domain.teamwork.report.model.FlowReport


fun CliktCommand.toFlow(report: FlowReport) {

    when(report) {
        is FlowReport.NoReport -> return
        is FlowReport.Valid -> {
            println(TextStyles.bold("Flow Metrics"))
            println(
                table {
                    body {
                        row {
                            cell("Flow time") { cellBorders = Borders.NONE }
                            cell(report.leadTimes().toString()) { cellBorders = Borders.NONE }
                        }
                        row {
                            cell("Efficiency") { cellBorders = Borders.NONE }
                            cell(report.efficiency().toString())  {  cellBorders = Borders.NONE}
                        }
                        row {
                            cell("Cancel Time") { cellBorders = Borders.NONE }
                            cell(report.cancelLeadTimes().toString()) { cellBorders = Borders.NONE }
                        }
                        row {
                            cell("WIP") { cellBorders = Borders.NONE }
                            cell("%.2f".format(report.wip().average)) { cellBorders = Borders.NONE }
                        }
                    }
                }
            )
        }
    }

}