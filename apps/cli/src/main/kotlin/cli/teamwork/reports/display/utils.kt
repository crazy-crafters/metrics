package cli.teamwork.reports.display

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.terminal
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.rendering.TextStyles
import com.github.ajalt.mordant.rendering.Widget
import com.github.ajalt.mordant.terminal.Terminal


fun Terminal.printAll(widgets: List<Widget>) {
    widgets.forEach { widget ->
        println(widget)
    }
}

fun CliktCommand.println(w: Widget) {
    this.currentContext.terminal.println(w)
}

fun CliktCommand.bold(s: String, prettyprint: Boolean=true) = if (prettyprint) s else TextStyles.bold(s)
fun CliktCommand.italic(s: String, prettyprint: Boolean=true) = if (prettyprint) s else TextStyles.italic(s)

fun CliktCommand.green(s: String, prettyprint: Boolean=true) = if (prettyprint) s else TextColors.green(s)
fun CliktCommand.red(s: String, prettyprint: Boolean=true) = if (prettyprint) s else TextColors.red(s)