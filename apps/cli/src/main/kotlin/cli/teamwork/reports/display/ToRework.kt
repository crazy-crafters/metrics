package cli.teamwork.reports.display

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.rendering.TextStyles
import domain.teamwork.report.model.ReviewReport

fun CliktCommand.toRework(report: ReviewReport) {
    when (report) {
        is ReviewReport.NoReport -> println(red("There is no Review report"))
        is ReviewReport.Valid -> when {
            report.withRework.isEmpty() -> println(TextStyles.bold("There is no issue to review"))
            else -> {
                println(TextStyles.bold("Issues to review"))
                report.withRework.sortedBy { it.id.toString() }.forEach {
                    println("${it.id} - ${it.title}: ${it.route.steps.joinToString()}")
                }
            }
        }
    }
}