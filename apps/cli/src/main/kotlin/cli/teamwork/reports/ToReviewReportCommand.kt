package cli.teamwork.reports

import cli.teamwork.reports.display.println
import cli.teamwork.reports.display.red
import cli.teamwork.reports.display.toConfiguration
import cli.teamwork.reports.display.toRework
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.CliktError
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.groups.cooccurring
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.widgets.Text
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.project.model.Project
import domain.teamwork.report.api.GetReport
import infra.teamwork.UndefinedStatusException
import domain.user.HandleUserProfile
import domain.user.UserProfile

class ToReviewReportCommand : CliktCommand(name = "review"), KoinComponent {
    override fun help(context: Context) = "List issues to review between two dates"

    class IntervalOptions : OptionGroup() {
        val from by option("-from", help = "From this date").dateTime().required()
        val to by option("-to", help = "To that date").dateTime().required()
    }

    private val interval by IntervalOptions().cooccurring()
    //private val deliveryController: DeliveryController by inject()
    private val handleConfiguration: HandleConfiguration by inject()
    private val getReport: GetReport by inject()

    private val getUserProfile: HandleUserProfile by inject()

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when(profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }.remap<Project>()

        if (interval == null)
            throw CliktError()

        try {
            val boardConfig = handleConfiguration.get(project)
            if (boardConfig is BoardConfiguration.None) {
                throw ProjectNotFoundException("default")
            }
            toConfiguration(boardConfig)

            displayGeneralStatsReport(project)

        } catch (e: ProjectNotFoundException) {
            println(
                Text(TextColors.brightRed(e.message!!))
            )
            throw e
        } catch (e: UndefinedStatusException) {
            println(
                Text(TextColors.brightRed("Error: ${e.message!!}"))
            )
            println(
                Text(TextColors.brightRed("Please, use the command status -update"))
            )
        }
    }

    private fun displayGeneralStatsReport(projectId: ID<Project>) {
        try {
            val from = interval!!.from
            val to = interval!!.to
            println("From $from")
            println("To   $to")
            println("")
            toRework(getReport.rework(projectId, from, to))
        } catch (e: ProjectNotFoundException) {
            println(
                Text(TextColors.brightRed(e.message!!))
            )
            throw e
        } catch (e: UndefinedStatusException) {
            println(
                Text(TextColors.brightRed("Error: ${e.message!!}"))
            )
            println(
                Text(TextColors.brightRed("Please, use the command status -update"))
            )
        }
    }
}


