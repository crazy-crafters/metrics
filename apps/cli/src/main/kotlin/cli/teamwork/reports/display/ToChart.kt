package cli.teamwork.reports.display

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.rendering.TextStyles
import domain.teamwork.report.model.BurnChart
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.roundToInt

fun CliktCommand.toChart(chart: BurnChart) {
    when (chart) {
        BurnChart.Empty -> return
        is BurnChart.Found -> {
            val burnup = (TextColors.black on TextColors.green)
            val zero = (TextColors.green + TextStyles.underline)

            val max = chart.values.maxOf { it.value.points }.value
            val min = chart.values.minOf { it.value.points }.value

            val barWidth = "$max".length

            val scale: Int = max(
                1,
                ((max - min).toDouble() / 10.0).roundToInt()
            )


            val empty = " ".repeat(barWidth)
            (min..max step scale).map {
                var line = "${it.toString().padStart(barWidth, ' ')} │ " +
                        chart.values.map { it.value.points.value }.joinToString(" ") { value ->
                            if (value >= it && abs(value - it) <= scale / 2.0) {
                                if (it != 0)
                                    burnup(value.toString().padStart(barWidth, ' '))
                                else
                                    zero(empty)
                            } else if (value > it) {
                                burnup(empty)
                            } else {
                                empty
                            }
                        }
                line
            }.reversed().forEach {
                println(it)
            }

            val haxisDays =
                List(chart.values.size) { i -> i }.joinToString("") { " ${it.toString().padStart(2, '0').padStart(barWidth, ' ')}" }

            println(" ".repeat(barWidth + 1) + " " + "─".repeat(haxisDays.length + 1))
            println(" ".repeat(barWidth + 2) + haxisDays)
        }
    }
}