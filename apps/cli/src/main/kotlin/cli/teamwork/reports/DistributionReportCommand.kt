package cli.teamwork.reports

import cli.teamwork.reports.display.println
import cli.teamwork.reports.display.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.table.table
import com.github.ajalt.mordant.widgets.Text
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.statistics.Distribution
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.project.model.Project
import domain.teamwork.report.api.GetReport
import infra.teamwork.UndefinedStatusException
import domain.user.HandleUserProfile
import domain.user.UserProfile

class DistributionReportCommand : CliktCommand(name = "distribution"), KoinComponent {
    override fun help(context: Context) = "Display the leadtimes distribution of issues between two dates"

    val from by option("-from", help = "From this date").dateTime().required()
    val to by option("-to", help = "To that date").dateTime().required()


    private val deliveryController: GetReport by inject()
    private val handleConfiguration: HandleConfiguration by inject()

    private val getUserProfile: HandleUserProfile by inject()

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when(profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }.remap<Project>()

        try {
            val boardConfig = handleConfiguration.get(project)
            if (boardConfig is BoardConfiguration.None) {
                throw ProjectNotFoundException("default")
            }
            val histogram = deliveryController.leadTimes(project, from, to)
            toDistribution(histogram)
        } catch (e: ProjectNotFoundException) {
            println(
                Text(TextColors.brightRed(e.message!!))
            )
            throw e
        } catch (e: UndefinedStatusException) {
            println(
                Text(TextColors.brightRed("Error: ${e.message!!}"))
            )
            println(
                Text(TextColors.brightRed("Please, use the command status -update"))
            )
        }
    }
}

fun CliktCommand.toDistribution(distribution: Distribution) {

    val values = listOf(40, 50, 60, 70, 80).map { it to distribution.at(it.toDouble() / 100.0) }
    println(table {
        header {
            row(*values.map { "${it.first} %"}.toTypedArray())
        }
        body {
            row(*values.map { it.second}.map { if (it == null) "-" else "$it days" }.toTypedArray())
        }
    })

}

