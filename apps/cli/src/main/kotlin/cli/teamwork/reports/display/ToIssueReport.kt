package cli.teamwork.reports.display

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.table
import domain.teamwork.issue.model.IssueSummary

fun CliktCommand.toIssueReport(report: IssueSummary) {
    when(report) {
        IssueSummary.NoSummary -> return
        is IssueSummary.Valid ->  {
            println("Title: ${bold(report.title.toString())}")
            println("Lead Time: ${bold(report.leadTime.toString().format())}")
            println("Efficiency: ${bold("${report.efficiency} %")}")
            println()
            println("Timeline:")
            println(
                table {
                    body {
                        val timeline = report.timeline.steps.map { it.date.toString() to it.status }
                        timeline.forEach {(date, status) ->
                            val state = status::class.simpleName.toString()
                            row {
                                cell(date) { cellBorders = Borders.NONE }
                                cell(status) { cellBorders = Borders.NONE }
                                cell(state) { cellBorders = Borders.NONE }
                            }
                        }
                    }
                })
        }
    }
}