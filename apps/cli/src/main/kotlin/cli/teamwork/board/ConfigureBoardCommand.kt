package cli.teamwork.board

import cli.teamwork.reports.display.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.Login
import domain.user.HandleUserProfile
import domain.user.UserProfile

class ConfigureBoardCommand : CliktCommand(name = "configure"), KoinComponent {
    private val type by option("-t", "--type", help = "Type of the board (Jira or Azure)").required()
    private val url by option("-u", "--url", help = "URL of the board").required()
    private val username by option("-un", "--username", help = "Username").required()
    private val token by option("-token", "--token", help = "Token").required()
    private val boardId by option("-b", "--board", help = "ID of the board").required()

    private val controller: HandleConfiguration by inject()

    private val getUserProfile: HandleUserProfile by inject()

    override fun run() {
        val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))
        val project = when(profile) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return
            }
        }
        controller.save(
            project.remap(),
            BoardConfiguration.Valid(
                type = domain.teamwork.configuration.model.BoardType.valueOf(type),
                url = url,
                login = Login(user = username, pass = token),
                board = boardId,
                project = project.remap()
            )
        )
    }
}