package cli.teamwork.board

import cli.teamwork.reports.display.red
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.mordant.animation.coroutines.animateInCoroutine
import com.github.ajalt.mordant.animation.progress.advance
import com.github.ajalt.mordant.terminal.Terminal
import com.github.ajalt.mordant.widgets.progress.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import infra.teamwork.ScanBoard
import domain.user.HandleUserProfile
import domain.user.UserProfile

class UpdateBoardCommand : CliktCommand(name = "update"), KoinComponent {
    override fun help(context: Context) = "Update information and metrics by requesting the remote board for all issues and events"
    private val worker: ScanBoard by inject()
    private val getUserProfile: HandleUserProfile by inject()

    override fun run() = runBlocking {
        val project = when(val profile = getUserProfile.get(ID.from(System.getProperty("user.name")))) {
            is UserProfile.Found -> profile.settings.project
            UserProfile.NotFound -> {
                println(red("No configured profile"))
                return@runBlocking
            }
        }
        val terminal = Terminal()
        val progress = progressBarLayout {
            marquee(terminal.theme.warning("Issue Events"), width = 15)
            percentage()
            progressBar()
            completed(style = terminal.theme.success)
        }.animateInCoroutine(terminal)


        val display = GlobalScope.async {
            println("Job 1 started")
            progress.execute()
        }

        val process = GlobalScope.async {
            println("Job 2 started")
            worker.start(
                id = project.remap(),
                progression = { progression ->
                    progress.update { total = progression.total.toLong() }
                    progress.advance(progression.current.toLong())
                },
                eventSink = {}
            )
        }
        process.await()
    }
}