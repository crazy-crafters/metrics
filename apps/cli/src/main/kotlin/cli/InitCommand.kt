package cli

import cli.common.choice
import cli.common.input
import cli.common.yesNoQuestion
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import domain.project.api.HandleProject
import domain.project.model.NewProjectCommand
import domain.project.model.ProjectResult
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.teamwork.configuration.api.HandleConfiguration
import domain.teamwork.configuration.model.BoardConfiguration
import domain.teamwork.configuration.model.Login
import domain.user.HandleUserProfile
import domain.user.UserProfile
import domain.user.UserSettings
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class InitCommand : CliktCommand(name = "init"), KoinComponent {
    override fun help(context: Context) = "Initialize workspace"

    private val userController: HandleUserProfile by inject()
    private val saveProject: HandleProject by inject()

    val conf: HandleConfiguration by inject()

    override fun run() {
        val project = initProject()
        initTeamwork(project)

        val userSettings = UserSettings(
            user = ID.from(System.getProperty("user.name")),
            project = ID.from(project)
        )
        userController.save(userSettings)
    }

    private fun initProject(): String {
        if (!yesNoQuestion("Do you want to manage projects?")) {
            return when(val settings = userController.get(ID.from(System.getProperty("user.name")))) {
                is UserProfile.Found -> settings.settings.project.value
                UserProfile.NotFound -> "default"
            }
        }
        val name = input("Name of your project")
        val description = input("Short description of your project")

        return when(val it = saveProject.start(NewProjectCommand(name = Name.from(name), description = description, startDate = DateTime.now()))) {
            is ProjectResult.Found -> it.project.id.value
            else -> ""
        }
    }

    private fun initTeamwork(project: String) {
        if (!yesNoQuestion("Do you want to manage a teamwork board?")) {
            return
        }

        val type = choice<domain.teamwork.configuration.model.BoardType>()
        val board = input("Board name")
        val root = input("root URL")
        val username = input("username")
        val token = input("token")

        conf.save(
            ID.from(project),
            BoardConfiguration.Valid(
                project = ID.from(project),
                type = type,
                url = root,
                board = board,
                login = Login(user = username, pass = token)
            )
        )

    }
}

