package cli.project

import cli.teamwork.reports.display.red
import cli.teamwork.reports.id
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.core.terminal
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.mordant.rendering.TextColors
import com.github.ajalt.mordant.rendering.TextStyles
import com.github.ajalt.mordant.table.Borders
import com.github.ajalt.mordant.table.table
import domain.project.api.GetProject
import domain.project.api.HandleProject
import domain.project.model.*
import domain.sharedkernel.model.DateTime
import domain.sharedkernel.model.ID
import domain.sharedkernel.model.Name
import domain.squad.model.Squad
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

class ProjectCommand private constructor() : CliktCommand(name = "project"), KoinComponent {
    override fun help(context: Context) = "Manage projects"
    override val allowMultipleSubcommands = true

    override fun run() {}

    companion object {
        operator fun invoke() = ProjectCommand()
            .subcommands(
                StartProjectCommand(),
                StopProjectCommand(),
                AssignProjectCommand(),
                DisplayProjectCommand(),
                ListProjectCommand()
            )
    }
}


class StartProjectCommand : CliktCommand(name = "start"), KoinComponent {
    override fun help(context: Context) = "start a new project"
    private val name by option("-name", help = "Project name").required()
    private val description by option("-description", help = "Project description").required()
    private val handleProject: HandleProject by inject()

    override fun run() {
        val result = handleProject.start(
            NewProjectCommand(
                name = Name.from(name),
                description = description,
                startDate = DateTime.now()
            )
        )
        when (result) {
            is ProjectResult.Found -> terminal.println("The project '$name' has started with the ID ${result.project.id}")
            else -> terminal.println("The project '$name' cannot be created")
        }

    }
}

class StopProjectCommand : CliktCommand(name = "stop"), KoinComponent {
    override fun help(context: Context) = "stop a project"
    private val id by option("-id", help = "Project ID").id<Project>().required()

    protected val handleProject: HandleProject by inject()

    override fun run() {
        when (val result = handleProject.close(
            project = id,
            on = DateTime.now()
        )) {
            is ProjectResult.Found ->terminal.println("The project '${result.project.name}' has closed")
            else -> terminal.println(TextColors.red("Cannot close the project"))
        }

    }
}

class AssignProjectCommand : CliktCommand(name = "assign"), KoinComponent {
    override fun help(context: Context) = "assign a project to a squad"
    private val project by option("-project", help = "Project ID").id<Project>().required()
    private val squad by option("-squad", help = "Squad ID").id<Squad>().required()

    private val controller: HandleProject by inject()

    override fun run() {
        val result = controller.assign(
            project = project,
            to = squad,
            since = DateTime.now()
        )
        when (result) {
            is ProjectResult.Found ->terminal.println("The project '${result.project.name}' has been assigned to ${result.project.squad.toString()}")
            is ProjectResult.NotFound.NoEvents -> terminal.println(red("Cannot build an event"))
            is ProjectResult.NotFound.NoStartEvent -> terminal.println(red("The project has no Started event"))
        }
    }
}

class DisplayProjectCommand : CliktCommand(name = "display"), KoinComponent {
    override fun help(context: Context) = "display a project"
    private val project by option("-id", help = "Project ID").id<Project>().default(
        ID.from(System.getProperty("user.name"))
    )

    private val getProject: GetProject by inject()

    override fun run() {
        when (val result = getProject.information(project)) {
            is ProjectResult.Found -> result.let{ (result) ->
                terminal.println(TextStyles.bold(result.name.value))
                terminal.println(result.description)
                if (result.squad != noSquad) {
                    terminal.println("Assigned to the squad ${result.squad}")
                }

                val status = result.status
                when (status) {
                    is Status.Closed -> {
                        terminal.println("The project started on ${result.start}")
                        terminal.println("And ${TextColors.red("ended on")} ${status.on}")
                    }

                    else -> terminal.println("The project has been ${TextColors.green("in progress")} since ${result.start}")
                }
            }

            is ProjectResult.NotFound.NoStartEvent -> {
                terminal.println(TextColors.red("Missing start event"))
                return
            }

            is ProjectResult.NotFound.NoEvents -> {
                terminal.println(TextColors.red("There is no event for this project"))
                return
            }
        }


    }
}

class ListProjectCommand : CliktCommand(name = "list"), KoinComponent {
    override fun help(context: Context) = "list all projects"
    private val controller: GetProject by inject()

    override fun run() {
        val t = table {
            body {
                controller.all().forEach { project ->
                    row {
                        cell(project.id) { cellBorders = Borders.NONE }
                        cell(project.name) { cellBorders = Borders.NONE }
                    }
                }
            }
        }
        terminal.println(t)
    }
}