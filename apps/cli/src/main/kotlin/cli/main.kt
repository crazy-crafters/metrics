package cli


import cli.project.ProjectCommand
import cli.teamwork.TeamworkCommand
import cli.user.SetUserSettingsCommand
import com.github.ajalt.clikt.completion.CompletionCommand
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.core.main
import com.github.ajalt.clikt.core.subcommands
import org.koin.core.component.KoinComponent
import org.koin.core.context.GlobalContext.startKoin
import org.koin.dsl.module
import infra.project.projectModule
import infra.squad.squadModule
import infra.teamwork.EventSink
import infra.teamwork.teamworkModule
import infra.user.userModule
import java.nio.file.Path
import kotlin.io.path.createDirectories
import kotlin.io.path.exists

val appModule = module {
    val path = (System.getProperty("LOCAL_STORAGE") ?: System.getenv("LOCAL_STORAGE") ?: "").let {
        if (it.isBlank())
            Path.of(System.getProperty("user.home")).resolve(".blackbox")
        else
            Path.of(it)
    }

    if (!path.exists()) {
        path.createDirectories()
    }

    includes(
        squadModule(path),
        projectModule(path),
        userModule(path),
        teamworkModule(path),
        sharedModule,
        //projectGatewaysModule(),
        module {
            single<EventSink> {
                object : EventSink {
                    override fun publish(event: String) {
                        println(event)
                    }

                }
            }
        }
    )
}


val CLI: CliktCommand = object : CliktCommand(name = "bb"), KoinComponent {
    override fun help(context: Context) = "BlackBox cli"
    override fun run() {}
}.subcommands(
    TeamworkCommand(),
    ProjectCommand().subcommands(SwitchProjectCommand()),
    InitCommand(),
    SetUserSettingsCommand(),
    CompletionCommand()
)


fun main(args: Array<String>) {
    startKoin {
        allowOverride(true)
        modules(appModule)
    }
    CLI.main(args)
}

