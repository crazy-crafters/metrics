package cli.user

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.Context
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import domain.sharedkernel.model.ID
import domain.user.HandleUserProfile
import domain.user.User
import domain.user.UserProfile
import domain.user.UserSettings

class SettingsCommand private constructor() : CliktCommand(name = "settings"), KoinComponent {
    override fun help(context: Context) = "Manage user settings"
    override val allowMultipleSubcommands = true
    override fun run() {}

    companion object {
        operator fun invoke() = SettingsCommand()
            .subcommands(
                SetUserSettingsCommand()
            )
    }
}


class SetUserSettingsCommand : CliktCommand(name = "set"), KoinComponent {
    override fun help(context: Context) = "set user default settings"
    private val id by option("-project", help = "Default project ID").default("")

    private val handleUserProfile: HandleUserProfile by inject()

    override fun run() {
        if (id.isBlank()) {
            return
        }
        val user = ID.from<User>(System.getProperty("user.name"))
        val updatedSettings = when (val it = handleUserProfile.get(user)) {
            is UserProfile.Found -> it.settings.copy(project = ID.from(id))
            is UserProfile.NotFound -> UserSettings(
                user = user,
                project = ID.from(id)
            )
        }

        handleUserProfile.save(updatedSettings)
    }
}