package cli.common

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.terminal

fun CliktCommand.input(label: String): String {
    do {
        terminal.print("$label: ")
        return terminal.readLineOrNull(hideInput = false) ?: continue
    } while(true)
}

inline fun <reified T: Enum<T>> CliktCommand.choice(): T {
    val values = enumValues<T>()
    do {
        terminal.println("Select a number: ")
        values.forEachIndexed { index, t ->
            terminal.println("$index. ${t.name}")
        }
        val u = terminal.readLineOrNull(hideInput = false)?.toIntOrNull() ?: continue
        if (u < 0 || u >= values.count())
            continue
        return values[u]
    } while(true)
}