package cli.common

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.terminal

fun CliktCommand.yesNoQuestion(question: String): Boolean {
    do {
        terminal.print("$question [Yy] or [Nn] ")
        val key = terminal.readLineOrNull(hideInput = false)?.lowercase()
        if (key == null || key !in "yYnN") {
            continue
        }
        return key in "yY"
    } while(true)
}

