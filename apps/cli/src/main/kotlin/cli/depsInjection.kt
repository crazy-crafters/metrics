package cli

import org.koin.dsl.module
import infra.platform.InMemoryBus
import domain.sharedkernel.SystemEventsRepository
import domain.sharedkernel.EventBus

val sharedModule: org.koin.core.module.Module
    get() = module {
        single<InMemoryBus> {
            InMemoryBus()
        }

        single<EventBus> {
            get<InMemoryBus>()
        }

        single<SystemEventsRepository> {
            get<InMemoryBus>()
        }
    }